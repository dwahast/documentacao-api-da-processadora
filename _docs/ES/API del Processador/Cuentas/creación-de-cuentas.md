---
layout: default-es
title: Creación de Cuentas
tags: 
 - cuentas
description: Operación de alto nivel para crear cuentas con el procesador paySmart
permalink: es/cuentas/creacion
language: es
---

# **Creación de Cuentas**

1. [Introducción](#introduccion)
1. [Creación de Cuenta](#creacion-de-cuenta)
    1. [Solicitud](#solicitud)
    1. [Respuesta](#respuesta)

## **Introducción**


Dentro del contexto de la procesadora paySmart, una cuenta es un elemento lógico cuyo componente principal es un titular, ya sea una persona física o jurídica, que posee un saldo y es responsable de su liquidez. "Poseer el saldo" no implica necesariamente que paySmart controle o deba conocer la cantidad de dinero que el titular tiene disponible para realizar transacciones. Sin embargo, se recomienda que, si el emisor o subemisor lleva a cabo este control, asocie a la cuenta paySmart el mismo documento (CPF o CNPJ) al que está vinculado el saldo.

En términos generales, la función principal de las cuentas es agrupar las tarjetas asociadas al mismo titular. En el caso de un producto pospago, la cuenta es la entidad que también estará relacionada con una factura.

## **Creación de Cuenta**

La creación de una cuenta implica tres elementos obligatorios: Titular, Producto y Dirección de facturación:

| Campo | Descripción |
|----------|-----------|
| *``"accountOwner"``* | Titular: Describe la información necesaria para identificar de manera unívoca a la persona física o jurídica responsable de la cuenta que se está creando. |
| *``"accountOwner"."fullName"``* | Nombre completo: Nombre legal del titular. |
| *``"accountOwner"."identityDocumentNumber"``* | Documento de identidad: Número del documento que identifica al titular en organismos oficiales, generalmente un CPF o un CNPJ. |
| *``"accountOwner"."contactInformation"``* | Datos de contacto: Información para permitir la comunicación con el titular en caso de necesidad eventual. |
| *``"accountOwner"."contactInformation"."personalPhoneNumber1"``* | Número de contacto telefónico |
| *``"accountOwner"."contactInformation"."email"``* | Correo electrónico |
| *``"accountOwner"."contactInformation"."birthDate"``* | Fecha de nacimiento: Fecha de nacimiento del titular de la cuenta. **Nota**: Obligatoria solo para productos pospago o productos prepago que utilicen el servicio de KYC de paySmart.|
| *``"psProductCode"``* | Un producto: Define las características de las tarjetas que se generarán asociadas a esa cuenta (por ejemplo, si es de débito o crédito, nacional o internacional, persona física o jurídica). Cada producto recibe un identificador definido por paySmart, generalmente un código numérico de seis dígitos. Este código se proporciona al emisor/subemisor, que debe pasarlo como parámetro en cada nueva cuenta creada. |
| *``"billingAddress"``* | Dirección de facturación: La dirección fija del titular, donde este debe ser localizado en caso de necesidad de cobro. |
| *``"billingAddress"."addressLine1"``* | Una línea de dirección: Contiene la identificación de la vía principal de la dirección (nombre de la calle, y puede incluir número y complemento). |
| *``"billingAddress"."addressLine2"``* | Campo independiente para indicar el número. |
| *``"billingAddress"."city"``* | Municipio donde se encuentra ubicada la vía principal. |
| *``"billingAddress"."state"``* | Unidad de la federación, en el territorio brasileño, donde se encuentra la ciudad del titular. |
| *``"billingAddress"."neighborhood"``* | Barrio: Barrio de residencia del titular. |
| *``"billingAddress"."zipcode"``* | Código Postal (CP) para la ubicación de servicios de entrega. |
| *``"cardDeliveryAddress"``* | La dirección que se utilizará para todas las entregas de tarjetas vinculadas a la cuenta, en caso de que no se defina explícitamente en la solicitud de tarjeta. Contiene los mismos campos obligatorios que la dirección de facturación. |

### **Solicitud**  

Los datos enumerados anteriormente deben ser proporcionados en un cuerpo de solicitud en formato JSON a través de una solicitud HTTP en el endpoint [POST /accounts]({{ site.url }}{{ site.baseurl }}/spec-proc/). De esta manera, una solicitud mínima para la creación de una cuenta tendría el siguiente formato:

```json
{
  "psProductCode": "020101",
  "accountOwner": {
    "fullName": "Fulano da Silva",
    "identityDocumentNumber": "03873703805",
    "contactInformation": { "personalPhoneNumber1": "+5551",  "email": "a@b.c" }
  },
  "billingAddress": {
    "addressLine1": "R. Manoelito de Ornellas",
    "addressLine2": "55",
    "city": "Porto Alegre",
    "state": "RS",
    "neighborhood": "Praia de Belas",
    "zipcode": "990000"
  },
  "cardDeliveryAddress": {
    "addressLine1": "R. Manoelito de Ornellas",
    "addressLine2": "55",
    "city": "Porto Alegre",
    "state": "RS",
    "neighborhood": "Praia de Belas",
    "zipcode": "990000"
  }
}
```

### **Respuesta**
La solicitud de una nueva cuenta devuelve al solicitante un cuerpo JSON que informa el resultado de la misma. Siempre que se cree una cuenta con éxito (campo *``"resultCode"``* con el valor **"0"**), la estructura JSON de la respuesta incluirá un campo llamado *``"accountId"``*, que es un identificador prefijado por la cadena ``"cta-"`` seguida de un UUID-v4 en formato. Se proporciona un ejemplo de respuesta a continuación:


```json
{
  "resultData": {
    "resultCode": 0,
    "resultDescription": "Registro de nova conta incluído com sucesso!",
    "psResponseId": "dfd88b95-d28d-49ac-90ba-7ba0b35aebd0"
  },
  "account": {
    "accountId": "cta-5566fefb-af25-9984-9fb0-39f6d2021111",
    "psProductCode": "020101",
    "psProductName": "BANCO 1 BANDEIRA 1 PRE-PAGO"
  }
}
```

>Nota: Una cuenta siempre se crea con el estado **"Ativa"**.

No siempre es necesario crear cuentas para emitir tarjetas. En el modelo de tarjetas anónimas, la solicitud de tarjetas puede realizarse directamente y como resultado de este proceso se generará una tarjeta física con todos los datos necesarios para realizar transacciones, pero que no puede hacerlo hasta que se asocie a un titular. Este proceso se describe en la [Creación de Tarjetas Físicas]({{ site.url }}{{ site.baseurl }}/).