---
title: Códigos de motivo de transacción
name: ""
link: "transactions-code"
type: info
tags:
    - inclusives
    - inclusives creation
    - transaction codes
description: "Códigos para informar el motivo del ajuste financiero de la disputa."
permalink: /es/docs/disputes/transactions-code
language: es
---

<a href="{{site.baseurl}}/pt-br/docs/disputes/transactions-code">Códigos de transacción</a>