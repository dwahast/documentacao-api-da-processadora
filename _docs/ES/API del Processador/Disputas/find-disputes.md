---
title: Seguimiento de disputas
name: ""
link: "find-disputes"
method: GET
type: disputes
tags:
    - endpoint
    - disputas
    - acompanhamento disputa
description: "Punto final para monitorear múltiples disputas basadas en filtros definidos."
permalink: /es/docs/disputes/find-disputes
language: es
---
{% include endpoint_header.html %}

{% include endpoint_auth.html %}

### **Parámetros de búsqueda**

<table>
    <thead>
        <tr>
            <th>Nombre</th>
            <th>Obligatorio</th>
            <th>Descripción</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>limit</td>
            <td>No</td>
            <td>Número límite de objetos devueltos. <b>El valor debe estar entre 1 y 100.</b></td>
        </tr>
        <tr>
            <td>starting_after</td>
            <td>No</td>
            <td>Un cursor para uso en paginación. {starting_after} es el identificador único del objeto a partir del cual se desea listar. 
            Por ejemplo, si hubo un retorno de una lista de 100 objetos para esta llamada y el último tiene el identificador "obj1234", para obtener la página use "starting_after=obj1234".</td>
        </tr>
        <tr>
            <td>ending_before</td>
            <td>No</td>
            <td>Un cursor para uso en paginación. {ending_before} es el identificador único del objeto a partir del cual se desean listar los anteriores. 
            Por ejemplo, si hubo un retorno de una lista de 100 objetos para esta llamada y el primero tiene el identificador "obj1234", para obtener la página anterior use "ending_before=obj1234".</td>
        </tr>
        <tr>
            <td>dispute_reason</td>
            <td>No</td>
            <td>Código del motivo de las disputas a ser devueltas.</td>
        </tr>
        <tr>
            <td>dispute_status</td>
            <td>No</td>
            <td>Estado de las disputas a ser devueltas.</td>
        </tr>
        <tr>
            <td>beginning_date</td>
            <td>Sí</td>
            <td>Fecha que indica el primer día cuyos datos deben ser devueltos. <b>Debe ser una fecha en el formato yyyy-MM-DD. Ejemplo: "2020-07-09"</b></td>
        </tr>
        <tr>
            <td>ending_date</td>
            <td>No</td>
            <td>Fecha que indica el último día cuyos datos deben ser devueltos. <b>Debe ser una fecha en el formato yyyy-MM-DD. Ejemplo: "2020-07-09"</b></td>
        </tr>
    </tbody>
</table>

### **Request Examples**

#### Caso de Éxito

```
curl -X POST https://api.paysmart.com.br/paySmart/ps-processadora/v1/disputes?limit=2&beginning_date=2020-11-17&ending_date=2021-05-05&dispute_status=WAITING_FOR_RESPONSE_FROM_ACQUIRER&dispute_reason=81
```

#### Caso de Error

```
curl -X POST https://api.paysmart.com.br/paySmart/ps-processadora/v1/disputes?limit=2
```

### **Ejemplos de Respuesta**

#### Caso de Éxito

```json
{
    "resultData": {
        "psResponseId": "c5cc9a45-15a0-47b1-ba11-3cebd473883e",
        "resultCode": 0,
        "resultDescription": "Disputa encontrada com sucesso!"
    },
    "hasMore": "true",
    "disputes": [
        {
            "currentStage": "chargeback",
            "disputeMaxDateResponse": "Aguardando processamento na Elo.",
            "disputeId": "DIS-2396adf9-7c51-434b-97e4-356a924b2e29",
            "disputeStatus": "waiting_for_response_from_acquirer",
            "transaction": {
                "transactionSource": "brand",
                "fees": [],
                "creatingSystemName": "Autorizador ISO8583",
                "psProductName": "ISSUER",
                "terminalId": "20172289",
                "ISO8583MessageRequest": {
                    "de011": "273257",
                    "de022": "011",
                    "de032": "0025",
                    "de043": "ELO                    BARUERI       076",
                    "de042": "020001605270002",
                    "mti": "0100",
                    "de041": "20172289",
                    "de060": "000110000P500",
                    "de019": "076",
                    "de018": "5712",
                    "de007": "0427183257",
                    "de127": "12182",
                    "de049": "986",
                    "de126": "*****",
                    "de037": "119357719326",
                    "de004": "000000002000",
                    "de048": "*CDT002T0*PRD003070",
                    "de014": "2412",
                    "de058": "ALAMEDAXINGU          23543230007612345ELO                  ",
                    "de003": "003000",
                    "de013": "0427",
                    "de002": "************1585",
                    "de024": "100",
                    "de012": "153257"
                },
                "mcc": "5712",
                "transactionTime": "15:32:57",
                "merchantName": "ELO",
                "issuerId": "3",
                "merchantId": "020001605270002",
                "countryCode": "076",
                "disputeStatus": "chargeback",
                "international": false,
                "acquirerId": "0025",
                "transactionAuthorizationResponse": {
                    "approved": true
                },
                "entryMode": "manual",
                "psProductCode": "030102",
                "amount": {
                    "amount": {
                        "amount": 2000,
                        "currencyCode": 986
                    },
                    "debit_or_credit": "debit"
                },
                "merchantCity": "BARUERI",
                "acquirerTransactionId": "119357719326",
                "transactionStatus": "approved",
                "freeDescription": "Nova transação de compra registrada!",
                "transactionReceivedDateTime": "2021-04-27T18:32:58.051Z",
                "incremental": false,
                "transactionDate": "2021-04-27",
                "transactionId": "aut_8242fdd0-d815-4ddf-a8bd-cb531d3b00a3",
                "transactionType": "authorized_transaction",
                "cardBrandId": "Elo",
                "accountId": "cta-8acbd197-a061-486c-af38-2661256161eb",
                "forceAccept": false,
                "statusChangeHistory": [
                    {
                        "callingSystemName": "Autorizador ISO8583",
                        "stateChangeDescription": "Nova transação registrada.",
                        "stateChangeReasonCode": "other",
                        "newStatus": "pending",
                        "changeRequestReceivedDate": "2021-04-27T18:32:58.051Z",
                        "statusChangeSource": "brand"
                    }
                ],
                "disputeId": "DIS-2396adf9-7c51-434b-97e4-356a924b2e29",
                "merchantAddress": "ALAMEDAXINGU",
                "merchantZipcode": "235432300",
                "card": {
                    "BIN": "******",
                    "cardId": "crt-f4de6c2a-f501-482e-a353-a88f6a4b3aa3",
                    "last_four_digits": "1585",
                    "cardIdIssuer": "fdb5d5ea-2d5d-4937-865e-933fe46b3d3f"
                }
            }
        },
        {
            "currentStage": "chargeback",
            "disputeMaxDateResponse": "Aguardando processamento na Elo.",
            "disputeId": "DIS-3a3fa2a5-ccc0-4a24-8588-eea0eb5c02d1",
            "disputeStatus": "waiting_for_response_from_acquirer",
            "transaction": {
                "transactionSource": "brand",
                "fees": [],
                "creatingSystemName": "Autorizador ISO8583",
                "psProductName": "ISSUER",
                "terminalId": "20172289",
                "ISO8583MessageRequest": {
                    "de011": "305925",
                    "de022": "011",
                    "de032": "0025",
                    "de043": "ELO                    BARUERI       076",
                    "de042": "020001605270002",
                    "mti": "0100",
                    "de041": "20172289",
                    "de060": "000110000P500",
                    "de019": "076",
                    "de018": "5712",
                    "de007": "0430135925",
                    "de127": "12182",
                    "de049": "986",
                    "de126": "*****",
                    "de037": "136089172591",
                    "de004": "000000007000",
                    "de048": "*CDT002T0*PRD003070",
                    "de014": "2412",
                    "de058": "ALAMEDAXINGU          23543230007612345ELO                  ",
                    "de003": "003000",
                    "de013": "0430",
                    "de002": "************1585",
                    "de024": "100",
                    "de012": "105925"
                },
                "mcc": "5712",
                "transactionTime": "10:59:25",
                "merchantName": "ELO",
                "issuerId": "3",
                "merchantId": "020001605270002",
                "countryCode": "076",
                "disputeStatus": "chargeback",
                "international": false,
                "acquirerId": "0025",
                "transactionAuthorizationResponse": {
                    "approved": true
                },
                "entryMode": "manual",
                "psProductCode": "030102",
                "amount": {
                    "amount": {
                        "amount": 7000,
                        "currencyCode": 986
                    },
                    "debit_or_credit": "debit"
                },
                "merchantCity": "BARUERI",
                "acquirerTransactionId": "136089172591",
                "transactionStatus": "approved",
                "freeDescription": "Nova transação de compra registrada!",
                "transactionReceivedDateTime": "2021-04-30T13:59:25.841Z",
                "incremental": false,
                "transactionDate": "2021-04-30",
                "transactionId": "aut_1454fc73-66e8-4926-96e9-7b1cdd7bf7e8",
                "transactionType": "authorized_transaction",
                "cardBrandId": "Elo",
                "accountId": "cta-8acbd197-a061-486c-af38-2661156161eb",
                "forceAccept": false,
                "statusChangeHistory": [
                    {
                        "callingSystemName": "Autorizador ISO8583",
                        "stateChangeDescription": "Nova transação registrada.",
                        "stateChangeReasonCode": "other",
                        "newStatus": "pending",
                        "changeRequestReceivedDate": "2021-04-30T13:59:25.841Z",
                        "statusChangeSource": "brand"
                    }
                ],
                "disputeId": "DIS-3a3fa2a5-ccc0-4a24-8588-eea0eb5c02d1",
                "merchantAddress": "ALAMEDAXINGU",
                "merchantZipcode": "235432300",
                "card": {
                    "BIN": "******",
                    "cardId": "crt-f4de6c2a-f501-482e-a353-a88f6a4b3aa3",
                    "last_four_digits": "1585",
                    "cardIdIssuer": "fdb5d5ea-2d5d-4937-865e-933fe46b3d3f"
                }
            }
        }
    ]
}
```

#### Caso de Error

```json
{
    "resultData": {
        "resultCode": 982,
        "resultDescription": "Nenhuma disputa encontrada com os parâmetros de busca utilizados.",
        "psResponseId": "5c4d3c1e-e9d2-493c-a485-8ae8073a66f4"
    }
}
```

### **Descripción de la Respuesta**

<table>
    <thead>
        <tr>
            <th>Nombre</th>
            <th>Tipo</th>
            <th>Descripción</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>hasMore</td>
            <td>Booleano</td>
            <td>Indica que se alcanzó el límite de retorno y hay más elementos por devolver.</td>
        </tr>
        <tr>
            <td>disputes</td>
            <td>Array</td>
            <td>Lista de disputas encontradas.</td>
        </tr>
        <tr>
            <td>- currentStage</td>
            <td>String</td>
            <td>Etapa actual del proceso de disputa. Devuelve uno de los siguientes valores: chargeback, restatement, pre-arbitration, arbitration, lost, successful, withdrawal, undone.</td>
        </tr>
        <tr>
            <td>- disputeMaxDateResponse</td>
            <td>String</td>
            <td>Fecha máxima para una respuesta en formato Iso-8601, por ejemplo, 2021-06-20T00:00:00.000Z. Devuelve "Esperando procesamiento en Elo" si la disputa se creó con éxito en la API, pero aún no se ha enviado en la liquidación a la red de tarjetas.</td>
        </tr>
        <tr>
            <td>- disputeId</td>
            <td>String</td>
            <td>Identificador único de la disputa en paySmart.</td>
        </tr>
        <tr>
            <td>- disputeStatus</td>
            <td>String</td>
            <td>Estado actual de la disputa. Sirve para indicar qué lado de la disputa debe realizar la próxima acción. Devuelve uno de los siguientes valores: waiting_for_response_from_issuer, waiting_for_response_from_acquirer, waiting_for_response_from_brand, accepted, denied, issuer_won, issuer_lost, canceled_by_issuer.</td>
        </tr>
        <tr>
            <td>- transaction</td>
            <td>Objeto</td>
            <td>Datos de la transacción en disputa.</td>
        </tr>
        <tr>
            <td>resultData</td>
            <td>Objeto</td>
            <td>Datos de la respuesta de la API.</td>
        </tr>
        <tr>
            <td>- resultCode</td>
            <td>Entero</td>
            <td>Código de resultado del procesamiento.</td>
        </tr>
        <tr>
            <td>- resultDescription</td>
            <td>String</td>
            <td>Descripción textual del resultado del procesamiento.</td>
        </tr>
        <tr>
            <td>- psResponseID</td>
            <td>String</td>
            <td>Identificador único de la respuesta. Generado por paySmart.</td>
        </tr>
    </tbody>
</table>

{% include endpoint_footer.html %}