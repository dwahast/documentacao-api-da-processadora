---
title: Códigos de error de API de disputa
name: ""
link: "errors-code"
type: info
tags:
    - disputes
    - errors-code
description: "Posibles códigos de error devueltos por la API de disputas."
permalink: /es/docs/disputes/errors-code
language: es
---


<a href="{{site.baseurl}}/pt-br/docs/disputes/errors-code">Códigos de error</a>