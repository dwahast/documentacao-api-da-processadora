---
title: Seguimiento de disputa
name: "{disputeId}"
link: "get-dispute"
method: GET
type: disputes
tags:
    - endpoint
    - disputas
    - acompanhamento disputa
description: "Seguimiento de disputas"
permalink: /es/docs/disputes/get-dispute
language: es
---
{% include endpoint_header.html %}

Al consultar el estado de una disputa, es importante prestar atención a los campos <b>currentStage</b>, <b>disputeStatus</b> y <b>disputeMaxDateResponse</b>, ya que estos indican cuándo es necesario que el emisor realice una acción. Específicamente, se debe enviar una solicitud al endpoint <a href="./respond-dispute">Continuação de Disputas</a> cuando los dos primeros campos mencionados tienen los valores <b>restatement</b> y <b>waiting_for_response_from_issuer</b>, respectivamente.
{% include endpoint_auth.html %}

### **Ejemplos de Respuesta**

#### Caso de Éxito

```json
{
    "currentStage": "pre-arbitration",
    "disputeMaxDateResponse": "2021-06-20T00:00:00.000Z",
    "disputeId": "DIS-7be16041-7f04-40fc-a2d3-5283ab8aa2fd",
    "disputeStatus": "waiting_for_response_from_acquirer",
    "transaction": {
        "transactionSource": "brand",
        "fees": [],
        "creatingSystemName": "Autorizador ISO8583",
        "psProductName": "ISSUER",
        "terminalId": "20172289",
        "ISO8583MessageRequest": {
            "de011": "161321",
            "de022": "011",
            "de032": "0025",
            "de043": "ELO                    BARUERI       076",
            "de042": "020001605270002",
            "mti": "0100",
            "de041": "20172289",
            "de060": "000110000P500",
            "de019": "076",
            "de018": "5712",
            "de007": "0416211321",
            "de127": "12182",
            "de049": "986",
            "de126": "*****",
            "de037": "222496782252",
            "de004": "000000002000",
            "de048": "*CDT002T0*PRD003070",
            "de014": "2412",
            "de058": "ALAMEDAXINGU          23543230007612345ELO                  ",
            "de003": "003000",
            "de013": "0416",
            "de002": "************1585",
            "de024": "100",
            "de012": "181321"
        },
        "mcc": "5712",
        "transactionTime": "18:13:21",
        "merchantName": "ELO",
        "issuerId": "3",
        "merchantId": "020001605270002",
        "countryCode": "076",
        "disputeStatus": "pre-arbitration",
        "international": false,
        "acquirerId": "0025",
        "transactionAuthorizationResponse": {
            "approved": true
        },
        "entryMode": "manual",
        "psProductCode": "030102",
        "amount": {
            "amount": {
                "amount": 2000,
                "currencyCode": 986
            },
            "debit_or_credit": "debit"
        },
        "merchantCity": "BARUERI",
        "acquirerTransactionId": "222496782252",
        "transactionStatus": "confirmed",
        "freeDescription": "Nova transação de compra registrada!",
        "transactionReceivedDateTime": "2021-04-16T21:13:22.093Z",
        "incremental": false,
        "transactionDate": "2021-04-16",
        "transactionId": "aut_f7c56484-41db-4826-ab6d-0c4fd31fcf5c",
        "transactionType": "authorized_transaction",
        "cardBrandId": "Elo",
        "accountId": "cta-8acbd197-a061-486c-af38-5632315767be",
        "forceAccept": false,
        "statusChangeHistory": [
            {
                "callingSystemName": "Autorizador ISO8583",
                "stateChangeDescription": "Nova transação registrada.",
                "stateChangeReasonCode": "other",
                "newStatus": "pending",
                "changeRequestReceivedDate": "2021-04-16T21:13:22.093Z",
                "statusChangeSource": "brand"
            }
        ],
        "disputeId": "DIS-7be16041-7f04-40fc-a2d3-5283ab8aa2fd",
        "merchantAddress": "ALAMEDAXINGU",
        "merchantZipcode": "235432300",
        "card": {
            "BIN": "******",
            "cardId": "crt-f4de6c2a-f501-482e-a353-a88f6a4b3aa3",
            "last_four_digits": "1585",
            "cardIdIssuer": "fdb5d5ea-2d5d-4937-865e-933fe46b3d3f"
        }
    }
}
```

#### Caso de Error

```json
{
    "resultData": {
        "resultCode": 984,
        "resultDescription": "Disputa não encontrada.",
        "psResponseId": "1987dddc-4284-4bdc-9073-b79f21801096"
    }
}
```

### **Descripción de la Respuesta**

<table>
    <thead>
        <tr>
            <th>Nome</th>
            <th>Tipo</th>
            <th>Descrição</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>currentStage</td>
            <td>String</td>
            <td>Etapa actual del proceso de disputa. Retorna uno de los siguientes valores: chargeback, restatement, pre-arbitration, arbitration.</td>
        </tr>
        <tr>
            <td>disputeMaxDateResponse</td>
            <td>String</td>
            <td>Fecha máxima para una respuesta en formato Iso-8601, por ejemplo, 2021-06-20T00:00:00.000Z. Debe usarse en conjunto con el campo disputeStatus para monitorear cuándo el emisor deberá enviar una nueva solicitud.</td>
        </tr>
        <tr>
            <td>disputeId</td>
            <td>String</td>
            <td>Identificador único de la disputa en paySmart.</td>
        </tr>
        <tr>
            <td>disputeStatus</td>
            <td>String</td>
            <td>Estado actual de la disputa. Sirve para indicar qué lado de la disputa debe realizar la próxima acción. Retorna uno de los siguientes valores: waiting_for_response_from_issuer, waiting_for_response_from_acquirer, waiting_for_response_from_brand, accepted, denied, issuer_won, issuer_lost, canceled_by_issuer.</td>
        </tr>
        <tr>
            <td>transaction</td>
            <td>Objeto</td>
            <td>Datos de la transacción en disputa.</td>
        </tr>
        <tr>
            <td>resultData</td>
            <td>Objeto</td>
            <td>Datos de la respuesta de la API. <b>Solo en casos de error.</b></td>
        </tr>
        <tr>
            <td>- resultCode</td>
            <td>Integer</td>
            <td>Código de resultado del procesamiento.</td>
        </tr>
        <tr>
            <td>- resultDescription</td>
            <td>String</td>
            <td>Descripción textual del resultado del procesamiento.</td>
        </tr>
        <tr>
            <td>- psResponseID</td>
            <td>String</td>
            <td>Identificador único de la respuesta. Generado por paySmart.</td>
        </tr>
    </tbody>
</table>

{% include endpoint_footer.html %}
