---
title: Cancelación de disputas
name: "{disputeId}/undo"
link: "undo-dispute"
method: POST
type: dispute
tags:
    - endpoint
    - disputes
    - dispute cancelation
description: "Punto final para retirar una disputa y enviar su revocación."
permalink: /es/docs/disputes/undo-dispute
language: es
---
{% include endpoint_header.html %}

Es importante tener en cuenta que una disputa solo puede cancelarse a través de nuestra API si se encuentra en la etapa de Chargeback o Reapresentação. En etapas posteriores (prearbitraje o arbitraje), el emisor debe ponerse en contacto con el acreedor para resolver el problema mediante TE10/TE20.

{% include endpoint_auth.html %}

### **Argumentos**

<table>
    <thead>
        <tr>
            <th>Nombre</th>
            <th>Obligatorio</th>
            <th>Tipo</th>
            <th>Descripción</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>issuerDisputeReversalId</td>
            <td>No</td>
            <td>String</td>
            <td>Identificador único de la respuesta de la disputa. Generado por el emisor.</td>
        </tr>
        <tr>
            <td>textMessage</td>
            <td>No</td>
            <td>String</td>
            <td>Mensaje de texto justificando el motivo de la renuncia a la disputa.</td>
        </tr>
        <tr>
            <td>sourceAudit</td>
            <td>No</td>
            <td>Objeto</td>
            <td>Información de auditoría para el registro de información. Tiene los campos operatorId y processId</td>
        </tr>
        <tr>
            <td>- operatorId</td>
            <td>No</td>
            <td>String</td>
            <td>Identificación del operador solicitante.</td>
        </tr>
        <tr>
            <td>- processId</td>
            <td>No</td>
            <td>String</td>
            <td>Identificación del proceso solicitante.</td>
        </tr>
    </tbody>
</table>

### **Ejemplo de Solicitud**

```json
{
    "issuerDisputeReversalId": "a3e4cf3c-2a98-4a88-a370-5429356d7112",
    "textMessage": "Portador do cartão reconheceu a compra.",
    "sourceAudit": {
        "operatorId": "12340985312",
        "processId": "PID-12345"
    }
}
```

### **Ejemplo de Respuesta**

```json
{
    "dispute": {
        "disputeId": "DIS-ad56619b-c72f-4c44-a821-983ca2ebdd67",
        "disputeStatus": "canceled_by_issuer",
        "disputeRequest": {
            "issuerDisputeReversalId": "a3e4cf3c-2a98-4a88-a370-5429356d7112",
            "textMessage": "Portador do cartão reconheceu a compra.",
            "sourceAudit": {
                "operatorId": "12340985312",
                "processId": "PID-12345"
            }
        },
        "transaction": {
            "transactionSource": "brand",
            "fees": [],
            "creatingSystemName": "Autorizador ISO8583",
            "psProductName": "ISSUER",
            "terminalId": "20172289",
            "ISO8583MessageRequest": {
                "de011": "054748",
                "de022": "011",
                "de032": "0025",
                "de043": "ELO                    BARUERI       076",
                "de042": "020001605270002",
                "mti": "0100",
                "de041": "20172289",
                "de060": "000110000P500",
                "de019": "076",
                "de018": "5712",
                "de007": "0505134748",
                "de127": "12182",
                "de049": "986",
                "de126": "*****",
                "de037": "141438213853",
                "de004": "000000045661",
                "de048": "*CDT002T0*PRD003070",
                "de014": "2412",
                "de058": "ALAMEDAXINGU          23543230007612345ELO                  ",
                "de003": "003000",
                "de013": "0505",
                "de002": "************1585",
                "de024": "100",
                "de012": "104748"
            },
            "mcc": "5712",
            "transactionTime": "10:47:48",
            "merchantName": "ELO",
            "issuerId": "3",
            "merchantId": "020001605270002",
            "countryCode": "076",
            "disputeStatus": "chargeback",
            "international": false,
            "acquirerId": "0025",
            "transactionAuthorizationResponse": {
                "approved": true
            },
            "entryMode": "manual",
            "psProductCode": "030102",
            "amount": {
                "amount": {
                    "amount": 45661,
                    "currencyCode": 986
                },
                "debit_or_credit": "debit"
            },
            "merchantCity": "BARUERI",
            "acquirerTransactionId": "141438213853",
            "transactionStatus": "approved",
            "freeDescription": "Nova transação de compra registrada!",
            "transactionReceivedDateTime": "2021-05-05T13:47:48.602Z",
            "incremental": false,
            "transactionDate": "2021-05-05",
            "transactionId": "aut_ed8d5c54-188a-4b6a-a960-8bd05582850c",
            "transactionType": "authorized_transaction",
            "cardBrandId": "Elo",
            "accountId": "cta-8acbd197-a061-486c-af38-5632315767be",
            "forceAccept": false,
            "statusChangeHistory": [
                {
                    "callingSystemName": "Autorizador ISO8583",
                    "stateChangeDescription": "Nova transação registrada.",
                    "stateChangeReasonCode": "other",
                    "newStatus": "pending",
                    "changeRequestReceivedDate": "2021-05-05T13:47:48.602Z",
                    "statusChangeSource": "brand"
                }
            ],
            "disputeId": "DIS-ad56619b-c72f-4c44-a821-983ca2ebdd67",
            "merchantAddress": "ALAMEDAXINGU",
            "merchantZipcode": "235432300",
            "card": {
                "BIN": "******",
                "cardId": "crt-f4de6c2a-f501-482e-a353-a88f6a4b3aa3",
                "last_four_digits": "1585",
                "cardIdIssuer": "fdb5d5ea-2d5d-4937-865e-933fe46b3d3f"
            }
        }
    },
    "resultData": {
        "psResponseId": "eb9b9141-d8c2-4a41-81a2-d58567aaadc1",
        "resultCode": 0,
        "resultDescription": "Pedido de desistência de disputa criado com sucesso!",
        "issuerRequestId": "DIS-ad56619b-c72f-4c44-a821-983ca2ebdd67"
    }
}
```

### **Descripción de la Respuesta**

<table>
    <thead>
        <tr>
            <th>Nombre</th>
            <th>Tipo</th>
            <th>Descripción</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>dispute</td>
            <td>Objeto</td>
            <td>Objeto que contiene los datos relativos a la disputa creada. <b>Presente solo en caso de éxito.</b></td>
        </tr>
        <tr>
            <td>- disputeId</td>
            <td>String</td>
            <td>Identificador único de la disputa en paySmart.</td>
        </tr>
        <tr>
            <td>- disputeStatus</td>
            <td>String</td>
            <td>Estado actual de la disputa.</td>
        </tr>
        <tr>
            <td>- disputeRequest</td>
            <td>Objeto</td>
            <td>Request enviado por el emisor.</td>
        </tr>
        <tr>
            <td>- transaction</td>
            <td>Objeto</td>
            <td>Datos de la transacción en disputa.</td>
        </tr>
        <tr>
            <td>resultData</td>
            <td>Objeto</td>
            <td>Datos de la respuesta de la API.</td>
        </tr>
        <tr>
            <td>- resultCode</td>
            <td>Integer</td>
            <td>Código de resultado del procesamiento.</td>
        </tr>
        <tr>
            <td>- resultDescription</td>
            <td>String</td>
            <td>Descripción textual del resultado del procesamiento.</td>
        </tr>
        <tr>
            <td>- psResponseID</td>
            <td>String</td>
            <td>Identificador único de la respuesta. Generado por paySmart.</td>
        </tr>
        <tr>
            <td>- issuerRequestId</td>
            <td>String</td>
            <td>Identificador único de la solicitud generado por el emisor. Reflejado según se envió en la solicitud en el campo issuerDisputeId.</td>
        </tr>
    </tbody>
</table>

{% include endpoint_footer.html %}