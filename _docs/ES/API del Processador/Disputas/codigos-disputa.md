---
title: Códigos de motivo de disputa
name: ""
link: "codes"
type: info
tags:
    - disputes
    - dispute creation
    - dispute codes
description: "Códigos para informar el motivo de la disputa en la solicitud de creación."
permalink: /es/docs/disputes/codes
language: es
---

<a href="{{site.baseurl}}/pt-br/docs/disputes/undo-dispute">Codes</a>