---
title: Códigos de tipo de fraude
name: ""
link: "frauds-code"
type: info
tags:
    - fraud
    - fraud creation
    - fraud codes
description: "Códigos para informar el tipo de fraude en la solicitud de creación de disputa."
permalink: /es/docs/disputes/frauds-code
language: es
---

<a href="{{site.baseurl}}/pt-br/docs/disputes/frauds-code">Códigos de error</a>