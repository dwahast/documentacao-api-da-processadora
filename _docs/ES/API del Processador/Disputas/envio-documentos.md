---
title: Presentación de documentos para la disputa
name: ""
link: "send-docs"
type: info
tags:
    - disputes
    - disputes documents
description: "Proceso de presentación de documentos para sustentar la disputa."
permalink: /es/docs/disputes/send-docs
language: es
---

<a href="{{site.baseurl}}/pt-br/docs/disputes/send-docs">Presentación de documentos para la disputa</a>