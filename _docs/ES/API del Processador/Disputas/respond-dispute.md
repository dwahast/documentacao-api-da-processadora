---
title: Continuación de la disputa
name: "{disputeId}/response"
link: "respond-dispute"
method: POST
type: disputes
tags:
    - endpoint
    - disputas
    - continuação disputa
description: "Punto final para continuar una disputa que ha avanzado a las siguientes etapas del proceso"
permalink: /es/docs/disputes/respond-dispute
language: es
---
{% include endpoint_header.html %}
Se utiliza para responder a una opinión negativa de la bandera/acreditador. La respuesta puede aceptar la opinión y abandonar la disputa, o puede discrepar y avanzar la disputa a la siguiente fase.
Es importante prestar atención a la fecha límite de respuesta por parte del emisor. Este campo puede ser monitoreado a través del endpoint de <a href="{{site.baseurl}}/es/docs/disputes/get-dispute">Consulta de Estado de la Disputa</a>. Una vez transcurrido el plazo indicado, el emisor perderá automáticamente la disputa.

{% include endpoint_auth.html %}

### **Argumentos**

<table>
    <thead>
        <tr>
            <th>Nombre</th>
            <th>Obligatorio</th>
            <th>Tipo</th>
            <th>Descripción</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>issuerDisputeResponseId</td>
            <td>No</td>
            <td>String</td>
            <td>Identificador único de la respuesta de la disputa. Generado por el emisor.</td>
        </tr>
        <tr>
            <td>accept</td>
            <td>Sí</td>
            <td>Booleano</td>
            <td>Identifica si se debe aceptar la respuesta del adquirente/bandera. Si no se acepta, el proceso de disputa pasará a la siguiente etapa.</td>
        </tr>
        <tr>
            <td>disputeTextMessage</td>
            <td>Condicional</td>
            <td>String</td>
            <td>Mensaje de texto que justifica el motivo de continuar con la disputa. <b>Solo debe estar presente si accept es falso.</b></td>
        </tr>
        <tr>
            <td>sourceAudit</td>
            <td>No</td>
            <td>Objeto</td>
            <td>Información de auditoría para el registro de información. Tiene los campos operatorId y processId.</td>
        </tr>
        <tr>
            <td>- operatorId</td>
            <td>No</td>
            <td>String</td>
            <td>Identificación del operador solicitante.</td>
        </tr>
        <tr>
            <td>- processId</td>
            <td>No</td>
            <td>String</td>
            <td>Identificación del proceso solicitante.</td>
        </tr>
    </tbody>
</table>

### **Ejemplo de Solicitud**

```json
{
    "issuerDisputeResponseId": "a3e4cf3c-2a98-4a88-a370-5429356d7112",
    "accept": false,
    "disputeResponseTextMessage": "Serviço não prestado em 211019",
    "sourceAudit": {
        "operatorId": "12340985312",
        "processId": "PID-12345"
    }
}
```

### **Ejemplo de Respuesta**

```json
{
    "dispute": {
        "disputeId": "DIS-ad56619b-c72f-4c44-a821-983ca2ebdd67",
        "disputeStatus": "waiting_for_response_from_acquirer",
        "disputeRequest": {
            "issuerDisputeResponseId": "a3e4cf3c-2a98-4a88-a370-5429356d7112",
            "accept": false,
            "disputeResponseTextMessage": "Serviço não prestado em 211019",
            "sourceAudit": {
                "operatorId": "12340985312",
                "processId": "PID-12345"
            }
        },
        "transaction": {
            "transactionSource": "brand",
            "fees": [],
            "creatingSystemName": "Autorizador ISO8583",
            "psProductName": "ISSUER",
            "terminalId": "20172289",
            "ISO8583MessageRequest": {
                "de011": "054748",
                "de022": "011",
                "de032": "0025",
                "de043": "ELO                    BARUERI       076",
                "de042": "020001605270002",
                "mti": "0100",
                "de041": "20172289",
                "de060": "000110000P500",
                "de019": "076",
                "de018": "5712",
                "de007": "0505134748",
                "de127": "12182",
                "de049": "986",
                "de126": "*****",
                "de037": "141438213853",
                "de004": "000000045661",
                "de048": "*CDT002T0*PRD003070",
                "de014": "2412",
                "de058": "ALAMEDAXINGU          23543230007612345ELO                  ",
                "de003": "003000",
                "de013": "0505",
                "de002": "************1585",
                "de024": "100",
                "de012": "104748"
            },
            "mcc": "5712",
            "transactionTime": "10:47:48",
            "merchantName": "ELO",
            "issuerId": "3",
            "merchantId": "020001605270002",
            "countryCode": "076",
            "disputeStatus": "restatement",
            "international": false,
            "acquirerId": "0025",
            "transactionAuthorizationResponse": {
                "approved": true
            },
            "entryMode": "manual",
            "psProductCode": "030102",
            "amount": {
                "amount": {
                    "amount": 45661,
                    "currencyCode": 986
                },
                "debit_or_credit": "debit"
            },
            "merchantCity": "BARUERI",
            "acquirerTransactionId": "141438213853",
            "transactionStatus": "approved",
            "freeDescription": "Nova transação de compra registrada!",
            "transactionReceivedDateTime": "2021-05-05T13:47:48.602Z",
            "incremental": false,
            "transactionDate": "2021-05-05",
            "transactionId": "aut_ed8d5c54-188a-4b6a-a960-8bd05582165h",
            "transactionType": "authorized_transaction",
            "cardBrandId": "Elo",
            "accountId": "cta-8acbd197-a061-461c-af38-5632315776eb",
            "forceAccept": false,
            "statusChangeHistory": [
                {
                    "callingSystemName": "Autorizador ISO8583",
                    "stateChangeDescription": "Nova transação registrada.",
                    "stateChangeReasonCode": "other",
                    "newStatus": "pending",
                    "changeRequestReceivedDate": "2021-05-05T13:47:48.602Z",
                    "statusChangeSource": "brand"
                }
            ],
            "disputeId": "DIS-ad56619b-c72f-4c44-a821-983ca2ebdd67",
            "merchantAddress": "ALAMEDAXINGU",
            "merchantZipcode": "235432300",
            "card": {
                "BIN": "******",
                "cardId": "crt-f4de6c2a-f501-482e-a353-a88f6a4b3aa3",
                "last_four_digits": "1585",
                "cardIdIssuer": "fdb5d5ea-2d5d-4937-865e-933fe46b3d3f"
            }
        }
    },
    "resultData": {
        "psResponseId": "14afb7be-a010-4ee6-84fa-8d4a20eb315d",
        "resultCode": 0,
        "resultDescription": "Resposta criada com sucesso!",
        "issuerRequestId": "DIS-ad56619b-c72f-4c44-a821-983ca2ebdd67"
    }
```

### **Descripción de la Respuesta**

Here is the translation of the table to English:

<table>
    <thead>
        <tr>
            <th>Name</th>
            <th>Type</th>
            <th>Description</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>dispute</td>
            <td>Object</td>
            <td>Object containing data related to the created dispute. <b>Present only in case of success.</b></td>
        </tr>
        <tr>
            <td>- disputeId</td>
            <td>String</td>
            <td>Unique identifier of the dispute in paySmart.</td>
        </tr>
        <tr>
            <td>- disputeStatus</td>
            <td>String</td>
            <td>Current state of the dispute.</td>
        </tr>
        <tr>
            <td>- disputeRequest</td>
            <td>Object</td>
            <td>Request sent by the issuer.</td>
        </tr>
        <tr>
            <td>- transaction</td>
            <td>Object</td>
            <td>Data of the transaction being disputed.</td>
        </tr>
        <tr>
            <td>resultData</td>
            <td>Object</td>
            <td>Data from the API response.</td>
        </tr>
        <tr>
            <td>- resultCode</td>
            <td>Integer</td>
            <td>Processing result code.</td>
        </tr>
        <tr>
            <td>- resultDescription</td>
            <td>String</td>
            <td>Textual description of the processing result.</td>
        </tr>
        <tr>
            <td>- psResponseID</td>
            <td>String</td>
            <td>Unique identifier of the response. Generated by paySmart.</td>
        </tr>
        <tr>
            <td>- issuerRequestId</td>
            <td>String</td>
            <td>Unique identifier of the request generated by the issuer. Echoed as sent in the request in the issuerDisputeId field.</td>
        </tr>
    </tbody>
</table>

{% include endpoint_footer.html %}