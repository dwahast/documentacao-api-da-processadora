---
title: Creación de Disputa
name: ""
link: "add-dispute"
method: POST
type: disputes
tags:
    - endpoint
    - disputas
    - criação disputa
description: "Punto final para la creación de disputas."
permalink: /es/docs/disputes/add-dispute
language: es
---
{% include endpoint_header.html %} 

Es importante tener en cuenta que este punto final (endpoint) debe ser llamado únicamente para la creación de disputas. Para casos de continuación de una disputa en etapas posteriores, donde es necesario enviar nuevas solicitudes, consulte <a href="{{ site.url }}{{ site.baseurl }}/es/docs/respond-dispute">Continuación de Disputas</a>.

Además, antes de crear una disputa, se debe confirmar que cumple con todas las restricciones que podrían impedir su creación, como se detalla en <a href="{{site.baseurl}}/es/docs/disputes/restrictions">Restricciones para la Creación de Disputas</a>.

{% include endpoint_auth.html %}

### **Argumentos**

<table>
    <thead>
        <tr>
            <th>Nombre</th>
            <th>Obligatorio</th>
            <th>Tipo</th>
            <th>Descripción</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>accountId</td>
            <td>Sí</td>
            <td>String</td>
            <td>Identificador único de la cuenta, asignado por paySmart.</td>
        </tr>
        <tr>
            <td>transactionId</td>
            <td>Sí</td>
            <td>String</td>
            <td>Identificador único de la transacción en disputa.</td>
        </tr>
        <tr>
            <td>disputeCode</td>
            <td>Sí</td>
            <td>String</td>
            <td>Código de la disputa que indica el motivo de la apertura. Ver <a href="./codigos-disputa">Códigos de Motivo de Disputa</a></td>
        </tr>
        <tr>
            <td>fraudType</td>
            <td>No</td>
            <td>String</td>
            <td>Código del tipo de fraude que indica el motivo de la apertura de la disputa por fraude. Ver <a href="./codigos-fraude">Códigos de Tipo de Fraude</a></td>
        </tr>
        <tr>
            <td>disputeTextMessage</td>
            <td>Sí</td>
            <td>String</td>
            <td>Mensaje de texto justificando la disputa. El contenido esperado depende del código del motivo de la disputa y se puede encontrar en <a href="./codigos-disputa">Códigos de Motivo de Disputa</a>.</td>
        </tr>
        <tr>
            <td>issuerDisputeId</td>
            <td>No</td>
            <td>String</td>
            <td>Identificador único de la disputa. Generado por el emisor.</td>
        </tr>
        <tr>
            <td>Partial</td>
            <td>Sí</td>
            <td>Boolean</td>
            <td>Indica si se está disputando una parte o el valor total de la transacción. <b>Si es verdadero, es necesario informar amount_disputed.</b></td>
        </tr>
        <tr>
            <td>willAddDocuments</td>
            <td>No</td>
            <td>Boolean</td>
            <td>Indica si se adjuntarán documentos de respaldo a la disputa en el Portal ELO posteriormente.</td>
        </tr>
        <tr>
            <td>amount_disputed</td>
            <td>Condicional</td>
            <td>Objeto</td>
            <td>Valor en disputa. <b>Solo debe estar presente si partial es verdadero y debe contener el campo amount y, opcionalmente, currencyCode</b>.</td>
        </tr>
        <tr>
            <td>- amount</td>
            <td>Condicional</td>
            <td>Inteiro</td>
            <td>Valor omitiendo la coma.<b>Obligatorio estar presente si partial es verdadero.</b></td>
        </tr>
        <tr>
            <td>- currencyCode</td>
            <td>No</td>
            <td>Inteiro</td>
            <td>Código de la moneda, según ISO-4217.</td>
        </tr>
        <tr>
            <td>sourceAudit</td>
            <td>No</td>
            <td>Objeto</td>
            <td>Información de auditoría para el registro de información. Tiene los campos operatorId y processId</td>
        </tr>
        <tr>
            <td>- operatorId</td>
            <td>No</td>
            <td>String</td>
            <td>Identificación del operador solicitante.</td>
        </tr>
        <tr>
            <td>- processId</td>
            <td>No</td>
            <td>String</td>
            <td>Identificación del proceso solicitante.</td>
        </tr>
    </tbody>
</table>

### **Ejemplo de Solicitud**

```json
{
    "issuerDisputeId": "a3e4cf3c-2a98-4a88-a370-5429356d7112",
    "accountId": "cta-8acbd197-a061-486c-af38-5632315767be",
    "transactionId": "aut_3ee27dd3-c1b4-43b6-993e-6d126b3e67d2",
    "disputeCode": "30",
    "fraudType": "05",
    "disputeTextMessage": "Serviço não realizado",
    "partial": true,
    "amount_disputed": {
        "amount": 123,
        "currencyCode": 986
    },
    "willAddDocuments": false,
    "sourceAudit": {
        "operatorId": "12340985312",
        "processId": "PID-12345"
    }
}
```

### **Ejemplo de Respuesta**

```json
{
    "dispute": {
        "disputeId": "DIS-0406360e-58fd-45bf-818e-73fb95088745",
        "disputeStatus": "waiting_for_response_from_acquirer",
        "disputeRequest": {
            "issuerDisputeId": "a3e4cf3c-2a98-4a88-a370-5429356d7118",
            "accountId": "cta-8acbd197-a061-486c-af38-5632315776eb",
            "transactionId": "aut_3ee27dd3-c1b4-43b6-993e-6d126b3e67b7",
            "disputeCode": "81",
            "fraudType": "05",
            "disputeTextMessage": "FRAUDE",
            "partial": true,
            "willAddDocuments": false,
            "amount_disputed": {
                "amount": 123,
                "currencyCode": 986
            },
            "sourceAudit": {
                "operatorId": "12340985312",
                "processId": "PID-12345"
            }
        },
        "transaction": {
            "transactionDateTime": "2021-05-04T12:17:29",
            "settlementDateTime": null,
            "transactionId": "aut_3ee27dd3-c1b4-43b6-993e-6d126b3e67b7",
            "accountId": "cta-8acbd197-a061-486c-af38-5632315776eb",
            "psProductCode": null,
            "psProductName": null,
            "issuerId": "3",
            "cardBrandId": null,
            "creatingSystemName": null,
            "transactionSource": null,
            "transactionReceivedDateTime": null,
            "transactionDate": null,
            "transactionTime": null,
            "settlementDate": null,
            "transactionType": "SALE",
            "transactionStatus": null,
            "transactionAuthorizationResponse": {
                "approved": true,
                "denialReason": null
            },
            "acquirerId": "0025",
            "acquirerTransactionId": "936674959729",
            "merchantId": "020001605270002",
            "merchantName": "ELO",
            "merchantDocumentId": null,
            "merchantAddress": "ALAMEDAXINGU",
            "merchantCity": "BARUERI",
            "merchantUf": null,
            "merchantZipcode": null,
            "countryCode": "076",
            "mcc": "5712",
            "terminalId": "20172289",
            "amount": {
                "amount": 25615,
                "currencyCode": 986,
                "debit_or_credit": null
            },
            "international": null,
            "incremental": null,
            "internationalTransactionData": null,
            "entryMode": "MANUAL",
            "card": {
                "cardId": "crt-f4de6c2a-f501-482e-a353-a88f6a4b3bb7",
                "bin": "******",
                "last4Digits": "1585"
            },
            "undoData": null,
            "originalTransaction": null,
            "cancellingTransactionId": null,
            "disputeId": null,
            "disputeStatus": null,
            "fees": [],
            "statusChangeHistory": null,
            "ISO8583MessageRequest": {
                "mti": "0100",
                "de002": "************1585",
                "de003": "003000",
                "de004": "000000025615",
                "de005": null,
                "de006": null,
                "de007": "0504151729",
                "de008": null,
                "de009": null,
                "de010": null,
                "de011": "041729",
                "de012": "121729",
                "de013": "0504",
                "de014": "2412",
                "de015": null,
                "de016": null,
                "de018": "5712",
                "de019": "076",
                "de022": "011",
                "de023": null,
                "de024": "100",
                "de025": null,
                "de026": null,
                "de028": null,
                "de029": null,
                "de032": "0025",
                "de033": null,
                "de035": null,
                "de036": null,
                "de037": "936674959729",
                "de038": null,
                "de039": null,
                "de041": "20172289",
                "de042": "020001605270002",
                "de043": "ELO                    BARUERI       076",
                "de045": null,
                "de046": null,
                "de047": null,
                "de048": "*CDT002T0*PRD003070",
                "de049": "986",
                "de050": null,
                "de051": null,
                "de052": null,
                "de053": null,
                "de054": null,
                "de055": null,
                "de056": null,
                "de058": "ALAMEDAXINGU          23543230007612345ELO                  ",
                "de059": null,
                "de060": "000110000P500",
                "de062": null,
                "de063": null,
                "de090": null,
                "de095": null,
                "de105": null,
                "de106": null,
                "de107": null,
                "de121": null,
                "de122": null,
                "de123": null,
                "de124": null,
                "de125": null,
                "de126": "*****",
                "de127": "12182"
            },
            "ISO8583MessageResponse": null,
            "freeDescription": "Nova transação de compra registrada!",
            "forceAccept": false,
            "transferData": null,
            "fraudData": null
        }
    },
    "resultData": {
        "psResponseId": "09fb33e6-e937-4be3-9115-ef648ea1f950",
        "resultCode": 0,
        "resultDescription": "Requisição para inicio de Disputa criada com sucesso!",
        "issuerRequestId": "a3e4cf3c-2a98-4a88-a370-5429356d7112"
    }
}
```

### **Descripción de la Respuesta**

<table>
    <thead>
        <tr>
            <th>Nombre</th>
            <th>Tipo</th>
            <th>Descripción</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>dispute</td>
            <td>Objeto</td>
            <td>Objeto que contiene los datos relacionados con la disputa creada. <b>Presente solo en caso de éxito.</b></td>
        </tr>
        <tr>
            <td>- disputeId</td>
            <td>String</td>
            <td>Identificador único de la disputa en paySmart.</td>
        </tr>
        <tr>
            <td>- disputeStatus</td>
            <td>String</td>
            <td>Estado actual de la disputa.</td>
        </tr>
        <tr>
            <td>- disputeRequest</td>
            <td>Objeto</td>
            <td>Request enviado originalmente por el emisor.</td>
        </tr>
        <tr>
            <td>- transaction</td>
            <td>Objeto</td>
            <td>Datos de la transacción en disputa.</td>
        </tr>
        <tr>
            <td>resultData</td>
            <td>Objeto</td>
            <td>Datos de la respuesta de la API.</td>
        </tr>
        <tr>
            <td>- resultCode</td>
            <td>Integer</td>
            <td>Código de resultado del procesamiento.</td>
        </tr>
        <tr>
            <td>- resultDescription</td>
            <td>String</td>
            <td>Descripción textual del resultado del procesamiento.</td>
        </tr>
        <tr>
            <td>- psResponseID</td>
            <td>String</td>
            <td>Identificador único de la respuesta. Generado por paySmart.</td>
        </tr>
        <tr>
            <td>- issuerRequestId</td>
            <td>String</td>
            <td>Identificador único de la solicitud generado por el emisor. Ecoado conforme enviado en la solicitud en el campo issuerDisputeId.</td>
        </tr>
    </tbody>
</table>

{% include endpoint_footer.html %}
