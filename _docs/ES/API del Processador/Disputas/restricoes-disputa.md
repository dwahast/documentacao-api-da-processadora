---
title: Restricciones a la creación de disputas
name: ""
link: "restrictions"
type: info
# TODO: tags em inglês?
tags:
    - dispute
    - dispute restrictions
description: "Lista de verificación de posibles restricciones que impiden la creación de una disputa bajo cada bandera"
# TODO: permalink em ingles?
permalink: /es/docs/disputes/restrictions
language: es
---
{% include info_header.html %}

Si alguno de los motivos enumerados a continuación se cumple con la transacción en disputa, la API de Disputas devolverá un error de inmediato. La lista de posibles errores devueltos por la API se puede encontrar en <a href="{{site.baseurl}}/es/docs/disputes/codes">Códigos de Error de la API de Disputas</a>.

### **Lista de Restricciones para Elo**

En el caso de Elo, no es posible iniciar el proceso de disputa cuando:

- La transacción se ha realizado con chip.
- La transacción es de cashback. 