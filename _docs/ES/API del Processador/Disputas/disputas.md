---
title: Proceso de disputa
name: disputes
tags: 
 - disputes
description: Operación de disputa cuando se trata de transacciones no reconocidas por el usuario.
permalink: /es/docs/disputes
language: es
---

# **{{page.title}}**

- [**{{page.title}}**](#pagetitle)
  - [**Introducción**](#introducción)
  - [**Processo de Disputas**](#processo-de-disputas)
    - [**Disputas por motivo de fraude**](#disputas-por-motivo-de-fraude)
  - [**Homologación de la API de Disputas**](#homologación-de-la-api-de-disputas)
  - [**Posibles retornos**](#posibles-retornos)
    - [***``"disputeStatus"``***:](#disputestatus)
    - [***``"currentStage"``***](#currentstage)
  - [**Enlaces útiles**](#enlaces-útiles)

## **Introducción**

El proceso de disputas debe iniciarse si una transacción es marcada como no reconocida por el usuario y este solicita la impugnación. Para transacciones **ya liquidadas**, este proceso se lleva a cabo con la red de tarjetas en múltiples etapas. Sin embargo, antes de iniciar el proceso, se deben realizar algunas validaciones por parte del emisor.

Entre estas validaciones, la red de tarjetas siempre proporciona ciertas pautas de procedimientos genéricos que deben realizarse antes de abrir la disputa:
- En primer lugar, insistir con el cliente sobre la transacción. Esto se puede hacer confirmando datos como el nombre del establecimiento o la dirección, con el objetivo de asegurarse de que el titular no recuerde la compra.
- En el caso de una transacción con contraseña, preguntar si algún familiar o persona cercana podría haber tenido acceso a la tarjeta.
- Por último, advertir al titular que, si se demuestra que la transacción es legítima, podría incurrir en una multa por parte de la red de tarjetas.

Dadas estas orientaciones, si la respuesta es positiva, se inicia el proceso de disputa. 

## **Processo de Disputas**

La creación y seguimiento de disputas pueden llevarse a cabo a través de los siguientes puntos finales (endpoints):

<ul>
    {% assign endpoints=site.docs | where: "type", "disputes" %}
    {% for endpoint in endpoints %}
    <li><a href="{{site.baseurl}}/es/docs/{{endpoint.type}}/{{endpoint.link}}">
        {% if endpoint.tags contains "endpoint" %}{{endpoint.method}} /{{page.name}}/{{endpoint.name}}{% else %}{{endpoint.title}}{% endif %}
    </a></li>
    {% endfor %}
</ul>

Resumidamente, el proceso de disputas se llevará a cabo de la siguiente manera:

1. Al decidir crear una disputa, el emisor deberá enviar una solicitud que contenga los datos necesarios al primer endpoint indicado, <a href="{{site.baseurl}}/es/docs/disputes/add-dispute">POST /disputes</a>.
2. Una vez que la disputa se haya creado con éxito, el seguimiento de su estado se puede realizar a través del endpoint <a href="{{site.baseurl}}/es/docs/disputes/get-dispute">GET /disputes/{disputeId}</a>. Si se desea hacer un seguimiento de múltiples disputas simultáneamente, las solicitudes deben enviarse al endpoint <a href="{{site.baseurl}}/es/docs/disputes/find-disputes">GET /disputes</a>.
3. En caso de que la entidad emisora haya emitido un juicio negativo para la solicitud inicial de disputa, la etapa de la disputa se actualizará de "chargeback" a "restatement". En este escenario, el emisor debe enviar una solicitud al endpoint <a href="{{site.baseurl}}/es/docs/disputes/respond-dispute">POST /disputes/{disputeId}/response</a> indicando si desea continuar o no con la disputa. Si opta por continuar, la disputa pasará a la etapa de prearbitraje, "pre-arbitration".
4. Una vez enviada la solicitud de prearbitraje al adquirente, existen dos resultados posibles. En el primero, el adquirente acepta la disputa y reembolsa el monto enviado. En el segundo caso, el adquirente puede no estar de acuerdo con la disputa, afirmando que la transacción se realizó correctamente. En este escenario, solicitará el arbitraje por parte de la red de tarjetas, llevando el proceso de disputa a esta etapa, "arbitration".
5. A partir de este punto, corresponde a la red de tarjetas decidir qué lado ganó la disputa, informando al adquirente de su decisión. Por lo tanto, no es necesario enviar ninguna otra solicitud con información a la API de Disputas por parte del emisor, solo solicitudes de seguimiento de estado, como se indica en el punto 2.
6. Alternativamente, si es necesario cancelar la disputa, ya sea por el reconocimiento de la compra por parte del titular o por cualquier otro motivo, se debe enviar una solicitud al endpoint <a href="{{site.baseurl}}/es/docs/disputes/undo-dispute">POST /disputes/{disputeId}/undo</a>, indicando a paySmart y al adquirente la renuncia al proceso. Es importante tener en cuenta que la reversión a través de este endpoint solo se puede realizar en las etapas de Chargeback ("chargeback") o Restatement ("restatement"). En etapas posteriores, el emisor debe comunicarse con el adquirente para resolver el problema a través de TE10/TE20.

### **Disputas por motivo de fraude**
En el momento de la creación de una disputa, existe el campo *``"fraudType"``*. El valor proporcionado en este campo solo se validará en las disputas que se abran por motivo de fraude, es decir, con el campo *``"disputeCode"``* con el valor **81** o **83**, como se indica en la tabla de <a href="{{ site.baseurl }}/es/docs/disputes/codes">Códigos de Motivo de Disputa</a>.

## **Homologación de la API de Disputas**
Durante la homologación de los puntos finales de disputa, es necesario contactar siempre al analista de paySmart para realizar el seguimiento en relación con los cambios de estado de la disputa, debido a la participación en procesos como liquidación, comunicación con el adquirente y comunicación con la red de tarjetas.

## **Posibles retornos**

### ***``"disputeStatus"``***:

* waiting_for_response_from_issuer
* waiting_for_response_from_acquirer
* waiting_for_response_from_brand
* accepted
* denied
* canceled_by_issuer
* rejected_by_brand
* issuer_won
* issuer_lost

### ***``"currentStage"``***

| Valor | Descripción |
|-------|-----------|
| *``"chargeback"``* | Estado inicial |
| *``"restatement"``* | Respuesta negativa de la red de tarjetas, el emisor debe decidir si desea continuar con la disputa o no |
| *``"pre-arbitration"``* | Proceso realizado entre el emisor y el adquirente, para, en caso de ser aceptado y continuado, iniciar el proceso de arbitraje con la red de tarjetas |
| *``"arbitration"``* | Etapa en la cual ELO analiza la solicitud de arbitraje |
| *``"lost"``* | Proceso de disputa perdido |
| *``"undone"``* | Desistir del chargeback |
| *``"successful"``* | Disputa aprobada y reembolso enviado **(EN DESARROLLO)** |
| *``"withdrawal"``* | Cuando se desiste del restatement |
| *``"rejected_by_brand"``* | Creación de disputa rechazada por la red de tarjetas |

## **Enlaces útiles**

<ul>
    {% assign endpoints=site.docs | where: "type", "info" %}
    {% for endpoint in endpoints %}
        {% if endpoint.language == "es" %}
        <li><a href="{{site.baseurl}}/es/docs/disputes/{{endpoint.link}}">
            {{endpoint.title}}
        </a></li>
        {% endif %}
    {% endfor %}
</ul>