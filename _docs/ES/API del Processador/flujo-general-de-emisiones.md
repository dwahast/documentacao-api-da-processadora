---
layout: default-es
title: Visión general
tags: 
 - fluxo geral
 - figuras
description: Funcionamiento general del ciclo de emisiones
permalink: es/
language: es
---

# Visión general

En esta sección, se ofrece una descripción general del flujo de llamadas que un emisor o subemisor debe llevar a cabo para emitir tarjetas y procesar transacciones con el procesador paySmart. Para garantizar que la tarjeta llegue de manera adecuada al usuario final, titular de una cuenta en su institución, es esencial seguir los procedimientos detallados en las figuras que se presentan a continuación.

&nbsp;

![Modelo 1 do fluxo geral de emissão - Parte 1]({{ site.url }}{{ site.baseurl }}/assets/img/ES/API del Processador/grafico_processadora_1.png)
assets/img/ES/API del Processador/Gráfico processadora_1_ES.png
&nbsp;
&nbsp;


![Modelo 1 do fluxo geral de emissão - Parte 2]({{ site.url }}{{ site.baseurl }}/assets/img/ES/API del Processador/grafico_processadora_2.png)
&nbsp;
&nbsp;


![Modelo 1 do fluxo geral de emissão - Parte 3]({{ site.url }}{{ site.baseurl }}/assets/img/ES/API del Processador/grafico_processadora_3.png)

&nbsp;

Las figuras describen los principales puntos de interacción entre un Emisor, paySmart y un Personalizador:
1. El Emisor registra los datos de un Portador (persona física o jurídica), creando una cuenta en el sistema de paySmart. Para obtener más información, consulte la documentación correspondiente.  <a href="docs/contas">Saiba mais</a>
2. El Emisor solicita tarjetas que serán vinculadas a la cuenta previamente creada. Para obtener más información, consulte la documentación correspondiente. <a href="docs/cartoes">Saiba mais</a>
3. paySmart, en un proceso asincrónico, combina las solicitudes del Emisor en solicitudes completas para la personalización de tarjetas que serán procesadas por el Personalizador. Las tarjetas se personalizan y se envían a los Usuarios Finales.
4. Los Usuarios Finales, a través de una aplicación u otro canal proporcionado por el Emisor, realizan operaciones como desbloqueo, cambio de contraseña o consulta de saldo. Estas operaciones se llevan a cabo con el apoyo de paySmart. Para obtener más información, consulte la documentación correspondiente. <a href="docs/gerenciamento_cartao">Saiba mais</a>
5. Los Usuarios Finales realizan compras o retiros en terminales (POS, PDV o ATMs) proporcionados por Acreditantes. Estas transacciones se envían a la red de tarjetas, que a su vez las direcciona a paySmart y al Emisor. En el caso de Emisores que mantienen su propio saldo, deben estar preparados para responder a paySmart en consultas a través de la API de Integración con Emisores. Para obtener más información, consulte la documentación correspondiente. <a href="api/spec-proc/">Saiba mais</a>

