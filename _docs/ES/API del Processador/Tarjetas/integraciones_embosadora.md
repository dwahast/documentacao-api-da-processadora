---
layout: default-es
title: Fluxo completo de Emissão de Cartões
tags: 
 - gráfica
 - embossadora
 - emissão
 - cartões
 - rastreio
 - transportadora
 - courier
description: Fluxo completo de Emissão de Cartões
permalink: /es/docs/integracoes-embossadora
---

# **Integraciones y Preguntas sobre la Imprenta/Embosadora**

1. [Introdução](#introdução)
1. [Contratação da Gráfica](#contratação-da-gráfica)
    1. [Gráficas Parceiras](#gráficas-parceiras)
    1. [Multigráficas](#multigráficas)
        1. [Requisição API Processadora](#requisição-api-processadora)

## **Introducción**
Esta página tiene como objetivo detallar el flujo completo de emisión de tarjetas, desde la contratación de la imprenta hasta el seguimiento de los envíos.

Se describen las interacciones necesarias entre el emisor y la imprenta, así como algunos flujos de negocios y casos de uso, con el fin de ayudar al emisor en la planificación de las funcionalidades.

## **Contratación de la Imprenta**
En esta sección se presenta una visión general de qué son y cuáles son las Gráficas Asociadas, además de la modalidad de contratar múltiples imprentas por parte del emisor.

### **Imprentas Asociadas**
Una Imprenta Asociada no es más que una imprenta homologada con Elo y que sigue las políticas de PCI vigentes. paySmart, por su parte, no tiene una imprenta interna ni un contrato de prestación de servicios con estas imprentas asociadas.

Es responsabilidad del emisor ponerse en contacto con la imprenta de su interés y firmar un contrato de prestación de servicios. paySmart, como procesador, solo actúa como intermediario en las conversaciones y procesos que involucran el flujo de emisión de tarjetas.

Algunas de las Imprentas Asociadas son:
- Valid
- IntelCav: Tecnologia e Cartões
- Jallcard
- Thomas Greg & Sons
- Gemalto / Thales Group
- G&D (Giesecke e Devrient)
- Idemia
- Alterosa
- UP Technology

El emisor debe informar al Analista paySmart sobre la imprenta contratada y actualizarlo siempre que haya un cambio o contratación de una nueva imprenta.

### **Imprentas Asociadas**
Una Imprenta Asociada no es más que una imprenta homologada con Elo y que sigue las políticas de PCI vigentes. paySmart, por su parte, no tiene una imprenta interna ni un contrato de prestación de servicios con estas imprentas asociadas.

Es responsabilidad del emisor ponerse en contacto con la imprenta de su interés y firmar un contrato de prestación de servicios. paySmart, como procesador, solo actúa como intermediario en las conversaciones y procesos que involucran el flujo de emisión de tarjetas.

Algunas de las Imprentas Asociadas son:
- Valid
- IntelCav: Tecnologia e Cartões
- Jallcard
- Thomas Greg & Sons
- Gemalto / Thales Group
- G&D (Giesecke e Devrient)
- Idemia
- Alterosa
- UP Technology

El emisor debe informar al Analista paySmart sobre la imprenta contratada y actualizarlo siempre que haya un cambio o contratación de una nueva imprenta.

## **Diseño de Tarjetas**
Un punto muy importante en la personalización de la tarjeta es su diseño o card design (también conocido como cara).

El emisor puede tener tantos card designs como considere necesario para diferenciar sus tarjetas.

Para informar qué card design debe imprimirse en la tarjeta, en el momento de la solicitud de la tarjeta, el emisor debe proporcionar el código correspondiente al card design en el campo deliveryKitCode (DKC).

El DKC es un código compuesto por 6 dígitos y es transparente para paySmart, ya que no lo utilizamos. Por otro lado, el emisor y la imprenta deben tener estos códigos alineados internamente para que la imprenta imprima las tarjetas con el diseño correcto.

El DKC se transmite a la imprenta como los últimos 6 dígitos en la nomenclatura del archivo de embossing, como se puede verificar en 

## **Archivo de Embossing**
El emisor puede verificar la nomenclatura de un archivo de embossing al consultar el endpoint y verificar el campo

La nomenclatura del archivo de embossing sigue un patrón, como se puede observar a continuación:

CCCCCC_BBBBBB_PEPPPPPP_DDMMAAAA_HHMMSS_DDDDDD

| Acrónimo | Campo                                  | Tamaño | Descripción                                                                                                                                                        |
| -------- | -------------------------------------- | ------ | ------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| CCCCCC   | Código de Cliente                      | 6      | Identificador del cliente en paySmart (generalmente el nombre del emisor en 6 caracteres, completando con guion bajo ("_") si tiene menos de 6)                    |
| _        | Separador                              | 1      | Fijo. Carácter de guion bajo ("_")                                                                                                                                 |
| BBBBBB   | BIN                                    | 6      | BIN asociado al producto, que también identifica al emisor ante la bandera y las redes de captura                                                                  |
| _        | Separador                              | 1      | Fijo. Carácter de guion bajo ("_")                                                                                                                                 |
| PE       | Identificador del Perfil Electrónico   | 2      | Dos caracteres que indican si el producto es:<br><br>"CR" - Crédito Nacional<br>"DN" - Débito Nacional<br>"CI" - Crédito Internacional<br>"MN" - Múltiple Nacional |
| PPPPPP   | Identificador del Producto en paySmart | 6      | Es el producto indicado en la solicitud de la tarjeta en el campo *``"psProductCode"``*                                                                            |
| _        | Separador                              | 1      | Fijo. Carácter de guion bajo ("_")                                                                                                                                 |
| DDMMAAAA | Fecha                                  | 8      | Fecha de generación del archivo en formato DDMMAAAA                                                                                                                |
| _        | Separador                              | 1      | Fijo. Carácter de guion bajo ("_")                                                                                                                                 |
| HHMMSS   | Hora                                   | 6      | Hora de generación del archivo en formato HHMMSS                                                                                                                   |
| _        | Separador                              | 1      | Fijo. Carácter de guion bajo ("_")                                                                                                                                 |
| DDDDDD   | Card Design (DKC)                      | 6      | Indica el diseño a utilizar en el plástico de las tarjetas (diseño, cara, card design)                                                                             |


## **Seguimiento de las tarjetas**
Si el emisor desea realizar el seguimiento de las tarjetas o incluso proporcionar este seguimiento al propio portador, es posible.

Es importante entender que el proceso de seguimiento no depende solo de paySmart, sino que se realiza en conjunto con la(s) gráfica(s) y la(s) empresa(s) de transporte (courier) contratada(s).

### **Código de Identificación paySmart**
En la generación de la tarjeta, se asigna automáticamente un código de identificación único a cada tarjeta. Este código consta de 10 dígitos.

Es importante entender que este código generado es parcial y, solo por sí mismo, no es suficiente para rastrear la tarjeta.

Para verificar el código de seguimiento de la tarjeta, el emisor puede, al solicitar la tarjeta .................... o en un momento futuro .................. verificar el campo *``"trackingId"``*.

### **Código de Seguimiento Final (AR)**
El Código de Seguimiento (AR) consta de varias partes, con un formato como el siguiente:

Prefijo de la empresa de transporte + Código de Identificación paySmart + Sufijo de la empresa de transporte

AA0123456789BR

O emisor debe informar a la gráfica que desea utilizar el código de seguimiento y solicitar que se alinee con la empresa de transporte. Después de todos los alineamientos, la empresa de transporte creará el prefijo y sufijo que compondrán el AR.

Con el AR, el emisor podrá acceder al sitio web de la empresa de transporte y verificar la situación del envío.

> **Consejos de Implementación**
>
> El emisor puede mostrar la posición de seguimiento de la tarjeta a su titular de dos formas principales:
>
> **Sencilla:** Proporcionar en la aplicación un enlace para acceder al sitio web de la empresa de transporte enviando el AR como parámetro de búsqueda, mostrando todos los datos del envío.
>
> **Compleja:** Mostrar, dentro de la aplicación, la información de seguimiento, implementando un procedimiento de scraping que traiga los datos devueltos con la búsqueda del AR o incluso en una posible integración de APIs con la empresa de transporte.

 


## **Emissão de cartões em lote**
O sistema da paySmart não suporta lotes de cartões de forma nativa, mas disponibilizamos algumas opções para o emissor:

### **Utilização do campo extraData**

### **Solicitação de cartões em janelas específicas**

### 

## ****
## ****
## ****
## ****
## ****


