---
layout: default-es
title: Creación de Tarjetas Físicas
tags: 
 - cartões
 - cartões anônimos
description: Operação em alto nível para a criação de cartões físicos na processadora paySmart
permalink: es/tarjetas/fisicas/creacion
language: es
---

# **Creación de Tarjetas Físicas**

1. [Introducción](#Introducción)
2. [Creación de Tarjetas](#criação-de-cartões)
    1. [Cartões Nomeados](#cartões-nomeados)
        1. [Requisição](#requisição)
        1. [Parâmetros Opcionais](#parâmetros-opcionais)
    2. [Cartões Anônimos](#cartões-anônimos)
        1. [Requisição](#requisição)
    1. [Resposta](#resposta)


## **Introducción**

Una tarjeta es un método de retiro de efectivo que permite, a través de un plástico equipado con chip EMV, banda magnética y datos numéricos de validación, realizar transacciones que mueven el saldo de una cuenta específica mediante operaciones en todas las redes de adquirentes que pueden aceptar este formato de pago.

## **Creación de Tarjetas**

Existen dos flujos principales e independientes para la creación de tarjetas físicas, que incluyen tarjetas nominativas y tarjetas anónimas:

### **Tarjetas Nominativas** 

Una tarjeta de este formato es una tarjeta que tendrá el nombre del titular grabado en el plástico y está asociada a una cuenta creada previamente.

La solicitud de la tarjeta nominativa se realiza a través del método [POST accounts/{accountId}/newCardRequest]({{ site.url }}{{ site.baseurl }}/spec-proc/), donde *`{accountId}`* es el identificador de la cuenta, que debe haber sido creada previamente según el proceso descrito en la [Creación de Cuentas]({{ site.url }}{{ site.baseurl }}/docs/contas).

Los campos obligatorios para solicitar una tarjeta física nominativa a través de este método son:

| Campo           | Descripción                                                                                           |
|-----------------|-----------------------------------------------------------------------------------------------------|
| *`"cardholder"`* | Titular: Incorpora los datos necesarios para identificar de manera unívoca al titular de una tarjeta.                                                                           |
| *`"cardholder"."cardholderType"`* | Tipo de titular: Indica si el titular de la tarjeta es el titular de la cuenta (tipo *``"main"``*) o un dependiente (tipo *``"additional"``*).|
| *``"cardholder"."fullName"``* | Nombre completo: Nombre legal del titular.                                                     |
| *``"cardholder"."cardData"."embossingName"``*           | Nombre de embossing: Un nombre para la impresión o grabado en la tarjeta. |
| *``"cardholder"."identityDocumentNumber"``* | Documento de identidad: Número del documento que identifica al titular en organismos oficiales, generalmente un CPF o un CNPJ. |                                        
| *``"cardholder"."birthDate"``* | Fecha de nacimiento: Fecha de nacimiento del titular, requerida según las regulaciones del Banco Central.  |
| *``"cardholder"."nationality"``* | Nacionalidad: País de nacimiento del titular. |

#### **Solicitud**
Como ocurre de manera sistemática en las solicitudes a la API de la procesadora, la información se envía como cuerpo de la solicitud HTTP en [POST accounts/{accountId}/newCardRequest]({{ site.url }}{{ site.baseurl }}/spec-proc/). 

Una solicitud mínima para la creación de una tarjeta quedaría, entonces, en el siguiente formato:

```json
{
  "cardholder": {
    "cardholderType": "main",
    "fullName": "Fulano da Silva",
    "cardData": {
      "embossingName": "FULANO DA SILVA"
    },
    "identityDocumentNumber": "03873703805"
    "birthDate": "21/03/1983",
    "nationality": "Brasil"
  }
}
```
#### **Parámetros Opcionales**

##### ***``"cardDeliveryAddress"``***

Una información opcional, pero que puede adquirir gran relevancia según las reglas de negocio, es el parámetro "cardDeliveryAddress". 

Este parámetro permite sobrescribir la dirección de entrega proporcionada al crear la cuenta, lo que permite que diferentes titulares reciban sus tarjetas en direcciones diferentes.

##### ***``"deliveryKitCode"`` (DKC)***

Si el emisor tiene varios diseños de tarjeta diferentes para sus tarjetas, es posible identificarlos a través del campo *``"deliveryKitCode"``*.

Para ello, el emisor debe acordar con la imprenta respectiva el uso de un código numérico de 6 dígitos para identificar cada *diseño de tarjeta*. 

Para paySmart, los *DKC* (Delivery Kit Codes) son transparentes y no influyen en el proceso de generación de las tarjetas. Esta definición es importante a nivel de negociación entre el emisor y la imprenta.

##### ***``"bureaxId"``***

Si el emisor tiene interés en emitir tarjetas con más de una imprenta, se puede utilizar este campo para identificar el destino de la tarjeta. Si este campo no se envía, la tarjeta se enviará a la imprenta predeterminada.

>**Atenção**
>
>Si el emisor desea utilizar múltiples imprentas, debe ponerse en contacto con el analista a cargo del proyecto en paySmart y proporcionar información sobre las imprentas deseadas y cuál se considerará la predeterminada

### **Tarjetas Anónimas**

Una tarjeta de este formato es una tarjeta que **no** tendrá el nombre del titular grabado en el plástico y **no** está asociada a una cuenta. 

Una tarjeta se crea de manera anónima a través del método [POST cards/requestNewAnonymousCard]({{ site.url }}{{ site.baseurl }}/spec-proc/), sin vínculo a una cuenta.

Esta tarjeta podrá ser asociada a un titular (que pasará a ser el titular de una cuenta exclusivamente vinculada a esa tarjeta) en un momento futuro, como su adquisición en una red minorista, por ejemplo.

#### **Solicitud**

La solicitud de una tarjeta anónima solo requiere el código del producto *``"psProductCode"``* y una dirección de entrega *``"cardDeliveryAddress"``* como campos obligatorios. Por lo tanto, un cuerpo de solicitud simple como el que se ilustra a continuación sería una solicitud válida para una tarjeta anónima.

```json
{
  "psProductCode": "020101",
  "cardDeliveryAddress": {
    "addressLine1": "R. Manoelito de Ornellas",
    "city": "Porto Alegre",
    "state": "RS"
  }
}
```
### **Respuesta**

Para ambos escenarios (tarjetas nominativas o anónimas), el formato de la respuesta esperada es idéntico. El campo principal de retorno es el "cardId", que permitirá consultar y operar sobre una tarjeta durante todo su ciclo de vida.

Al igual que la cuenta, también es una composición de cadenas: el prefijo ``"crt-"`` seguido de un UUID en formato v4, como se muestra en el siguiente ejemplo de respuesta:

```json
{
  "resultData": {
    "resultCode": 0,
    "resultDescription": "Requisição de novo cartão feita com sucesso!",
    "psResponseId": "dfd88b95-d28d-49ac-90ba-7ba0b35aebd0"
  },
  "cardId": "crt-41cab09a-c20a-46d0-883a-8b2a359fc4ad"
}
```