---
layout: default-es
title: Generación de PIN Block
tags:
- pinblock
description: Informações de como gerar pinblock
permalink: /es/docs/geracao_pinblock
---

# **Generación de PIN Block**

- [**Generación de PIN Block**](#generación-de-pin-block)
  - [**Introducción**](#introducción)
  - [**Creación del *PIN Block***](#creación-del-pin-block)
    - [**Ejemplo en Java**](#ejemplo-en-java)
    - [**Uso**](#uso)

## **Introducción**

***PIN Block*** se puede entender como el ***PIN*** de la tarjeta cuando se cifra con la clave de transporte (*``"idTransportKey"``*).

En general, utilizamos el formato ***ISO-1*** para la generación del ***PIN Block***, ya que en este formato no es necesario proporcionar el ***PAN*** de la tarjeta, garantizando así su protección y confidencialidad.

En el endpoint ***[POST /cards/{cardId}/changePin]({{ site.url }}{{ site.baseurl }}/es/processor-api)***, utilizamos la clave de transporte ***idTransportKey*** = **4242**, que representa la clave 3DES **505152535455565758595A5B5C5D5E5F** (*Outgoing ZPK*) en entorno de homologación.

En producción, la clave de transporte ***idTransportKey*** = **4242** sigue siendo la misma, pero representará otra clave 3DES que será proporcionada en tiempo de proyecto por el analista responsable.

## **Creación del *PIN Block***

Es posible generar ***PIN Blocks ISO-1*** en el sitio <https://emvlab.org/pinblock/>, en la parte inferior del formulario, completando los campos de la siguiente manera:

- ***Convertir a***: ISO-1
- ***Outgoing Clear PIN Block***: "1" + longitud del PIN, con una posición + PIN + relleno con "F" hasta completar 16 posiciones.
    - Ejemplo para la contraseña 1234: "1" + "4" + "1234" + "FFFFFFFFFF" = 141234FFFFFFFFFF
- ***Outgoing PAN***: no es necesario completar para ISO-1.
- ***Outgoing ZPK***: 505152535455565758595A5B5C5D5E5F

Haga clic en ***Encrypt***: El ***PIN Block*** aparecerá justo debajo y se puede utilizar en la API.

### **Ejemplo en Java**

También es posible crear el ***PIN Block*** a partir del código Java proporcionado a continuación:

```java
private final static char[] hexArray = "0123456789ABCDEF".toCharArray();
public static byte[] hexStringToByteArray(String s) {
    int len = s.length();
    byte[] data = new byte[len / 2];
    for (int i = 0; i < len; i += 2) {
        data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                + Character.digit(s.charAt(i+1), 16));
    }
    return data;
}


public static String byteArrayToHexString(byte[] bytes) {
    char[] hexChars = new char[bytes.length * 2];
    for ( int j = 0; j < bytes.length; j++ ) {
        int v = bytes[j] & 0xFF;
        hexChars[j * 2] = hexArray[v >>> 4];
        hexChars[j * 2 + 1] = hexArray[v & 0x0F];
    }
    return new String(hexChars);
}

public static byte[] concat(byte[] a, byte[] b) {
    byte [] temp = new byte[a.length + b.length];
    System.arraycopy(a, 0, temp, 0, a.length);
    System.arraycopy(b, 0, temp, a.length, b.length);
    return temp;
}
public static String encrypt3DES(String message, String key) throws Exception {

    byte[] keyBytes = hexStringToByteArray(key);

    byte [] keyBytesPadding = new byte[8];
    System.arraycopy(keyBytes, 0, keyBytesPadding, 0, 8);
    keyBytes = concat(keyBytes, keyBytesPadding);


    final SecretKey secretKey = new SecretKeySpec(keyBytes, "DESede");
    final IvParameterSpec iv = new IvParameterSpec(new byte[8]);
    //final Cipher cipher = Cipher.getInstance("DESede/CBC/PKCS5Padding");
    final Cipher cipher = Cipher.getInstance("DESede/CBC/NoPadding");
    cipher.init(Cipher.ENCRYPT_MODE, secretKey, iv);

    final byte[] plainTextBytes = hexStringToByteArray(message);
    System.out.println("plain content bytes: " + plainTextBytes.length);
    final byte[] cipherText = cipher.doFinal(plainTextBytes);
    // final String encodedCipherText = new sun.misc.BASE64Encoder()
    // .encode(cipherText);


    return byteArrayToHexString(cipherText).toUpperCase();
}
```
### **Uso**

```java
String apiPinBlock = encrypt3DES("141234FFFFFFFFFF", "505152535455565758595A5B5C5D5E5F");
```

El resultado debería ser: **1ED680FA74CDD97A**, un formato que será aceptado por nuestra API.