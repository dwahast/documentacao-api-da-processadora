---
layout: default-es
title: Creación de Tarjetas Virtuales
tags: 
 - cartões
 - cartões anônimos
description: Operação em alto nível para a criação de cartões físicos na processadora paySmart
permalink: es/tarjetas/virtuales/creacion
language: es
---

# **Creación de Tarjetas Virtuales**

1. [Introducción](#Introducción)
1. [Criação de Cartão Virtual](#criação-de-cartão-virtual)
    1. [Requisição](#requisição)
    1. [Resposta](#resposta)
1. [Status dos Cartões Virtuais](#status-dos-cartões-virtuais) 


## **Introducción**

Una tarjeta virtual es un elemento de retiro de efectivo que posee los mismos datos necesarios que una tarjeta física para realizar transacciones no presenciales en el entorno de comercio electrónico, es decir, un número único de identificación (PAN), una fecha de vencimiento y un código de seguridad (CVV).

Su principal beneficio es tener un ciclo de vida muy flexible, al no estar materializado en una tarjeta física, lo que permite personalizar completamente su fecha de vencimiento y el valor por transacción por parte del usuario. Esto evita o reduce los daños en caso de fraude, ya que el oponente que obtenga los datos de la tarjeta estará limitado por las configuraciones establecidas por el legítimo titular de la tarjeta.

## **Criação de Cartão Virtual**

La forma recomendada de generar una tarjeta virtual en la API de la Procesadora es vinculándola directamente a una cuenta creada previamente según el proceso descrito en [Creación de Cuentas]({{ site.url }}{{ site.baseurl }}/docs/contas).

Los datos necesarios para la creación de una tarjeta virtual son:

| Campo | Descripción |
|-------|-----------|
| *``"birthDate"``* | Fecha de nacimiento del titular de la cuenta. Necesaria para cumplir con la obligación de proporcionar este dato en la [creación de una tarjeta física]({{ site.url }}{{ site.baseurl }}/docs/cartoes). |
| *``"constraints"``* | Restricciones: define las reglas y limitaciones que el titular desea establecer para su tarjeta, de acuerdo con el uso que pretende darle. |
| *``"constraints"."currency_code"``* | 
La moneda con la que desea realizar transacciones: representada por un código numérico de tres dígitos según el estándar [ISO-4217](https://pt.wikipedia.org/wiki/ISO_4217). Los más comunes son **"986"** (real brasileño) y **"0840"** (dólar estadounidense). Puede pasarse con el valor **"*"**, lo que indica la aceptación de transacciones en cualquier moneda. |
| *``"constraints"."max_amount"``* | 
El valor máximo por transacción: expresado en centavos (**R$ 50,00** debe representarse como **"5000"**, por ejemplo). Si se pasa el valor **"*"**, la tarjeta virtual podrá realizar transacciones de cualquier valor.|
| *``"constraints"."expiration_timestamp"``* | 
La fecha de vencimiento: es una string en formato **"YYYY-MM-DDTHH:mm:ssZ"** que indica la fecha de vencimiento de la tarjeta. Todos los elementos del formato son obligatorios, incluida la zona horaria, para evitar ambigüedades en la expiración real. Por ejemplo, **"2021-12-31T23:59:59-03:00"** significa el **31 de diciembre de 2021, a las 23:59:59 en la hora estándar de Brasilia**. De esta manera, **"2022-01-01T02:59:59Z" representa la misma hora en la zona horaria GMT**. Si se pasa un **"*"**, se calculará una validez de seis años a partir de la fecha actual.


### **Solicitud**

Los datos mencionados anteriormente deben proporcionarse en un cuerpo en formato JSON a través de una solicitud HTTP en el endpoint [POST /accounts/{accountId}/virtualcards]({{ site.url }}{{ site.baseurl }}/spec-proc/), donde *``{accountId}``* es el identificador único devuelto al crear una cuenta con éxito.

De esta manera, una solicitud mínima para la creación de una tarjeta virtual quedaría en el siguiente formato:

```json
  {
        "birthDate": "21/03/1983",
        "constraints": {
            "currency_code": 986,
            "max_amount": "*",
            "expiration_timestamp": "2021-10-17T23:59:59-03:00"
        }
    }
```

### **Respuesta**

Una solicitud exitosa de una tarjeta virtual devolverá los datos necesarios de la tarjeta para su entrada en cualquier sistema de comercio electrónico, como se muestra en la respuesta a continuación:

```json
{
    "resultData": {
        "resultCode": 0,
        "resultDescription": "Comando concluído com sucesso",
        "psResponseId": "993d32c5-3ed4-4cf6-b28f-dfb1f31d6694"
    },
    "virtualCard": {
        "vCardId": "vcrt-1751b1e7-84d2-463e-a792-3c5e2a5a2eab",
        "vPan": "5092570047467931",
        "vCvv": "391",
        "vDateExp": "10/21",
        "vCardholder": "FULANO DA SILVA"
    }
}
```

>**Atención**
>
>Una tarjeta virtual creada puede utilizarse de inmediato, sin necesidad de desbloqueo.


## **Estado de las Tarjetas Virtuales**

| Status | Descripción |
|--------|-----------|
| Token Ativo | Token apto para recibir transacciones |
| Token Desativado | Se ha superado la cantidad de transacciones permitidas para ese token |
| Token Cancelado | El token ha sido cancelado por el emisor |
| Token Expirado | Vencimiento del token expirado |
| Token Bloqueado | El token ha sido bloqueado debido a un exceso de transacciones denegadas |

## **Observación**

> **Obs. 1:**
>
>Una tarjeta virtual que se encuentra en los estados **Expirado** o **Cancelado** no puede cambiar de estado, es decir, ya se encuentra en una situación final.