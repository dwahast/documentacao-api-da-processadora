---
layout: default-es
title: Gerenciamento de Cartões
description: Dados sobre o gerenciamento de cartões
tags: 
- cartões
- cartão físico
- cartão virtual
permalink: /es/cartoes/gerenciamento
---

# **Gestión de tarjetas**

- [**Gestión de tarjetas**](#gestión-de-tarjetas)
  - [**Bloqueo y Desbloqueo de Tarjetas**](#bloqueo-y-desbloqueo-de-tarjetas)
  - [**Matriz de Bloqueo de Tarjetas**](#matriz-de-bloqueo-de-tarjetas)
    - [**Observaciones**](#observaciones)

## **Bloqueo y Desbloqueo de Tarjetas**

Uno de los pasos del ciclo de gestión de tarjetas es la posibilidad de determinar si está habilitada para realizar transacciones. Al ser impresa, una tarjeta se configura con el estado ***BLOCKED***, es decir, no está habilitada para realizar transacciones. Cuando llega a manos del usuario final, se puede desbloquear mediante el endpoint [POST cards/{cardId}/unblockCardRequest]({{ site.url }}{{ site.baseurl }}/es/processor-api), donde **cardId** es el identificador único de la tarjeta devuelto al momento de su solicitud. El único dato obligatorio a enviar en el cuerpo JSON de la solicitud es el código de desbloqueo `"unblockCode"`, que siempre será **"0"** para casos de activación y reactivación. También se recomienda enviar el campo que contiene la descripción del motivo del desbloqueo (`"reason"`), para facilitar el seguimiento de los eventos. Así, un mensaje mínimo de desbloqueo sería:

```json
{
  "unblockCode": 0,
  "reason": "Cartão recebido."
}
```
El bloqueo también se puede realizar en cualquier momento a través del endpoint [POST cards/{cardId}/blockCardRequest]({{ site.url }}{{ site.baseurl }}/es/processor-api). En este caso, los códigos de bloqueo deben tratarse caso por caso, mediante la interacción con el usuario para proporcionar el motivo del bloqueo. Esto se debe a que, dependiendo del código informado, no se permitirá un futuro desbloqueo o incluso la reemisión de la tarjeta.

En casos de bloqueo preventivo, por ejemplo, el código **"15"** permite que se realice un desbloqueo (código **"0"**) posteriormente. En el caso de robo, identificado por el código **"36"**, no se permitirá el desbloqueo.

El cuerpo de la solicitud de desbloqueo sería:
```json
{
  "blockCode": 36,
  "reason": "Cartão roubado."
}
```

## **Matriz de Bloqueo de Tarjetas**
En la tabla siguiente, se presenta la lista de códigos de bloqueo, con su descripción y comportamiento esperado:

<div class="datatable-begin"></div>

| Código | Estado                                        | Transita para                                                  | Permite Reemissão | Descripción del motivo                                                                                                                                                                                                 |
| ------ | --------------------------------------------- | -------------------------------------------------------------- | ----------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 0      | Activo                                        | 10,12,15,16,17,18,22,30,31,32,33,34,35,36,37,75,90,91,92,93,94 | No                | Activo (Sin Bloqueo)                                                                                                                                                                                                   |
| 1      | Bloqueo de Envío                              | 0,10,20,21,22,35,36,75,90,94                                   | No                | Tarjeta bloqueada durante el proceso de generación hasta la activación por el titular                                                                                                                                  |
| 10     | Problema en el Embossing                      | 90                                                             | Sí                | La tarjeta presenta algún error de embossing que permite continuar la utilización hasta que sea reemplazada                                                                                                            |
| 12     | Pedido de Nueva Vía                           | 90                                                             | Sí                | La tarjeta fue bloqueada por algún proceso manual (por ejemplo, interfaz del emisor) que solicitó la generación de una nueva vía para la tarjeta. La vía anterior permanece válida hasta que la nueva vía sea activada |
| 15     | Bloqueado por Renovación                      | 90                                                             | Sí                | La tarjeta fue bloqueada por un proceso automatizado que solicitó la generación de una nueva vía para la tarjeta. La vía anterior permanece válida hasta que la nueva vía sea activada                                 |
| 16*    | Bloqueado por error de contraseña             | 0,90,92,93,94                                                  | Sí                | La tarjeta fue bloqueada en el autorizador por exceder el número de intentos de error de contraseña. Puede ser desbloqueada por el emisor.                                                                             |
| 17     | Otros motivos - bloqueo temporal              | 0,90,92,93,94                                                  | Sí                | Tarjeta bloqueada temporalmente por motivo diferente de renovación, solicitud de segunda vía, embossing, etc.                                                                                                          |
| 18     | Bloqueo de Seguridad                          | 0,35,36,90                                                     | No                | Tarjeta bloqueada preventivamente por el emisor por motivo de seguridad. Puede ser desbloqueada y liberada para uso después de validaciones                                                                            |
| 20     | Fracaso o Extravío en la Entrega (Individual) | 90                                                             | Sí                | Tarjeta dada como extraviada durante el proceso de entrega. Incluso si se encuentra, ya no se puede utilizar.                                                                                                          |
| 21     | Fracaso o Extravío en la Entrega              | 0,1,90                                                         | Sí                | Tarjeta dada como extraviada durante el proceso de entrega.                                                                                                                                                            |
| 22     | Extraviado por el Usuario                     | 90                                                             | Sí                | Tarjeta dada como extraviada por su usuario o titular. Incluso si se encuentra por el titular, ya no se puede utilizar                                                                                                 |
| 30     | Bloqueo Preventivo de Fraude                  | 0,90,92                                                        | Sí                | Tarjeta bloqueada preventivamente por el emisor, por sospecha de fraude. Puede ser desbloqueada y liberada para uso después de la confirmación de uso y posesión por el titular                                        |
| 31     | Fraude - Sospecha de Fraude                   | 32,33,34                                                       | Sí                | Tarjeta identificada como utilizada en una situación sospechosa, por su titular o por algún tipo de análisis por el emisor                                                                                             |
| 32     | Fraude - Conivencia con el Establecimiento    | 90,92                                                          | No                | Tarjeta identificada como involucrada en el uso fraudulento, en connivencia con algún establecimiento también involucrado en actividad fraudulenta.                                                                    |
| 33     | Fraude - Autofraude                           | 90,92                                                          | No                | Tarjeta adulterada en alguna de sus características por el propio titular                                                                                                                                              |
| 34     | Fraude - Falsificado                          | 90,92                                                          | Sí                | Tarjeta falsificada o adulterada                                                                                                                                                                                       |
| 35     | Pérdida                                       | 90                                                             | No                | Tarjeta bloqueada por motivo de pérdida alegada por el titular                                                                                                                                                         |
| 36     | Robo                                          | 90                                                             | No                | Tarjeta bloqueada por motivo de robo alegado por el titular                                                                                                                                                            |
| 37     | Otros motivos - bloqueo definitivo            | 90                                                             | Sí                | Tarjeta bloqueada de manera definitiva por motivo diferente de pérdida, robo, extravío, etc.                                                                                                                           |
| 75     | Tarjeta Expurgada                             | -                                                              | No                | Tarjeta excluida de la base por proceso automático que analiza condiciones contractuales, de utilización, saldo, inactividad, etc.                                                                                     |
| 90     | Cuenta cerrada                                | 75                                                             | No                | Tarjeta cancelada por motivo de cierre de la cuenta.                                                                                                                                                                   |
| 91     | Cancelado por cobro                           | -                                                              | No                | Tarjeta cancelada por motivo de cierre de la cuenta por motivo de cobro.                                                                                                                                               |
| 92     | Cancelado - a pedido del emisor               | -                                                              | No                | Tarjeta cancelada por motivo de cierre de la cuenta a pedido del emisor.                                                                                                                                               |
| 93     | Cancelado - a pedido del usuario              | -                                                              | No                | Tarjeta cancelada por motivo de cierre de la cuenta a pedido del titular.                                                                                                                                              |
| 94     | Titular Fallecido                             | -                                                              | No                | Tarjeta cancelada por motivo de cierre de la cuenta por fallecimiento del titular de la cuenta.                                                                                                                        |

<div class="datatable-end"></div>

### **Observaciones**

>**Obs. 1:**
>
>Los códigos que presentan '-' son códigos de estado final.

>**Obs. 2:**
>
>La lista de transiciones de bloqueo presentada es la recomendada para su uso. En caso necesario, se pueden agregar nuevos códigos de transición para cada bloqueo.

> **Obs. 3:** 
>
>**16***: Cuando la tarjeta esté bloqueada por el código **16** (Bloqueado por error de contraseña), el desbloqueo deberá realizarse mediante el endpoint [POST /cards/{cardId}/changePin]({{ site.url }}{{ site.baseurl }}/es/processor-api).
>
> Es decir, se debe cambiar la contraseña de la tarjeta, lo que desbloqueará automáticamente la tarjeta.
>
> En este caso, aunque el endpoint [POST /cards/{cardId}/unblockCardRequest]({{ site.url }}{{ site.baseurl }}/es/processor-api) devuelva un resultado positivo, la tarjeta seguirá bloqueada hasta que se realice el cambio de contraseña.