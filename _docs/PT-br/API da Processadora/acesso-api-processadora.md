---
title: Acesso à API da Processadora
tags:
  - acesso
  - endpoints
  - credenciais
description: Instruções de acesso à API da Processadora
permalink: /pt-br/docs/acesso
language: pt-br
---

# **Acesso à API da Processadora**

- [Acesso à API da Processadora](#acesso-à-api-da-processadora)
  - [Introdução](#introdução)
  - [Endpoints Base](#endpoints-base)
  - [Requisições](#requisições)
  - [Geração de certificado](#geração-de-certificado)
    - [Chave Privada e Chave Pública](#chave-privada-e-chave-pública)
    - [SSH (Secure Shell)](#ssh-secure-shell)
    - [GPG (GNU Privacy Guard)](#gpg-gnu-privacy-guard)
    - [Certificado de Solicitação de Assinatura (CSR)](#certificado-de-solicitação-de-assinatura-csr)
    - [Como gerar um certificado](#como-gerar-um-certificado)
  - [Código de apoio](#código-de-apoio)

## **Introdução**

O acesso à API é feito por um canal HTTPS com autenticação mútua. O certificado apresentado pela paySmart na API é emitido por uma autoridade certificadora confiável, não necessitando de configuração adicional no lado do solicitante. Já o certificado do cliente consiste em um par composto por:

- Uma chave privada RSA (com mínimo 2048 _bits_) emitida pelo próprio cliente, para a qual deve gerada uma requisição de assinatura do tipo CSR a ser enviada para a paySmart.
- Um certificado público assinado pela paySmart, utilizando o CSR fornecido.

No ambiente de homologação, a paySmart poderá fornecer a chave privada e o certificado público para acesso ao respectivo endpoint.

## **Endpoints Base**

| Ambiente    | Endpoint Base                                               |
| ----------- | ----------------------------------------------------------- |
| Homologação | https://api-hml.paysmart.com.br/paySmart/ps-processadora/v1 |
| Produção    | https://api.paysmart.com.br/paySmart/ps-processadora/v1     |

## **Requisições**

Três informações devem ser enviadas nas requisições HTTP.

- **API-Key**: É uma chave hexadecimal de 20 _bytes_ (40 caracteres) que deve ser enviada no cabeçalho da requisição HTTP, identificando o emissor. É criada pela paySmart e fornecida ao emissor, que deve enviá-la em todas as requisições, a fim de ser identificado de maneira lógica pela API.

- **IdempotencyKey**: É uma chave no formato UUID-v4 cujo o propósito é recuperar a resposta de uma requisição do tipo POST que eventualmente não tenha sido observada no decorrer do fluxo de operações. Isso significa que, se uma ou mais requisições forem disparadas com a mesma "_IdempotencyKey_" de uma chamada anterior (considerando que a API tenha recebido todas elas), todas aquelas que foram efetuadas depois receberão exatamente a mesma resposta da primeira, sem nenhum impacto nos dados já processados. De maneira geral, isso se estende para o seguinte uso: duas requisições diferentes jamais devem ser feitas com a mesma chave de idempotência.

- **User-Agent**: É usado para identificar a origem da requisição. Pode ser enviado qualquer valor nesse campo, mas caso ele não seja enviado, será retornado o erro de acesso **403 (_Forbidden_)**.

Ir para <a href="{{ site.url }}{{ site.baseurl }}/pt-br/processor-api">Especificação da API da Processadora</a>

## **Geração de certificado**

### **Chave Privada e Chave Pública**

Uma chave privada é uma chave de criptografia mantida em sigilo, utilizada para descriptografar mensagens criptografadas com a chave pública correspondente. Por outro lado, uma chave pública é uma chave de criptografia que pode ser compartilhada publicamente. Sua finalidade é criptografar mensagens que só podem ser descriptografadas usando a chave privada correspondente.

### **SSH (Secure Shell)**

O SSH é um protocolo de rede que possibilita conexões seguras e criptografadas entre dois sistemas. Para estabelecer uma conexão segura entre o cliente SSH e o servidor SSH, são utilizadas chaves SSH para autenticação.

Ao utilizar o SSH, é gerado um par de chaves composto por uma chave privada e uma chave pública. A chave pública é adicionada ao servidor SSH, permitindo a autenticação do cliente. Por outro lado, a chave privada é mantida em sigilo no cliente SSH e é usada para autenticar o cliente durante o processo de conexão SSH.

### **GPG (GNU Privacy Guard)**

O GPG é uma implementação do PGP (Pretty Good Privacy), um sistema de criptografia de dados amplamente utilizado. Ele emprega um par de chaves composto por uma chave privada e uma chave pública.

A chave privada no GPG é utilizada para assinar digitalmente mensagens ou arquivos, bem como para descriptografar mensagens recebidas. Já a chave pública é compartilhada com outras pessoas para permitir a verificação da autenticidade das mensagens assinadas por você e a criptografia de mensagens destinadas a você.

Tanto o SSH quanto o GPG são tecnologias amplamente empregadas para autenticação e comunicações seguras. O SSH é frequentemente utilizado para acessar e controlar servidores remotamente, enquanto o GPG é empregado para assinatura digital e criptografia de e-mails ou arquivos.

### **Certificado de Solicitação de Assinatura (CSR)**

Um Certificado de Solicitação de Assinatura (CSR) é um arquivo gerado por um servidor ou um dispositivo que deseja obter um certificado digital de uma autoridade certificadora (CA). O CSR contém informações relevantes que identificam o solicitante do certificado e também inclui uma chave pública associada.

O processo de obtenção de um CSR geralmente envolve a criação de um par de chaves criptográficas: uma chave privada e uma chave pública. A chave privada é mantida em segredo e é usada para assinar digitalmente o CSR. A chave pública é incluída no CSR e será incorporada ao certificado emitido pela CA.

O CSR contém informações como o nome do domínio, a localização geográfica do solicitante, detalhes de contato e outras informações relevantes. Essas informações são verificadas pela CA antes da emissão do certificado. Uma vez que o CSR é gerado, ele é submetido à CA, juntamente com outros dados necessários para a validação. A CA revisa o CSR, autentica o solicitante e realiza uma verificação completa antes de emitir o certificado digital.

O certificado resultante contém a chave pública do solicitante, bem como outras informações, como a assinatura digital da CA, o período de validade do certificado e o propósito para o qual o certificado pode ser usado.

Os Certificados de Solicitação de Assinatura (CSRs) são amplamente utilizados em várias aplicações, como SSL/TLS para garantir conexões seguras em sites, autenticação de e-mails, autenticação de servidores e muitas outras situações em que a segurança e a confiabilidade são fundamentais.

Em suma, um CSR é um componente essencial no processo de obtenção de um certificado digital, fornecendo informações sobre o solicitante e sua chave pública, permitindo a criação de um certificado confiável emitido por uma autoridade certificadora reconhecida.
Aqui está um passo a passo para criar um par de chaves pública e privada:

### **Como gerar um certificado**

1. Abra um terminal ou prompt de comando
2. Execute o seguinte comando para gerar uma chave privada RSA de 2048 bits:
```shell
  openssl genpkey -algorithm RSA -out private.key
```
  * A chave privada será gerada e armazenada no arquivo `private.key`.
3. Execute o seguinte comando para extrair a chave pública a partir da chave privada:
```shell
openssl rsa -pubout -in private.key -out public.key
```
  * A chave pública será gerada e armazenada no arquivo `public.key`. 
  * Agora você tem um par de chaves pública e privada. A chave privada (`private.key`) deve ser mantida em sigilo, pois é usada para descriptografar mensagens criptografadas com a chave pública correspondente. A chave pública (`public.key`) pode ser compartilhada publicamente para criptografar mensagens destinadas a você. 
  * É importante lembrar que a segurança das chaves privadas é fundamental. Certifique-se de armazenar a chave privada em um local seguro e protegido por senha.
4. Execute o seguinte comando para gerar o arquivo .csr a partir da chave privada:
```shell
openssl req -new -key private.key -out request.csr
```
  * Serão solicitadas informações para preencher o CSR, como o país, estado, cidade, organização, etc. Preencha os campos com as informações corretas. Revisar e enviar o CSR:
5. Abra o arquivo request.csr com um editor de texto e verifique se as informações estão corretas.
6. Envie o conteúdo do arquivo .csr para a autoridade certificadora (CA) para processar a solicitação e emitir o certificado digital.
   * É importante lembrar que a criação de um CSR envolve informações precisas e confiáveis. Certifique-se de fornecer dados corretos e atualizados para evitar problemas na emissão do certificado. 
   * Caso você esteja usando um servidor web específico, pode haver instruções adicionais ou um processo específico para gerar um CSR. Verifique a documentação do seu servidor web ou consulte a autoridade certificadora para obter informações mais detalhadas, caso necessário.

### **Código de apoio**

**OBS: Voltado para ambiente Linux**

```shell
#!/bin/bash

# Função para criar chave SSH
create_ssh_key() {
    echo "Criando chave SSH..."
    read -p "Informe o nome do arquivo de chave: " ssh_key_file
    read -p "Informe o e-mail associado à chave SSH: " ssh_email

    ssh-keygen -t rsa -b 4096 -C "$ssh_email" -f "$ssh_key_file"

    echo "Chave SSH criada com sucesso!"
}

# Função para criar chave GPG
create_gpg_key() {
    echo "Criando chave GPG..."
    read -p "Informe o nome completo associado à chave GPG: " gpg_name
    read -p "Informe o e-mail associado à chave GPG: " gpg_email

    gpg --full-generate-key

    echo "Chave GPG criada com sucesso!"
}

# Função para exportar chave SSH
export_ssh_key() {
    echo "Exportando chave SSH..."
    read -p "Informe o nome do arquivo de chave: " ssh_key_file
    read -p "Informe o nome de usuário remoto: " remote_username
    read -p "Informe o endereço do servidor remoto: " remote_server

    ssh-copy-id -i "$ssh_key_file.pub" "$remote_username@$remote_server"

    echo "Chave SSH exportada com sucesso para o servidor remoto!"
}

# Função para exportar chave GPG
export_gpg_key() {
    echo "Exportando chave GPG..."
    read -p "Informe o e-mail associado à chave GPG: " gpg_email
    read -p "Informe o nome do arquivo de exportação: " gpg_export_file

    gpg --export --armor "$gpg_email" > "$gpg_export_file"

    echo "Chave GPG exportada com sucesso para o arquivo $gpg_export_file!"
}

# Menu principal
echo "Bem-vindo ao script de geração e exportação de chaves!"
echo "Selecione uma opção:"
echo "1. Criar chave SSH"
echo "2. Criar chave GPG"
echo "3. Exportar chave SSH"
echo "4. Exportar chave GPG"
echo "0. Sair"

read -p "Opção selecionada: " option

case $option in
    1)
        create_ssh_key
        ;;
    2)
        create_gpg_key
        ;;
    3)
        export_ssh_key
        ;;
    4)
        export_gpg_key
        ;;
    0)
        echo "Saindo do script..."
        exit
        ;;
    *)
        echo "Opção inválida!"
        ;;
esac
```
