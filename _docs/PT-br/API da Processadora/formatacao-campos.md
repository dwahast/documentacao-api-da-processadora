---
title: Formatação de Campos da API da Processadora
tags: 
 - campos
description: Formatação dos campos utilizados pela API da Porcessadora paySmart
permalink: /pt-br/docs/formatacao-campos
language: pt-br
---

# **Formatação de Campos**

| Campo | Tamanho |
| ------ | ------ |
| *``"issuerRequestId"``* | 50 |
| *``"issuerCardId"``* | 50 |
| *``"extraData"``* | 30 |
| *``"motherName"``* | 40 |
| *``"cardholder"``*.*``"fullName"``* | 50 |
| *``"cardholder"``*.*``"identityDocumentNumber"``* | 14 |
| *``"cardholder"``*.*``"nationality"``* | 24 |
| *``"cardholder"``*.*``"otherIdentityDocumentNumber"``*.*``"identityDocumentNumber"``* | 10 |
| *``"cardholder"``*.*``"otherIdentityDocumentNumber"``*.*``"state"``* | 2 |
| *``"cardholder"``*.*``"otherIdentityDocumentNumber"``*.*``"issuedBy"``* | 3 |
| *``"cardholder"``*.*``"cardData"``*.*``"embossingName"``* | 24 |
| *``"cardholder"``*.*``"contactInformation"``*.*``"personalPhoneNumber1"``* | 14 |
| *``"cardholder"``*.*``"contactInformation"``*.*``"personalPhoneNumber2"``* | 14 |
| *``"cardholder"``*.*``"contactInformation"``*.*``"comercialPhoneNumber1"``* | 14 |
| *``"cardholder"``*.*``"contactInformation"``*.*``"email"``* | 60 |
| *``"cardDeliveryAddress"``*.*``"recipient"``* | 60 |
| *``"cardDeliveryAddress"``*.*``"addressLine1"``* | 60 |
| *``"cardDeliveryAddress"``*.*``"addressLine2"``* | 5 |
| *``"cardDeliveryAddress"``*.*``"addressLine3"``* | 30 |
| *``"cardDeliveryAddress"``*.*``"reference"``* | 60 |
| *``"cardDeliveryAddress"``*.*``"neighborhood"``* | 30 |
| *``"cardDeliveryAddress"``*.*``"city"``* | 30 |
| *``"cardDeliveryAddress"``*.*``"state"``* | 2 |
| *``"cardDeliveryAddress"``*.*``"zipcode"``* | 9 |
| *``"cardDeliveryAddress"``*.*``"country"``* | 24 |
| *``"sourceAudit"``*.*``"processId"``* | 20 |

