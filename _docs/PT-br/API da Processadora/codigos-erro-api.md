---
title: Códigos de Erro da API da Processadora
tags: 
 - codigo
 - retorno
 - erro
description: Códigos de erros retornados pela API da Processadora. 
permalink: /pt-br/docs/codigos-erro-api
language: pt-br
---

# **Códigos de erro da API da Processadora**

Mapeamento de erros retornados pela **API da Processadora** e seus respectivos códigos.


| Código | Mensagem                                                                                                                   |
|--------|----------------------------------------------------------------------------------------------------------------------------|
| 0      | "Comando concluído com sucesso"                                                                                            |
| 6      | "Erro de comunicação"                                                                                                      |
| 14     | "Cartão inválido"                                                                                                          |
| 15     | "Cartão requisitado, mas emissão rejeitada"                                                                                |
| 16     | "Cartão em processo de emissão / sem dados para cifragem"                                                                  |
| 17     | "Código de bloqueio inválido"                                                                                              |
| 18     | "Código de bloqueio não corresponde a uma operação de desbloqueio"                                                         |
| 19     | "Código de bloqueio não corresponde a uma operação de bloqueio"                                                            |
| 20     | "Desbloqueio não permitido para este cartão"                                                                               |
| 21     | "Cartão já desbloqueado"                                                                                                   |
| 22     | "Cartão já bloqueado por um motivo mais crítico ao informado"                                                              |
| 23     | "Status atual do cartão não permite reemissão"                                                                             |
| 24     | "Já existe uma outra solicitação de reemissão pendente para este cartão"                                                   |
| 62     | "Dados inválidos de senha para validação"                                                                                  |
| 70     | "Senha está bloqueada"                                                                                                     |
| 71     | "Senha inválida"                                                                                                           |
| 75     | "Senha ou CVV inválido"                                                                                                    |
| 91     | "Sistema indisponível"                                                                                                     |
| 104    | "Dados inválidos"                                                                                                          |
| 109    | "Conta / cartão bloqueado"                                                                                                 |
| 110    | "Conta inválida"                                                                                                           |
| 115    | "Faltam dados mandatórios: [lista de dados faltantes]"                                                                     |
| 120    | "Formato inválido para o campo"                                                                                            |
| 121    | "Cartão já associado a um portador"                                                                                        |
| 122    | "Conta vinculada a um cartão anônimo não pode receber novos cartões"                                                       |
| 123    | "Conta cancelada"                                                                                                          |
| 124    | "Validações falharam: [lista de dados inválidos] "                                                                         |
| 125    | "Transação não encontrada"                                                                                                 |
| 127    | "Resposta inválida do serviço de tokenização"                                                                              |
| 128    | "Falha na obtenção de um PAN virtual disponível"                                                                           |
| 129    | "Não foi possível fazer a segurança criptográfica do PAN"                                                                  |
| 130    | "Geração de cartões virtuais não permitida para cartões anônimos"                                                          |
| 131    | "Cartões só podem ser anônimos para impressão. \cardholder\" deve ser não nulo ou \"inhibitPhysicalCard\" deve ser falso"" |
| 132    | "Não foi possível obter o token de autenticação para validação de QRCode"                                                  |
| 133    | "Erro na API de obtenção da chave da bandeira"                                                                             |
| 134    | "Resposta inválida da bandeira na obtenção do resultado da transação com QR Code"                                          |
| 135    | "Formato inválido para a entrada de PIN. ISO-1 mandatório"                                                                 |
| 136    | "Não foi possível obter os dados do cartão virtual"                                                                        |
| 137    | "Não foi possível cifrar os dados para transação de QRCode"                                                                |
| 147    | "Transação de QRCode duplicada"                                                                |
| 804    | "Produto não cadastrado para este emissor"                                                                                 |
| 808    | "Outra requisição idempotente já está rodando"                                                                             |
| 809    | "Já existe uma conta / solicitação de conta para este documento / produto / solicitante"                                   |
| 810    | "Não foi possível encontrar a estrutura JSON esperada no corpo da requisição"                                              |
| 811    | "API-Key não enviada ou inválida"                                                                                          |
| 812    | "Erro ao obter transações da API interna de transações"                                                                    |
| 813    | "Campo IdempotencyKey ausente no cabeçalho"                                                                                |
| 977    | "Não foi possível calcular um hash de comparação para o PAN"                                                               |
| 979    | "Data de vencimento da fatura deve ser maior que 0 e menor que 29."                                                        |
| 980    | "Erro na criação da conta no processo de saldos e limites."                                                                |
| 981    | "Não foi possível encontrar configurações do serviço de QR Code Elo"                                                       |
| 982    | "Nenhuma disputa encontrada com os parâmetros de busca utilizados."                                                        |
| 983    | "Disputa não está na etapa de reapresentação"                                                                              |
| 984    | "Disputa não encontrada"                                                                                                   |
| 985    | "Transação inclusiva inicial (TE-05 ou TE-06) não existe para essa disputa."                                               |
| 986    | "Transação inclusiva já existe para essa transação."                                                                       |
| 987    | "Valor parcial não pode ser maior ou igual ao valor total da transação."                                                   |
| 988    | "Configurações para a razão da disputa não existente."                                                                     |
| 989    | "Não foi possível encontrar alguns parâmetros da transação."                                                               |
| 990    | "Transação a ser disputada não foi confirmada na liquidação, tente novamente nos próximos dias.                            |"
| 991    | "Transação a ser disputada está cancelada"                                                                                 |
| 992    | "Disputa já existe para essa transação"                                                                                    |
| 993    | "Não foi possível encontrar configurações da API de Transações para o emissor representado pela API-Key informada"         |
| 994    | "Não foi possível encontrar configurações do Colchão para o emissor representado pela API-Key informada"                   |
| 995    | "Não foi possível encontrar as configurações do serviço de tokenização"                                                    |
| 996    | "Não foi possível encontrar configurações para o emissor representado pela API-Key informada"                              |
| 997    | "Não foi possível encontrar as configurações para validar o PIN / CVV para esse cartão"                                    |
| 998    | "Erro interno de gravação"                                                                                                 |
| 999    | "Erro interno paySmart"                                                                                                    |
