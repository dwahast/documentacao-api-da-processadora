---
title: API de Contas e Cartões
description: Especificação da API de Contas e Cartões
permalink: /pt-br/accounts/spec

layout: swagger_layout
spec_path: accounts-and-cards-spec.json
---

# Documentação API processadora
