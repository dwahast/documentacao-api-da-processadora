---
title: Criação de Contas
tags: 
 - contas
description: Operação em alto nível para a criação de contas na processadora paySmart
permalink: /pt-br/accounts/
---

# **Criação de Contas**

1. [Introdução](#introdução)
1. [Criação de Conta](#criação-de-conta)
    1. [Requisição](#requisição)
    1. [Resposta](#resposta)

## **Introdução**


Dentro do contexto da processadora paySmart, uma **conta** é um elemento lógico cujo componente principal é um titular (a pessoa física ou jurídica) que detém um saldo e que é responsável pela liquidez da mesma. "Deter o saldo" não significa necessariamente que a paySmart controle ou mesmo precise saber quanto o titular tem à disposição para fazer transações. No entanto, a prática recomendada é que se o emissor ou sub-emissor faz esse controle, associe à conta paySmart o mesmo documento (CPF ou CNPJ) ao qual o saldo está vinculado.

De maneira geral, a principal função das contas é agregar cartões associados ao mesmo titular. Considerando um produto pós-pago, a conta é ainda a entidade que estará associada a uma fatura. 

## **Criação de Conta**

A criação de uma conta envolve três elementos obrigatórios (Titular, Produto e Endereço de cobrança):

| Campo | Descrição |
|----------|-----------|
| *``"accountOwner"``* | Titular: descreve as informações necessárias para identificar de maneira inequívoca a pessoa física ou jurídica responsável pela conta que está sendo criada |
| *``"accountOwner"."fullName"``* | Nome completo: nome civil do titular |
| *``"accountOwner"."identityDocumentNumber"``* | Documento de identidade: número do documento que identifica o titular em órgãos oficiais, normalmente um CPF ou um CNPJ |
| *``"accountOwner"."contactInformation"``* | Informações de contato: informações para possibilitar a comunicação com o titular em caso de eventual necessidade |
| *``"accountOwner"."contactInformation"."personalPhoneNumber1"``* | Número de contato telefônico |
| *``"accountOwner"."contactInformation"."email"``* | Email |
| *``"accountOwner"."contactInformation"."birthDate"``* | Data de nascimento: data de nascimento do titular da conta <br>**Obs**.: obrigatória somente para produtos pós-pago ou produtos pré-pago que utilizem o serviço de KYC da paySmart |
| *``"psProductCode"``* | Um produto: define as características dos cartões que serão gerados associados àquela conta (por exemplo, se ele é de débito ou crédito, doméstico ou internacional, pessoa física ou jurídica). Cada produto recebe um identificador definido pela paySmart, usualmente um código numérico de seis algarismos. Esse código é informado ao emissor / sub-emissor, que deverá passá-lo como parâmetro a cada nova conta criada |
| *``"billingAddress"``* | Endereço de cobrança: endereço fixo do titular, onde o mesmo deve ser localizado em necessidade de cobrança |
| *``"billingAddress"."addressLine1"``* | Uma linha de endereço: contém a identificação do logradouro principal do endereço (nome da rua, podendo ainda abranger número e complemento) |
| *``"billingAddress"."addressLine2"``* | Um número: campo separado para o número |
| *``"billingAddress"."city"``* | Cidade: município onde está estabelecido o logradouro |
| *``"billingAddress"."state"``* | Estado: unidade da federação, em território brasileiro, onde se localiza a cidade do titular |
| *``"billingAddress"."neighborhood"``* | Bairro: bairro de residência do titular |
| *``"billingAddress"."zipcode"``* | CEP: CEP para localização de serviços de entrega |
| *``"cardDeliveryAddress"``* | Endereço de entrega: endereço que será herdado para todas as entregas de cartão vinculados a conta, caso não seja definido explicitamente na requisição de cartão. Possui os mesmos campos obrigatórios que o endereço de cobrança |

### **Requisição**  

Os dados listados acima devem ser informados em um corpo no formato JSON por uma requisição HTTP no endpoint [POST /accounts]({{ site.url }}{{ site.baseurl }}/spec-proc/). Dessa maneira, uma requisição mínima para a criação de uma conta ficaria no seguinte formato:

```json
{
  "psProductCode": "020101",
  "accountOwner": {
    "fullName": "Fulano da Silva",
    "identityDocumentNumber": "03873703805",
    "contactInformation": { "personalPhoneNumber1": "+5551",  "email": "a@b.c" }
  },
  "billingAddress": {
    "addressLine1": "R. Manoelito de Ornellas",
    "addressLine2": "55",
    "city": "Porto Alegre",
    "state": "RS",
    "neighborhood": "Praia de Belas",
    "zipcode": "990000"
  },
  "cardDeliveryAddress": {
    "addressLine1": "R. Manoelito de Ornellas",
    "addressLine2": "55",
    "city": "Porto Alegre",
    "state": "RS",
    "neighborhood": "Praia de Belas",
    "zipcode": "990000"
  }
}
```

### **Resposta**

A requisição de uma nova conta retorna ao solicitante um corpo JSON informando o resultado da mesma. Sempre que uma conta for criada com sucesso (campo *``"resultCode"``* com o valor **"0"**), a estrutura JSON de resposta irá retornar um campo chamado *``"accountId"``*, um identificador prefixado pela string ``"cta-"`` seguida de uma outra no formato UUID-v4. Um exemplo de resposta é fornecido abaixo:

```json
{
  "resultData": {
    "resultCode": 0,
    "resultDescription": "Registro de nova conta incluído com sucesso!",
    "psResponseId": "dfd88b95-d28d-49ac-90ba-7ba0b35aebd0"
  },
  "account": {
    "accountId": "cta-5566fefb-af25-9984-9fb0-39f6d2021111",
    "psProductCode": "020101",
    "psProductName": "BANCO 1 BANDEIRA 1 PRE-PAGO"
  }
}
```

>Uma conta é sempre criada com o estado **"Ativa"**.

Não é sempre necessário criar contas para que se possa emitir cartões. No modelo de cartões anônimos, a requisição de cartões pode ser realizada diretamente, e como resultado desse processo será gerado um cartão físico com todos os dados necessários para realizar transações, mas que não pode fazê-lo até receber um portador. Esse processo é descrito em [Criação de Cartões Físicos]({{ site.url }}{{ site.baseurl }}/pt-br/cartoes/fisicos/criacao).


