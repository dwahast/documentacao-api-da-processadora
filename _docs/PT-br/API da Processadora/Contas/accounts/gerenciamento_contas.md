---
title: Gerenciamento de contas
title: Gerenciamento de Contas
description: Dados sobre o gerenciamento de contas
tags:
- contas
permalink: /pt-br/accounts/management
---

# **Gerenciamento de contas**

1. [Introdução](#introdução)
1. [Bloqueio e Desbloqueio de Contas](#bloqueio-e-desbloqueio-de-contas)
    1. [Códigos de Bloqueio](#códigos-de-bloqueio)
    1. [Matriz de Bloqueio](#matriz-de-bloqueio)
1. [Cancelamento de Contas](#cancelamento-de-contas)
    1. [Códigos de Cancelamento](#códigos-de-cancelamento)
1. [Matriz de bloqueio](#matriz-de-bloqueio)

## **Introdução**

Assim como no gerenciamento de cartões, o ciclo de gerenciamento de contas também inclui a possibilidade de bloqueio e desbloqueio. No entanto, o bloqueio de uma conta tem impacto tanto sobre o comportamento das ações aplicadas à conta, como também algumas ações aplicadas a todos os cartões a ela associados. Esses comportamentos são definidos de acordo com parâmetros de configuração associados aos códigos de bloqueio.

A tabela abaixo mostra os parâmetros que acompanham cada um dos códigos e suas respectivas descrições:

<div class="datatable-begin"></div>

Configuração         | Descrição                                                               |  Opções possíveis
-------------------- | ----------------------------------------------------------------------- | ----------------------------------------------
Autorizar transações | Define se a conta permite autorização de transações                     | Sim ou Não
Processar transações | Define se a conta permite processamento de transações                   | Lançar normalmente, Não Lançar débitos, Não lançar
Calcular encargos    | Define se a conta permite o cálculo de encargos, como juros por exemplo | Sim ou Não
Cobrar multas        | Define se a conta permite a cobrança de multas                          | Sim ou Não
Cobrar tarifas       | Define se a conta permite a cobrança de tarifas                         | Sim ou Não

<div class="datatable-end"></div>

> **Atenção** 
>
>A configuração padrão desses parâmetros está disponível na seção [Matriz de bloqueio](#matriz-de-bloqueio).

<br>

## **Bloqueio e Desbloqueio de Contas**

Para definir uma conta como bloqueada, o endpoint [POST /accounts/{accountId}/blockAccount]({{ site.url }}{{ site.baseurl }}/pt-br/processor-api) pode ser chamado enviando o código de bloqueio (*``"blockCode"``*) no corpo JSON da requisição, como no exemplo abaixo. O parâmetro *``"accountId"``* refere-se ao identificador único da conta, retornado no momento da requisição de criação.

```json
{
  "blockCode": 10,
}
```

Para o desbloqueio da conta, o endpoint a ser usado é o [POST /accounts/{accountId}/unblockAccount]({{ site.url }}{{ site.baseurl }}/pt-br/processor-api), onde o único dado mandatório a ser enviado é o código de desbloqueio (*``"unblockCode"``*), como mostrado no exemplo abaixo. Como no caso anterior, o parâmetro *``"accountId"``* refere-se ao identificador único da conta.

```json
{
  "unblockCode": 0,
}
```

### **Códigos de Bloqueio**

A tabela abaixo representa a lista de possíveis códigos de bloqueio (além do código de desbloqueio **00**) permitidos, juntamente com suas respectivas interpretações.


<div class="datatable-begin"></div>

Código  | Status     |  Descrição                            
------- | ---------- |  ------------------------------------ 
00      | Ativa      |  Conta sem bloqueio                   
10      | Bloqueada  |  Atraso - até 10 dias                 
11      | Bloqueada  |  Atraso - 11 a 20 dias                
12      | Bloqueada  |  Atraso - 21 a 30 dias                           
16      | Bloqueada  |  Conta em atraso - inadimplência      
18      | Bloqueada  |  Bloqueio de segurança                
20      | Bloqueada  |  Excesso Limite - até 5%              
21      | Bloqueada  |  Excesso Limite - 5,01% A 20%         
22      | Bloqueada  |  Excesso Limite - 20,01% A 50%        
23      | Bloqueada  |  Excesso Limite - Acima de 50,01%     
30      | Bloqueada  |  Bloqueio Preventivo de Fraudes       

<div class="datatable-end"></div>

<br>

### **Matriz de bloqueio**

As regras de transições entre os códigos de bloqueio são regidas pela lista de transições (representada na tabela pela coluna "**Pode evoluir para**"). 

Segundo a matriz de bloqueio padrão abaixo, uma conta em situação **Ativa** com código **00** pode evoluir para qualquer um dos seguintes códigos: **10**, **18**, **20**, **21**, **22**, **23**, **30**, **70**, **80**, **81**, **90** ou **94**. 

Os parâmetros de configuração **Autorizar transações**, **Processar transações**, **Calcular encargos**, **Cobrança de multas** e **Cobrança de tarifas** definem o comportamento da conta para cada uma destas ações. Uma conta em situação **Bloqueada** com código de bloqueio **11**, por exemplo, não autorizará novas transações.

<div class="datatable-begin"></div>

Código  | Status     | Descrição                            | Pode evoluir para                                             | Autorizar transações | Processar transações | Calcular encargos | Cobrança de multas | Cobrança de tarifas 
------- | ---------- | ------------------------------------ | ------------------------------------------------------------- | -------------------- | -------------------- | ----------------- | ------------------ | ------------------- 
00      | Ativa      | Conta sem bloqueio                   | 10, 16, 18, 20, 21, 22, 23, 24, 30, 70, 80, 81, 90, 94        | Sim                  | Não Lançar           | Sim               | Sim                | Sim                 
00      | Conta Nova | Conta sem bloqueio                   | 10, 16, 18, 20, 21, 22, 23, 24, 30, 70, 80, 81, 90, 94        | Sim                  | Lançar normalmente   | Sim               | Sim                | Sim                 
00      | Não Ativada| Conta sem bloqueio                   | 10, 16, 18, 20, 21, 22, 23, 24, 30, 70, 80, 81, 90, 94        | Sim                  | Não Lançar Débitos   | Sim               | Não                | Sim                 
01      | Conta Nova | Pendente de retorno processo KYC     | 00, 82                                                        | Não                  | Não Lançar           | Sim               | Não                | Sim                 
10      | Ativa      | Atraso - até 10 dias                 | 00, 11, 16, 80, 81, 82, 90, 94                                | Sim                  | Lançar normalmente   | Sim               | Sim                | Sim                 
11      | Bloqueada  | Atraso - 11 a 20 dias                | 00, 12, 16, 80, 81, 82, 90, 94                                | Não                  | Lançar normalmente   | Sim               | Sim                | Sim                 
12      | Bloqueada  | Atraso - 21 a 30 dias                | 00, 13, 16, 80, 81, 82, 90, 94                                | Não                  | Lançar normalmente   | Não               | Sim                | Sim                 
13      | Bloqueada  | Atraso - 31 a 45 dias                | 00, 14, 16, 80, 81, 82, 90, 94                                | Não                  | Lançar normalmente   | Sim               | Sim                | Sim                 
14      | Bloqueada  | Atraso - 46 a 60 dias                | 00, 15, 16, 80, 81, 82, 90, 94                                | Não                  | Lançar normalmente   | Sim               | Sim                | Sim                 
15      | Bloqueada  | Atraso - Acima de 60 dias            | 00, 16, 81, 82, 90, 91, 94                                    | Não                  | Lançar normalmente   | Não               | Sim                | Não                 
16      | Bloqueada  | Conta em atraso - inadimplência      | 00, 80, 91                                                    | Não                  | Lançar normalmente   | Sim               | Sim                | Sim                 
18      | Bloqueada  | Bloqueio de segurança                | 00, 30, 80, 81, 82, 90, 94                                    | Não                  | Lançar normalmente   | Não               | Sim                | Não                 
20      | Bloqueada  | Excesso Limite - até 5%              | 00, 10, 11, 12, 16, 21, 22, 23, 24, 80, 81, 82, 90, 91, 94 | Não                  | Lançar normalmente   | Sim               | Sim                | Não                 
21      | Bloqueada  | Excesso Limite - 5,01% A 20%         | 00, 10, 11, 12, 16, 20, 22, 23, 24, 80, 81, 82, 90, 91, 94 | Não                  | Lançar normalmente   | Sim               | Sim                | Não                 
22      | Bloqueada  | Excesso Limite - 20,01% A 50%        | 00, 10, 11, 12, 16, 20, 21, 23, 24, 80, 81, 82, 90, 91, 94 | Não                  | Lançar normalmente   | Sim               | Não                | Não                 
23      | Bloqueada  | Excesso Limite - Acima de 50,01%     | 00, 10, 11, 12, 16, 20, 21, 22, 24, 80, 81, 82, 90, 91, 94 | Não                  | Lançar normalmente   | Sim               | Não                | Sim                 
24      | Bloqueada  | Conta com excesso de limite          | 00, 10, 11, 12, 16, 80, 81, 82, 90, 91, 94                 | Não                  | Lançar normalmente   | Sim               | Sim                | Sim                 
30      | Bloqueada  | Bloqueio Preventivo de Fraudes       | 00, 10, 11, 12, 16, 20, 21, 22, 23, 31, 80, 81, 90, 91, 94 | Não                  | Lançar normalmente   | Sim               | Não                | Sim                 
31      | Cancelada  | Bloqueio por motivo de fraude (comprovada) | 80, 81, 90, 94                                       | Não                  | Lançar normalmente   | Sim               | Não                | Sim                 
70      | Inativada  | Bloqueio por inatividade             | 00, 71, 80, 81, 90, 94, 95                                    | Não                  | Lançar normalmente   | Sim               | Não                | Sim                 
71      | Encerrada  | Encerrada por inatividade            | -                                                             | Não                  | Não Lançar           | Não               | Não                | Não                 
80      | Cancelada  | Cancelamento pelo emissor            | -                                                             | Não                  | Lançar normalmente   | Sim               | Não                | Sim                 
81      | Cancelada  | Cancelamento pelo usuário            | -                                                             | Não                  | Lançar normalmente   | Sim               | Sim                | Sim                 
82      | Cancelada  | Cancelamento devido restrição no KYC | -                                                             | Não                  | Lançar normalmente   | Sim               | Não                | Sim                 
90      | Encerrada  | Encerramento pelo emissor            | -                                                             | Não                  | Lançar normalmente   | Sim               | Sim                | Não                 
91      | Enquadrada | Cancelamento por cobrança            | -                                                             | Não                  | Lançar normalmente   | Sim               | Sim                | Não                 
94      | Encerrada  | Titular Falecido                     | -                                                             | Não                  | Lançar normalmente   | Sim               | Sim                | Não                 
95      | Expurgada  | Expurgo por inatividade              | -                                                             | Não                  | Lançar normalmente   | Sim               | Sim                | Não                 

<div class="datatable-end"></div>

#### **Observações**

> **Obs. 1:**
>
>Os códigos que apresentam '-' são códigos de estágio final.

> **Obs. 2:**
>
> A matriz de bloqueio apresentada é a recomendada para uso. Caso necessário, poderão ser ser feitas alterações nos parâmetros de configuração ou adicionados novos códigos de transições para cada bloqueio.

### **Cancelamento de contas**

O cancelamento de uma conta é uma operação de efeito amplo que sinaliza o fim do vínculo de um titular com a processadora paySmart. Todos os cartões de uma conta cancelada também passam para esse status, não podendo mais ser desbloqueados. 
O cancelamento de uma conta pode ser feito pelo endpoint [POST /accounts/{accountId}/cancelAccount]({{ site.url }}{{ site.baseurl }}/pt-br/processor-api), onde *``"accountId"``* é o identificador único da conta retornado no momento de sua solicitação. O único dado mandatório a ser enviado no corpo JSON da requisição é o código de cancelamento (*``"cancellationCode"``*). No entanto, também é recomendado enviar o campo contendo a descrição do motivo de cancelamento (*``"reason"``*), para facilitar o rastreamento dos eventos. Com isso, uma mensagem mínima de cancelamento ficaria:

```json
{
  "cancellationCode": 80,
  "reason": "Usuário encerrou a conta."
}
```

### **Códigos de Cancelamento**

A interpretação dos código de cancelamento de conta é mais simples que os códigos de bloqueio de contas.

Abaixo, a lista de códigos de cancelamento de contas e seu comportamento esperado:

<div class="datatable-begin"></div>

Código  | Status     |  Descrição                            
------- | ---------- |  ------------------------------------ 
80      | Cancelada  |  Cancelamento pelo emissor            
81      | Cancelada  |  Cancelamento pelo usuário            

<div class="datatable-end"></div>

&nbsp;

