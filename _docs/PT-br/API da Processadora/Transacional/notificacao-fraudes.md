---
title: Processo de Notificação de Fraude
name: fraudNotification
tags: 
 - fraudes
description: Operação para notificar a identificação de fraudes em transações que não vão ser disputadas. 
permalink: /pt-br/docs/notificacao-fraudes
language: pt-br
---

# **{{page.title}}**

1. [Introdução](#introdução)
1. [Criação de Notificação](#criação-de-notificação)
    1. [Requisição](#requisição)
1. [Tipos de Fraude](#tipos-de-fraude)
1. [Observações](#observações)

## **Introdução**

O processo de notificação de fraude deve ser usado quando uma transação é identificada como fraudulenta, mas não vai (ou não pode) ser aberta uma disputa para aquela transação, como transações por *QR Code*. Esse processo permite avisar a bandeira sobre fraudes.

## **Criação de Notificação**

A criação de notificações de fraude pode ser realizada através do endpoint [POST /frauds/notification]({{ site.url }}{{ site.baseurl }}/pt-br/processor-api). Esse endpoint recebe sempre a identificação da transação (Através de um *``"accountId"``* e um *``"transactionId"``*) e o tipo de fraude.

### **Requisição**

Os campos mandatórios para a criação de uma notificação de fraude são:

|Nome|Tipo|Descrição|
|-|-|-|
|*``"accountId"``*|string|Identificador único da conta, atribuído pela paySmart|
|*``"transactionId"``*|string|Identificador da transação fraudada|
|*``"fraudType"``*|string|Tipo da fraude|


Um exemplo de requisição com todos os campos preenchidos pode ser observado abaixo:

```json
{
  "issuerFraudId": "a3e4cf3c-2a98-4a88-a370-5429356d7112",
  "accountId": "67dac5d8-c1f9-44d6-876a-761ac84db261",
  "transactionId": "c02ef1ca-70fc-4582-9b20-0cfd04d9374b",
  "fraudType": "05",
  "cardholderData": {
    "name": "Fulano da Silva",
    "zipcode": "91037-198",
    "city": "Porto Alegre",
    "state": "RS"
  },
  "sourceAudit": {
    "operatorId": "12340985312",
    "processId": "PID-12345"
  }
}
```

## **Tipos de Fraude**

Sempre que uma fraude é notificada é preciso indicar o tipo de fraude, sendo os seguintes tipos atualmente suportados:

|Valor|Descrição|
|-|-|
|``"00"``|Cartão perdido pelo portador|
|``"01"``|Cartão roubado do portador|
|``"02"``|Cartão extraviado (portador não recebeu cartão do emissor, porém emissor alega que enviou o cartão)|
|``"03"``|Falsidade Ideológica|
|``"04"``|Cartão clonado/falsificado|
|``"05"``|Transação autorizada sem o envolvimento do portador|
|``"06"``|Número do cartão foi usado em transação de marketing direto ou no comércio eletrônico|
|``"07"``|Fraude Familiar (familiar teve acesso ao cartão e fez transação sem autorização do portador do cartão)|
|``"08"``|Engenharia Social (cliente caiu em golpe de fraudador, entregando senha e/ou cartão e/ou não validando valor da compra no Ponto de Venda)|
|``"09"``|Auto Fraude (cliente contesta transação, mas há evidências de que foi o cliente que realizou a transação contestada)|
|``"10"``|Fraude Interna (fraude resultante da participação de funcionários do emissor na emissão e/ou utilização do cartão ou vazamento de dados do cliente)|

## **Observações**

>**Obs. 1:**
>
>Caso uma disputa seja aberta para uma transação por motivo de fraude, não é preciso separadamente enviar uma notificação de fraude. O próprio processo de disputa gera essa notificação.  
