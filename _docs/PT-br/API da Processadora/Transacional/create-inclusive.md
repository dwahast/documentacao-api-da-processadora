---
title: Criação de Inclusivas
name: ""
link: "create"
method: POST
type: inclusives
tags:
    - endpoint
    - inclusivas
    - criação inclusiva
description: "Endpoint para criação de inclusivas (TE10 e TE20)."
permalink: /pt-br/docs/inclusives/create
language: pt-br
---

{% include endpoint_header.html %}

{% include endpoint_auth.html %}

### **Argumentos**

<table>
    <thead>
        <tr>
            <th>Nome</th>
            <th>Obrigatório</th>
            <th>Tipo</th>
            <th>Descrição</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>accountId</td>
            <td>Sim</td>
            <td>String</td>
            <td>Identificador único da conta, atribuído pela paySmart.</td>
        </tr>
        <tr>
            <td>transactionId</td>
            <td>Sim</td>
            <td>String</td>
            <td>Identificador único da transação em disputa.</td>
        </tr>
        <tr>
            <td>code</td>
            <td>Sim</td>
            <td>String</td>
            <td>Código da tarnsação. Deve assumir o valor TE10 ou TE20.</td>
        </tr>
        <tr>
            <td>reasonCode</td>
            <td>Sim</td>
            <td>String</td>
            <td>Código do motivo da transação. Olhar <a href="./codigos-transacao">Códigos de Motivo de Transferência</a></td>
        </tr>
        <tr>
            <td>text</td>
            <td>Sim</td>
            <td>String</td>
            <td> Mensagem de texto justificando a transferência.</td>
        </tr>
        <tr>
            <td>partial</td>
            <td>Sim</td>
            <td>Boolean</td>
            <td>Indica se está sendo disputada uma parte ou o valor total da transação. <b>Se for verdadeiro, é necessário informar amount.</b></td>
        </tr>
        <tr>
            <td>amount</td>
            <td>Condicional</td>
            <td>Objeto</td>
            <td>Valor sendo transferido. <b>Só deve estar presente se a partial for true e deve conter o campo amount e, opcionalmente, o currencyCode</b>.</td>
        </tr>
        <tr>
            <td>- amount</td>
            <td>Condicional</td>
            <td>Inteiro</td>
            <td>Valor omitindo a vírgula.<b>Obrigatório estar presente caso partial seja true.</b></td>
        </tr>
        <tr>
            <td>- currencyCode</td>
            <td>Não</td>
            <td>Inteiro</td>
            <td>Código da moeda, conforme ISO-4217.</td>
        </tr>
    </tbody>
</table>

### **Exemplo de Requisição**

```json
{
    "accountId": "67dac5d8-c1f9-44d6-876a-761ac84db261",
    "transactionId": "c02ef1ca-70fc-4582-9b20-0cfd04d9374b",
    "code": "TE10",
    "reasonCode": "04",
    "text": "Acordo amigável após discussões com o credenciador acerca da disputa da transação.",
    "partial": true,
    "amount": {
    "amount": 123,
        "currencyCode": 986
    }
}
```

### **Exemplo de Resposta**

```json
{
    "resultData": {
        "resultCode": 0,
        "resultDescription": "Transação inclusiva criada com sucesso",
        "issuerRequestId": "",
        "psResponseId": "PS-1711c68a-7ca9-4e73-aecf-05bf8e956f55"
    },
    "inclusiveTransaction": {
        "inclusiveTransactionId": "TI-ca50514f-aaae-432c-815d-00f6febc3883",
        "accountId": "cta-f2da36fa-aef3-4a4e-aa4f-7819b2d52db0",
        "transactionId": "aut-6981fa93-253a-4137-ac22-038f5f2d7846",
        "code": "TE10",
        "reasonCode": "04",
        "text": "Acordo amigável após discussões com o credenciador acerca da disputa da transação.",
        "createdAt": "2021-12-08"
    }
}
```

### **Descrição da Resposta**

<table>
    <thead>
        <tr>
            <th>Nome</th>
            <th>Tipo</th>
            <th>Descrição</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>inclusiveTransaction</td>
            <td>Objeto</td>
            <td>Objeto contendo os dados referentes à transação inclusiva criada. <b>Presente somente em caso de sucesso.</b></td>
        </tr>
        <tr>
            <td>resultData</td>
            <td>Objeto</td>
            <td>Dados da resposta da API.</td>
        </tr>
        <tr>
            <td>- resultCode</td>
            <td>Integer</td>
            <td>Código de resultado do processamento.</td>
        </tr>
        <tr>
            <td>- resultDescription</td>
            <td>String</td>
            <td>Descrição textual do resultado do processamento.</td>
        </tr>
        <tr>
            <td>- psResponseID</td>
            <td>String</td>
            <td>Identificador único da resposta. Gerado pela paySmart.</td>
        </tr>
        <tr>
            <td>- issuerRequestId</td>
            <td>String</td>
            <td>Identificador único da requisição gerado pelo emissor. Ecoado conforme enviado na requisição no campo issuerDisputeId.</td>
        </tr>
    </tbody>
</table>

{% include endpoint_footer.html %}