---
title: Cancelamento de Transação Inclusiva
name: "{inclusiveTransactionId}/undo"
link: "undo"
method: POST
type: inclusives
tags:
    - endpoint
    - inclusivas
    - cancelamento inclusiva
description: "Endpoint para desfazimento de uma transação inclusiva e envio de sua reversão."
permalink: /pt-br/docs/inclusives/undo
language: pt-br
---
{% include endpoint_header.html %}

{% include endpoint_auth.html %}

### **Exemplo de Requisição**

```
curl -X POST https://api.paysmart.com.br/paySmart/ps-processadora/v1/inclusives/{inclusiveTransactionId}/undo
```

### **Exemplo de Resposta**

```json
{
    "resultData": {
        "psResponseId": "eb9b9141-d8c2-4a41-81a2-d58567aaadc1",
        "resultCode": 0,
        "resultDescription": "Transação inclusiva desfeita com sucesso!",
        "issuerRequestId": "TI-ad56619b-c72f-4c44-a821-983ca2ebdd67"
    }
}
```

### **Descrição da Resposta**

<table>
    <thead>
        <tr>
            <th>Nome</th>
            <th>Tipo</th>
            <th>Descrição</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>resultData</td>
            <td>Objeto</td>
            <td>Dados da resposta da API.</td>
        </tr>
        <tr>
            <td>- resultCode</td>
            <td>Integer</td>
            <td>Código de resultado do processamento.</td>
        </tr>
        <tr>
            <td>- resultDescription</td>
            <td>String</td>
            <td>Descrição textual do resultado do processamento.</td>
        </tr>
        <tr>
            <td>- psResponseID</td>
            <td>String</td>
            <td>Identificador único da resposta. Gerado pela paySmart.</td>
        </tr>
        <tr>
            <td>- issuerRequestId</td>
            <td>String</td>
            <td>Identificador único da requisição gerado pelo emissor. Ecoado conforme enviado na requisição no campo issuerDisputeId.</td>
        </tr>
    </tbody>
</table>

{% include endpoint_footer.html %}