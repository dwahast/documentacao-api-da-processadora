---
title: Processo de Geração de Créditos e Débitos
name: inclusives
tags: 
 - inclusivas
description: API para geração de TEs 10 e 20 para acertos com os credenciadores
permalink: pt-br/docs/inclusives
language: pt-br
---

# **{{page.title}}**

1. [Introdução](#introdução)

## **Introdução**

API para geração de créditos e débitos para serem enviados para os credenciadores através de TEs 10 e 20. Seu uso se destina a situações de ajuste financeiro de disputa por fora do fluxo padrão junto à bandeira.
Uma vez que o envio da transação inclusiva esteja combinado com o credenciador, o emissor dispõe dos seguintes endpoints para uso:

<ul>
    {% assign endpoints=site.docs | where: "type", "inclusives" %}
    {% for endpoint in endpoints %}
    <li><a href="{{site.baseurl}}/pt-br/docs/{{endpoint.type}}/{{endpoint.link}}">
        {% if endpoint.tags contains "endpoint" %}{{endpoint.method}} /{{page.name}}/{{endpoint.name}}{% else %}{{endpoint.title}}{% endif %}
    </a></li>
    {% endfor %}
</ul>