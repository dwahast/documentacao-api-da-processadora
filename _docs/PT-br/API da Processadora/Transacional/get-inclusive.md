---
title: Consulta de Transação Inclusiva
name: "{inclusiveTransactionId}"
link: "get-inclusives"
method: GET
type: inclusives
tags:
  - endpoint
  - inclusivas
  - consulta inclusiva
description: "Endpoint para consulta de uma transação inclusiva específica"
permalink: /pt-br/docs/inclusives/get-inclusives
language: pt-br
---
{% include endpoint_header.html %}

{% include endpoint_auth.html %}

#### Caso de Sucesso

```json
{
  "inclusiveTransactionId": "TI-ca50514f-aaae-432c-815d-00f6febc3883",
  "accountId": "cta-f2da36fa-aef3-4a4e-aa4f-7819b2d52db0",
  "transactionId": "aut-6981fa93-253a-4137-ac22-038f5f2d7846",
  "code": "TE10",
  "reasonCode": "04",
  "text": "Acordo amigável após discussões com o credenciador acerca da disputa da transação.",
  "createdAt": "2021-12-08"
}
```

#### Caso de Erro

```json
{
  "resultData": {
    "resultCode": 984,
    "resultDescription": "Transação inclusiva não encontrada.",
    "psResponseId": "1987dddc-4284-4bdc-9073-b79f21801096"
  }
}
```

### **Descrição da Resposta**

<table>
    <thead>
        <tr>
            <th>Nome</th>
            <th>Tipo</th>
            <th>Descrição</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>inclusiveTransactionId</td>
            <td>String</td>
            <td>Identificador da transação inclusiva</td>
        </tr>
        <tr>
            <td>accountId</td>
            <td>String</td>
            <td>Identificador da conta</td>
        </tr>
        <tr>
            <td>transactionId</td>
            <td>String</td>
            <td>Identificador da transação</td>
        </tr>
        <tr>
            <td>code</td>
            <td>String</td>
            <td>Código do tipo da transação inclusiva</td>
        </tr>
        <tr>
            <td>reasonCode</td>
            <td>String</td>
            <td>Código de motivo de ajuste financeiro de disputa</td>
        </tr>
        <tr>
            <td>text</td>
            <td>String</td>
            <td>Texto explicando o motivo da transação inclusiva</td>
        </tr>
        <tr>
            <td>createdAt</td>
            <td>String</td>
            <td>Data de criação da transação inclusiva</td>
        </tr>
        <tr>
            <td>resultData</td>
            <td>Objeto</td>
            <td>Dados da resposta da API. <b>Somente em casos de erro.</b></td>
        </tr>
        <tr>
            <td>- resultCode</td>
            <td>Integer</td>
            <td>Código de resultado do processamento.</td>
        </tr>
        <tr>
            <td>- resultDescription</td>
            <td>String</td>
            <td>Descrição textual do resultado do processamento.</td>
        </tr>
        <tr>
            <td>- psResponseID</td>
            <td>String</td>
            <td>Identificador único da resposta. Gerado pela paySmart.</td>
        </tr>
    </tbody>
</table>

{% include endpoint_footer.html %}
