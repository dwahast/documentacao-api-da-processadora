---
title: Consulta de Transações Inclusivas
name: ""
link: "find-inclusives"
method: GET
type: inclusives
tags:
  - endpoint
  - inclusivas
  - consulta inclusivas
description: "Endpoint para consulta de múltiplas transações inclusivas com base nos filtros definidos."
permalink: /pt-br/docs/inclusives/find-inclusives
language: pt-br
---

{% include endpoint_header.html %}

{% include endpoint_auth.html %}

### **Parâmetros de Busca**

<table>
    <thead>
        <tr>
            <th>Nome</th>
            <th>Obrigatório</th>
            <th>Descrição</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>limit</td>
            <td>Não</td>
            <td>Número limite de objetos retornados. <b>O valor deve ser entre 1 e 100.</b></td>
        </tr>
        <tr>
            <td>beginning_date</td>
            <td>Sim</td>
            <td>Data indicando o primeiro dia cujos dados devem ser retornados. <b>Deve ser uma data no formato yyyy-MM-DD. Exemplo: "2020-07-09"</b></td>
        </tr>
        <tr>
            <td>ending_date</td>
            <td>Sim</td>
            <td>Data indicando o último dia cujos dados devem ser retornados. <b>Deve ser uma data no formato yyyy-MM-DD. Exemplo: "2020-07-09"</b></td>
        </tr>
    </tbody>
</table>

### **Exemplos de Requisição**

#### Caso de Sucesso

```
curl -X GET https://api.paysmart.com.br/paySmart/ps-processadora/v1/inclusives?limit=2&beginning_date=2020-11-17&ending_date=2021-12-31
```

#### Caso de Erro

```
curl -X GET https://api.paysmart.com.br/paySmart/ps-processadora/v1/inclusives?limit=2
```

### **Exemplos de Resposta**

#### Caso de Sucesso

```json
{
  "hasMore": "true",
  "inclusives": [
    {
      "inclusiveTransactionId": "TI-ca32442f-aaae-332c-815d-00f6fe543883",
      "accountId": "cta-n2oa36fa-aef3-4a4e-aa4f-7819b2d52db0",
      "transactionId": "aut-8169fa93-253a-4137-ac22-038f5f2d7846",
      "code": "TE10",
      "reasonCode": "04",
      "text": "Acordo amigável após discussões com o credenciador acerca da disputa da transação.",
      "createdAt": "2021-05-17"
    },
    {
      "inclusiveTransactionId": "TI-ca50514f-aaae-432c-815d-00f6febc3883",
      "accountId": "cta-f2da36fa-aef3-4a4e-aa4f-7819b2d52db0",
      "transactionId": "aut-6981fa93-253a-4137-ac22-038f5f2d7846",
      "code": "TE20",
      "reasonCode": "04",
      "text": "Acordo amigável após discussões com o credenciador acerca da disputa da transação.",
      "createdAt": "2021-12-08"
    }
  ]
}
```

#### Caso de Erro

```json
{
  "resultData": {
    "resultCode": 982,
    "resultDescription": "Nenhuma transação inclusiva encontrada com os parâmetros de busca utilizados.",
    "psResponseId": "5c4d3c1e-e9d2-493c-a485-8ae8073a66f4"
  }
}
```

### **Descrição da Resposta**

<table>
    <thead>
        <tr>
            <th>Nome</th>
            <th>Tipo</th>
            <th>Descrição</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>hasMore</td>
            <td>Boolean</td>
            <td>Indica que atingiu o limite de retorno e existem mais elementos a serem retornados.</td>
        </tr>
        <tr>
            <td>inclusives</td>
            <td>Array</td>
            <td>Lista de transações inclusivas encontradas.</td>
        </tr>
        <tr>
            <td>- inclusiveTransactionId</td>
            <td>String</td>
            <td>Identificador da transação inclusiva</td>
        </tr>
        <tr>
            <td>- accountId</td>
            <td>String</td>
            <td>Identificador da conta</td>
        </tr>
        <tr>
            <td>- transactionId</td>
            <td>String</td>
            <td>Identificador da transação</td>
        </tr>
        <tr>
            <td>- code</td>
            <td>String</td>
            <td>Código do tipo da transação inclusiva</td>
        </tr>
        <tr>
            <td>- reasonCode</td>
            <td>String</td>
            <td>Código de motivo de ajuste financeiro de disputa</td>
        </tr>
        <tr>
            <td>- text</td>
            <td>String</td>
            <td>Texto explicando o motivo da transação inclusiva</td>
        </tr>
        <tr>
            <td>- createdAt</td>
            <td>String</td>
            <td>Data de criação da transação inclusiva</td>
        </tr>
        <tr>
            <td>resultData</td>
            <td>Objeto</td>
            <td>Dados da resposta da API.</td>
        </tr>
        <tr>
            <td>- resultCode</td>
            <td>Integer</td>
            <td>Código de resultado do processamento.</td>
        </tr>
        <tr>
            <td>- resultDescription</td>
            <td>String</td>
            <td>Descrição textual do resultado do processamento.</td>
        </tr>
        <tr>
            <td>- psResponseID</td>
            <td>String</td>
            <td>Identificador único da resposta. Gerado pela paySmart.</td>
        </tr>
    </tbody>
</table>

{% include endpoint_footer.html %}
