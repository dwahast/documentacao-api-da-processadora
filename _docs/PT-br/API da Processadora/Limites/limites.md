---
title: Limites
permalink: /pt-br/pospaid/limits/
language: pt-br
---

# Limites

No uso de produtos de crédito, é obrigatório definir-se um limite para consumo, como para proteção do emissor. Logo temos algumas operações fornecidas em nossa API. Como:

- Definição de Limite
    - Motor de Crédito
- Ajuste de Limite
    - Motor de Crédito
- Consulta de Limite

## Motor de Crédito
O motor de crédito é uma ferramenta oferecida para auxiliar a definição de limite de crédito considerando a sua alta complexidade, hoje a paysmart fornece um lógica base, 
mas de qualquer forma essa deve ser definida pelo emissor considerando o RISCO de oferecer crédito a um mal pagante.

A operação de Motor de crédito é fornecida como um serviço separado e pode ter custos adicionais, consulte o suporte para mais informações.
