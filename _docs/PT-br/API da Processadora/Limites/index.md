---
title: Pós-pago
permalink: /pt-br/pospaid
language: pt-br
---

# Pós-Pago

Funcionalidades pós-pago da processadora
- Limites
- Faturas
- Cobrança
    - Campanhas
    - Controle de Inadimplência
- Notificações