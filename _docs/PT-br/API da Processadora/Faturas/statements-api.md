---
title: API de Limites
description: Especificação da API de Limites
permalink: /pt-br/pospaid/statements/spec/

layout: swagger_layout
spec_path: "API da Processadora/Pós-Pago/statements-api.yml"
language: pt-br
---

# Documentação API processadora
