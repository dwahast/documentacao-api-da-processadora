---
title: Acompanhamento de Disputa
name: "{disputeId}"
link: "get-dispute"
method: GET
type: disputes
tags:
    - endpoint
    - disputas
    - dispute tracking
description: "Endpoint para acompanhamento de uma disputa específica"
permalink: /pt-br/docs/disputes/get-dispute
language: pt-br
---
{% include endpoint_header.html %}

Ao consultar o status de uma disputa, deve-se ficar atento aos campos <b>currentStage</b>, <b>disputeStatus</b> e <b>disputeMaxDateResponse</b>, pois estes indicam quando é necessário uma ação por parte do emissor. Mais especificamente, é preciso que envie-se uma requisição para o endpoint <a href="./respond-dispute">Continuação de Disputas</a> quando os dois primeiros campos citados assumem os valores de <b>restatement</b> e <b>waiting_for_response_from_issuer</b>, respectivamente. 

{% include endpoint_auth.html %}

### **Exemplos de Resposta**

#### Caso de Sucesso

```json
{
    "currentStage": "pre-arbitration",
    "disputeMaxDateResponse": "2021-06-20T00:00:00.000Z",
    "disputeId": "DIS-7be16041-7f04-40fc-a2d3-5283ab8aa2fd",
    "disputeStatus": "waiting_for_response_from_acquirer",
    "transaction": {
        "transactionSource": "brand",
        "fees": [],
        "creatingSystemName": "Autorizador ISO8583",
        "psProductName": "ISSUER",
        "terminalId": "20172289",
        "ISO8583MessageRequest": {
            "de011": "161321",
            "de022": "011",
            "de032": "0025",
            "de043": "ELO                    BARUERI       076",
            "de042": "020001605270002",
            "mti": "0100",
            "de041": "20172289",
            "de060": "000110000P500",
            "de019": "076",
            "de018": "5712",
            "de007": "0416211321",
            "de127": "12182",
            "de049": "986",
            "de126": "*****",
            "de037": "222496782252",
            "de004": "000000002000",
            "de048": "*CDT002T0*PRD003070",
            "de014": "2412",
            "de058": "ALAMEDAXINGU          23543230007612345ELO                  ",
            "de003": "003000",
            "de013": "0416",
            "de002": "************1585",
            "de024": "100",
            "de012": "181321"
        },
        "mcc": "5712",
        "transactionTime": "18:13:21",
        "merchantName": "ELO",
        "issuerId": "3",
        "merchantId": "020001605270002",
        "countryCode": "076",
        "disputeStatus": "pre-arbitration",
        "international": false,
        "acquirerId": "0025",
        "transactionAuthorizationResponse": {
            "approved": true
        },
        "entryMode": "manual",
        "psProductCode": "030102",
        "amount": {
            "amount": {
                "amount": 2000,
                "currencyCode": 986
            },
            "debit_or_credit": "debit"
        },
        "merchantCity": "BARUERI",
        "acquirerTransactionId": "222496782252",
        "transactionStatus": "confirmed",
        "freeDescription": "Nova transação de compra registrada!",
        "transactionReceivedDateTime": "2021-04-16T21:13:22.093Z",
        "incremental": false,
        "transactionDate": "2021-04-16",
        "transactionId": "aut_f7c56484-41db-4826-ab6d-0c4fd31fcf5c",
        "transactionType": "authorized_transaction",
        "cardBrandId": "Elo",
        "accountId": "cta-8acbd197-a061-486c-af38-5632315767be",
        "forceAccept": false,
        "statusChangeHistory": [
            {
                "callingSystemName": "Autorizador ISO8583",
                "stateChangeDescription": "Nova transação registrada.",
                "stateChangeReasonCode": "other",
                "newStatus": "pending",
                "changeRequestReceivedDate": "2021-04-16T21:13:22.093Z",
                "statusChangeSource": "brand"
            }
        ],
        "disputeId": "DIS-7be16041-7f04-40fc-a2d3-5283ab8aa2fd",
        "merchantAddress": "ALAMEDAXINGU",
        "merchantZipcode": "235432300",
        "card": {
            "BIN": "******",
            "cardId": "crt-f4de6c2a-f501-482e-a353-a88f6a4b3aa3",
            "last_four_digits": "1585",
            "cardIdIssuer": "fdb5d5ea-2d5d-4937-865e-933fe46b3d3f"
        }
    }
}
```

#### Caso de Erro

```json
{
    "resultData": {
        "resultCode": 984,
        "resultDescription": "Disputa não encontrada.",
        "psResponseId": "1987dddc-4284-4bdc-9073-b79f21801096"
    }
}
```

### **Descrição da Resposta**

<table>
    <thead>
        <tr>
            <th>Nome</th>
            <th>Tipo</th>
            <th>Descrição</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>currentStage</td>
            <td>String</td>
            <td>Estágio atual do processo de disputa. Retorna um dos seguintes valores: chargeback, restatement, pre-arbitration, arbitration.</td>
        </tr>
        <tr>
            <td>disputeMaxDateResponse</td>
            <td>String</td>
            <td>Data máxima para uma resposta no formato Iso-8601, por exemplo 2021-06-20T00:00:00.000Z. Deve ser usado em conjunto com o campo disputeStatus para monitor quando o emissor deverá enviar um novo request.</td>
        </tr>
        <tr>
            <td>disputeId</td>
            <td>String</td>
            <td>Identificador único da disputa na paySmart.</td>
        </tr>
        <tr>
            <td>disputeStatus</td>
            <td>String</td>
            <td>Estado atual da disputa. Serve para indicar qual lado da disputa deve realizar a próxima ação. Retorna um dos seguintes valores: waiting_for_response_from_issuer,  waiting_for_response_from_acquirer, waiting_for_response_from_brand, accepted, denied, issuer_won, issuer_lost, canceled_by_issuer.</td>
        </tr>
        <tr>
            <td>transaction</td>
            <td>Objeto</td>
            <td>Dados da transação sendo disputada.</td>
        </tr>
        <tr>
            <td>resultData</td>
            <td>Objeto</td>
            <td>Dados da resposta da API. <b>Somente em casos de erro.</b></td>
        </tr>
        <tr>
            <td>- resultCode</td>
            <td>Integer</td>
            <td>Código de resultado do processamento.</td>
        </tr>
        <tr>
            <td>- resultDescription</td>
            <td>String</td>
            <td>Descrição textual do resultado do processamento.</td>
        </tr>
        <tr>
            <td>- psResponseID</td>
            <td>String</td>
            <td>Identificador único da resposta. Gerado pela paySmart.</td>
        </tr>
    </tbody>
</table>

{% include endpoint_footer.html %}
