---
title: Criação de Disputa
name: ""
link: "add-dispute"
method: POST
type: disputes
tags:
    - endpoint
    - disputas
    - criação disputa
description: "Endpoint para criação de disputas."
permalink: /pt-br/docs/disputes/add-dispute
language: pt-br
---
{% include endpoint_header.html %} 

Importante atentar que este endpoint deve ser chamado somente para a criação de disputas. Para os casos de continuação de uma disputa em etapas posteriores, em que é necessário o envio de novos requests, olhar <a href="{{ site.url }}{{ site.baseurl }}/pt-br/docs/respond-dispute">Continuação de Disputas</a>. 

Além disso, antes da criação de uma disputa deve-se confirmar que a mesma atende nenhuma restrição que impeça sua criação, como listado em <a href="{{site.baseurl}}/pt-br/docs/disputes/restricoes-disputa">Restrições para Criação de Disputas</a>.

{% include endpoint_auth.html %}

### **Argumentos**

<table>
    <thead>
        <tr>
            <th>Nome</th>
            <th>Obrigatório</th>
            <th>Tipo</th>
            <th>Descrição</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>accountId</td>
            <td>Sim</td>
            <td>String</td>
            <td>Identificador único da conta, atribuído pela paySmart.</td>
        </tr>
        <tr>
            <td>transactionId</td>
            <td>Sim</td>
            <td>String</td>
            <td>Identificador único da transação em disputa.</td>
        </tr>
        <tr>
            <td>disputeCode</td>
            <td>Sim</td>
            <td>String</td>
            <td>Código da disputa que indica o motivo da abertura. Olhar <a href="./codigos-disputa">Códigos de Motivo de Disputa</a></td>
        </tr>
        <tr>
            <td>fraudType</td>
            <td>Não</td>
            <td>String</td>
            <td>Código do tipo de fraude que indica o motivo da abertura da disputa com motivo de fraude. Olhar <a href="./codigos-fraude">Códigos de Tipo de Fraude</a></td>
        </tr>
        <tr>
            <td>disputeTextMessage</td>
            <td>Sim</td>
            <td>String</td>
            <td> Mensagem de texto justificando a disputa. Conteúdo esperado depende do código do motivo da disputa e pode ser encontrado em <a href="./codigos-disputa">Códigos de Motivo de Disputa</a>.</td>
        </tr>
        <tr>
            <td>issuerDisputeId</td>
            <td>Não</td>
            <td>String</td>
            <td>Identificador único da disputa. Gerado pelo emissor.</td>
        </tr>
        <tr>
            <td>Partial</td>
            <td>Sim</td>
            <td>Boolean</td>
            <td>Indica se está sendo disputada uma parte ou o valor total da transação. <b>Se for verdadeiro, é necessário informar amount_disputed.</b></td>
        </tr>
        <tr>
            <td>willAddDocuments</td>
            <td>Não</td>
            <td>Boolean</td>
            <td>Indica se documentos apoiando a disputa vão ser anexados no Portal ELO posteriormente.</td>
        </tr>
        <tr>
            <td>amount_disputed</td>
            <td>Condicional</td>
            <td>Objeto</td>
            <td>Valor sendo disputado. <b>Só deve estar presente se a partial for true e deve conter o campo amount e, opcionalmente, o currencyCode</b>.</td>
        </tr>
        <tr>
            <td>- amount</td>
            <td>Condicional</td>
            <td>Inteiro</td>
            <td>Valor omitindo a vírgula.<b>Obrigatório estar presente caso partial seja true.</b></td>
        </tr>
        <tr>
            <td>- currencyCode</td>
            <td>Não</td>
            <td>Inteiro</td>
            <td>Código da moeda, conforme ISO-4217.</td>
        </tr>
        <tr>
            <td>sourceAudit</td>
            <td>Não</td>
            <td>Objeto</td>
            <td>Informações de auditoria para registro de informações. Possui os campos operatorId e processId</td>
        </tr>
        <tr>
            <td>- operatorId</td>
            <td>Não</td>
            <td>String</td>
            <td>Identificação do operador solicitante.</td>
        </tr>
        <tr>
            <td>- processId</td>
            <td>Não</td>
            <td>String</td>
            <td>Identificação do processo solicitante.</td>
        </tr>
    </tbody>
</table>

### **Exemplo de Requisição**

```json
{
    "issuerDisputeId": "a3e4cf3c-2a98-4a88-a370-5429356d7112",
    "accountId": "cta-8acbd197-a061-486c-af38-5632315767be",
    "transactionId": "aut_3ee27dd3-c1b4-43b6-993e-6d126b3e67d2",
    "disputeCode": "30",
    "fraudType": "05",
    "disputeTextMessage": "Serviço não realizado",
    "partial": true,
    "amount_disputed": {
        "amount": 123,
        "currencyCode": 986
    },
    "willAddDocuments": false,
    "sourceAudit": {
        "operatorId": "12340985312",
        "processId": "PID-12345"
    }
}
```

### **Exemplo de Resposta**

```json
{
    "dispute": {
        "disputeId": "DIS-0406360e-58fd-45bf-818e-73fb95088745",
        "disputeStatus": "waiting_for_response_from_acquirer",
        "disputeRequest": {
            "issuerDisputeId": "a3e4cf3c-2a98-4a88-a370-5429356d7118",
            "accountId": "cta-8acbd197-a061-486c-af38-5632315776eb",
            "transactionId": "aut_3ee27dd3-c1b4-43b6-993e-6d126b3e67b7",
            "disputeCode": "81",
            "fraudType": "05",
            "disputeTextMessage": "FRAUDE",
            "partial": true,
            "willAddDocuments": false,
            "amount_disputed": {
                "amount": 123,
                "currencyCode": 986
            },
            "sourceAudit": {
                "operatorId": "12340985312",
                "processId": "PID-12345"
            }
        },
        "transaction": {
            "transactionDateTime": "2021-05-04T12:17:29",
            "settlementDateTime": null,
            "transactionId": "aut_3ee27dd3-c1b4-43b6-993e-6d126b3e67b7",
            "accountId": "cta-8acbd197-a061-486c-af38-5632315776eb",
            "psProductCode": null,
            "psProductName": null,
            "issuerId": "3",
            "cardBrandId": null,
            "creatingSystemName": null,
            "transactionSource": null,
            "transactionReceivedDateTime": null,
            "transactionDate": null,
            "transactionTime": null,
            "settlementDate": null,
            "transactionType": "SALE",
            "transactionStatus": null,
            "transactionAuthorizationResponse": {
                "approved": true,
                "denialReason": null
            },
            "acquirerId": "0025",
            "acquirerTransactionId": "936674959729",
            "merchantId": "020001605270002",
            "merchantName": "ELO",
            "merchantDocumentId": null,
            "merchantAddress": "ALAMEDAXINGU",
            "merchantCity": "BARUERI",
            "merchantUf": null,
            "merchantZipcode": null,
            "countryCode": "076",
            "mcc": "5712",
            "terminalId": "20172289",
            "amount": {
                "amount": 25615,
                "currencyCode": 986,
                "debit_or_credit": null
            },
            "international": null,
            "incremental": null,
            "internationalTransactionData": null,
            "entryMode": "MANUAL",
            "card": {
                "cardId": "crt-f4de6c2a-f501-482e-a353-a88f6a4b3bb7",
                "bin": "******",
                "last4Digits": "1585"
            },
            "undoData": null,
            "originalTransaction": null,
            "cancellingTransactionId": null,
            "disputeId": null,
            "disputeStatus": null,
            "fees": [],
            "statusChangeHistory": null,
            "ISO8583MessageRequest": {
                "mti": "0100",
                "de002": "************1585",
                "de003": "003000",
                "de004": "000000025615",
                "de005": null,
                "de006": null,
                "de007": "0504151729",
                "de008": null,
                "de009": null,
                "de010": null,
                "de011": "041729",
                "de012": "121729",
                "de013": "0504",
                "de014": "2412",
                "de015": null,
                "de016": null,
                "de018": "5712",
                "de019": "076",
                "de022": "011",
                "de023": null,
                "de024": "100",
                "de025": null,
                "de026": null,
                "de028": null,
                "de029": null,
                "de032": "0025",
                "de033": null,
                "de035": null,
                "de036": null,
                "de037": "936674959729",
                "de038": null,
                "de039": null,
                "de041": "20172289",
                "de042": "020001605270002",
                "de043": "ELO                    BARUERI       076",
                "de045": null,
                "de046": null,
                "de047": null,
                "de048": "*CDT002T0*PRD003070",
                "de049": "986",
                "de050": null,
                "de051": null,
                "de052": null,
                "de053": null,
                "de054": null,
                "de055": null,
                "de056": null,
                "de058": "ALAMEDAXINGU          23543230007612345ELO                  ",
                "de059": null,
                "de060": "000110000P500",
                "de062": null,
                "de063": null,
                "de090": null,
                "de095": null,
                "de105": null,
                "de106": null,
                "de107": null,
                "de121": null,
                "de122": null,
                "de123": null,
                "de124": null,
                "de125": null,
                "de126": "*****",
                "de127": "12182"
            },
            "ISO8583MessageResponse": null,
            "freeDescription": "Nova transação de compra registrada!",
            "forceAccept": false,
            "transferData": null,
            "fraudData": null
        }
    },
    "resultData": {
        "psResponseId": "09fb33e6-e937-4be3-9115-ef648ea1f950",
        "resultCode": 0,
        "resultDescription": "Requisição para inicio de Disputa criada com sucesso!",
        "issuerRequestId": "a3e4cf3c-2a98-4a88-a370-5429356d7112"
    }
}
```

### **Descrição da Resposta**

<table>
    <thead>
        <tr>
            <th>Nome</th>
            <th>Tipo</th>
            <th>Descrição</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>dispute</td>
            <td>Objeto</td>
            <td>Objeto contendo os dados referentes à disputa criada. <b>Presente somente em caso de sucesso.</b></td>
        </tr>
        <tr>
            <td>- disputeId</td>
            <td>String</td>
            <td>Identificador único da disputa na paySmart.</td>
        </tr>
        <tr>
            <td>- disputeStatus</td>
            <td>String</td>
            <td>Estado atual da disputa.</td>
        </tr>
        <tr>
            <td>- disputeRequest</td>
            <td>Objeto</td>
            <td>Request enviado originalmente pelo emissor.</td>
        </tr>
        <tr>
            <td>- transaction</td>
            <td>Objeto</td>
            <td>Dados da transação sendo disputada.</td>
        </tr>
        <tr>
            <td>resultData</td>
            <td>Objeto</td>
            <td>Dados da resposta da API.</td>
        </tr>
        <tr>
            <td>- resultCode</td>
            <td>Integer</td>
            <td>Código de resultado do processamento.</td>
        </tr>
        <tr>
            <td>- resultDescription</td>
            <td>String</td>
            <td>Descrição textual do resultado do processamento.</td>
        </tr>
        <tr>
            <td>- psResponseID</td>
            <td>String</td>
            <td>Identificador único da resposta. Gerado pela paySmart.</td>
        </tr>
        <tr>
            <td>- issuerRequestId</td>
            <td>String</td>
            <td>Identificador único da requisição gerado pelo emissor. Ecoado conforme enviado na requisição no campo issuerDisputeId.</td>
        </tr>
    </tbody>
</table>

{% include endpoint_footer.html %}
