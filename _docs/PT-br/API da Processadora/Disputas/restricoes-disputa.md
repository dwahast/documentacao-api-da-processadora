---
title: Restrições para Criação de Disputas
name: ""
link: restrictions
type: info
tags:
    - dispute
    - dispute restrictions
description: "Checklist de possíveis restrições que impedem a criação de uma disputa em cada bandeira."
permalink: /pt-br/docs/disputes/restrictions
language: pt-br
---
{% include info_header.html %}

Caso qualquer um dos motivos listados abaixo seja atendido pela transação que está sendo disputada, a API de Disputas retornará erro imediatamente. A lista de possíveis erros retornados pela API pode ser encontrada em <a href="{{site.baseurl}}/pt-br/docs/disputes/errors-code">Códigos de Erro da API de Disputas</a>.

### **Checklist de Restrições para Elo**

No caso da Elo, não é possível iniciar o processo de disputa quando:

- A transação tenha sido realizada com chip.
- A transação é de cashback. 