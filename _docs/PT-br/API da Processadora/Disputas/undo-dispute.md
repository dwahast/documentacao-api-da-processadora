---
title: Cancelamento de Disputa
name: "{disputeId}/undo"
link: "undo-dispute"
method: POST
type: disputes
tags:
    - endpoint
    - disputes
    - dispute cancelation
description: "Endpoint para desistência de uma disputa e envio de sua reversão."
permalink: /pt-br/docs/disputes/undo-dispute
language: pt-br
---
{% include endpoint_header.html %}

Importante cuidar que uma disputa só pode ser cancelada por meio de nossa API caso esteja no estágio de Chargeback ou Reapresentação. Em etapas posteriores (pré-arbitragem ou arbitragem), o emissor deve contatar o credenciador para solucionar a questão via TE10/TE20.

{% include endpoint_auth.html %}

### **Argumentos**

<table>
    <thead>
        <tr>
            <th>Nome</th>
            <th>Obrigatório</th>
            <th>Tipo</th>
            <th>Descrição</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>issuerDisputeReversalId</td>
            <td>Não</td>
            <td>String</td>
            <td>Identificador único da resposta da disputa. Gerado pelo emissor.</td>
        </tr>
        <tr>
            <td>textMessage</td>
            <td>Não</td>
            <td>String</td>
            <td>Mensagem de texto justificando o motivo da desistência da disputa.</td>
        </tr>
        <tr>
            <td>sourceAudit</td>
            <td>Não</td>
            <td>Objeto</td>
            <td>Informações de auditoria para registro de informações. Possui os campos operatorId e processId</td>
        </tr>
        <tr>
            <td>- operatorId</td>
            <td>Não</td>
            <td>String</td>
            <td>Identificação do operador solicitante.</td>
        </tr>
        <tr>
            <td>- processId</td>
            <td>Não</td>
            <td>String</td>
            <td>Identificação do processo solicitante.</td>
        </tr>
    </tbody>
</table>

### **Exemplo de Requisição**

```json
{
    "issuerDisputeReversalId": "a3e4cf3c-2a98-4a88-a370-5429356d7112",
    "textMessage": "Portador do cartão reconheceu a compra.",
    "sourceAudit": {
        "operatorId": "12340985312",
        "processId": "PID-12345"
    }
}
```

### **Exemplo de Resposta**

```json
{
    "dispute": {
        "disputeId": "DIS-ad56619b-c72f-4c44-a821-983ca2ebdd67",
        "disputeStatus": "canceled_by_issuer",
        "disputeRequest": {
            "issuerDisputeReversalId": "a3e4cf3c-2a98-4a88-a370-5429356d7112",
            "textMessage": "Portador do cartão reconheceu a compra.",
            "sourceAudit": {
                "operatorId": "12340985312",
                "processId": "PID-12345"
            }
        },
        "transaction": {
            "transactionSource": "brand",
            "fees": [],
            "creatingSystemName": "Autorizador ISO8583",
            "psProductName": "ISSUER",
            "terminalId": "20172289",
            "ISO8583MessageRequest": {
                "de011": "054748",
                "de022": "011",
                "de032": "0025",
                "de043": "ELO                    BARUERI       076",
                "de042": "020001605270002",
                "mti": "0100",
                "de041": "20172289",
                "de060": "000110000P500",
                "de019": "076",
                "de018": "5712",
                "de007": "0505134748",
                "de127": "12182",
                "de049": "986",
                "de126": "*****",
                "de037": "141438213853",
                "de004": "000000045661",
                "de048": "*CDT002T0*PRD003070",
                "de014": "2412",
                "de058": "ALAMEDAXINGU          23543230007612345ELO                  ",
                "de003": "003000",
                "de013": "0505",
                "de002": "************1585",
                "de024": "100",
                "de012": "104748"
            },
            "mcc": "5712",
            "transactionTime": "10:47:48",
            "merchantName": "ELO",
            "issuerId": "3",
            "merchantId": "020001605270002",
            "countryCode": "076",
            "disputeStatus": "chargeback",
            "international": false,
            "acquirerId": "0025",
            "transactionAuthorizationResponse": {
                "approved": true
            },
            "entryMode": "manual",
            "psProductCode": "030102",
            "amount": {
                "amount": {
                    "amount": 45661,
                    "currencyCode": 986
                },
                "debit_or_credit": "debit"
            },
            "merchantCity": "BARUERI",
            "acquirerTransactionId": "141438213853",
            "transactionStatus": "approved",
            "freeDescription": "Nova transação de compra registrada!",
            "transactionReceivedDateTime": "2021-05-05T13:47:48.602Z",
            "incremental": false,
            "transactionDate": "2021-05-05",
            "transactionId": "aut_ed8d5c54-188a-4b6a-a960-8bd05582850c",
            "transactionType": "authorized_transaction",
            "cardBrandId": "Elo",
            "accountId": "cta-8acbd197-a061-486c-af38-5632315767be",
            "forceAccept": false,
            "statusChangeHistory": [
                {
                    "callingSystemName": "Autorizador ISO8583",
                    "stateChangeDescription": "Nova transação registrada.",
                    "stateChangeReasonCode": "other",
                    "newStatus": "pending",
                    "changeRequestReceivedDate": "2021-05-05T13:47:48.602Z",
                    "statusChangeSource": "brand"
                }
            ],
            "disputeId": "DIS-ad56619b-c72f-4c44-a821-983ca2ebdd67",
            "merchantAddress": "ALAMEDAXINGU",
            "merchantZipcode": "235432300",
            "card": {
                "BIN": "******",
                "cardId": "crt-f4de6c2a-f501-482e-a353-a88f6a4b3aa3",
                "last_four_digits": "1585",
                "cardIdIssuer": "fdb5d5ea-2d5d-4937-865e-933fe46b3d3f"
            }
        }
    },
    "resultData": {
        "psResponseId": "eb9b9141-d8c2-4a41-81a2-d58567aaadc1",
        "resultCode": 0,
        "resultDescription": "Pedido de desistência de disputa criado com sucesso!",
        "issuerRequestId": "DIS-ad56619b-c72f-4c44-a821-983ca2ebdd67"
    }
}
```

### **Descrição da Resposta**

<table>
    <thead>
        <tr>
            <th>Nome</th>
            <th>Tipo</th>
            <th>Descrição</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>dispute</td>
            <td>Objeto</td>
            <td>Objeto contendo os dados referentes à disputa criada. <b>Presente somente em caso de sucesso.</b></td>
        </tr>
        <tr>
            <td>- disputeId</td>
            <td>String</td>
            <td>Identificador único da disputa na paySmart.</td>
        </tr>
        <tr>
            <td>- disputeStatus</td>
            <td>String</td>
            <td>Estado atual da disputa.</td>
        </tr>
        <tr>
            <td>- disputeRequest</td>
            <td>Objeto</td>
            <td>Request enviado pelo emissor.</td>
        </tr>
        <tr>
            <td>- transaction</td>
            <td>Objeto</td>
            <td>Dados da transação sendo disputada.</td>
        </tr>
        <tr>
            <td>resultData</td>
            <td>Objeto</td>
            <td>Dados da resposta da API.</td>
        </tr>
        <tr>
            <td>- resultCode</td>
            <td>Integer</td>
            <td>Código de resultado do processamento.</td>
        </tr>
        <tr>
            <td>- resultDescription</td>
            <td>String</td>
            <td>Descrição textual do resultado do processamento.</td>
        </tr>
        <tr>
            <td>- psResponseID</td>
            <td>String</td>
            <td>Identificador único da resposta. Gerado pela paySmart.</td>
        </tr>
        <tr>
            <td>- issuerRequestId</td>
            <td>String</td>
            <td>Identificador único da requisição gerado pelo emissor. Ecoado conforme enviado na requisição no campo issuerDisputeId.</td>
        </tr>
    </tbody>
</table>

{% include endpoint_footer.html %}