---
title: Códigos de Tipo de Fraude
name: ""
link: "frauds-code"
type: info
tags:
    - fraude
    - criação fraude
    - códigos fraude
description: "Códigos para informar o tipo de fraude no request de criação de disputa."
permalink: /pt-br/docs/disputes/frauds-code
language: pt-br
---
{%include info_header.html%}

<table>
    <thead>
        <tr>
            <th>Código</th>
            <th>Mensagens de Disputa (Separadas por |)</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>00</td>
            <td>Cartão perdido pelo portador.</td>
        </tr>
        <tr>
            <td>01</td>
            <td>Cartão roubado do portador.</td>
        </tr>
        <tr>
            <td>02</td>
            <td>Cartão extraviado (portador não recebeu cartão do emissor, porém emissor alega que enviou o cartão).</td>
        </tr>
        <tr>
            <td>03</td>
            <td>Falsidade Ideológica.</td>
        </tr>
        <tr>
            <td>04</td>
            <td>Cartão clonado/falsificado.</td>
        </tr>
        <tr>
            <td>05</td>
            <td>Transação autorizada sem o envolvimento do portador.</td>
        </tr>
        <tr>
            <td>06</td>
            <td>Número do cartão foi usado em transação de marketing direto ou no comércio eletrônico.</td>
        </tr>
        <tr>
            <td>07</td>
            <td>Fraude Familiar (familiar teve acesso ao cartão e fez transação sem autorização do portador do cartão).</td>
        </tr>
        <tr>
            <td>08</td>
            <td>Engenharia Social (cliente caiu em golpe de fraudador, entregando senha e/ou cartão e/ou não validando valor da compra no Ponto de Venda).</td>
        </tr>
        <tr>
            <td>09</td>
            <td>Auto Fraude (cliente contesta transação, mas há evidências de que foi o cliente que realizou a transação contestada).</td>
        </tr>
        <tr>
            <td>10</td>
            <td>Fraude Interna (fraude resultante da participação de funcionários do emissor na emissão e/ou utilização do cartão ou vazamento de dados do cliente).</td>
        </tr>
    </tbody>
</table>
