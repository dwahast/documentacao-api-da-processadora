---
title: Acompanhamento de Disputas
name: ""
link: "find-disputes"
method: GET
type: disputes
tags:
    - endpoint
    - disputes
    - dispute tracking
description: "Endpoint para acompanhamento de múltiplas disputas com base nos filtros definidos."
permalink: /pt-br/docs/disputes/find-disputes
language: pt-br
---
{% include endpoint_header.html %}

{% include endpoint_auth.html %}

### **Parâmetros de Busca**

<table>
    <thead>
        <tr>
            <th>Nome</th>
            <th>Obrigatório</th>
            <th>Descrição</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>limit</td>
            <td>Não</td>
            <td>Número limite de objetos retornados. <b>O valor deve ser entre 1 e 100.</b></td>
        </tr>
        <tr>
            <td>starting_after</td>
            <td>Não</td>
            <td>Um cursor para uso em paginação. {starting_after} é o identificador único do objeto a partir do qual se quer listar. 
        Por exemplo, se houve um retorno de uma lista de 100 objetos para esta chamada sendo que o último possui identificador "obj1234", para se obter a página use "starting_after=obj1234".</td>
        </tr>
        <tr>
            <td>ending_before</td>
            <td>Não</td>
            <td>Um cursor para uso em paginação. {ending_before} é o identificador único do objeto a partir do qual se quer listar os anteriores. 
        Por exemplo, se houve um retorno de uma lista de 100 objetos para esta chamada sendo que o primeiro possui identificador "obj1234", para se obter a página anterior use "ending_before=obj1234".</td>
        </tr>
        <tr>
            <td>dispute_reason</td>
            <td>Não</td>
            <td>Código do motivo das disputas a serem retornadas.</td>
        </tr>
        <tr>
            <td>dispute_status</td>
            <td>Não</td>
            <td>Status das disputas a serem retornadas.</td>
        </tr>
        <tr>
            <td>beginning_date</td>
            <td>Sim</td>
            <td>Data indicando o primeiro dia cujos dados devem ser retornados. <b>Deve ser uma data no formato yyyy-MM-DD. Exemplo: "2020-07-09"</b></td>
        </tr>
        <tr>
            <td>ending_date</td>
            <td>Não</td>
            <td>Data indicando o último dia cujos dados devem ser retornados. <b>Deve ser uma data no formato yyyy-MM-DD. Exemplo: "2020-07-09"</b></td>
        </tr>
    </tbody>
</table>

### **Exemplos de Requisição**

#### Caso de Sucesso

```
curl -X POST https://api.paysmart.com.br/paySmart/ps-processadora/v1/disputes?limit=2&beginning_date=2020-11-17&ending_date=2021-05-05&dispute_status=WAITING_FOR_RESPONSE_FROM_ACQUIRER&dispute_reason=81
```

#### Caso de Erro

```
curl -X POST https://api.paysmart.com.br/paySmart/ps-processadora/v1/disputes?limit=2
```

### **Exemplos de Resposta**

#### Caso de Sucesso

```json
{
    "resultData": {
        "psResponseId": "c5cc9a45-15a0-47b1-ba11-3cebd473883e",
        "resultCode": 0,
        "resultDescription": "Disputa encontrada com sucesso!"
    },
    "hasMore": "true",
    "disputes": [
        {
            "currentStage": "chargeback",
            "disputeMaxDateResponse": "Aguardando processamento na Elo.",
            "disputeId": "DIS-2396adf9-7c51-434b-97e4-356a924b2e29",
            "disputeStatus": "waiting_for_response_from_acquirer",
            "transaction": {
                "transactionSource": "brand",
                "fees": [],
                "creatingSystemName": "Autorizador ISO8583",
                "psProductName": "ISSUER",
                "terminalId": "20172289",
                "ISO8583MessageRequest": {
                    "de011": "273257",
                    "de022": "011",
                    "de032": "0025",
                    "de043": "ELO                    BARUERI       076",
                    "de042": "020001605270002",
                    "mti": "0100",
                    "de041": "20172289",
                    "de060": "000110000P500",
                    "de019": "076",
                    "de018": "5712",
                    "de007": "0427183257",
                    "de127": "12182",
                    "de049": "986",
                    "de126": "*****",
                    "de037": "119357719326",
                    "de004": "000000002000",
                    "de048": "*CDT002T0*PRD003070",
                    "de014": "2412",
                    "de058": "ALAMEDAXINGU          23543230007612345ELO                  ",
                    "de003": "003000",
                    "de013": "0427",
                    "de002": "************1585",
                    "de024": "100",
                    "de012": "153257"
                },
                "mcc": "5712",
                "transactionTime": "15:32:57",
                "merchantName": "ELO",
                "issuerId": "3",
                "merchantId": "020001605270002",
                "countryCode": "076",
                "disputeStatus": "chargeback",
                "international": false,
                "acquirerId": "0025",
                "transactionAuthorizationResponse": {
                    "approved": true
                },
                "entryMode": "manual",
                "psProductCode": "030102",
                "amount": {
                    "amount": {
                        "amount": 2000,
                        "currencyCode": 986
                    },
                    "debit_or_credit": "debit"
                },
                "merchantCity": "BARUERI",
                "acquirerTransactionId": "119357719326",
                "transactionStatus": "approved",
                "freeDescription": "Nova transação de compra registrada!",
                "transactionReceivedDateTime": "2021-04-27T18:32:58.051Z",
                "incremental": false,
                "transactionDate": "2021-04-27",
                "transactionId": "aut_8242fdd0-d815-4ddf-a8bd-cb531d3b00a3",
                "transactionType": "authorized_transaction",
                "cardBrandId": "Elo",
                "accountId": "cta-8acbd197-a061-486c-af38-2661256161eb",
                "forceAccept": false,
                "statusChangeHistory": [
                    {
                        "callingSystemName": "Autorizador ISO8583",
                        "stateChangeDescription": "Nova transação registrada.",
                        "stateChangeReasonCode": "other",
                        "newStatus": "pending",
                        "changeRequestReceivedDate": "2021-04-27T18:32:58.051Z",
                        "statusChangeSource": "brand"
                    }
                ],
                "disputeId": "DIS-2396adf9-7c51-434b-97e4-356a924b2e29",
                "merchantAddress": "ALAMEDAXINGU",
                "merchantZipcode": "235432300",
                "card": {
                    "BIN": "******",
                    "cardId": "crt-f4de6c2a-f501-482e-a353-a88f6a4b3aa3",
                    "last_four_digits": "1585",
                    "cardIdIssuer": "fdb5d5ea-2d5d-4937-865e-933fe46b3d3f"
                }
            }
        },
        {
            "currentStage": "chargeback",
            "disputeMaxDateResponse": "Aguardando processamento na Elo.",
            "disputeId": "DIS-3a3fa2a5-ccc0-4a24-8588-eea0eb5c02d1",
            "disputeStatus": "waiting_for_response_from_acquirer",
            "transaction": {
                "transactionSource": "brand",
                "fees": [],
                "creatingSystemName": "Autorizador ISO8583",
                "psProductName": "ISSUER",
                "terminalId": "20172289",
                "ISO8583MessageRequest": {
                    "de011": "305925",
                    "de022": "011",
                    "de032": "0025",
                    "de043": "ELO                    BARUERI       076",
                    "de042": "020001605270002",
                    "mti": "0100",
                    "de041": "20172289",
                    "de060": "000110000P500",
                    "de019": "076",
                    "de018": "5712",
                    "de007": "0430135925",
                    "de127": "12182",
                    "de049": "986",
                    "de126": "*****",
                    "de037": "136089172591",
                    "de004": "000000007000",
                    "de048": "*CDT002T0*PRD003070",
                    "de014": "2412",
                    "de058": "ALAMEDAXINGU          23543230007612345ELO                  ",
                    "de003": "003000",
                    "de013": "0430",
                    "de002": "************1585",
                    "de024": "100",
                    "de012": "105925"
                },
                "mcc": "5712",
                "transactionTime": "10:59:25",
                "merchantName": "ELO",
                "issuerId": "3",
                "merchantId": "020001605270002",
                "countryCode": "076",
                "disputeStatus": "chargeback",
                "international": false,
                "acquirerId": "0025",
                "transactionAuthorizationResponse": {
                    "approved": true
                },
                "entryMode": "manual",
                "psProductCode": "030102",
                "amount": {
                    "amount": {
                        "amount": 7000,
                        "currencyCode": 986
                    },
                    "debit_or_credit": "debit"
                },
                "merchantCity": "BARUERI",
                "acquirerTransactionId": "136089172591",
                "transactionStatus": "approved",
                "freeDescription": "Nova transação de compra registrada!",
                "transactionReceivedDateTime": "2021-04-30T13:59:25.841Z",
                "incremental": false,
                "transactionDate": "2021-04-30",
                "transactionId": "aut_1454fc73-66e8-4926-96e9-7b1cdd7bf7e8",
                "transactionType": "authorized_transaction",
                "cardBrandId": "Elo",
                "accountId": "cta-8acbd197-a061-486c-af38-2661156161eb",
                "forceAccept": false,
                "statusChangeHistory": [
                    {
                        "callingSystemName": "Autorizador ISO8583",
                        "stateChangeDescription": "Nova transação registrada.",
                        "stateChangeReasonCode": "other",
                        "newStatus": "pending",
                        "changeRequestReceivedDate": "2021-04-30T13:59:25.841Z",
                        "statusChangeSource": "brand"
                    }
                ],
                "disputeId": "DIS-3a3fa2a5-ccc0-4a24-8588-eea0eb5c02d1",
                "merchantAddress": "ALAMEDAXINGU",
                "merchantZipcode": "235432300",
                "card": {
                    "BIN": "******",
                    "cardId": "crt-f4de6c2a-f501-482e-a353-a88f6a4b3aa3",
                    "last_four_digits": "1585",
                    "cardIdIssuer": "fdb5d5ea-2d5d-4937-865e-933fe46b3d3f"
                }
            }
        }
    ]
}
```

#### Caso de Erro

```json
{
    "resultData": {
        "resultCode": 982,
        "resultDescription": "Nenhuma disputa encontrada com os parâmetros de busca utilizados.",
        "psResponseId": "5c4d3c1e-e9d2-493c-a485-8ae8073a66f4"
    }
}
```

### **Descrição da Resposta**

<table>
    <thead>
        <tr>
            <th>Nome</th>
            <th>Tipo</th>
            <th>Descrição</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>hasMore</td>
            <td>Boolean</td>
            <td>Indica que atingiu o limite de retorno e existem mais elementos a serem retornados.</td>
        </tr>
        <tr>
            <td>disputes</td>
            <td>Array</td>
            <td>Lista de disputas encontradas.</td>
        </tr>
        <tr>
            <td>- currentStage</td>
            <td>String</td>
            <td>Estágio atual do processo de disputa. Retorna um dos seguintes valores: chargeback, restatement, pre-arbitration, arbitration, lost, successful, withdrawal, undone.</td>
        </tr>
        <tr>
            <td>- disputeMaxDateResponse</td>
            <td>String</td>
            <td>Data máxima para uma resposta no formato Iso-8601, por exemplo 2021-06-20T00:00:00.000Z. Retorna "Aguardando processamento na Elo" caso a disputa tenha sido criado com sucesso na API, porém ainda não foi enviada na liquidação para a bandeira.</td>
        </tr>
        <tr>
            <td>- disputeId</td>
            <td>String</td>
            <td>Identificador único da disputa na paySmart.</td>
        </tr>
        <tr>
            <td>- disputeStatus</td>
            <td>String</td>
            <td>Estado atual da disputa. Serve para indicar qual lado da disputa deve realizar a próxima ação. Retorna um dos seguintes valores: waiting_for_response_from_issuer,  waiting_for_response_from_acquirer, waiting_for_response_from_brand, accepted, denied, issuer_won, issuer_lost, canceled_by_issuer.</td>
        </tr>
        <tr>
            <td>- transaction</td>
            <td>Objeto</td>
            <td>Dados da transação sendo disputada.</td>
        </tr>
        <tr>
            <td>resultData</td>
            <td>Objeto</td>
            <td>Dados da resposta da API.</td>
        </tr>
        <tr>
            <td>- resultCode</td>
            <td>Integer</td>
            <td>Código de resultado do processamento.</td>
        </tr>
        <tr>
            <td>- resultDescription</td>
            <td>String</td>
            <td>Descrição textual do resultado do processamento.</td>
        </tr>
        <tr>
            <td>- psResponseID</td>
            <td>String</td>
            <td>Identificador único da resposta. Gerado pela paySmart.</td>
        </tr>
    </tbody>
</table>

{% include endpoint_footer.html %}