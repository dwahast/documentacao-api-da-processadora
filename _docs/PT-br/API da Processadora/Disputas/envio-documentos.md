---
title: Envio de Documentos para a Disputa
name: ""
link: "send-docs"
type: info
tags:
    - disputas
    - documentos de disputa
description: "Processo de envio de documentos para dar suporte à disputa."
permalink: /pt-br/docs/disputes/send-docs
language: pt-br
---
{% include info_header.html %}

- Se ele tiver algum documento pra anexar, pedir para enviar (o emissor precisa ter um endereço pra receber).
- Por último, dependendo do motivo alegado, informar que o banco pode solicitar o envio de documentos comprobatórios.