---
title: Códigos de Erro da API de Disputas
name: ""
link: "errors-code"
type: info
tags:
    - disputas
    - códigos erro
description: "Possíveis códigos de erro retornados pela API de Disputas."
permalink: /pt-br/docs/disputes/errors-code
language: pt-br
---
{%include info_header.html%}

<table>
    <thead>
        <tr>
            <th>Código</th>
            <th>Mensagem de Erro</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>47</td>
            <td>Não foram encontrados dados para a conta informada.</td>
        </tr>
        <tr>
            <td>77</td>
            <td>Campo obrigatório não informado.</td>
        </tr>
        <tr>
            <td>125</td>
            <td>Transação não encontrada.</td>
        </tr>
        <tr>
            <td>972</td>
            <td>O prazo limite para criação ou resposta da disputa já terminou.</td>
        </tr>
        <tr>
            <td>974</td>
            <td>Não é possível criar uma disputa para cashbacks.</td>
        </tr>
        <tr>
            <td>975</td>
            <td>Não é possível criar uma disputa para transações realizadas com chip.</td>
        </tr>
        <tr>
            <td>976</td>
            <td>Campo 'amount_disputed' obrigatório para casos de disputa de valor parcial.</td>
        </tr>
        <tr>
            <td>982</td>
            <td>Nenhuma disputa encontrada com os parâmetros de busca utilizados.</td>
        </tr>
        <tr>
            <td>983</td>
            <td>Disputa não está na etapa de reapresentação.</td>
        </tr>
        <tr>
            <td>984</td>
            <td>Disputa não encontrada.</td>
        </tr>
        <tr>
            <td>985</td>
            <td>Transação inclusiva inicial (TE-05 ou TE-06) não existe para essa disputa.</td>
        </tr>
        <tr>
            <td>986</td>
            <td>Disputa não pode ser desfeita. Emissor deve contatar o Credenciador para solucionar a questão via TE10/TE20.</td>
        </tr>
        <tr>
            <td>987</td>
            <td>Valor parcial não pode ser maior ou igual ao valor total da transação.</td>
        </tr>
        <tr>
            <td>988</td>
            <td>Configurações para a razão da disputa não existente.</td>
        </tr>
        <tr>
            <td>989</td>
            <td>Não foi possível encontrar alguns parâmetros da transação.</td>
        </tr>
        <tr>
            <td>990</td>
            <td>Transação a ser disputada não foi confirmada na liquidação, tente novamente nos próximos dias.</td>
        </tr>
        <tr>
            <td>991</td>
            <td>Transação a ser disputada está cancelada.</td>
        </tr>
        <tr>
            <td>992</td>
            <td>Disputa já existe para essa transação.</td>
        </tr>
        <tr>
            <td>996</td>
            <td>Não foi possível encontrar configurações para o emissor representado pela API-Key informada</td>
        </tr>
        <tr>
            <td>999</td>
            <td>Erro Interno</td>
        </tr>
    </tbody>
</table>