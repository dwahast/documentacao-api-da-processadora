---
title: Códigos de Motivo de Transação
name: ""
link: "transactions-code"
type: info
tags:
    - inclusivas
    - criação inclusivas
    - códigos transação
description: "Códigos para informar o motivo de ajuste financeiro de disputa."
permalink: /pt-br/docs/disputes/transactions-code
language: pt-br
---
{%include info_header.html%}

<table>
    <thead>
        <tr>
            <th>Código</th>
            <th>Mensagens de Motivo</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>01</td>
            <td>Transações rejeitadas.</td>
        </tr>
        <tr>
            <td>02</td>
            <td>Ajuste por falhas operacionais parceiro com fluxo financeiro.</td>
        </tr>
        <tr>
            <td>03</td>
            <td>Ajuste por falhas operacionais parceiro sem fluxo financeiro.</td>
        </tr>
        <tr>
            <td>04</td>
            <td>Acordo amigável.</td>
        </tr>
        <tr>
            <td>05</td>
            <td>Exceção.</td>
        </tr>
        <tr>
            <td>08</td>
            <td>Decisão de caso de Arbitragem ou taxa de depósito.</td>
        </tr>
        <tr>
            <td>10</td>
            <td>Liquidação de desembolso de fundos de préconformidade.</td>
        </tr>
    </tbody>
</table>