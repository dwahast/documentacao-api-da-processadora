---
title: Códigos de Motivo de Disputa
name: ""
link: "codes"
type: info
tags:
    - disputas
    - criação disputa
    - códigos disputa
description: "Códigos para informar o motivo da disputa no request de criação."
permalink: /pt-br/docs/disputes/codes
language: pt-br
---
{%include info_header.html%}

<table>
    <thead>
        <tr>
            <th>Código</th>
            <th>Mensagens de Disputa (Separadas por |)</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>30</td>
            <td>Serviço não prestado ou Mercadoria não recebida.</td>
        </tr>
        <tr>
            <td>41</td>
            <td>Transação recorrente cancelada.</td>
        </tr>
        <tr>
            <td>53</td>
            <td>Mercadoria com defeito ou em desacordo.</td>
        </tr>
        <tr>
            <td>57</td>
            <td>Múltiplas transações fraudulentas.</td>
        </tr>
        <tr>
            <td>60</td>
            <td>Documento ilegível.</td>
        </tr>
        <tr>
            <td>62</td>
            <td>Transação falsificada (Transferência de responsabilidade do chip).</td>
        </tr>
        <tr>
            <td>71</td>
            <td>Autorização negada.</td>
        </tr>
        <tr>
            <td>72</td>
            <td>Sem autorização.</td>
        </tr>
        <tr>
            <td>73</td>
            <td>Cartão vencido.</td>
        </tr>
        <tr>
            <td>74</td>
            <td>Apresentação tardia.</td>
        </tr>
        <tr>
            <td>75</td>
            <td>Portador não se lembra da transação.</td>
        </tr>
        <tr>
            <td>77</td>
            <td>Número de cartão inexistente.</td>
        </tr>
        <tr>
            <td>80</td>
            <td>Valor da transação ou número de cartão incorreto.</td>
        </tr>
        <tr>
            <td>81</td>
            <td>Fraude em ambiente de cartão presente.</td>
        </tr>
        <tr>
            <td>82</td>
            <td>Duplicidade de processamento.</td>
        </tr>
        <tr>
            <td>83</td>
            <td>Fraude em ambiente de cartão não presente.</td>
        </tr>
        <tr>
            <td>85</td>
            <td>Crédito não processado.</td>
        </tr>
        <tr>
            <td>86</td>
            <td>Pagamentos por outros meios</td>
        </tr>
    </tbody>
</table>
