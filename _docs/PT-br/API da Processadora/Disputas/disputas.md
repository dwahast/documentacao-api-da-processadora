---
title: Processo de Disputas
name: disputes
tags: 
 - disputes
description: Operação de disputa quando em caso de transações não reconhecida pelo usuário.
permalink: /pt-br/docs/disputes
language: pt-br
---

# **{{page.title}}**

1. [Introdução](#introdução)
1. [Processo de Disputas](#processo-de-disputas)
    1. [Disputas por Motivo de Fraude](#disputas-por-motivo-de-fraude)
1. [Homologação da API de Disputas](#homologação-da-api-de-disputas)
1. [Possíveis Retornos](#possíveis-retornos)
1. [Links Úteis](#outros-links-úteis)

## **Introdução**

O processo de disputas deve ser iniciado se uma transação é dada como não reconhecida pelo usuário e este solicita a contestação. Para transações **já liquidadas**, este processo é feito junto à bandeira em múltiplas etapas. Porém, antes de dar início ao processo, é necessário algumas validações por parte do emissor.

Dentre essas validações, a bandeira sempre fornece certas orientações de procedimentos genéricos que devem ser feitos previamente à abertura da disputa:
- Em primeiro lugar, insistir com o cliente sobre a transação. Isso pode ser feito confirmando dados como o nome do estabelecimento ou endereço, visando certificar que o portador não se lembra da compra;
- Em caso de uma transação com senha, perguntar se algum familiar ou pessoa próxima pode ter tido acesso ao cartão;
- Por fim, avisar para o portador que, caso a transação seja comprovada como dele mesmo, poderá incorrer em multa por parte da bandeira.

Dado essas orientações, se a resposta for positiva dá-se início ao processo de disputa. 

## **Processo de Disputas**

A criação e o acompanhameto das disputas podem ser feitos através dos seguintes endpoints:

<ul>
    {% assign endpoints=site.docs | where: "type", "disputes" %}
    {% for endpoint in endpoints %}
    <li><a href="{{site.baseurl}}/pt-br/docs/{{endpoint.type}}/{{endpoint.link}}">
        {% if endpoint.tags contains "endpoint" %}{{endpoint.method}} /{{page.name}}/{{endpoint.name}}{% else %}{{endpoint.title}}{% endif %}
    </a></li>
    {% endfor %}
</ul>

Resumidamente, o processo de disputas se dará da seguinte forma:

1. Ao decidir criar uma disputa, o emissor deverá mandar uma requisição contendo os dados necessários para o primeiro endpoint informado, <a href="{{site.baseurl}}/pt-br/docs/disputes/add-dispute">POST /disputes</a>.
2. Uma vez que a disputa tenha sido criada com sucesso, o acompanhamento de seu status pode ser feito através do endpoint <a href="{{site.baseurl}}/pt-br/docs/disputes/get-dispute">GET /disputes/{disputeId}</a>. Caso deseje-se acompanhar múltiplas disputas simultaneamente, as requisições devem ser enviadas para o endpoint <a href="{{site.baseurl}}/pt-br/docs/disputes/find-disputes">GET /disputes</a>.
3. Caso a credenciadora tenha dado um parecer negativo para o pedido inicial de disputa, o estágio da disputa será atualizado, passando de "chargeback" para "restatement". Nesse cenário, o emissor deve enviar uma requisição para o endpoint <a href="{{site.baseurl}}/pt-br/docs/disputes/respond-dispute">POST /disputes/{disputeId}/response</a> informando se deseja ou não dar continuidade à disputa. Se optar pela continuação, a disputa entrará no estágio de pré-arbitragem, "pre-arbitration".
4. Enviado o pedido de pré-arbitragem para o credenciador, exitem dois resultados possíveis. No primeiro, o credenciador acata o pedido de disputa, reembolsando o valor enviado. No segundo caso, o credenciador pode discordar da disputa, afirmando que a transação foi feita de forma correta. Nesse cenário, ele irá requisitar a arbitragem por parte da bandeira, encaminhando o processo de disputa para esse estágio, "arbitration".
5. A partir deste ponto, cabe à bandeira decidir qual lado venceu a disputa, informando o credenciador de sua decisão. Portanto, não é necessário o envio de nenhuma outra requisição com informações para a API de Disputas por parte do emissor, somente requests de acompanhamento de status, como informado no tópico 2.
6. Alternativamente, caso haja a necessidade de cancelamento da disputa - seja por reconhecimento da compra por parte do portador ou por qualquer outro motivo - deve-se enviar uma requisição para o endpoint <a href="{{site.baseurl}}/pt-br/docs/disputes/undo-dispute">POST /disputes/{disputeId}/undo</a>, sinalizando para a paySmart e para o credenciador a desistência do processo. Importante notar que o desfazimento por meio deste endpoint só pode ser feito nas etapas de Chargeback ("chargeback") ou Reapresentação ("restatement"). Em estágios posteriores, o emissor deve contatar o credenciador para solucionar a questão via TE10/TE20.

### **Disputas por motivo de fraude**
No momento de criação de uma disputa, existe o campo *``"fraudType"``*. O valor informado neste campo só será validado nas disputas que forem abertas por motivo de fraude, ou seja, com o campo *``"disputeCode"``* com o valor **81** ou **83**, conforme previsto na tabela de <a href="{{ site.baseurl }}/pt-br/docs/disputes/codes">Códigos de Motivo de Disputa</a>

## **Homologação da API de Disputas**
Durante a homologação dos endpoints de disputa, é necessário acionar sempre o analista da paySmart, para realização do acompanhamento em relação às mudanças de status da disputa, devido ao fato do envolvimento de processos como liquidação, comunicação com credenciador e comunicação com a bandeira.

## **Possíveis retornos**

### ***``"disputeStatus"``***:

* waiting_for_response_from_issuer
* waiting_for_response_from_acquirer
* waiting_for_response_from_brand
* accepted
* denied
* canceled_by_issuer
* rejected_by_brand
* issuer_won
* issuer_lost

### ***``"currentStage"``***

| Valor | Descrição |
|-------|-----------|
| *``"chargeback"``* | Estado inicial |
| *``"restatement"``* | Devolutiva negativa da bandeira, o emissor deve escolher se quer continuar com a disputa ou não |
| *``"pre-arbitration"``* | Processo que é feito entre emissor e credenciador, para assim caso aceito e seguido adiante, começar o processo de arbitragem com a bandeira |
| *``"arbitration"``* | Fase em que a ELO faz a análise do pedido de arbitragem |
| *``"lost"``* | Processo de disputa perdido |
| *``"undone"``* | Desiste do chargeback |
| *``"successful"``* | Disputa aprovada e estorno enviado **(EM DESENVOLVIMENTO)** |
| *``"withdrawal"``* | Quando desiste do restatement |
| *``"rejected_by_brand"``* | Criação de disputa rejeitada pela bandeira |

## **Links Úteis**

<ul>
    {% assign endpoints=site.docs | where: "type", "info" %}
    {% for endpoint in endpoints %}
        {% if endpoint.language == "pt-br" %}
        <li><a href="{{site.baseurl}}/pt-br/docs/disputes/{{endpoint.link}}">
            {{endpoint.title}}
        </a></li>
        {% endif %}
    {% endfor %}
</ul>