---
title: Gerenciamento de Cartões
description: Dados sobre o gerenciamento de cartões
tags: 
- cartões
- cartão físico
- cartão virtual
permalink: /pt-br/cartoes/gerenciamento
---

# **Gerenciamento de cartões**

1. [Bloqueio e Desbloqueio de Cartões](#bloqueio-e-desbloqueio-de-cartões)
1. [Matriz de Bloqueio de Cartões](#matriz-de-bloqueio-de-cartões)
1. [Observações](#observações)

## **Bloqueio e Desbloqueio de Cartões**

Um dos passos do ciclo de gerenciamento de cartões é a possibilidade de determinar se ele está apto para realizar transações. Ao ser impresso, um cartão é configurado com o status ***BLOCKED***, ou seja, não está habilitado para realizar transações. Ao chegar às mãos do usuário final, ele poderá ser desbloqueado através do endpoint [POST cards/{cardId}/unblockCardRequest]({{ site.url }}{{ site.baseurl }}/pt-br/processor-api), onde **cardId** é o identificador único do cartão retornado no momento de sua solicitação. O único dado mandatório a ser enviado no corpo JSON da requisição é o código de desbloqueio `"unblockCode"`, que será sempre **"0"** para casos de ativação e reativação. Também é recomendado enviar o campo contendo a descrição do motivo de desbloqueio (`"reason"`), para facilitar o rastreamento dos eventos. Com isso, uma mensagem mínima de desbloqueio ficaria:

```json
{
  "unblockCode": 0,
  "reason": "Cartão recebido."
}
```
O bloqueio também pode ser feito a qualquer momento, através do endpoint [POST cards/{cardId}/blockCardRequest]({{ site.url }}{{ site.baseurl }}/pt-br/processor-api). Neste caso, os códigos de bloqueio devem ser tratados caso a caso, através de interação com o usuário para o fornecimento do motivo do bloqueio. Isso ocorre porque, dependendo do código informado, um futuro desbloqueio ou mesmo a reemissão do cartão não será permitida. 
 
Em casos de bloqueio preventivo, por exemplo, o código **"15"**, permite que um desbloqueio (código **"0"**) posterior seja feito. No caso de roubo, mapeado pelo código **"36"**, o desbloqueio não será permitido. 
 
O corpo da requisição de desbloqueio ficará:
```json
{
  "blockCode": 36,
  "reason": "Cartão roubado."
}
```

## **Matriz de Bloqueio de Cartões**
Na tabela abaixo, temos a lista de códigos de bloqueio, com sua descrição e comportamento esperado:

<div class="datatable-begin"></div>

Código  | Status                                        | Transita para                                                   | Permite Reemissão | Descrição do motivo 
------- | --------------------------------------------- | --------------------------------------------------------------- | ----------------- | -------------------
0       | Ativo                                         | 10,12,15,16,17,18,22,30,31,32,33,34,35,36,37,75,90,91,92,93,94  | Não               | Ativo (Sem Bloqueio)
1       | Bloqueio de Envio                             | 0,10,20,21,22,35,36,75,90,94                                    | Não               | Cartão bloqueado durante o processo de geração até a ativação pelo titular
10      | Problema no Embossing                         | 90                                                              | Sim               | Cartão apresenta algum erro de embossing que permite continuar utilização até que seja substituído
12      | Pedido de Nova Via                            | 90                                                              | Sim               | Cartão foi bloqueado por algum processo manual (ex.: interface do emissor) que solicitou a geração de uma nova via para o cartão. Via anterior permanece válida até que a nova via seja ativada
15      | Bloqueado por Renovação                       | 90                                                              | Sim               | Cartão foi bloqueado por processo automatizado que solicitou a geração de uma nova via para o cartão. Via anterior permanece válida até que a nova via seja ativada
16*     | Bloqueado por erro de senha                   | 0,90,92,93,94                                                   | Sim               | Cartão foi bloqueado no autorizador por ter excedido o número de tentativas de erro de senha. Poderá ser desbloqueado pelo emissor.
17      | Outros motivos - bloqueio temporário          | 0,90,92,93,94                                                   | Sim               | Cartão bloqueado em modo temporário por motivo diferente de renovação, pedido de segunda via, embossing etc.
18      | Bloqueio de Segurança                         | 0,35,36,90                                                      | Não               | Cartão bloqueado preventivamente pelo emissor por motivo de segurança. Poderá ser desbloqueado e liberado para uso após validações
20      | Insucesso ou Extravio na Entrega (Individual) | 90                                                              | Sim               | Cartão dado como extraviado durante o processo de entrega. Mesmo que encontrado, não mais poderá ser utilizado.
21      | Insucesso ou Extravio na Entrega              | 0,1,90                                                          | Sim               | Cartão dado como extraviado durante o processo de entrega.
22      | Extraviado Usuário                            | 90                                                              | Sim               | Cartão dado como extraviado pelo seu usuário ou titular. Mesmo que encontrado pelo titular, não mais poderá ser utilizado
30      | Bloqueio Preventivo de Fraude                 | 0,90,92                                                         | Sim               | Cartão bloqueado preventivamente pelo emissor, por suspeição de fraude. Poderá ser desbloqueado e liberado para uso apóss confirmação de uso e posse pelo titular
31      | Fraude - Suspeita de Fraude                   | 32,33,34                                                         | Sim               | Cartão identificado como tendo sido utilizado em situação suspeita, pelo seu titular ou por algum tipo de análise pelo emissor
32      | Fraude - Conivência com o Estabelecimento     | 90,92                                                           | Não               | Cartão identificado como envolvido em utilização fraudulenta, em conivência com algum estabelecimento também envolvido em atividade fraudulenta.
33      | Fraude - Autofraude                           | 90,92                                                           | Não               | Cartão adulterado em alguma de suas características pelo próprio titular
34      | Fraude - Falsificado                          | 90,92                                                           | Sim               | Cartão falsificado ou adulterado
35      | Perda                                         | 90                                                              | Não               | Cartão bloqueado por motivo de perda alegada pelo titular
36      | Roubo                                         | 90                                                              | Não               | Cartão bloqueado por motivo de roubo alegado pelo titular
37      | Outros motivos - bloqueio definitivo          | 90                                                              | Sim               | Cartão bloqueado em modo definitivo por motivo diferente de perda, roubo, extravio etc. 
75      | Cartão Expurgado                              | -                                                               | Não               | Cartão excluido da base por processo automático que analisa condições contratuais, de utilização, saldo, inatividade, etc
90      | Conta encerrada                               | 75                                                              | Não               | Cartão cancelado por motivo de encerramento da conta.
91      | Cancelado por cobrança                        | -                                                               | Não               | Cartão cancelado por motivo de encerramento da conta por motivo de cobrança.
92      | Cancelado - a pedido do emissor               | -                                                               | Não               | Cartão cancelado por motivo de encerramento da conta a pedido do emissor.
93      | Cancelado - a pedido do usuário               | -                                                               | Não               | Cartão cancelado por motivo de encerramento da conta a pedido do portador.
94      | Titular Falecido                              | -                                                               | Não               | Cartão cancelado por motivo de encerramento da conta por falecimento do titular da conta.

<div class="datatable-end"></div>

### **Observações**

>**Obs. 1:**
>
>Os códigos que apresentam '-' são códigos de estágio final.

>**Obs. 2:**
>
>A lista de transições de bloqueio apresentada é a recomendada para uso. Caso necessário poderão ser adicionados novos códigos de transições para cada bloqueio.

> **Obs. 3:** 
>
>**16***: Quando o cartão estiver bloqueado pelo código **16** (Bloqueado por erro de senha), o desbloqueio deverá ser realizado por meio do endpoint [POST /cards/{cardId}/changePin]({{ site.url }}{{ site.baseurl }}/pt-br/processor-api).
>
> Ou seja, deve ser realizada a troca de senha do cartão, o que automaticamente realizará o seu desbloqueio.
>
> Nesse caso, mesmo que o endpoint [POST /cards/{cardId}/unblockCardRequest]({{ site.url }}{{ site.baseurl }}/pt-br/processor-api) retorne um resultado positivo, o cartão continuará bloqueado até que seja realizada a troca de senha do mesmo.