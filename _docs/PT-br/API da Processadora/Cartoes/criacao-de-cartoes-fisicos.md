---
title: Criação de Cartões Físicos
tags: 
 - cartões
 - cartões anônimos
description: Operação em alto nível para a criação de cartões físicos na processadora paySmart
permalink: /pt-br/cartoes/fisicos/criacao
---

# **Criação de Cartões Físicos**

1. [Introdução](#introdução)
2. [Criação de Cartões](#criação-de-cartões)
    1. [Cartões Nomeados](#cartões-nomeados)
        1. [Requisição](#requisição)
        1. [Parâmetros Opcionais](#parâmetros-opcionais)
    2. [Cartões Anônimos](#cartões-anônimos)
        1. [Requisição](#requisição)
    1. [Resposta](#resposta)


## **Introdução**

Um cartão é um método de *cashout* que permite, através de um plástico equipado com *chip* EMV, tarja magnética e dados numéricos de validação, realizar transações que movimentam o saldo de determinada conta através de operações em todas as redes de adquirência capazes de aceitar esse formato de pagamento. 

## **Criação de Cartões**

Há dois grandes fluxos independentes para a criação de cartões físicos, consistindo em cartões nomeados e cartões anônimos:

### **Cartões Nomeados** 

Um cartão desse formato é um cartão que terá o nome do portador gravado no plástico e é associado a uma conta, criada previamente. 

A solicitação do cartão nomeado é realizada por meio do método [POST accounts/{accountId}/newCardRequest]({{ site.url }}{{ site.baseurl }}/pt-br/processor-api) onde *`{accountId}`* é o identificador da conta, que deve ter sido criada, anteriormente, conforme o processo descrito em [Criação de Contas]({{ site.url }}{{ site.baseurl }}/docs/contas).

Os campos mandatórios para solicitar um cartão físico nomeado através deste método são:

| Campo           | Descrição                                                                                           |
|-----------------|-----------------------------------------------------------------------------------------------------|
| *`"cardholder"`* | Portador: agrega os dados para identificar de maneira inequívoca o portador de um cartão.                                                                                |
| *`"cardholder"."cardholderType"`* | Tipo do portador: informa se o portador do cartão é o titular da conta (tipo *``"main"``*) ou seu dependente (tipo *``"additional"``*)                                                       |
| *``"cardholder"."fullName"``* | Nome completo: nome civil do portador                                                     |
| *``"cardholder"."cardData"."embossingName"``*           | Nome de embossing: um nome para *embossing* ou gravaçao no cartão |
| *``"cardholder"."identityDocumentNumber"``* | Documento de identidade: número do documento que identifica o portador em órgãos oficiais, normalmente um CPF ou um CNPJ |                                        
| *``"cardholder"."birthDate"``* | Data de nascimento: data de nascimento do portador, necessária conforme regulamento do Banco Central  |
| *``"cardholder"."nationality"``* | Nacionalidade: país de nascimento do portador |

#### **Requisição**

Como ocorre de maneira sistemática nas requisições da API da processadora, as informações são passadas como corpo da requisição HTTP em [POST accounts/{accountId}/newCardRequest]({{ site.url }}{{ site.baseurl }}/pt-br/processor-api). 

Uma requisição mínima para a criação de um cartão ficaria, então, no seguinte formato:

```json
{
  "cardholder": {
    "cardholderType": "main",
    "fullName": "Fulano da Silva",
    "cardData": {
      "embossingName": "FULANO DA SILVA"
    },
    "identityDocumentNumber": "03873703805"
    "birthDate": "21/03/1983",
    "nationality": "Brasil"
  }
}
```
#### **Parâmetros Opcionais**

##### ***``"cardDeliveryAddress"``***

Uma informação opcional, mas que pode ganhar grande relevância de acordo com as regras de negócio, é o parâmetro *``"cardDeliveryAddress"``*. 

Esse parâmetro permite sobrescrever o endereço de entrega passado na criação da conta, permitindo que diferentes portadores recebam seus cartões em diferentes endereços.

##### ***``"deliveryKitCode"`` (DKC)***

Caso o emissor possua vários *layouts* ou *card designs* diferentes para os seus cartões, é possível identificá-los por meio do campo *``"deliveryKitCode"``*. 

Para tal, o emissor deve acordar com a respectiva gráfica a utilização de um código numérico de 6 dígitos para identificar cada *card design*.

Para a paySmart, os *DKCs* são transparentes e não influenciam no processo de geração dos cartões. Essa definição é importante a nível de negociação entre emissor e gráfica.

##### ***``"bureaxId"``***

Caso o emissor teha interesse em emitir cartões com mais de uma gráfica, pode-se utilizar este campo para identificar o destino do cartão. Caso este campo não seja enviado, o cartão será encaminhado para a gráfica padrão.

>**Atenção**
>
>Caso o emissor queira utilizar multigráficas, deverá entrar em contato com o analista responsável pelo projeto na paySmart e informar quais as gráficas desejadas e qual deverá ser considerada a padrão.

### **Cartões Anônimos**

Um cartão desse formato é um cartão que **não** terá o nome do portador gravado no plástico e **não** é associado a uma conta. 

Um cartão é criado de maneira anônima, através do método [POST cards/requestNewAnonymousCard]({{ site.url }}{{ site.baseurl }}/pt-br/processor-api), sem vínculo a uma conta. 

Esse cartão poderá ser associado a um portador (que passa também a ser titular de uma conta vinculada exclusivamente àquele cartão) em um momento futuro, como sua aquisição em uma rede de varejo, por exemplo. 

#### **Requisição**

A solicitação de um cartão anônimo requer apenas o código do produto *``"psProductCode"``* e um endereço de entrega *``"cardDeliveryAddress"``* como campos mandatórios. Portanto, um corpo simples como o ilustrado abaixo já seria uma solicitação válida para um cartão anônimo.

```json
{
  "psProductCode": "020101",
  "cardDeliveryAddress": {
    "addressLine1": "R. Manoelito de Ornellas",
    "city": "Porto Alegre",
    "state": "RS"
  }
}
```
### **Resposta**

Para os dois cenários (cartões nomeados ou anônimos), o formato da resposta esperada é idêntico. O principal campo de retorno é o *``"cardId"``*, que permitirá consultar e operar sobre um cartão durante todo seu ciclo de vida. 

Como a conta, ele também é uma composição de *strings*: o prefixo ``"crt-"`` com um UUID no formato v4, conforme segue no exemplo de resposta a seguir:

```json
{
  "resultData": {
    "resultCode": 0,
    "resultDescription": "Requisição de novo cartão feita com sucesso!",
    "psResponseId": "dfd88b95-d28d-49ac-90ba-7ba0b35aebd0"
  },
  "cardId": "crt-41cab09a-c20a-46d0-883a-8b2a359fc4ad"
}
```