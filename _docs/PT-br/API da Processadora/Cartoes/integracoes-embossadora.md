---
title: Fluxo completo de Emissão de Cartões
tags: 
 - gráfica
 - embossadora
 - emissão
 - cartões
 - rastreio
 - transportadora
 - courier
description: Fluxo completo de Emissão de Cartões
permalink: /pt-br/docs/integracoes-embossadora
---

# **Integrações e Dúvidas sobre a Gráfica/Embossadora**

1. [Introdução](#introdução)
1. [Contratação da Gráfica](#contratação-da-gráfica)
    1. [Gráficas Parceiras](#gráficas-parceiras)
    1. [Multigráficas](#multigráficas)
        1. [Requisição API Processadora](#requisição-api-processadora)

## **Introdução**
Esta página visa detalhar o fluxo completo de emissão de cartões, desde a contratação da gráfica até o rastreio das remessas.

 as interações necessárias entre emissor e gráfica, assim como alguns fluxos de negócio e casos de uso, podendo assim auxiliar o emissor no planelamento das funcionalidades.

## **Contratação da Gráfica**
Neta seção, é apresentada uma visão geral do que e quais são as Gráficas Parceiras, além da modalidade da contratação de múltiplas gráficas por parte do emissor.

### **Gráficas Parceiras**
Uma Gráfica Parceira nada mais é que uma gráfica homologada com a Elo e que segue as políticas de PCI vigentes. A paySmart, por sua vez, não possui uma gráfica interna nem contrato de prestação de serviços com essas gráficas parceiras.

É dever do emissor entrar em contato com a gráfica de seu interesse e firmar um contrato de prestação de serviços. A paySmart, como processadora, apenas realiza o intermédio nas conversas e processos que envolvem o fluxo de emissão de cartões.

Algumas das Gráficas Parceiras são:
- Valid
- IntelCav: Tecnologia e Cartões
- Jallcard
- Thomas Greg & Sons
- Gemalto / Thales Group
- G&D (Giesecke e Devrient)
- Idemia
- Alterosa
- UP Technology

O emissor deve informar ao Analista paySmart qual a gráfica contratada e atualizá-lo sempre que houver a troca ou contratação de uma nova gráfica. 

### **Multigráficas**
O emissor pode contratar múltiplas gráficas para processamento dos arquivos e emissão dos cartões.

Caso opte por mais de uma gráfica, deve informar ao Analista paySmart responsável pelo projeto a lista das gráficas contratadas e qual será a gráfica padrão para emissão dos cartões.

#### **Requisição API Processadora**
Ao realizar a solicitação de cartões, o emissor pode indicar para qual gráfica deseja enviar e emitir os cartões.

Tal indicação deverá ocorrer no campo *`"bureauxId"`*, informando o código referente à gráfica desejada, conforme tabela abaixo:

Código | Gráfica
-|-
0001 | Valid
0002 | Intelcav: Tecnologia e Cartões
0003 | Jallcard
0004 | Thomas Greg & Sons
0005 | Gemallto / Thales Group
0006 | G&D (Giesecke e Devrient)
0007 | Idemia
0008 | Alterosa
0009 | UP Technology

## ****
## **Layout dos cartões**
Um ponto muito importante da personalização do cartão é o seu layout ou card design (também chamado de face).

O emissor pode ter tantos card designs quanto achar necessários para a diferenciação dos seus cartões.

Para informar qual card design deve ser impresso no cartão, no ato da solicitação do cartão, o emissor deverá informar o código referente ao card design no campo deliveryKitCode (DKC).

O DKC é um código composto de 6 dígitos e que é transparente para a paySmart, pois nós não o utilizamos. Em contrapartida, emissor e gráfica precisam ter esses códigos alinhados internamente, para que a gráfica imprima os cartões com o layout correto.

O DKC é tansmitido para a gráfica como os 6 últimos dígitos na nomenclatura do arquivo de embossing, como pode ser verificado em 

## **Arquivo de Embossing**
O emissor pode verificar a nomenclatura de um arquivo de embossing ao consultar o endpoint e verificar pelo campo

A nomenclatura do arquivo de embossing segue um padrão, conforme pode ser observado abaixo:

CCCCCC_BBBBBB_PEPPPPPP_DDMMAAAA_HHMMSS_DDDDDD

Acrônimo | Campo | Tamanho | Descrição
-|-|-|-
CCCCCC | Código de Cliente | 6 | Identificador do Cliente na paySmart (geralmente o nome do emissor em 6 caracteres, completando com undescore ("_") caso possua menos de 6)
_ | Separador | 1 | Fixo. Caractere underscore ("_")
BBBBBB | BIN | 6 | BIN associado ao produto, que também identifica o emissor perante a bandeira e redes de captura 
_ | Separador | 1 | Fixo. Caractere underscore ("_")
PE | Identificador do Perfil Eletrônico | 2 | Dois caracteres indicando se o produto é:<br><br>"CR" - Crédito Nacional<br>"DN" - Débito Nacional<br>"CI" - Crédito Internacional<br>"MN" - Múltiplo Nacional
PPPPPP | Identificador do Produto na paySmart | 6 | É o produto informado na solicitação do cartão no campo *``"psProductCode"``*
_ | Separador | 1 | Fixo. Caractere underscore ("_")
DDMMAAAA | Data | 8 | Data de geração do arquivo no formato DDMMAAAA
_ | Separador | 1 | Fixo. Caractere underscore ("_")
HHMMSS | Hora | 6 | Hora de geração do arquivo no formato HHMMSS
_ | Separador | 1 | Fixo. Caractere underscore ("_")
DDDDDD | Card Design (DKC) | 6 | Indica a arte a ser utilizada no plástico dos cartões (layout, face, card design)


## **Rastreio dos cartões**
Caso o emissor deseje realizar o rastreio dos cartões ou, até mesmo, disponibilizar esse rastreio para o próprio portador, será possível.

É importante entender que o processo de rastreio não depende somente da paySmart, mas é realizado em conjunto à(s) gráfica(s) e à(s) transportadora(s) (courier) contratadas.

### **Código de Identificação paySmart**
Na geração do cartão, é atribuído automaticamente um código de identificação único a cada cartão. Esse código é composto de 10 dígitos.

É importante entender que esse código gerado é parcial e, somente ele, não é suficiente para rastrear o cartão.

Para verificar o código de rastreio do cartão, o emissor pode, no ato da solicitação do cartão .................... ou em momento futuro .................. verificar pelo campo *``"trackingId"``*.

### **Código de Rastreio Final (AR)**
O Código de Rastreio (AR) é composto de algumas partes, sendo formatado conforme exemplo:

Prefixo transportadora + Código de Identificação paySmart + Sufixo transportadora

AA0123456789BR



O emissor deve informar à gráfica que deseja utilizar o código de rastreio e solicitar que seja alinhado com a transportadora. Após todos os alinhamentos, a transportadora criará o prefixo e sufixo que irão compor o AR.

De posse do AR, o emissor poderá acessar a página web da transportadora e buscar a situação da remessa. 

> **Dicas de Implementação**
>
> O emissor pode exibir a posição de rastreio do cartão ao seu portador de duas principais formas:
>
> **Simples:** Disponibilizar no app um link para acesso ao site da transportadora já enviando o AR como parâmetro de busca, trazendo todos os dados do envio.
>
> **Complexa:** Exibir, dentro do app, as informações de rastreio, implementando uma rotina de scraping que traga os dados retornados com a busca do AR ou mesmo em uma possível integração de APIs com a transportadora.

 


## **Emissão de cartões em lote**
O sistema da paySmart não suporta lotes de cartões de forma nativa, mas disponibilizamos algumas opções para o emissor:

### **Utilização do campo extraData**

### **Solicitação de cartões em janelas específicas**

### 

## ****
## ****
## ****
## ****
## ****


