---
title: Criação de Cartões Virtuais
tags: 
 - virtualcards
 - cartão virtual
description: Operação em alto nível para a criação de cartões virtuais na processadora paySmart
permalink: /pt-br/cartoes/virtuais/criacao
---

# **Criação de Cartões Virtuais**

1. [Introdução](#introdução)
1. [Criação de Cartão Virtual](#criação-de-cartão-virtual)
    1. [Requisição](#requisição)
    1. [Resposta](#resposta)
1. [Status dos Cartões Virtuais](#status-dos-cartões-virtuais) 

## **Introdução**

Um cartão virtual é um elemento de *cashout* que possui os mesmos dados que são necessários a um cartão em mídia física para realizar uma transação não presencial no ambiente de *e-commerce*, ou seja, um número único de identificação (PAN), uma data de expiração e um código de segurança (CVV). 

Seu principal benefício é ter um ciclo de vida bastante flexível, por não estar materializado em um plástico, podendo ter sua expiração e valor por transação totalmente personalizados pelo usuário - isso evita ou diminui os danos em caso de uma fraude, visto que o oponente que venha a obter os dados do cartão estará limitado pelas configurações definidas pelo portador legítimo do cartão.

## **Criação de Cartão Virtual**

A maneira recomendada de gerar um cartão virtual na API da processadora paySmart é vinculando-o diretamente a uma conta previamente criada conforme o processo descrito em [Criação de Contas]({{ site.url }}{{ site.baseurl }}/pt-br/accounts). 

Os dados necessários para a criação de um cartão virtual são:

| Campo | Descrição |
|-------|-----------|
| *``"birthDate"``* | Data de nascimento: data de nascimento do titular da conta. Necessário para manter a compatibilidade com a obrigatoriedade desse dado na [criação de um cartão físico]({{ site.url }}{{ site.baseurl }}/pt-br/cartoes/fisicos/criacao) |
| *``"constraints"``* | Restrições: define as regras e limitações que o portador deseja estabelecer para seu cartão, de acordo com o uso que pretende dar para o mesmo |
| *``"constraints"."currency_code"``* | A moeda com a qual deseja transacionar: representada por um código numérico de três dígitos conforme o padrão [ISO-4217](https://pt.wikipedia.org/wiki/ISO_4217). Os mais comuns são **"986"** (real brasileiro) e **"0840"** (dólar americano). Pode ser passado com o valor **"*"**, indicando aceitação de transações em qualquer moeda |
| *``"constraints"."max_amount"``* | O valor máximo por transação: expresso em quantidade de centavos (**R$ 50,00** deve ser representado por **"5000"**, por exemplo). Se o valor **"*"** for passado nesse campo, o cartão virtual poderá fazer transações de qualquer valor |
| *``"constraints"."expiration_timestamp"``* | O tempo de expiração: é uma *string* no formato **"YYYY-MM-DDTHH:mm:ssZ"** que indica a data de expiração do cartão. Todos os elementos do formato são obrigatórios, incluindo a *timezone*, para evitar ambiguidades na expiração verdadeira. Então, por exemplo, **"2021-12-31T23:59:59-03:00"** significaria **31 de dezembro de 2021, às 23:59:59 no horário brasileiro**. Dessa forma, **"2022-01-01T02:59:59Z" representa o mesmo horário no fuso GMT**. Se for passado um **"*"** nesse campo, uma validade de seis anos a partir da data atual será calculada |


### **Requisição**

Os dados listados acima devem ser informados em um corpo no formato JSON por uma requisição HTTP do tipo no endpoint [POST /accounts/{accountId}/virtualcards]({{ site.url }}{{ site.baseurl }}/pt-br/processor-api), onde *``{accountId}``* é o identificador único retornado no momento de uma criação bem sucedida de uma conta. 

Dessa maneira, uma requisição mínima para a criação de uma cartão virtual ficaria no seguinte formato:

```json
  {
        "birthDate": "21/03/1983",
        "constraints": {
            "currency_code": 986,
            "max_amount": "*",
            "expiration_timestamp": "2021-10-17T23:59:59-03:00"
        }
    }
```

### **Resposta**

Uma requisição bem sucedida de cartão virtual irá retornar os dados necessários do cartão para a entrada em qualquer sistema de *e-commerce*, conforme a resposta abaixo:

```json
{
    "resultData": {
        "resultCode": 0,
        "resultDescription": "Comando concluído com sucesso",
        "psResponseId": "993d32c5-3ed4-4cf6-b28f-dfb1f31d6694"
    },
    "virtualCard": {
        "vCardId": "vcrt-1751b1e7-84d2-463e-a792-3c5e2a5a2eab",
        "vPan": "5092570047467931",
        "vCvv": "391",
        "vDateExp": "10/21",
        "vCardholder": "FULANO DA SILVA"
    }
}
```

>**Atenção**
>
>Um cartão virtual criado pode ser utilizado imediatamente, sem necessidade de desbloqueio.


## **Status dos Cartões Virtuais**

| Status | Descrição |
|--------|-----------|
| Token Ativo | Token apto para receber transações |
| Token Desativado | Foi excedido a quantidade de transações permitidas para aquele token |
| Token Cancelado | Token foi cancelado pelo emissor |
| Token Expirado | Validade do token expirada |
| Token Bloqueado | Token foi bloqueado por excesso de transações negadas |

## **Observações**

> **Obs. 1:**
>
>Um cartão virtual que esteja nos status **Expirado** ou **Cancelado** não pode assumir um novo status, ou seja, já está em uma situação final.