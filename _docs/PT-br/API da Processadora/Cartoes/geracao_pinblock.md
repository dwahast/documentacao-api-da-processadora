---
title: Geração de Pinblock
description: Informações de como gerar pinblock
tags:
- pinblock
permalink: /pt-br/docs/geracao_pinblock
---

# **Geração PINBlock**

1. [Introdução](#geração-pinblock)
1. [Criação do *PINBlock*](#criação-do-pinblock)
    1. [Exemplo Java](#exemplo-java)
    1. [Utilização](#utilização)

## **Introdução**

***PINBlock*** pode ser entendido como o ***PIN*** do cartão, quando ele é cifrado pela chave de transporte (*``"idTransportKey"``*).

Em geral, utilizamos o formato ***ISO-1*** para a geração do ***PINBlock***, uma vez que nesse formato não há a necessidade de que seja informado o ***PAN*** do cartão, garantindo assim a sua proteção e confidencialidade.

No endpoint ***[POST /cards/{cardId}/changePin]({{ site.url }}{{ site.baseurl }}/pt-br/processor-api)***, utilizamos a chave de transporte ***idTransportKey*** = **4242**, que representa a chave 3DES **505152535455565758595A5B5C5D5E5F** (*Outgoing ZPK*), em homologação.

Em produção, a chave de transporte ***idTransportKey*** = **4242** permanece a mesma, mas irá representar outra chave 3DES, que será encaminhada em tempo de projeto pelo analista responsável.


## **Criação do *PINBlock***

É possível criar ***PINBlocks ISO-1*** no site <https://emvlab.org/pinblock/>, na parte de baixo do formulário, preenchendo os campos da seguinte forma:

- ***Convert to***: ISO-1
- ***Outgoing Clear PINBlock***: "1" + tamanho do PIN, com uma posição + PIN + preenchimento com "F" até completar 16 posições. 
    - Exemplo para a senha 1234: "1" + "4" + "1234" + "FFFFFFFFFF" = 141234FFFFFFFFFF
- ***Outgoing PAN***: não é necessário preencher para ISO-1.
- ***Outgoing ZPK***: 505152535455565758595A5B5C5D5E5F

Clique em ***Encrypt***: O ***PINBlock*** aparecerá logo abaixo, podendo ser utilizado na API.

### **Exemplo Java**

É possível, também, criar o ***PINBlock*** à partir do código Java disponibilizado abaixo:

```java
private final static char[] hexArray = "0123456789ABCDEF".toCharArray();
public static byte[] hexStringToByteArray(String s) {
    int len = s.length();
    byte[] data = new byte[len / 2];
    for (int i = 0; i < len; i += 2) {
        data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                + Character.digit(s.charAt(i+1), 16));
    }
    return data;
}


public static String byteArrayToHexString(byte[] bytes) {
    char[] hexChars = new char[bytes.length * 2];
    for ( int j = 0; j < bytes.length; j++ ) {
        int v = bytes[j] & 0xFF;
        hexChars[j * 2] = hexArray[v >>> 4];
        hexChars[j * 2 + 1] = hexArray[v & 0x0F];
    }
    return new String(hexChars);
}

public static byte[] concat(byte[] a, byte[] b) {
    byte [] temp = new byte[a.length + b.length];
    System.arraycopy(a, 0, temp, 0, a.length);
    System.arraycopy(b, 0, temp, a.length, b.length);
    return temp;
}
public static String encrypt3DES(String message, String key) throws Exception {

    byte[] keyBytes = hexStringToByteArray(key);

    byte [] keyBytesPadding = new byte[8];
    System.arraycopy(keyBytes, 0, keyBytesPadding, 0, 8);
    keyBytes = concat(keyBytes, keyBytesPadding);


    final SecretKey secretKey = new SecretKeySpec(keyBytes, "DESede");
    final IvParameterSpec iv = new IvParameterSpec(new byte[8]);
    //final Cipher cipher = Cipher.getInstance("DESede/CBC/PKCS5Padding");
    final Cipher cipher = Cipher.getInstance("DESede/CBC/NoPadding");
    cipher.init(Cipher.ENCRYPT_MODE, secretKey, iv);

    final byte[] plainTextBytes = hexStringToByteArray(message);
    System.out.println("plain content bytes: " + plainTextBytes.length);
    final byte[] cipherText = cipher.doFinal(plainTextBytes);
    // final String encodedCipherText = new sun.misc.BASE64Encoder()
    // .encode(cipherText);


    return byteArrayToHexString(cipherText).toUpperCase();
}
```
### **Utilização**

```java
String apiPinBlock = encrypt3DES("141234FFFFFFFFFF", "505152535455565758595A5B5C5D5E5F");
```

O resultado deve ser: **1ED680FA74CDD97A**, um formato que será aceito pela nossa API.
