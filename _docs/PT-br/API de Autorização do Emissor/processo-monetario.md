---
title: Processo Monetário
tags: 
 - processo monetario
description: Operações do Processo Monetário
permalink: /pt-br/docs/processo-monetario
language: pt-br
---

# **Processo Monetário**

1. [Introdução](#introdução)
	1. [Ajuste do valor em casos de pré-autorização](#1-ajuste-do-valor-em-casos-de-pré-autorização)
	1. [Estornos feitos via arquivo de liquidação](#2-estornos-feitos-via-arquivo-de-liquidação)
1. [Execução do Serviço](#execução-do-serviço)
1. [Propriedades das Transações](#propriedades-das-transações)
	1. [Campo *callingSystemName*](#campo-callingsystemname)
	1. [Campo *forceAccept*](#campo-forceaccept)
1. [Transações](#transações)
	1. [Compra (*/purchases*)](#compra-purchases) 
	1. [Estorno (*/chargebacks*)](#estorno-chargebacks)

## **Introdução**

O Processo Monetário é o serviço da Evertec Brasil responsável por ajustar automaticamente o estado e os valores das transações de acordo com o que é apresentado nos arquivos de liquidação vindos da bandeira. 
 
O Processo Monetário atua, basicamente, em duas situações:

### **1. Ajuste do valor em casos de pré-autorização**

Pré-Autorização é um recurso utilizado para verificação e bloqueio temporário dos fundos disponíveis no cartão de crédito. Ao realizar uma Pré-Autorização, o Estabelecimento Comercial (EC) consegue garantir a disponibilidade do valor de um pagamento para uma transação específica antes desta ser concluída. Existem dois tipos possíveis de solicitação para transações de Pré-Autorização: **Solicitação Inicial de Pré-Autorização** e **Solicitação de Incremento de Valor**.

Esses dois tipos de solicitação serão identificados no **BIT 48** no subelemento **PRE** que é o “Indicador de Pré-Autorização e Incremento de Pré-Autorização”. O domínio desse subcampo é **“01”** - Pré-Autorização e **“02”** - Incremento.

Para uma Pré-Autorização ser liquidada com um valor diferente do autorizado, ela precisa ser autorizada com o valor **101** no **BIT 24**, que indica que o valor daquela transação é estimado. Se o valor desse campo for **100**, isto indica que o valor é exato e o valor liquidado deve ser o mesmo que o autorizado. 

Um exemplo prático desse cenário seria o aluguel de veículos, onde é comum haver uma pré-autorização de um valor mais alto para dar garantias ao estabelecimento. Ao final, normalmente o valor autorizado é diferente.

Dado os cenários, o Processo Monetário pode ter dois comportamentos:

- Quando, na liquidação, a transação vier com valor **MENOR*** do que o autorizado, é feito um estorno parcial do valor que foi autorizado a mais (chamada em **/chargebacks**).
- Quando, na liquidação, a transação vier com valor **MAIOR** do que o autorizado, é feita uma nova autorização com o valor incremental (chamada em **/purchases**).

> *Uma transação pode ser liquidada com um valor menor do que o autorizado mesmo sem as características de valor estimado, pois podem ocorrer eventos onde o Estabelecimento Comercial retorna crédito ao Portador sem realizar o estorno da transação de autorização, enviando assim a liquidação com valor menor que o autorizado.

### **2. Estornos feitos via arquivo de liquidação**

O EC pode enviar o estorno/cancelamento via arquivo de liquidação, sem enviar uma transação com **mti 0400/0420** no fluxo de autorização. Essa operação pode ser realizada em até 365 dias corridos da apresentação da transação.

Em determinadas situações, como a indisponibilidade de produtos adquiridos por um consumidor em um e-commerce, defeito com mercadoria, entre outros cenários, é comum que a devolução seja feita diretamente pelo arquivo de liquidação. Quando o Processo Monetário identifica uma situação assim, ele acionará o endpoint **/chargebacks**, fazendo referência à transação original, para que o crédito seja restabelecido ao portador. 

## **Execução do Serviço**
O processo monetário é executado duas vezes por dia (10:30 e 14:30 - horário do Brasil), sete dias por semana. Ele irá fazer várias tentativas até compatibilizar o estado da transação com o apresentado no arquivo de liquidação, repetindo as operações, dia após dia, até que tal compatibilização seja atingida.

## **Propriedades das Transações**
Ressaltamos aqui dois campos muito importantes nas transações, quando falamos sobre Processo Monetário. São eles:

### **1. Campo *callingSystemName***
Através do campo callingSystemName das requisições é possível identificar o tipo de requisição. Transações do Processo Monetário serão indicadas pelo nome callingSystemName: "processo-monetario".

### **2. Campo *forceAccept***
As transações de compra, a partir do endpoint **/purchases**, realizadas pelo Processo Monetário, são enviadas com o campo ***forceAccept = true***, o que indica que a transação **obrigatoriamente** precisa ser aceita pelo emissor.  Pois quando a transação chega pela liquidação, isso implica que esta já foi processada pela bandeira e será descontada do emissor, logo não faz sentido negá-la no nível do portador. **O saldo do portador deve ser sensibilizado mesmo que o mesmo seja insuficiente**.

As transações de chargeback, identificadas por **/chargebacks**, são enviadas ao emissor com o campo ***forceAccept = false***.

## **Transações**
Apresentamos abaixo alguns exemplos de transações que podem ser encaminhadas pelo Processo Monetário:

### **Compra (*/purchases*)**

Exemplo de requisição de compra com o endpoint ***/purchases*** do Processo Monetário:

HTTP Code | Objeto                    | Descrição
----------|---------------------------| ---------------------
200       | **Success**               | Transação aprovada com sucesso (transação com *``"forceAccept"``* = *``"true"``*)
404       | **NotFoundError**         | Conta não encontrada ou cartão inexistente
500       | **GeneralError**          | Erro interno do sistema do emissor
503       | **SystemDownError**       | Sistema Indisponível

```json
{
	"purchase_id": "PM-C-aut_f611210e-b450-4f93-8486-093e9d5e6be6-05-2722221A0899349.221511",
    "account_id": "cta-0b4779e9-bcfd-4996-a59f-ba846177fe87",
    "psProductCode": "001001",
    "psProductName": "CARTAO EMISSOR EXEMPLO",
    "countryCode": "076",
    "source": "PAYSMART",
    "productType": "pre",
    "callingSystemName": "processo-monetario",
    "preAuthorization": true,
    "incrementalAuthorization": false,
    "authorization": {
        "code": "00",
        "description": "Transação Autorizada"
    },
    "brand": "ELO",
    "card": {
        "paysmart_id": "crt-28e71890-6393-4229-992c-647a790a837d",
        "issuer_id": "57a5544a-3d39-409c-ac14-a2ffe3b76afa",
        "pan": "************9152"
    },
    "total_amount": {
        "amount": 100,
        "currency_code": 986
    },
    "original_amount": {
        "amount": 100,
        "currency_code": 986
    },
    "entry_mode": "CHIP",
    "processing_code": {
        "tipo_transacao": "PURCHASE",
        "source_account_type": "CREDIT_CARD_ACCOUNT",
        "destination_account_type": "NOT_APPLICABLE"
    },
    "holder_validation_mode": "OTHER",
    "fees": [],
    "establishment": {
        "mcc": "5712",
        "name": "ELO",
        "city": "BARUERI",
        "address": "NA",
        "zipcode": "35432300",
        "country": "BR"
    },
    "internacional": false,
    "original_iso8583": {
        "mti": "0100",
        "de003": "003000",
        "de007": "0312181144",
        "de011": "011565",
        "de012": "151135",
        "de013": "0312",
        "de018": "5411",
        "de032": "0025",
        "de037": "202289011564",
        "de041": "20172289",
        "de042": "020001605270002"
    },
    "forceAccept": true
}
```

> **Observações:**
>
> - Campo ***purchase_id*** inicia com a tag **PM** indicando tratar-se de transação do Processo Monetário
>
> - Campo ***callingSystemName*** identificando a origem da transação como **"processo-monetario"**
>
> - Campo ***forceAccept = true*** indicando a **obrigatoriedade de aceitação da transação por parte do emissor, mesmo que o portador não tenha saldo disponível**.


Resposta esperada:

```json
   {
	"authorization_id": 722873,
	"balance": {
		"amount": 0,
		"currency_code": 986
	},
	"message": "Operação realizada com sucesso.",
	"code": 200
}
```

> Para o Pós-Pago, o campo **Balance** não está presente na resposta.


### **Estorno (*/chargebacks*)**

A operação de estorno desfaz uma compra, um saque ou uma transferência, devolvendo o saldo consumido de forma parcial ou total. Neste caso, o Processo Monetário realiza um estorno encaminhado na liquidação.

HTTP Code | Objeto                         | Descrição
----------| -------------------------------| ---------------------
200       | **Success**                    | Estorno realizado com sucesso.
404       | **NotFoundError**              | Compra, saque ou transferência original não encontrada.
409       | **TransactionAlreadyCanceled** | Transação já foi estornada.
412       | **OperationNotPermitted**      | Operação não permitida para a transação.
499       | **Acknowledge**                | Mensagem recebida pelo emissor para casos de transação negada.
503       | **SystemDownError**            | Sistema Indisponível.
500       | **GeneralError**               | Erro interno do sistema do emissor.

Exemplo de requisição de estorno com o endpoint ***/chargebacks*** do Processo Monetário:

```json
{
	"chargeback_id": "PM-CB-aut_dfbfc249-5f94-49e8-85b0-dfbd06d3267f",
	"original_purchase_id": "aut_dfbfc249-5f94-49e8-85b0-dfbd06d3267f",
	"account_id": "cta-0b4779e9-bcfd-4996-a59f-ba846177fe87",
    "psProductCode": "001001",
    "psProductName": "CARTAO EMISSOR EXEMPLO",
    "productType": "pre",
    "countryCode": "076",
    "source": "PAYSMART",
    "callingSystemName": "processo-monetario",
    "original_authorization_id": 945202,
    "authorization": {
        "code": "00",
        "description": "Transacao Autorizada"
    },
    "brand": "ELO",
    "card": {
        "paysmart_id": "crt-381a1890-6313-4239-091d-647a700a831d",
        "issuer_id": "1",
        "pan": "************9152"
    },
    "chargeback_mode": "TOTAL",
    "original_amount": {
        "amount": 6100,
        "currency_code": 986
    },
    "chargeback_amount": {
        "amount": 6100,
        "currency_code": 986
    },
    "entry_mode": "BAR_OR_QR_CODE",
    "chargeback_reason": {
        "code": 0,
        "description": "Estorno encaminhado na liquidação."
    },
    "iso8583_message": {
        "mti": "0400",
        "de003": "003000",
        "de007": "1116171051",
        "de011": "555040",
        "de012": "162052",
        "de013": "1211",
        "de018": "3462",
        "de037": "106375555049"
    },
    "forceSynchronous": false
}
```
> **Observações:**
>
> - Campo ***chargeback_id*** inicia com a tag **PM** indicando tratar-se de transação do Processo Monetário
>
> - Campo ***callingSystemName*** identificando a origem da transação como **"processo-monetario"**
>
> - Campo ***forceAccept*** não enviado, sendo considerado como ***false***.