---
title: Operações de Estorno
tags: 
 - estorno
description: Operações de estorno de compra, saque ou transferência. 
permalink: /pt-br/docs/estorno
language: pt-br
---

# Operações de Estorno

[POST /chargebacks](estorno#post-chargebacks) | 
[POST /chargebacks/cancel](estorno#post-chargebackscancel) |
[Objetos de Estorno](estorno#objetos-de-estorno) |
[Chargeback](estorno#chargeback) | 
[ChargebackCancellation](estorno#chargebackcancellation)

### POST [/chargebacks](../assets/api/openapi.json#/chargeback/createChargeback)

#### Estorno de uma compra previamente realizada.

&nbsp;

![Solicitação de estorno de compra]({{ site.url }}{{ site.baseurl }}/assets/img/pt-BR/SolicitaEstornoCompra.png)

&nbsp;

**ATENÇÃO: Embora a ilustração acima faça referência ao estorno de uma compra, o mesmo vale para estornos de saques e de transferências.**


A operação de estorno desfaz uma compra, um saque ou uma transferência, devolvendo o saldo consumido de forma parcial ou total. Normalmente o estorno é disparado a partir da interação de um cliente com um lojista, que efetua o estorno em um terminal do mesmo adquirente da transação original. Normalmente disparado a partir de mensagens com MTI = 400.

Da mesma forma que ocorre com uma requisição de compra, saque ou transferência, uma requisição de estorno de compra, saque ou transferência também pode ser negada pela processadora e enviada para o emissor somente para reconhecimento e registro da requisição em seu sistema, sem que o mesmo precise realizar nenhuma validação de saldo, entre outras validações, para este caso. Neste caso o MTI da mensagem de estorno será igual à '`0400`' e o código de autorização será diferente de '`00`'. Para este caso o emissor deve retornar **Acknowledge** indicando o reconhecimento e aceite da transação negada.

Requisições de estorno de compra, saque ou transferência aprovadas pela bandeira e processadora devem ser validadas pelo emissor e possuir o saldo da conta do cartão retornado de acordo com o valor a ser estornado, caso o estorno seja aprovado pelo emissor. 

Operações de estorno podem ser total, indicado pelo campo '`chargeback_mode`' igual à '`total`' ou parcial, indicado pelo campo '`chargeback_mode`' igual à '`parcial`'. 

Para os casos de estorno total o valor a ser estornado, indicado pelo campo '`chargeback_amount`', deverá ser igual ao valor total da transação original sendo estornada. 

Para os casos de estorno parcial o valor a ser estornado poderá corresponder a uma parte do valor total da transação sendo estornada. Sendo que poderão ocorrer um ou mais estornos parciais até completar o valor total de transação original, caso seja necessário. O emissor deve validar os estornos parciais com o saldo da conta/cartão.

Para este endpoint o emissor deve retornar **Success** caso a requisição de estorno de compra seja aprovada pelo emissor após todas validações terem sido executadas com sucesso. Caso a operação de estorno de compra não seja permitida para a conta/cartão por algum motivo que não esteja nos padrões de retorno deve ser retornado **OperationNotPermitted**. Se a conta/cartão da transação ou compra original não existir na base do emissor deve ser retornado **NotFoundError**. Caso o sistema do emissor esteja em baixa e não possa responder a requisição no momento deve retornar **SystemDownError**. Ocorrendo algum erro interno no sistema do emissor deve ser retornado **GeneralError**.

Nos casos de estorno(chargeback) de um saque o '`original_purchase_id`' do estorno corresponderá ao ID da transação de saque à ser estornada.

Nos casos de estorno(chargeback) de uma transferência o '`original_purchase_id`' do estorno corresponderá ao ID da transação de transferência à ser estornada.

&nbsp;

* Parâmetros enviados no corpo da requisição:

Objeto         | Descrição 
---------------| ---------------------
**Chargeback** | Objeto de estorno de compra contendo as informações da transação de estorno.

&nbsp;

* Possíveis retornos para o endpoint:

HTTP Code | Objeto                         | Descrição
----------| -------------------------------| ---------------------
200       | **Success**                    | Estorno realizado com sucesso.
404       | **NotFoundError**              | Compra, saque ou transferência original não encontrada.
409       | **TransactionAlreadyCanceled** | Transação já foi estornada.
412       | **OperationNotPermitted**      | Operação não permitida para a transação.
499       | **Acknowledge**                | Mensagem recebida pelo emissor para casos de transação negada.
503       | **SystemDownError**            | Sistema Indisponível.
500       | **GeneralError**               | Erro interno do sistema do emissor.

&nbsp;

#### Exemplo de requisição de estorno de compra:

```json
{    
    "chargeback_id": "aut_f07338bd-dcd8-4ca0-92c0-001b5598d5a6",
    "original_purchase_id": "aut_9acbfd86-f17a-4231-9d3a-70dfd4d3ca99",
    "account_id": "cta-be31f86a-dad1-43d2-b7b6-264123591160",
    "psProductCode": "010101",
    "psProductName": "CARTÃO PAYSMART GIFT CARD PF",
    "countryCode": "076",
    "source": "brand",
    "callingSystemName": "Autorizador ISO8583",
    "original_authorization_id": "158766",
    "authorization": { 
        "code": "00",
        "description": "Sucesso/Transacao Aprovada"
    },
    "brand": "elo",
    "card": { 
        "paysmart_id": "220",
        "issuer_id": "3",
        "pan": "************3368",
        "panseq": "00"
    },
    "chargeback_mode": "parcial",
    "original_amount": { 
        "amount": 22000,
        "currency_code": "986"
    },
    "chargeback_amount": { 
        "amount": 500,
        "currency_code": 986
    },
    "entry_mode": "chip",
    "chargeback_reason": { 
        "code": 0,
        "description": "Cancelamento do Consumidor." 
    }, 
    "iso8583_message": { 
        "mti": "0400",
        "de002": "************3368",
        "de003": "003000",
        "de004": "000000000500",
        "de005": null,
        "de006": null,
        "de007": "1203135840",
        "de008": null,
        "de009": null,
        "de010": null,
        "de011": "097178",
        "de012": "105840",
        "de013": "1203",
        "de014": "2412",
        "de015": null,
        "de016": null,
        "de018": "5712",
        "de019": "076",
        "de022": "051",
        "de023": "000",
        "de024": "401",
        "de025": "00",
        "de026": null,
        "de028": null,
        "de029": null,
        "de032": "0025",
        "de033": null,
        "de035": "************3368D24126067611010000000",
        "de036": null,
        "de037": "192348306599",
        "de038": "158766",
        "de039": null,
        "de041": "20172289",
        "de042": "020001605270002",
        "de043": "ELO                    BARUERI       076",
        "de045": null,
        "de046": null,
        "de047": null,
        "de048": "*PRD003070",
        "de049": "986",
        "de050": null,
        "de051": null,
        "de052": null,
        "de053": null,
        "de054": "3056986C000000022000",
        "de055": null,
        "de056": null,
        "de058": "ALAMEDAXINGU          23543230007612345ELO",
        "de059": null,
        "de060": "000001000P500",
        "de062": null,
        "de063": null,
        "de090": "010009717710584012031587660000000000000000",
        "de095": null,
        "de105": null,
        "de106": null,
        "de107": null,
        "de121": null,
        "de122": null,
        "de123": null,
        "de124": null,
        "de125": null,
        "de126": null,
        "de127": 12192
    },
    "additionalTerminalData": {
      "terminalType": "terminalWithService",
      "partialApprovalIndicator": "notSupported",
      "terminalLocationIndicator": "local",
      "cardholderPresenceIndicator": "present",
      "cardPresenceIndicator": "cardPresent",
      "cardCaptureCapabilityIndicator": "cardCaptureCapability",
      "transactionStatusIndicator": "normal",
      "transactionSecurityIndicator": "noSuspectedFraud",
      "terminalPOSType": "atm",
      "terminalInputCapability": "chip"
    },
    "eloProductCode": "070"
}
```

&nbsp;

* Para transações por QrCode ou com cartão tokenizado, o objeto da transação poderá apresentar o campo "tokenPaymentData", com informações referentes ao pagamento via token.

### Exemplo do campo referente aos dados de pagamento via token para transações por QrCode ou com cartão tokenizado:

```json
{
  "tokenPaymentData": {
    "fPan": "1234",
    "requesterIdToken": "12345678901",
    "pan": "************1234",
    "tokenPSN": "001",
    "tokenExpirationDate": "2908",
    "tokenStatus": "0100",
    "tokenCryptogramVerificationResults": "0200",
    "EMVTokenCryptogramVerificationResults": "0200",
    "tokenConstraintsVerificationStatus": "0300",
    "transactionDateTimeConstraint": "OK",
    "transactionAmountConstraint": "OK",
    "usageConstraint": "OK",
    "tokenATCVerificationResults": "E001",
    "CVE2TokenCryptogramVerificationStatus": "E001",
    "merchantVerification": "OK",
    "magstripeTokenCryptogramVerificationResults": "0200",
    "CVE2OutputTokenCryptogramVerificationResults": "0201"
  }
}
```
---

#### Exemplo de resposta do estorno acima:

```json
{ 
    "message": "Operação realizada com sucesso.",
    "code": 0,
    "authorization_id": 784200,
    "balance": { 
        "amount": 200000,
        "currency_code": 986 
    }
}
```

&nbsp;

#### Exemplo de requisição de estorno de saque:

```json
{    
    "chargeback_id": "aut_d01235b7-acd6-4cb0-52c1-0a1be596d5c3",
    "original_purchase_id": "aut_9aabed84-d17f-4235-9a3f-77dfa4d7ca36",
    "account_id": "cta-be31f86a-dad1-43d2-b7b6-264123591160",
    "psProductCode": "010101",
    "psProductName": "CARTÃO PAYSMART GIFT CARD PF",
    "countryCode": "076",
    "source": "brand",
    "callingSystemName": "Autorizador ISO8583",
    "original_authorization_id": "234820",
    "authorization": { 
        "code": "00",
        "description": "Sucesso/Transacao Aprovada"
    },
    "brand": "elo",
    "card": { 
        "paysmart_id": "220",
        "issuer_id": "3",
        "pan": "************3368",
        "panseq": "00"
    },
    "chargeback_mode": "parcial",
    "original_amount": { 
        "amount": 10000,
        "currency_code": "986"
    },
    "chargeback_amount": { 
        "amount": 5000,
        "currency_code": 986
    },
    "entry_mode": "chip",
    "chargeback_reason": { 
        "code": 0,
        "description": "Cancelamento do Consumidor." 
    }, 
    "iso8583_message": { 
        "mti": "0400",
        "de002": "************3368",
        "de003": "013000",
        "de004": "000000005000",
        "de005": null,
        "de006": null,
        "de007": "1203135840",
        "de008": null,
        "de009": null,
        "de010": null,
        "de011": "234820",
        "de012": "111234",
        "de013": "0208",
        "de014": "2412",
        "de015": null,
        "de016": null,
        "de018": "5712",
        "de019": "076",
        "de022": "051",
        "de023": "000",
        "de024": "401",
        "de025": "00",
        "de026": null,
        "de028": null,
        "de029": null,
        "de032": "0025",
        "de033": null,
        "de035": "************3368D24126067611010000000",
        "de036": null,
        "de037": "192348306599",
        "de038": "158766",
        "de039": null,
        "de041": "20172289",
        "de042": "020001605270002",
        "de043": "ELO                    BARUERI       076",
        "de045": null,
        "de046": null,
        "de047": null,
        "de048": "*PRD003070",
        "de049": "986",
        "de050": null,
        "de051": null,
        "de052": null,
        "de053": null,
        "de054": "3056986C000000022000",
        "de055": null,
        "de056": null,
        "de058": "ALAMEDAXINGU          23543230007612345ELO",
        "de059": null,
        "de060": "000001000P500",
        "de062": null,
        "de063": null,
        "de090": "020023482011123402081587660000000000000000",
        "de095": null,
        "de105": null,
        "de106": null,
        "de107": null,
        "de121": null,
        "de122": null,
        "de123": null,
        "de124": null,
        "de125": null,
        "de126": null,
        "de127": 12192
    },
    "additionalTerminalData": {
      "terminalType": "terminalWithService",
      "partialApprovalIndicator": "notSupported",
      "terminalLocationIndicator": "local",
      "cardholderPresenceIndicator": "present",
      "cardPresenceIndicator": "cardPresent",
      "cardCaptureCapabilityIndicator": "cardCaptureCapability",
      "transactionStatusIndicator": "normal",
      "transactionSecurityIndicator": "noSuspectedFraud",
      "terminalPOSType": "atm",
      "terminalInputCapability": "chip"
    },
    "eloProductCode": "070"
}
```

#### Exemplo de resposta do estorno acima:

```json
{ 
    "message": "Operação realizada com sucesso.",
    "code": 0,
    "authorization_id": 784200,
    "balance": { 
        "amount": 200000,
        "currency_code": 986 
    }
}
```


&nbsp;

#### Exemplo de requisição de estorno de transferência:

```json
{    
    "chargeback_id": "aut_a23417c2-f2e1-6cd2-49a2-1b2ca46712b1",
    "original_purchase_id": "aut_abc13456-a34c-4331-a23e-73dfb4d3cc3f",
    "account_id": "cta-be31f86a-dad1-43d2-b7b6-264123591160",
    "psProductCode": "010101",
    "psProductName": "CARTÃO PAYSMART GIFT CARD PF",
    "countryCode": "076",
    "source": "brand",
    "callingSystemName": "Autorizador ISO8583",
    "original_authorization_id": "123456",
    "authorization": { 
        "code": "00",
        "description": "Sucesso/Transacao Aprovada"
    },
    "brand": "elo",
    "card": { 
        "paysmart_id": "220",
        "issuer_id": "3",
        "pan": "************3368",
        "panseq": "00"
    },
    "chargeback_mode": "total",
    "original_amount": { 
        "amount": 10000,
        "currency_code": 986
    },
    "chargeback_amount": { 
        "amount": 10000,
        "currency_code": 986
    },
    "entry_mode": "chip",
    "chargeback_reason": { 
        "code": 0,
        "description": "Cancelamento do Consumidor." 
    }, 
    "iso8583_message": { 
        "mti": "0400",
        "de002": "************3368",
        "de003": "103000",
        "de004": "000000010000",
        "de005": null,
        "de006": null,
        "de007": "1203135840",
        "de008": null,
        "de009": null,
        "de010": null,
        "de011": "234820",
        "de012": "111234",
        "de013": "0208",
        "de014": "2412",
        "de015": null,
        "de016": null,
        "de018": "5712",
        "de019": "076",
        "de022": "051",
        "de023": "000",
        "de024": "400",
        "de025": "00",
        "de026": null,
        "de028": null,
        "de029": null,
        "de032": "0025",
        "de033": null,
        "de035": "************3368D24126067611010000000",
        "de036": null,
        "de037": "192348306599",
        "de038": "123456",
        "de039": null,
        "de041": "20172289",
        "de042": "020001605270002",
        "de043": "ELO                    BARUERI       076",
        "de045": null,
        "de046": null,
        "de047": null,
        "de048": "*PRD003070",
        "de049": "986",
        "de050": null,
        "de051": null,
        "de052": null,
        "de053": null,
        "de054": "3056986C000000022000",
        "de055": null,
        "de056": null,
        "de058": "ALAMEDAXINGU          23543230007612345ELO",
        "de059": null,
        "de060": "000001000P500",
        "de062": null,
        "de063": null,
        "de090": "020023482011123402081587660000000000000000",
        "de095": null,
        "de105": null,
        "de106": null,
        "de107": null,
        "de121": null,
        "de122": null,
        "de123": null,
        "de124": null,
        "de125": null,
        "de126": null,
        "de127": 12192
    },
    "additionalTerminalData": {
      "terminalType": "terminalWithService",
      "partialApprovalIndicator": "notSupported",
      "terminalLocationIndicator": "local",
      "cardholderPresenceIndicator": "present",
      "cardPresenceIndicator": "cardPresent",
      "cardCaptureCapabilityIndicator": "cardCaptureCapability",
      "transactionStatusIndicator": "normal",
      "transactionSecurityIndicator": "noSuspectedFraud",
      "terminalPOSType": "atm",
      "terminalInputCapability": "chip"
    },
    "eloProductCode": "070"
}
```

#### Exemplo de resposta do estorno acima:

```json
{ 
    "message": "Operação realizada com sucesso.",
    "code": 0,
    "authorization_id": 784200,
    "balance": { 
        "amount": 200000,
        "currency_code": 986 
    }
}
```

&nbsp;

* Para o Pós-Pago o campo "Balance" não está presente na resposta.

&nbsp;

---

&nbsp;

### POST [/chargebacks/cancel](../assets/api/openapi.json#/chargeback/createChargebackCancellation)

#### Reversão de estorno.

&nbsp;

![Solicitação de cancelamento de estorno de compra](../assets/img/pt-BR/SolicitaEstornoCancelaCompra.png)

&nbsp;
 
**ATENÇÃO: Embora a ilustração acima faça referência ao cancelamento de um estorno de uma compra, o mesmo vale para estornos de saques e de transferências.**

A operação de reversão desfaz um estorno. Normalmente, a reversão é disparada automaticamente a partir de um terminal do adquirente ou a partir do backend da bandeira caso um estorno não tenha sido completado com sucesso. Normalmente disparado a partir de mensagens com MTI = 420.

Da mesma forma que ocorre com uma requisição de estorno, uma requisição de cancelamento de estorno também pode ser negada pela processadora e enviada para o emissor somente para reconhecimento e registro da requisição em seu sistema, sem que o mesmo precise realizar nenhuma validação de saldo, entre outras validações. Neste caso o MTI da mensagem de cancelamento será igual à '`0420`' e o código de autorização será diferente de '`00`'. Para este caso o emissor deve retornar **Acknowledge** indicando o reconhecimento e aceita da transação negada.

Requisições de cancelamento de estorno aprovadas pela bandeira e processadora devem ser validadas pelo emissor e possuir o saldo da conta do cartão descontados de acordo com o valor do cancelamento do estorno, caso o cancelamento seja aprovado pelo emissor. Geralmente cancelamentos de estorno devem ser aceitos pelo emissor, pois são mensagens geradas automaticamente em caso de erro no processamento da transação de estorno, tanto na mensagem de requisição quanto na resposta da requisição. 

Para este endpoint o emissor deve retornar **Success** caso a requisição de cancelamento de estorno seja aprovada pelo emissor após todas validações terem sido executadas com sucesso. Caso a operação de cancelamento de estorno não seja permitida para a conta/cartão por algum motivo que não esteja nos padrões de retorno deve ser retornado **OperationNotPermitted**. Se a conta/cartão da transação ou o estorno da compra original não existir na base do emissor deve ser retornado **NotFoundError**. Caso o sistema do emissor esteja em baixa e não possa responder a requisição no momento deve retornar **SystemDownError**. Ocorrendo algum erro interno no sistema do emissor deve ser retornado **GeneralError**.

&nbsp;

* Parâmetros enviados no corpo da requisição:

Objeto                     | Descrição 
---------------------------| ---------------------
**ChargebackCancellation** | Objeto de cancelamento de estorno de compra, saque ou transferência contendo as informações da transação de cancelamento do estorno.

&nbsp;

* Possíveis retornos para o endpoint:

HTTP Code | Objeto                         | Descrição
----------| -------------------------------| ---------------------
200       | **Success**                    | Estorno desfeito com sucesso.
404       | **NotFoundError**              | Estorno original não encontrado.
409       | **TransactionAlreadyCanceled** | Estorno já foi cancelado.
412       | **OperationNotPermitted**      | Operação não permitida para a transação.
499       | **Acknowledge**                | Mensagem recebida pelo emissor para casos de transação negada.
503       | **SystemDownError**            | Sistema Indisponível.
500       | **GeneralError**               | Erro interno do sistema do emissor.

&nbsp;

#### Exemplo de requisição de cancelamento de estorno:

```json
{    
    "cancellation_id": "aut_dafdcec5-48b4-49c2-b8dd-1777b615b414",
    "original_chargeback_id": "aut_5a2dbb12-410c-43aa-a668-7cb16ac7562e",
    "account_id": "cta-be31f86a-dad1-43d2-b7b6-264123591160",
    "psProductCode": "010101",
    "psProductName": "CARTÃO PAYSMART GIFT CARD PF",
    "countryCode": "076",
    "source": "brand",
    "callingSystemName": "Autorizador ISO8583",
    "original_authorization_id": "932960",
    "authorization": {  
        "code": "00",
        "description": "Sucesso/Transacao Aprovada"    
    },
    "brand": "elo",
    "card": { 
        "paysmart_id": "220",
        "issuer_id": "3",
        "pan": "************3368",
        "panseq": "00"
    },
    "original_amount": { 
        "amount": 18200,
        "currency_code": "986"
    },
    "entry_mode": "chip",
    "cancellation_reason": { 
        "code": "21",
        "description": "Tempo ultrapassado na espera da resposta."    
    },
    "iso8583_message": { 
        "mti": "0420",
        "de002": "************3368",
        "de003": "003000",
        "de004": "000000018200",
        "de005": null,
        "de006": null,
        "de007": "1203125026",
        "de008": null,
        "de009": null,
        "de010": null,
        "de011": "097137",
        "de012": "095026",
        "de013": "1203",
        "de014": "2412",
        "de015": null,
        "de016": null,
        "de018": "5712",
        "de019": "076",
        "de022": "051",
        "de023": "000",
        "de024": "400",
        "de025": "21",
        "de026": null,
        "de028": null,
        "de029": null,
        "de032": "0025",
        "de033": null,
        "de035": "509257******3368D24126067611010000000",
        "de036": null,
        "de037": "202289097136",
        "de038": "932960",
        "de039": null,
        "de041": "20172289",
        "de042": "020001605270002",
        "de043": "ELO                    BARUERI       076",
        "de045": null,
        "de046": null,
        "de047": null,
        "de048": "*PRD003070",
        "de049": "986",
        "de050": null,
        "de051": null,
        "de052": null,
        "de053": null,
        "de054": null,
        "de055": null,
        "de056": null,
        "de058": "ALAMEDAXINGU          23543230007612345ELO",
        "de059": null,
        "de060": "000001000P500",
        "de062": null,
        "de063": null,
        "de090": "040009713709502512039329600000000000000000",
        "de095": null,
        "de105": null,
        "de106": null,
        "de107": null,
        "de121": null,
        "de122": null,
        "de123": null,
        "de124": null,
        "de125": null,
        "de126": null,
        "de127": "12192"
    },
    "additionalTerminalData": {
      "terminalType": "terminalWithService",
      "partialApprovalIndicator": "notSupported",
      "terminalLocationIndicator": "local",
      "cardholderPresenceIndicator": "present",
      "cardPresenceIndicator": "cardPresent",
      "cardCaptureCapabilityIndicator": "cardCaptureCapability",
      "transactionStatusIndicator": "normal",
      "transactionSecurityIndicator": "noSuspectedFraud",
      "terminalPOSType": "atm",
      "terminalInputCapability": "chip"
    },
    "eloProductCode": "070"
}
```

#### Exemplo de resposta do cancelamento acima:

```json
{    
    "message": "Operação realizada com sucesso.",
    "code": 0,
    "authorization_id": 517976,
    "balance": { 
        "amount": 200000,
        "currency_code": 986    
    }
}
```

* Para o Pós-Pago o campo "Balance" não está presente na resposta.

&nbsp;
---

### Objetos de Estorno

Os Esquemas do objetos de **ESTORNO** são estruturados da seguinte forma:

### [Chargeback](../assets/api/openapi.json#/Chargeback)

Objeto que representa uma requisição de estorno. Enviado nas requisições de operação de estorno de compra.

Atributo             | Tipo    | Obrigatório | Exemplo         | Descrição 
---------------------|---------|-------------|-----------------|--------------------
**chargeback_id**    | String  | **Sim**     | 06deb28a-d5ab-45bd-8094-054694394b2a | Identificador interno paySmart da transação de estorno.
**original_purchase_id** | String  | **Sim**     | 06deb28a-d5ab-45bd-8094-054694394b2a | Identificador interno paySmart da transação original a ser estornada. Pode ser o ID de uma compra, de um saque ou de uma transferência.
**account_id**       | String  | **Sim**     | 08deb38b-a4ac-42be-a0c5-1674a44b94c1 | Identificador interno paySmart da conta.
**psProductCode**    | String  | **Sim**     | 020101          | Código do produto interno paySmart.
**psProductName**    | String  | Não         | CARTÃO PAYSMART GIFT CARD PF | Nome do produto interno paySmart.
**countryCode**      | String  | **Sim**     | 076             | Código do país conforme ISO 3166-1.
**source**           | Enum String | **Sim** | brand           | Sistema originador da transação. Possíveis valores são: paySmart, issuer, brand.
**callingSystemName** | String | **Sim**     | Autorizador-8583 | Nome/identificador do serviço que fez essa chamada. Geralmente Autorizador-8583.
**original_authorization_id** | Integer | **Sim** | 123456          | Código de autorização retornado na transação original. Em algumas situações, esse valor pode não corresponder ao da transação original.
**authorization**    | Authorization | **Sim** | Ver definição do tipo | Informações de Autorização provenientes da bandeira ou processadora.
**brand**            | Enum String  | **Sim** | elo             | A bandeira ou esquema de pagamentos de onde a transação se originou. Possíveis valores são: elo, goodcard, mastercard, visa.
**card**             | Card   | **Sim**      | Ver definição do tipo | Informações do cartão originador da transação.
**chargeback_mode**  | Enum String | **Sim** | total           | Indica se o estorno é total ou parcial. Possíveis valores são: total, parcial.
**original_amount**   | Amount  | **Sim**     | Ver definição do tipo | Valor original da transação na moeda local da transação.
**chargeback_amount** | Amount  | **Sim**     | Ver definição do tipo | Valor a ser estornado da transação. Pode ser um valor inferior ao valor total da transação original em casos de estorno parcial.
**entry_mode**        | EntryMode | **Sim**   | chip            | Modo usado para a entrada do PAN na transação.
**chargeback_reason** | CancellationReason | **Sim** | Ver definição do tipo | Motivo do estorno da transação original.
**iso8583_message**  | ISO8583Message | **Sim** | Ver definição do tipo | Mensagem original ISO-8583 enviada pela bandeira e processada pela paySmart.
additionalTerminalData   | AdditionalTerminalData | Não | Ver difinição do tipo | Contempla os dados adicionais pertencentes ao ponto de venda/saque (por exemplo, um POS) no momento em que a transação foi realizada.
tokenPaymentData     | TokenPaymentData  | Não      | Ver definições do tipo |   Contém os Informações dos Dados Transacionais - Conjunto de dados de Pagamento via Token para estornos por QrCode ou cartão tokenizado.
hceTransaction      | Boolean                | Não      | false           |   Indica se é uma transação feita com cartão HCE (true) ou outro tipo de cartão (false).
eloProductCode | 	EloProductCode | Não  | Ver definição do tipo      | Código do produto da transação. Usado pela bandeira ELO. 


&nbsp;
---

### [ChargebackCancellation](../assets/api/openapi.json#/ChargebackCancellation)

Objeto que representa uma requisição de cancelemamento de um estorno de compra. Enviado nas requisições de operação de cancelamento de estorno de compra.

Atributo             | Tipo    | Obrigatório | Exemplo                              | Descrição 
---------------------|---------|-------------|--------------------------------------|--------------------
**cancellation_id**  | String  | **Sim**     | 06deb28a-d5ab-45bd-8094-054694394b2a | Identificador interno paySmart da transação de cancelamento.
**original_chargeback_id** | String  | **Sim**     | 06deb28a-d5ab-45bd-8094-054694394b2a | Identificador interno paySmart da transação de estorno a ser cancelada.
**account_id**       | String  | **Sim**     | 08deb38b-a4ac-42be-a0c5-1674a44b94c1 | Identificador interno paySmart da conta.
**psProductCode**    | String  | **Sim**     | 020101                               | Código do produto interno paySmart.
**psProductName**    | String  | Não         | CARTÃO PAYSMART GIFT CARD PF         | Nome do produto interno paySmart.
**countryCode**      | String  | **Sim**     | 076                                  | Código do país conforme ISO 3166-1.
**source**           | Enum String | **Sim** | brand                                | Sistema originador da transação. Possíveis valores são: paySmart, issuer, brand.
**callingSystemName** | String | **Sim**     | Autorizador-8583                     | Nome/identificador do serviço que fez essa chamada. Geralmente Autorizador-8583.
**original_authorization_id** | Integer | **Sim** | 123456                               | Código de autorização retornado na transação original. Em algumas situações, esse valor pode não corresponder ao da transação original.
**authorization**    | Authorization | **Sim** | Ver definição do tipo                | Informações de Autorização provenientes da bandeira ou processadora.
**brand**            | Enum String  | **Sim** | elo                                  | A bandeira ou esquema de pagamentos de onde a transação se originou. Possíveis valores são: elo, goodcard, mastercard, visa.
**card**             | Card   | **Sim**      | Ver definição do tipo                | Informações do cartão originador da transação.
**original_amount**   | Amount  | **Sim**     | Ver definição do tipo                | Valor original da transação na moeda local da transação.
**entry_mode**        | EntryMode | **Sim**   | chip                                 | Modo usado para a entrada do PAN na transação.
**cancellation_reason** | CancellationReason | **Sim** | Ver definição do tipo                | Motivo do cancelamento da transação original.
**iso8583_message**  | ISO8583Message | **Sim** | Ver definição do tipo                | Mensagem original ISO-8583 enviada pela bandeira e processada pela paySmart.
additionalTerminalData | AdditionalTerminalData | Não | Ver difinição do tipo                | Contempla os dados adicionais pertencentes ao ponto de venda/saque (por exemplo, um POS) no momento em que a transação foi realizada.
tokenPaymentData     | TokenPaymentData  | Não      | Ver definições do tipo               |   Contém os Informações dos Dados Transacionais - Conjunto de dados de Pagamento via Token para estornos por QrCode ou cartão tokenizado.
hceTransaction      | Boolean                | Não      | false                                |   Indica se é uma transação feita com cartão HCE (true) ou outro tipo de cartão (false).
eloProductCode | 	EloProductCode | Não  | Ver definição do tipo      | Código do produto da transação. Usado pela bandeira ELO. 
