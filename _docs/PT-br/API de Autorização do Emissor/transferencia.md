---
title: Operações de Transferência
tags: 
 - transferencia
description: Operações de transferência e cancelamentos de transferência. 
permalink: /pt-br/docs/transferencia
language: pt-br
---

# Operações de Transferência

[POST /transfers](transferencia#post-transfers) | 
[Transferências Internacionais](transferencia#transferências-internacionais) | 
[POST /transfers/cancel](transferencia#post-transferscancel) | 
[Objetos de Transferência](transferencia#objetos-de-transferência) | 
[Transfer](transferencia#transfer) | 
[TransferCancellation](transferencia#transfercancellation) | 
[TransferData](transferencia#transferdata)

### POST [/transfers](../assets/api/openapi.json#/transfer/createTransfer)

#### Consome ou credita o saldo de uma conta de acordo com o valor total informado.

&nbsp;

![Solicitação de Autorização de transferência]({{ site.url }}{{ site.baseurl }}/assets/img/pt-BR/SolicitaAutorizaTransferencia.png)

&nbsp;

Cria um novo registro de retirada de fundos ou recebimento de fundos de acordo com o código de processamento da transação. 
O registro pode ser de uma transferência negada (apenas informativo) ou de uma transferência aprovada, consumindo o saldo se for uma retirada de fundos ou creditando valor ao saldo se for um recebimento de fundos. 
O valor total informado considera o valor original e as taxas associadas à transação. 

Uma requisição de transferência que foi aprovada pela bandeira e pela processadora deve ser validada pelo emissor quanto ao saldo e demais validações específicas do mesmo. 
Uma transação que foi negada pela bandeira ou pela processadora deve ser aceita pelo emissor sem comprometimento de saldo e demais validações do mesmo. 

Uma requisição de transferência aprovada pela bandeira e pela processadora e que deve ser validada pelo emissor possui o campo MTI da mensagem original ISO-8583 igual à '`0100`' e código de autorização, campo `authorization.code`, igual à '`00`'

Uma requisição de transferência negada pela bandeira e pela processadora e que não deve ser validada pelo emissor possui o campo MTI da mensagem original ISO-8583 igual à '`0100`' ou '`0120`'  e código de autorização diferente de '`00`'

Para os casos onde a requisição é negada pela bandeira ou processadora o emissor deve retornar o objeto **Acknowledge** aceitando a negação da transação.

Para autorizar uma transferência o emissor deve validar o saldo da conta/cartão com o valor total informado na transação, caso seja uma operação de retirada de fundos. 
Quando a operação de transferência for um recebimento de fundos, o valor total informado deverá ser adicionado ao saldo da conta/cartão. 
Deve ser validado se a conta/cartão existe e está habilitado para realizar a transação. 
Deve ser validado se a transação é permitida para a conta/cartão. 
Pode também ser validado o MCC do estabelecimento comercial se é um MCC válido para a conta/cartão transacionar.

Todas as taxas aplicadas a transação de transferência já estão incluídas no valor total da transação, não sendo necessário adicionar nenhuma taxa extra ao debitar o valor total da conta/cartão do portador, caso operação de retirada de fundos.

Para este endpoint o emissor deve retornar **Success** caso a requisição da transação seja aprovada pelo emissor após todas validações terem sido executadas com sucesso. 
Caso a conta/cartão não possua saldo suficiente para realizar uma operação de retirada de fundos, o emissor deve retornar **InsufficientFunds**. 
Caso a operação de transferência não seja permitida para a conta/cartão por algum motivo que não esteja nos padrões de retorno deve ser retornado **OperationNotPermitted**. 
Se a conta/cartão da transação não existir na base do emissor deve ser retornado **NotFoundError**. 
Caso o MCC da transação não seja permitido para a operação para a conta/cartão da transação deve ser retornado **InvalidMcc**. 
Caso o sistema do emissor esteja em baixa e não possa responder a requisição no momento deve retornar **SystemDownError**. 
Ocorrendo algum erro interno no sistema do emissor deve ser retornado **GeneralError**.

&nbsp;

##### ATENÇÃO!
Existe uma situação onde o emissor deve acatar e refletir no saldo do portador o valor de uma transferência mesmo que o cartão/conta do portador não possua saldo suficiente.
Esta situação será indicada com o campo '**_forceAccept_**' igual a '**_true_**' no objeto **_Transfer_**.
Para este caso o emissor deve sempre retornar **HTTP Code 200** com o objeto **Success** no corpo da resposta. Conforme exemplo abaixo.
O emissor ainda pode recusar a transferência por cartão/conta inexistente ou transação duplicada.

```json
{
	"message": "Operação realizada com sucesso.",
	"code": 0,
	"authorization_id": 322169,
	"balance": {
		"amount": 0,
		"currency_code": 986
	}
}
```

* Para o Pós-Pago o campo "Balance" não está presente na resposta.
 
&nbsp;

#### Transferências Internacionais.

Transferências podem ser realizadas em moeda estrangeira para os emissores que possuem produtos internacionais. Para estes casos o valor total da transação sempre será convertido para a moeda do emissor, no caso Reais, e terá as taxas de Markup e IOF somadas ao valor total. O valor original da transação será o valor na moeda local onde ocorreu a transação.



&nbsp;

* Taxas que podem ser aplicadas a transação de transferência:

Taxa         | Desrição
-------------| ----------------
iof          | IOF aplicado ao valor total de transação, para transações internacionais.
markup       | Markup aplicado ao valor total da transação, para transações internacionais.
others       | Outras taxas que podem ser aplicadas ao valor total da transação.


&nbsp;

* Parâmetros enviados no corpo da requição:

Objeto              | Descrição 
--------------------| ---------------------
**Transfer**        | Objeto de transferência contendo as informações da transação. A operação poderá ser uma retirada de fundos ou recebimento de fundos.

&nbsp;

* Retornos para o endpoint:

HTTP Code | Objeto                    | Descrição
----------| --------------------------| ---------------------
200       | **Success**               | Transação aprovada com sucesso.
400       | **InsufficientFunds**     | Saldo insuficiente.
412       | **OperationNotPermitted** | Operação não permitida para a transação.
404       | **NotFoundError**         | Conta não encontrada ou cartão inexistente.
459       | **FraudSuspectError**     | Suspeita de fraude, transação negada.
483       | **InvalidMcc**            | MCC inválido.
499       | **Acknowledge**           | Mensagem recebida pelo emissor para casos de transação negada.
503       | **SystemDownError**       | Sistema Indisponível.
500       | **GeneralError**          | Erro interno do sistema do emissor.

&nbsp;

#### Exemplo de requisição de retirada de fundos:

```json
{
	"transfer_id": "aut_a8e7a6be-3e21-45e3-a433-45cb6a1e85dd",
	"account_id": "cta-be31f86a-dad1-43d2-b7b6-264123591160",
	"psProductCode": "010101",
	"psProductName": "CARTÃO PAYSMART GIFT CARD PF",
	"countryCode": "076",
	"source": "brand",
	"callingSystemName": "Autorizador ISO8583",
	"preAuthorization": false,
	"incrementalAuthorization": false,
	"authorization": {
		"code": "00",
		"description": "Sucesso/Transacao Aprovada"
	},
	"brand": "elo",
	"card": {
		"paysmart_id": "crt-82535ae1-0070-4c7f-8999-ce4431a0146d",
		"issuer_id": 3,
		"pan": "************3368",
		"panseq": "00"
	},
	"total_amount": {
		"amount": 11800,
		"currency_code": 986
	},
	"dollar_amount": null,
	"original_amount": {
		"amount": 11800,
		"currency_code": 986
	},
	"dollar_real_rate": null,
	"spread": null,
	"entry_mode": "chip",
	"processing_code": {
		"tipo_transacao": "fundsWithdrawal",
		"source_account_type": "credit_card_account",
		"destination_account_type": "not_applicable"
	},
	"holder_validation_mode": "online_pin",
	"fees": [],
	"establishment": {
		"mcc": 5712,
		"name": "ELO",
		"city": "BARUERI",
		"address": "ALAMEDAXINGU",
		"zipcode": "235432300",
		"country": "076",
		"cnpj": null
	},
	"internacional": false,
	"original_iso8583": {
		"mti": "0100",
		"de002": "************3368",
		"de003": "103000",
		"de004": "000000011800",
		"de005": null,
		"de006": null,
		"de007": "0219141112",
		"de008": null,
		"de009": null,
		"de010": null,
		"de011": "101841",
		"de012": "111112",
		"de013": "0219",
		"de014": "2412",
		"de015": null,
		"de016": null,
		"de018": "5712",
		"de019": "076",
		"de022": "051",
		"de023": "000",
		"de024": "100",
		"de025": null,
		"de026": null,
		"de028": null,
		"de029": null,
		"de032": "0025",
		"de033": null,
		"de035": "************3368D2412606***1010000000",
		"de036": null,
		"de037": "202289101841",
		"de038": null,
		"de039": null,
		"de041": "20172289",
		"de042": "020001605270002",
		"de043": "ELO                    BARUERI       076",
		"de045": null,
		"de046": null,
		"de047": null,
		"de048": "*CDT002T0*PRD003070",
		"de049": "986",
		"de050": null,
		"de051": null,
		"de052": "EFBDAF1CFDB9575A",
		"de053": null,
		"de054": null,
		"de055": "5F2A02098682020000950500000400009A032002199C01009F02060000000118009F03060000000000009F10200FA501000000000000000000000000000F0000000000000000000000049410109F1A0200769F2701009F3303E0A0C09F34030203009F3501229F360200589F370462EB99679F26088ED974CC3CA589E5",
		"de056": null,
		"de058": "ALAMEDAXINGU          23543230007612345ELO",
		"de059": null,
		"de060": "000001000PH00",
		"de062": null,
		"de063": null,
		"de090": null,
		"de095": null,
		"de105": null,
		"de106": null,
		"de107": null,
		"de121": null,
		"de122": null,
		"de123": null,
		"de124": null,
		"de125": null,
		"de126": null,
		"de127": 12192
	},
    "forceAccept": false,
    "transferData": {
      "paymentType": "P2P",
      "uniqueReferenceNumber": "123456abcde",
      "sendersName": "Mario Chaves",
      "sendersAddress": "Av. Assis Brasil, 1001",
      "sendersCity": "Porto Alegre",
      "countryOrStateCodeIfUS": "076",
      "cardholderZipcode": "90110230",
      "cardholderIdentificationNumber": "31953048056",
      "originOfFunds": "01",
      "birthDateOfSender": "11231976",
      "recipientsName": "Paulo Nunes",
      "additionalTransferData": "string",
      "recipientCode": "string",
      "fundSenderEmail": "mariochaves@email.com",
      "fundRecipientEmail": "paulonunes@email.com",
      "fundSenderPhone": "51991021523",
      "fundRecipientPhone": "51991022533",
      "deviceID": "string",
      "cardholderCpfOrCnpj": "00031953048056",
      "BINOrigin": 60102010,
      "BINDestination": 60102010,
      "originCardLast4digits": 1234,
      "destinationCardLast4digits": 1234
    },
    "additionalTerminalData": {
      "terminalType": "terminalWithService",
      "partialApprovalIndicator": "notSupported",
      "terminalLocationIndicator": "local",
      "cardholderPresenceIndicator": "present",
      "cardPresenceIndicator": "cardPresent",
      "cardCaptureCapabilityIndicator": "cardCaptureCapability",
      "transactionStatusIndicator": "normal",
      "transactionSecurityIndicator": "noSuspectedFraud",
      "terminalPOSType": "atm",
      "terminalInputCapability": "chip"
    },
    "eloProductCode": "070"
}
```

&nbsp;

* Para transações por QrCode ou com cartão tokenizado, o objeto da transação poderá apresentar o campo "tokenPaymentData", com informações referentes ao pagamento via token.

### Exemplo do campo referente aos dados de pagamento via token para transações por QrCode ou com cartão tokenizado:

```json
{
  "tokenPaymentData": {
    "fPan": "1234",
    "requesterIdToken": "12345678901",
    "pan": "************1234",
    "tokenPSN": "001",
    "tokenExpirationDate": "2908",
    "tokenStatus": "0100",
    "tokenCryptogramVerificationResults": "0200",
    "EMVTokenCryptogramVerificationResults": "0200",
    "tokenConstraintsVerificationStatus": "0300",
    "transactionDateTimeConstraint": "OK",
    "transactionAmountConstraint": "OK",
    "usageConstraint": "OK",
    "tokenATCVerificationResults": "E001",
    "CVE2TokenCryptogramVerificationStatus": "E001",
    "merchantVerification": "OK",
    "magstripeTokenCryptogramVerificationResults": "0200",
    "CVE2OutputTokenCryptogramVerificationResults": "0201"
  }
}
```
---

#### Exemplo de resposta para a transferência acima:

```json
{
	"message": "Operação realizada com sucesso.",
	"code": 0,
	"authorization_id": 322169,
	"balance": {
		"amount": 200000,
		"currency_code": 986
	}
}
```

* Para o Pós-Pago o campo "Balance" não está presente na resposta.

&nbsp;

---
&nbsp;

### POST [/transfers/cancel](../assets/api/openapi.json#/purchase/createTransferCancellation)

#### Reversão de transferência.

&nbsp;

![Solicitação de Cancelamento de transferencia](../assets/img/pt-BR/SolicitaCancelaTransferencia.png)

&nbsp;

A operação de reversão desfaz uma transferência, devolvendo o saldo consumido se a transferência a ser desfeita for um recebimento de fundos. Caso a operação a ser desfeita seja uma retirada de fundos o saldo deverá ser acrescido do valor. Normalmente a reversão é disparada automaticamente a partir de um terminal do adquirente/credenciador ou a partir do backend da bandeira caso uma transação não tenha sido completada com sucesso. Normalmente disparado a partir de mensagens com MTI = 420.

Da mesma forma que ocorre com uma requisição de transferência, uma requisição de cancelamento de transferência também pode ser negada pela processadora e enviada para o emissor somente para reconhecimento e registro da requisição em seu sistema, sem que o mesmo precise realizar nenhuma validação de saldo, entre outras validações, para este caso. Neste caso o MTI da mensagem de cancelamento será igual à '`0420`' e o código de autorização será diferente de '`00`'. Para este caso o emissor deve retornar **Acknowledge** indicando o reconhecimento e aceita da transação negada.

Requisições de cancelamento de transferência aprovada pela bandeira e processadora devem ser validadas pelo emissor e possuir o saldo da conta do cartão retornado de acordo com o valor total do cancelamento da transferência, caso o cancelamento seja aprovado pelo emissor. Geralmente cancelamentos devem ser aceitos pelo emissor, pois são mensagens geradas automaticamente em caso de erro no processamento da transação, tanto na mensagem de requisição quanto na resposta da requisição. 

Para este endpoint o emissor deve retornar **Success** caso a requisição de cancelamento seja aprovada pelo emissor após todas validações terem sido executadas com sucesso. Caso a operação de cancelamento não seja permitida para a conta/cartão por algum motivo que não esteja nos padrões de retorno deve ser retornado **OperationNotPermitted**. Se a conta/cartão da transação ou transferência original não existir na base do emissor deve ser retornado **NotFoundError**. Caso o sistema do emissor esteja em baixa e não possa responder a requisição no momento deve retornar **SystemDownError**. Ocorrendo algum erro interno no sistema do emissor deve ser retornado **GeneralError**.

&nbsp;

* Parâmetros enviados no corpo da requição:

Objeto                   | Descrição 
-------------------------| ---------------------
**TransferCancellation** | Objeto de cancelamento de transferência contendo as informações da transação de cancelamento.

&nbsp;

* Possíveis retornos para o endpoint:

HTTP Code | Objeto                         | Descrição
----------| -------------------------------| ---------------------
200       | **Success**                    | Transação desfeita com sucesso.
404       | **NotFoundError**              | Transação original não encontrada.
409       | **TransactionAlreadyCanceled** | Transação já foi cancelada.
412       | **OperationNotPermitted**      | Operação não permitida para a transação.
499       | **Acknowledge**                | Mensagem recebida pelo emissor para casos de transação negada.
503       | **SystemDownError**            | Sistema Indisponível.
500       | **GeneralError**               | Erro interno do sistema do emissor.

&nbsp;

#### Exemplo de requisição de cancelamento de uma retirada de fundos:

```json
{
    "cancellation_id": "aut_3594713f-89c3-4b7b-aa5c-c0c179318be8",
    "original_transfer_id": "aut_a8e7a6be-3e21-45e3-a433-45cb6a1e85dd",
    "account_id": "cta-be31f86a-dad1-43d2-b7b6-264123591160",
    "psProductCode": "010101",
    "psProductName": "CARTÃO PAYSMART GIFT CARD PF",
    "countryCode": "076",
    "source": "brand",
    "callingSystemName": "Autorizador ISO8583",
    "original_authorization_id": 393981,
    "authorization": {
        "code": "00",
        "description": "Sucesso/Transacao Aprovada"
    },
    "brand": "elo",
    "card": { 
        "paysmart_id": "crt-82535ae1-0070-4c7f-8999-ce4431a0146d",
        "issuer_id": 3,
        "pan": "************3368",
        "panseq": "00"
    },
    "original_amount": { 
        "amount": 7700,
        "currency_code": 986
    },
    "entry_mode": "chip",
    "cancellation_reason": { 
        "code": 21,
        "description": "Tempo ultrapassado na espera da resposta"
    },
    "iso8583_message": {
        "mti": "0420",
        "de002": "************3368",
        "de003": "103000",
        "de004": "000000007700",
        "de005": null,
        "de006": null,
        "de007": "0213125802",
        "de008": null,
        "de009": null,
        "de010": null,
        "de011": "101336",
        "de012": "095802",
        "de013": "0213",
        "de014": "2412",
        "de015": null,
        "de016": null,
        "de018": "5712",
        "de019": "076",
        "de022": "051",
        "de023": "000",
        "de024": "400",
        "de025": "21",
        "de026": null,
        "de028": null,
        "de029": null,
        "de032": "0025",
        "de033": null,
        "de035": "************3368D2412606***1010000000",
        "de036": null,
        "de037": "202289101336",
        "de038": "393981",
        "de039": null,
        "de041": "20172289",
        "de042": "020001605270002",
        "de043": "ELO                    BARUERI       076",
        "de045": null,
        "de046": null,
        "de047": null,
        "de048": "*PRD003070",
        "de049": "986",
        "de050": null,
        "de051": null,
        "de052": null,
        "de053": null,
        "de054": null,
        "de055": null,
        "de056": null,
        "de058": "ALAMEDAXINGU          23543230007612345ELO",
        "de059": null,
        "de060": "000001000P500",
        "de062": null,
        "de063": null,
        "de090": "010010133609580102133939810000000000000000",
        "de095": null,
        "de105": null,
        "de106": null,
        "de107": null,
        "de121": null,
        "de122": null,
        "de123": null,
        "de124": null,
        "de125": null,
        "de126": null,
        "de127": 12192
    },
    "transferData": {
      "paymentType": "P2P",
      "uniqueReferenceNumber": "123456abcde",
      "sendersName": "Mario Chaves",
      "sendersAddress": "Av. Assis Brasil, 1001",
      "sendersCity": "Porto Alegre",
      "countryOrStateCodeIfUS": "076",
      "cardholderZipcode": "90110230",
      "cardholderIdentificationNumber": "31953048056",
      "originOfFunds": "01",
      "birthDateOfSender": "11231976",
      "recipientsName": "Paulo Nunes",
      "additionalTransferData": "string",
      "recipientCode": "string",
      "fundSenderEmail": "mariochaves@email.com",
      "fundRecipientEmail": "paulonunes@email.com",
      "fundSenderPhone": "51991021523",
      "fundRecipientPhone": "51991022533",
      "deviceID": "string",
      "cardholderCpfOrCnpj": "00031953048056",
      "BINOrigin": 60102010,
      "BINDestination": 60102010,
      "originCardLast4digits": 1234,
      "destinationCardLast4digits": 1234
    },
    "additionalTerminalData": {
      "terminalType": "terminalWithService",
      "partialApprovalIndicator": "notSupported",
      "terminalLocationIndicator": "local",
      "cardholderPresenceIndicator": "present",
      "cardPresenceIndicator": "cardPresent",
      "cardCaptureCapabilityIndicator": "cardCaptureCapability",
      "transactionStatusIndicator": "normal",
      "transactionSecurityIndicator": "noSuspectedFraud",
      "terminalPOSType": "atm",
      "terminalInputCapability": "chip"
    },
    "eloProductCode": "070"
}
```

#### Exemplo de resposta do cancelamento acima:

```json
{
    "message": "Operação realizada com sucesso.",
    "code": 0,
    "authorization_id": 797292,
    "balance": { 
        "amount": 200000,
        "currency_code": 986
    }
}
```

* Para o Pós-Pago o campo "Balance" não está presente na resposta.

&nbsp;
---

### Objetos de Transferência

Os Esquemas do objetos de **TRANSFERÊNCIA** são estruturados da seguinte forma:

&nbsp;
---
### [Transfer](../assets/api/openapi.json#/Transfer)

Objeto que representa uma transferência entre contas. Uma transferência pode ser uma retirada de fundos ou um recebimento de fundos.

Atributo             | Tipo    | Obrigatório | Exemplo                         | Descrição 
---------------------|---------|-------------|---------------------------------|--------------------
**transfer_id**      | String  | **Sim**     | 06deb28a-d5ab-45bd-8094-054694394b2a | Identificador interno paySmart da transação.
**account_id**       | String  | **Sim**     | 08deb38b-a4ac-42be-a0c5-1674a44b94c1 | Identificador interno paySmart da conta.
**psProductCode**    | String  | **Sim**     | 020101                          | Código do produto interno paySmart.
**psProductName**    | String  | Não         | CARTÃO PAYSMART GIFT CARD PF         | Nome do produto interno paySmart.
**countryCode**      | String  | **Sim**     | 076                             | Código do país conforme ISO 3166-1.
**source**           | Enum String | **Sim** | brand                           | Sistema originador da transação. Possíveis valores são: paySmart, issuer, brand.
**callingSystemName** | String | **Sim**     | Autorizador-8583                | Nome/identificador do serviço que fez essa chamada. Geralmente Autorizador-8583.
**preAuthorization** | Boolean | **Sim**     | false                           | Indica se é uma pré-autorização(true) ou autorização normal(false).
**incrementalAuthorization** | Boolean | **Sim** | false                           | Indica se é uma autorização incremental(true) ou não(false).
**authorization**    | Authorization | **Sim** | Ver definição do tipo         | Informações de Autorização provenientes da bandeira ou processadora.
**brand**            | Enum String  | **Sim** | elo                            | A bandeira ou esquema de pagamentos de onde a transação se originou. Possíveis valores são: elo, goodcard, mastercard, visa.
**card**             | Card   | **Sim**      | Ver definição do tipo           | Informações do cartão originador da transação.
**total_amount**     | Amount  | **Sim**     | Ver definição do tipo           | Valor total da transação com todas as taxas da transação somandas, caso existam.
dollar_amount        | Amount  | Não         | Ver definição do tipo           | Valor da transação em dolares caso enviado na mensagem ISO-8583 de autorização ou valor na moeda local da transação. Somente se transação internacional.
**original_amount**  | Amount  | **Sim**     | Ver definição do tipo           | Valor original da transação na moeda local da transação.
dollar_real_rate     | String  | Não         | 4.40                            | Cotação do Dolar para conversão de câmbio Dólar -> Real na data da transação. Somente se transação internacional.
spread               | String  | Não         | 0.04                            | Spread (ou markup) aplicado ao valor total da transação. Somente se transação internacional.
**entry_mode**       | EntryMode | **Sim**   | chip                            | Modo usado para a entrada do PAN na transação.
**processing_code**  | ProcessingCode | **Sim** | Ver definição do tipo           | Código de processamento da transação.
holder_validation_mode | Enum String | Não   | online_pin                      | Modo usado para validar o portador.
**fees**             | Array Fee | **Sim**   | Ver definição do tipo           | Lista de taxas da transação.
**establishment**    | Establishment | **Sim** | Ver definição do tipo         | Informações do estabelecimento onde ocorreu a transação.
**internacional**    | Boolean | **Sim**     | false                           | Indica se a transação é internacional(true) ou nacional(false).
**original_iso8583** | ISO8583Message | **Sim** | Ver definição do tipo        | Mensagem original ISO-8583 enviada pela bandeira e processada pela paySmart.
**forceAccept**      | Boolean          | Não    | false                       | Força que a transação seja aceita, mesmo que não tenha saldo suficiente ou algo do tipo. O emissor ainda pode recusar a compra por cartão/conta inexistente ou transação duplicada.  
**transferData**     | TransferData  | **Sim**  | Ver defnição do tipo         | Dados específicos da transferência, com informações como tipo de transferência, entre outros. 
authorizationAdvice  | Boolean        | Não   | true                         | Indica se esta transferência é um aviso de autorização de transferência (true) ou é uma requisição de transferência que deve ser validada pelo emissor (false ou não informado). Avisos de autorização devem ser aceitos pelo emissor, pois já foram aprovados pelo sistema de Stand-In da bandeira ou processadora mediante parâmetros informados pelo emissor no sistema de Stand-In.
additionalTerminalData   | AdditionalTerminalData | Não | Ver difinição do tipo | Contempla os dados adicionais pertencentes ao ponto de venda/saque (por exemplo, um POS) no momento em que a transação foi realizada.
tokenPaymentData     | TokenPaymentData  | Não      | Ver definições do tipo  |   Contém os Informações dos Dados Transacionais - Conjunto de dados de Pagamento via Token para transferências por QrCode ou cartão tokenizado.
hceTransaction      | Boolean                | Não      | false                                |   Indica se é uma transação feita com cartão HCE (true) ou outro tipo de cartão (false).
eloProductCode | 	EloProductCode | Não  | Ver definição do tipo      | Código do produto da transação. Usado pela bandeira ELO. 


&nbsp;
---

### [TransferCancellation](../assets/api/openapi.json#/TransferCancellation)

Objeto que representa uma requisição de cancelamento de transferência. Enviado nas requisições de operação de cancelamento de uma retirada de fundos ou de um recebimento de fundos.

Atributo             | Tipo    | Obrigatório | Exemplo                         | Descrição 
---------------------|---------|-------------|---------------------------------|--------------------
**cancellation_id**  | String  | **Sim**     | 06deb28a-d5ab-45bd-8094-054694394b2a | Identificador interno paySmart da transação.
**original_transfer_id** | String  | **Sim**     | 06deb28a-d5ab-45bd-8094-054694394b2a | Identificador interno paySmart da transação original a ser cancelada.
**account_id**       | String  | **Sim**     | 08deb38b-a4ac-42be-a0c5-1674a44b94c1 | Identificador interno paySmart da conta.
**psProductCode**    | String  | **Sim**     | 020101                          | Código do produto interno paySmart.
**psProductName**    | String  | Não         | CARTÃO PAYSMART GIFT CARD PF         | Nome do produto interno paySmart.
**countryCode**      | String  | **Sim**     | 076                             | Código do país conforme ISO 3166-1.
**source**           | Enum String | **Sim** | brand                           | Sistema originador da transação. Possíveis valores são: paySmart, issuer, brand.
**callingSystemName** | String | **Sim**     | Autorizador-8583                | Nome/identificador do serviço que fez essa chamada. Geralmente Autorizador-8583.
**original_authorization_id** | Integer | **Sim** | 123456                     | Código de autorização retornado na transação original. Em algumas situações, esse valor pode não corresponder ao da transação original.
**authorization**    | Authorization | **Sim** | Ver definição do tipo         | Informações de Autorização provenientes da bandeira ou processadora.
**brand**            | Enum String  | **Sim** | elo                            | A bandeira ou esquema de pagamentos de onde a transação se originou. Possíveis valores são: elo, goodcard, mastercard, visa.
**card**             | Card   | **Sim**      | Ver definição do tipo           | Informações do cartão originador da transação.
**original_amount**  | Amount  | **Sim**     | Ver definição do tipo           | Valor original da transação na moeda local da transação.
**entry_mode**       | EntryMode | **Sim**   | chip                            | Modo usado para a entrada do PAN na transação.
**cancellation_reason** | CancellationReason | **Sim** | Ver definição do tipo | Motivo do cancelamento da transação original.
**iso8583_message**  | ISO8583Message | **Sim** | Ver definição do tipo        | Mensagem original ISO-8583 enviada pela bandeira e processada pela paySmart.
**transferData**     | TransferData  | **Sim**  | Ver defnição do tipo         | Dados específicos da transferência sendo cancelada, com informações como tipo de transferência, entre outros. 
additionalTerminalData   | AdditionalTerminalData | Não | Ver difinição do tipo | Contempla os dados adicionais pertencentes ao ponto de venda/saque (por exemplo, um POS) no momento em que a transação foi realizada.
tokenPaymentData     | TokenPaymentData  | Não      | Ver definições do tipo  |   Contém os Informações dos Dados Transacionais - Conjunto de dados de Pagamento via Token para transferências por QrCode ou cartão tokenizado.
hceTransaction      | Boolean                | Não      | false                                |   Indica se é uma transação feita com cartão HCE (true) ou outro tipo de cartão (false).
eloProductCode | 	EloProductCode | Não  | Ver definição do tipo      | Código do produto da transação. Usado pela bandeira ELO. 

&nbsp;
---

### [TransferData](../assets/api/openapi.json#/TransferData)

Objeto que representa os dados específicos da transferência, que pode ser uma retirada de fundos ou recebimento de fundos, de acordo com o código de processamento da transação.
* Código de processamento igual à **'fundsWithdrawal'** representa uma retirada de fundos.
* Código de processamento igual à **'transferFunds'** representa um recebimento de fundos.

Os atributos que não são obrigatórios serão enviados somente se capturados pelo Gateway de Transferências da bandeira.

&nbsp;

Atributo             | Tipo    | Obrigatório | Exemplo          | Descrição 
---------------------|---------|-------------|------------------|--------------------
**paymentType**      | Enum String  | **Sim**  | P2P            | Tipo de Pagamento. Este campo deve conter um dos seguintes itens: A2A, B2A, P2P, CSB ou DSB.
uniqueReferenceNumber | string | Não        | 123456abcde       | Número Único de referência. Número de Referência atribuído pelo Originador da Transferência de Fundos (Gateway).
sendersName         | String   | Não       | Mario Chaves       | Nome do Remetente. Nome de quem está enviando os fundos na transferência.
sendersAddress      | String   | Não       | Av. Assis Brasil, 1001. | Endereço do Remetente. Endereço de quem está enviando os fundos na transferência.
sendersCity         | String   | Não       | Porto Alegre      | Cidade do Remetente. Cidade de quem está enviando os fundos na transferência.
countryOrStateCodeIfUS | String | Não     | 076                 | Código do País ou código estado se o país for os Estados Unidos. Código do País de quem está enviando os fundos na transferência. Se o país for Estados Unidos deve ser enviado o código do estado e não o do país.
cardholderZipcode   | String  | Não      | 90110230           | CEP do Portador. CEP de quem está retirando ou recebendo os fundos na transferência. Na transação de retirada de fundos - Campo refere-se ao Destinatário. Na transação de recebimento de fundos - Campo refere-se ao Remetente.
cardholderIdentificationNumber | String | Não | 31953048056 | Número de identificação do portador impactado com esta transação.
originOfFunds       | String  | Não     | 01                | Origem dos fundos. A origem dos fundos que o remetente está utilizando. Valores válidos incluem: 01 - Crédito; 02 - Débito; 03 - Pré pago; 04 - Dinheiro; 05 - Cheque; 06 - ACH/FED; 07-16 - Reservado para Uso Futuro; 17 - Conta Corrente; 18 - Conta Poupança; 19 - Outro.
birthDateOfSender  | String  | Não     | 11231976            | Data de Nascimento do remetente. O formato deve ser MMDDAAAA.
recipientsName     | String | Não     | Paulo Nunes           | Nome do destinatário. Nome de quem está recebendo os fundos da transferência (recebedor).
additionalTransferData | String | Não | N/A                 | Dados Adicionais da Transferência. Exclusivamente para transações domésticas. Carrega dados complementares para transações de transferência de fundos. O conteúdo das informações deste elemento é livre e estabelecido pelo próprio Gateway.
recipientCode     | String | Não   | 1234                    | Código do Destinatário. Exclusivamente para transações domésticas. Apresenta o código do Destinatário constante no Diretório do Gateway (código do recebedor).
fundSenderEmail   | String | Não | mariochaves@email.com    | E-mail do Remetente dos fundos. Exclusivamente para transações domésticas, sendo enviado se capturado pelo gateway.
fundRecipientEmail | String | Não | paulonunes@email.com    | E-mail do Destinatário dos fundos. Exclusivamente para transações domésticas, sendo enviado se capturado pelo gateway.
fundSenderPhone    | String | Não | 51991021523             | Telefone do Remetente dos fundos. Exclusivamente para transações domésticas, sendo enviado se capturado pelo gateway.
fundRecipientPhone | String | Não | 51991022533             | Telefone do Destinatário dos fundos. Exclusivamente para transações domésticas, sendo enviado se capturado pelo gateway.
deviceID           | String | Não | N/A                     | Device ID. Exclusivamente para transações domésticas, onde este campo deve ser enviado com o conteúdo que identifica de forma única o dispositivo que originou a transação de retirada ou recebimento de fundos.
cardholderCpfOrCnpj | String | Não | 00031953048056         | CPF/CNPJ do Portador do cartão desta transação. Exclusivamente para transações domésticas, onde este campo deve ser enviado se a informação for capturada pelo gateway. Dado que identifica o CPF / CNPJ do portador do cartão desta transação. Para transação de retirada de fundos, deverá ser considerado o CPF / CNPJ do remetente dos fundos da transferência. Para transação de recebimento de fundos, deverá ser considerado o CPF / CNPJ do destinatário dos fundos da transferência.
BINOrigin          | String | Não | 60102010                 | BIN Retirada. Exclusivamente para transações domésticas de recebimento de fundos, onde este campo é mandatório se a origem dos fundos for uma conta cartão.
BINDestination     | String | Não | 60102010                | BIN Destino. Exclusivamente para transações domésticas de retirada de fundos, onde este campo é mandatório se o destino dos fundos for uma conta cartão.
originCardLast4digits | String | Não | 1234                 | 4 últimos dígitos do cartão da retirada de fundos. Exclusivamente para transações domésticas de recebimento de fundos, onde este campo é mandatório se a origem dos fundos for uma conta cartão.
destinationCardLast4digits | String | Não | 1234            | 4 últimos dígitos do cartão de destino dos fundos. Exclusivamente para transações domésticas de retirada de fundos, onde este campo é mandatório se o destino dos fundos for uma conta cartão.
