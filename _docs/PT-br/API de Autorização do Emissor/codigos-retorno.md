---
title: Códigos de Retorno e Erro da API do Emissor Pré-Pago e Pós-Pago
tags: 
 - codigo
 - retorno
 - erro
description: Códigos de Retorno e Erro da API do Emissor Pré-Pago e Pós-Pago (API unificada). 
permalink: /pt-br/docs/codigos-retorno
language: pt-br
---

# Códigos de Retorno e Erro da API do Emissor Pré-Pago e Pós-Pago

Mapeamento de códigos de autorização ou erro que podem ser enviados para o emissor através do campo *``"authorization"."code"``* nos endpoints da **API do Emissor Pré-Pago ou Pós-Pago**.

> **Atenção**
>
>**Obs. 1:** Orientamos que o campo *``"authorization"."code"``* seja o primeiro campo validado ao receber uma requisição, uma vez que se o valor for **DIFERENTE** de *``"00"``*, basta que o emissor responsa com *``HTTP Code = 499``*, sem realizar nenhuma outra validação interna.
>
>**Obs. 2:** Orientamos fortemente que o emissor realize as validações sobre o campo *``"authorization"."code"``* e não sobre o campo *``"authorization"."description"``*.

COD. ERRO PAYSMART (*``"authorization"."code"``*) | COD. RETORNO BANDEIRA (*``"DE039``"*) | DESCRIÇÃO DO ERRO (**NÃO** corresponde ao campo *``"authorization"."description"``*) 
------------------ | --------------------- | --------------------
"00"	               | 00	                   | Transação aprovada.
"5"	               | 04	                   | Transação negada - Genérica, Refazer a transação.
"6"	               | 04	                   | Erro genérico na mensagem, não enquadrado em outras situações específicas.
"7"	               | 78	                   | Cartão bloqueado - motivo bloqueio de segurança - código de bloqueio 18.
"10"	               | 57	                   | Transação negada.
"12"	               | 04	                   | Refazer a transação.
"13"	               | 13	                   | Valor da transação inválido.
"14"	               | 14	                   | Número do cartão inválido / Cartão não cadastrado na base / Cartão existente na base, bloqueado com motivo diferente dos citados nas demais condições.
"17A"	               | 62	                   | Conta com status de bloqueada (atraso etc.).
"17B"	               | 14*                   | Conta com status de cancelada (pedido do titular).
"17C"	               | 14*                   | Conta com status de encerrada (encerrada pelo banco).
"17D"	               | 14*                   | Conta com status de enquadrada (crédito em liquidação).
"19"	               | 56	                   | Emissor da mensagem não encontrado no arquivo de configuração de regras de emissor.
"21"	               | 12	                   | Cancelamento invalido - Caso de estorno/reversão quando a mensagem original foi encontrada na base de transações mas NÃO FOI estornada devido ter se esgotado o prazo permitido e o cancelamento não pode ser concluído .
"23"	               | 23	                   | Valor da prestação inválido, fora dos limites parametrizados para o Emissor. 
"25"	               | 56	                   | Número da conta inválido ou não cadastrado.
"28"	               | 96	                   | Transação não autorizada - indisponibilidade de algum arquivo.
"30A"	               | 30	                   | Erro de formatação da mensagem - Faltando campos mandatórios para a transação. (Ex: bit22, bit2, bit4, bit55).
"30B"	               | 30	                   | Erro de formatação da mensagem - Transação internacional (Código de país[Bit 19] diferente de 076[Brasil]).
"30C"	               | 30	                   | Erro de formatação da mensagem - Existência de campos com valor ou conteúdo inválido.
"30D"	               | 30	                   | Erro de formatação da mensagem - Transação de autorização com CHIP e PINData não informado.
"30E"	               | 63	                   | Erro de formatação da mensagem - Transação manual(digitada) e CVE2 não informado(Bit126 ausente).
"30F"	               | 30	                   | Erro de formatação da mensagem - PAN do Bit 2 diferente do PAN da trilha 2 do cartão(Bit 35) quando presente.
"30G"	               | 30	                   | Erro de formatação da mensagem - Moeda local da transação(Bit 49) diferente de USD e não veio valor da transação na moeda de liquidação(Bit 5) nem a moeda da liquidação(Bit 50).
"30H"	               | 30	                   | Erro de formatação da mensagem - Veio valor convertido(Bit 6) para moeda do emissor(Bit 51), mas a moeda do emissor é diferente de Reais.
"30I"	               | 30	                   | Erro de formatação da mensagem - Transação de estorno(0400) / cancelamento (0420) parcial e valor original não veio na mensagem(Bit 54).
"30J"	               | 30	                   | Erro de formatação da mensagem - Saque/cash advance sem trilha 2.
"30K"	               | 30	                   | Erro de formatação da mensagem - Transação com tarja e capacidade de aceitar PIN e PIN não informado.
"38A"	               | 38	                   | COMPRA | Excedido o número de tentativas permitidas de digitação da senha.
"38B"	               | 75	                   | SAQUE | Excedido o número de tentativas permitidas de digitação da senha.
"41"	               | 41	                   | Cartão perdido.
"41A"	               | 41	                   | Transação negada – Cartão extraviado. (COD BLOQUEIO 21).
"41B"	               | 41	                   | Transação negada – Cartão extraviado. (COD BLOQUEIO 22).
"43"	               | 43	                   | Cartão roubado.
"51"	               | 51	                   | Saldo insuficiente - O saldo do cliente ou limite de crédito não é suficiente para efetuar o pagamento solicitado.
"51A"	               | 51	                   | Saldo disponível na Conta Colchão insuficiente.
"54"	               | 54	                   | Cartão vencido.
"55"	               | 55	                   | Senha inválida. A senha digitada pelo cliente não confere.
"56"	               | 56	                   | Sem registro do cartão no sistema.
"57A"	               | 57	                   | Transação não permitida para o cartão - Transações com conta de origem diferente de conta de cartão de crédito.
"57B"	               | 57	                   | Transação não permitida para o cartão - Modo de entrada do cartão diferente dos modos suportados pelo emissor.
"57C"	               | 57	                   | Transação não permitida para o cartão - Compra parcelada não suportada.
"57D"	               | 12	                   | Transação não permitida para este cartão - Estorno de uma transação com prazo excedido, maior que 30 dias.
"57E"	               | 57	                   | Transação não permitida para o cartão - Transação financeira(0200) com código de processamento diferente de saque, consulta de saque(ou Bit 48 subcampo VPS ausente ou diferente de 1) e consulta de saldo.
"57F"	               | 57	                   | Transação inválida para o cartão.
"57G"	               | 57	                   | Transação não permitida para o cartão - Cash Over Não suportado. 
"57H"	               | 51	                   | Transação não permitida para o cartão - Verificação de Conta de Cartão não suportada.
<code style="color : red">"57I"</code>	               | -	                   | <code style="color : red">Código inutilizado.</code> 
"57J"	               | 57	                   | Transação Fallback quando fallback não deve ser autorizado.
"57K"	               | 57	                   | Transação não permitida para o cartão - Verificação de endereço não suportado.
"57L"	               | 57	                   | Código da transação (processing code) não localizado na tabela de parametrização de transações para o produto
"58"	               | 58	                   | MCC inválido, não cadastrada na base de dados  - VALIDAÇÃO NÃO IMPLEMENTADA.
"59A"	               | 54	                   | Transação negada por suspeita de fraude - A data de validade do cartão informada na mensagem ISO-8583 não confere com a data da base de dados.
"59B"	               | 59	                   | Transação negada por suspeita de fraude - Cartão bloqueado na base por motivo de suspeita de fraude (código bloqueio 31).
<code style="color : red">"59C"</code>	               | -	                   | <code style="color : red">Código inutilizado.</code>
"59D"	               | 13	                   | Valor da transação acima do valor máximo parametrizado para o produto.
"59E"	               | 59	                   | Transação negada por suspeita de fraude - ELO Áquila.
"61A"	               | 61	                   | Autorizador na ELO negou a autorização. Excedeu limite para a atividade na Elo e o Banco emissor está fora do ar ou não respondeu a tempo. Esse código, nessa condição, não será retornado pela paySmart
"61B"	               | 61	                   | Valor do limite diário de compras ou saques parametrizado para o produto foi excedido.
"62"	               | 62	                   | Bloqueio Temporário de Cobrança.
"63"	               | 63	                   | Transação negada - Violação de Segurança.
"63A"	               | 63	                   | Transação não autorizada - Cartão Virtual com dados de validação incorretos
"63B"	               | 63	                   | Transação não autorizada - Cartão Virtual cancelado
"63C"	               | 63	                   | Transação não autorizada - Cartão Virtual desativado
"63D"	               | 54	                   | Transação não autorizada - Cartão Virtual expirado
"63E"	               | 57	                   | Transação não autorizada - Excedido o limite de tentativas incorretas de validação
"63F"	               | 13	                   | Transação não autorizada - Valor inválido
"63G"	               | 57	                   | Transação não autorizada - Moeda inválida
"63H"	               | 54	                   | Transação não autorizada - Data inválida
"63I"	               | 57	                   | Transação não autorizada - Dados do Emissor inválidos
"63J"	               | 57	                   | Transação não autorizada - Limite de utilizações esgotado
"63K"	               | 96	                   | Transação não autorizada - Erro interno paySmart
"63L"	               | 63	                   | Transação não autorizada - Cartão Virtual bloqueado
"63M"	               | 96	                   | Não foi possível completar a operação
"64"	               | 64	                   | Valor abaixo do mínimo permitido pelo emissor. 
"65"	               | 65*                   | Limite de número de compras e saques permitidos excedido. 
"76"	               | 76	                   | Conta “PARA” inválida ou inexistente.
"76A"	               | 12	                   | Cancelamento invalido - Caso de estorno/reversão quando a mensagem original não foi encontrada na base de transações.
"77"	               | 77	                   | Conta “DE” inválida ou inexistente.
"77A"	               | 12	                   | Cancelamento invalido.
"77B"	               | 12	                   | Cancelamento já foi efetuado - Caso de estorno/reversão quando a mensagem original foi encontrada na base de transações do Mongo mas o cancelamento ou estorno já tinha sido efetuado em uma transação anterior.
<code style="color : red">"78"</code>	               | <code style="color : red">"78"</code>	                   | <code style="color : red">Cartão novo não desbloqueado pelo portador junto ao emissor - Bloqueio temporário.</code><sub>Código inutilizado</sub>
"78A"	               | 78	                   | Cartão novo não desbloqueado pelo portador junto ao emissor - Bloqueio temporário.
<code style="color : green">"78B"</code>	               | <code style="color : green">"78"</code>	                   | <code style="color : green">Cartão bloqueado temporariamente.</code><sub>Criado em 09/2023</sub>
<code style="color : green">"78C"</code>	               | <code style="color : green">"78"</code>	                   | <code style="color : green">Cartão bloqueado por excesso de tentativas de senha inválida. Realize a troca de senha.</code><sub>Criado em 09/2023</sub>
<code style="color : green">"78D"</code>	               | <code style="color : green">"78"</code>	                   | <code style="color : green">Funcionalidade do cartão bloqueada pelo portador.</code><sub>Criado em 09/2023</sub>
<code style="color : green">"78E"</code>	               | <code style="color : green">"78"</code>	                   | <code style="color : green">Contactless do cartão desativado pelo portador.</code><sub>Criado em 09/2023</sub>
"7A"	               | 59	                   | Cartão bloqueado - motivo bloqueio preventivo de fraude - código de bloqueio 30.
"7B"	               | 14*                   | Cartão bloqueado - motivo bloqueio fraude/falsificado - código de bloqueio 34.
"7C"	               | 14*                   | Cartão bloqueado - motivo bloqueio auto-fraude - código de bloqueio 33.
"7D"	               | 14*                   | Cartão bloqueado - motivo bloqueio fraude - conivência estabelecimento - código de bloqueio 32.
"7E"	               | 14*                   | Cartão bloqueado - motivo bloqueio pedido de nova via - código de bloqueio 12.
"7F"	               | 14*                   | Cartão bloqueado - motivo bloqueio problema no embossing - código de bloqueio 10.
"7G"	               | 14*                   | Cartão bloqueado - excluído da base - código de bloqueio 75
"81A"	               | 82	                   | Erro durante validação dos dados EMV devido falha na comunicação com HSM.
"81B"	               | 82	                   | Erro na validação dos dados EMV  (bit 55) que não tenha sido por motivo de falha na comunicação com HSM.
"82"	               | 82	                   | Erro na validação de CVV (1 ou 2).
"85"	               | 85	                   | Sem razão para negar.
"91A"	               | 96	                   | Erros de configuração do sistema, nos arquivos de configuração, acesso as bases de dados do Mongo e MySQL. 
"91B"	               | 96	                   | Erros salvando mensagem de transação no Mongo(registro duplicado ou outro erro).
"91C"	               | 96	                   | Qualquer outro erro do sistema, como timeout nas threads de autorização EMV ou validação do PIN.
"94"	               | 57	                   | Detectada duplicidade de transação provável causa por transmissão de mensagem em duplicidade.
"96A"	               | 96	                   | Falha do sistema - Falha na conversão da moeda de transação internacional. 
"96B"	               | 96	                   | Falha do sistema - Qualquer falha ou mal funcionamento do sistema que não se enquadra nas situações de erro previstas .
"AB"	               | AB	                   | Cartão utilizado na função incorreta, deve ser utilizada função Crédito. 
"AC"	               | AC	                   | Cartão utilizado na função incorreta, deve ser utilizada função Débito.
"FM"	               | FM	                   | Portador deve utilizar o CHIP.
"N1"	               | N1	                   | O sistema de autorização está ATIVO.
"N2"	               | N2	                   | O sistema de autorização está em BAIXA.
"N3"	               | N3	                   | O sistema de autorização está INATIVO.
