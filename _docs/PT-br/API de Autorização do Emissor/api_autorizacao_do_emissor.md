---
title: API de Autorização do Emissor
tags: 
 - api
 - autorizacao
 - prepago
description: Operações que os emissores que implementam produtos pré-pago devem implementar. 
permalink: /pt-br/docs/api-pre-pos-pago
language: pt-br
---

# **API de Autorização do Emissor**


A API de Autorização do Emissor é destinada a todos emissores que implementam produtos para cartões Pré-Pagos e/ou Pós-Pagos para consulta e consumo de saldo.

Esta especificação descreve os serviços que serão consumidos pela paySmart em uma API cuja implementação é de responsabilidade do emissor. As funções serão chamadas na sequência da interação entre a bandeira e paySmart no fluxo de pagamentos, após serem realizadas todas as validações de segurança por parte da bandeira e da processadora respectivamente.

Para o Pós-Pago, o Emissor pode decidir deixar tanto o controle de saldo e limites quanto o controle das faturas para a Processadora, que disponibiliza APIs para isso.

&nbsp;


## Autenticação e Segurança

Para autenticação e identificação do originador da transação é enviada uma API Key que pode ser combinada entre as partes Processadora e Emissor.

Esta API Key é enviada no header da requisição, conforme exemplos abaixos.

```
curl --location --request GET 'https://virtserver.swaggerhub.com/paySmart/api-emissor_pay_smart_pre_pago/v1-oas3/status' --header 'X-API-KEY: dbb8bdb8-e6bb-4397-b282-b0f9a20d87c7' --header 'accept: application/json'
```

```
curl --location --request GET 'https://virtserver.swaggerhub.com/paySmart/api-emissor_pay_smart_pos_pago/v1-oas3/status' --header 'X-API-KEY: dbb8bdb8-e6bb-4397-b282-b0f9a20d87c7' --header 'accept: application/json'
```

&nbsp;

## [Esquemas dos objetos utilizados na API](esquemas.md)

Página em que se encontra documento os esquemas dos objetos utilizados nas operações da API.
