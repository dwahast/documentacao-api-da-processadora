---
title: Operações de Compra
tags: 
 - compra
 - purchase
description: Operações de compra e cancelamentos de compra. 
permalink: /pt-br/docs/compra
language: pt-br
---

# **Operações de Compra**

1. [Endpoints](#endpoints)
    1. [POST /purchases](post-purchases)
        1. [Validação](#validação)
        1. [Requisição](#requisição)
            1. [Compra Doméstica](#compra-doméstica)
            1. [Compra Internacional](#compra-internacional)
            1. [Compra Parcelada](#compra-parcelada)
            1. [Transação iniciada pelo Estabelecimento (MIT)](#transação-iniciada-pelo-estabelecimento-mit)
            1. [QR Code](#qr-code)
            1. [Autorizações de compra aprovadas pelo sistema de *Stand-In*](#autorizações-de-compra-aprovadas-pelo-sistema-de-stand-in)
            1. [Compra com troco](#autorizações-de-compra-com-troco)
            1. [Autorização parcial de compra](#autorização-parcial-de-compra)

        1. [Retorno](#retorno)
    1. [POST /purchases/cancel](#post-purchasescancel)
1. [Objetos](#objetos)
    1. [Purchase](#purchase) 
    1. [PurchaseCancellation](#purchasecancellation)
    1. [PurchaseWithCashbackData](#purchasewithcashbackdata)

## **Endpoints**

### [POST /purchases]({{ site.url }}{{ site.baseurl }}/pt-br/processor-api)

>Consome o saldo de uma conta de acordo com o valor total informado.

&nbsp;

![Solicitação de Autorização de compra]({{ site.url }}{{ site.baseurl }}/assets/img/pt-BR/SolicitaAutorizaCompra.png)

&nbsp;

Cria um novo registro de compra. O registro pode ser de um aviso de compra negada (apenas informativo) ou de uma requisição de compra aprovada, consumindo o saldo de acordo com o valor total informado, o qual considera o valor original e as taxas associadas à transação. 

#### **Validação**

Validações a serem realizadas pelo emissor para requisições de compra aprovadas ou negadas pela bandeira e processadora paySmart.

##### **Requisição Aprovada**

Uma requisição de compra aprovada pela bandeira e pela processadora possui o campo MTI da mensagem original ISO-8583 igual à **0100** ou **0200** e código de autorização (campo *`"authorization"`.`"code"`*) igual à **00**. 

O emissor deverá validar:

Validação | Obrigatoriedade
-|-
Se o saldo da conta/cartão é maior ou igual ao valor total informado na transação | **Obrigatório**
Se a conta/cartão existe e está habilitado para realizar a compra | **Obrigatório**
Se a transação é permitida para a conta/cartão | **Obrigatório**
Se o MCC do estabelecimento comercial é um MCC válido para a conta/cartão transacionar | Opcional

>**Atenção**
>
>**Obs. 1:** Todas as taxas aplicadas a transação de compra já estão incluídas no valor total da transação, não sendo necessário adicionar nenhuma taxa extra ao debitar o valor total da conta/cartão do portador.
>
>**Obs. 2:** Existe uma nova situação onde o emissor deve acatar e refletir no saldo do portador o valor de uma compra mesmo que o cartão/conta do portador não possua saldo suficiente.
Esta situação será indicada com o campo *``"forceAccept"``* igual a *``"true"``* no objeto ***Purchase***.
Para este caso o emissor deve sempre retornar **HTTP Code 200** com o objeto **Success** no corpo da resposta. O emissor ainda pode recusar a compra por cartão/conta inexistente ou transação duplicada.
>
>**Obs. 3:** As compras que passarem pelo sistema anti-fraude da bandeira poderão conter informações de análise de fraude da bandeira e/ou credenciador. Estas informações estão contidas no objeto do *schema* ***FraudData***.

##### **Requisição Negada**

Uma requisição de compra negada pela bandeira e pela processadora e que **não deve ser validada** pelo emissor possui o campo MTI da mensagem original ISO-8583 igual à **0100**, **0120** ou **0200** e código de autorização **diferente** de **00** (**!= 00**).

Deve ser aceita pelo emissor sem comprometimento de saldo e demais validações do mesmo.

Os casos de transação negada pela bandeira ou processadora descritos acima podem ser ilustrados pelo fluxo abaixo.

&nbsp;

![Aviso de Autorização de compra negada pela bandeira ou processadora](../assets/img/AvisoCompraNegada.png)

&nbsp;

#### **Requisição**

A requisição de compra é realizada utilizando o objeto abaixo:

Objeto              | Descrição 
--------------------| ---------------------
**Purchase**        | Objeto compra contendo as informações da transação.

##### **Compra Doméstica**

Exemplo de aviso de autorização de compra doméstica:

```json
{
  "purchase_id": "aut_b3f1aebf-2cb5-44b5-85fd-13326a0799be",
  "account_id": "cta-597f552d-019a-4f97-a5f9-67ac59e2b883",
  "psProductCode": "003003",
  "psProductName": "ISSUER PESSOA FISICA INTERNACIONAL",
  "countryCode": "076",
  "source": "BRAND",
  "productType": "pre",
  "callingSystemName": "Autorizador ISO8583",
  "preAuthorization": false,
  "incrementalAuthorization": false,
  "authorization": {
    "code": "00",
    "description": "Sucesso/Transacao Aprovada"
  },
  "brand": "ELO",
  "card": {
    "paysmart_id": "crt-e01f67ea-b514-4955-8803-5e07cf81d92c",
    "pan": "************1591",
    "panseq": "00"
  },
  "total_amount": {
    "amount": 24600,
    "currency_code": 986
  },
  "original_amount": {
    "amount": 24600,
    "currency_code": 986
  },
  "entry_mode": "ECOMMERCE",
  "processing_code": {
    "tipo_transacao": "PURCHASE",
    "source_account_type": "CREDIT_CARD_ACCOUNT",
    "destination_account_type": "NOT_APPLICABLE"
  },
  "holder_validation_mode": "OTHER",
  "fees": [],
  "establishment": {
    "mcc": "5712",
    "name": "ELO",
    "city": "BARUERI",
    "address": "ALAMEDAXINGU",
    "zipcode": "235432300",
    "country": "076"
  },
  "internacional": false,
  "original_iso8583": {
    "mti": "0120",
    "de002": "************1591",
    "de003": "003000",
    "de004": "000000024600",
    "de007": "0323232243",
    "de011": "018810",
    "de012": "202243",
    "de013": "0323",
    "de014": "2812",
    "de018": "5712",
    "de019": "076",
    "de022": "070",
    "de024": "100",
    "de025": "74",
    "de032": "0025",
    "de037": "202289018810",
    "de038": "188100",
    "de039": "00",
    "de041": "20172289",
    "de042": "020001605270002",
    "de043": "ELO                    BARUERI       076",
    "de046": "  20000",
    "de048": "*BAN029          ASV34AK8918810X    *PRD003070",
    "de049": "986",
    "de058": "ALAMEDAXINGU          23543230007612345ELO                  ",
    "de060": "2025100000V00",
    "de062": "*TID02012345678901234567890",
    "de127": "12221"
  },
  "forceAccept": true,
  "nrid": "ASV34AK8918810X",
  "authorizationAdvice": true,
  "additionalTerminalData": {
    "terminalType": "terminalWithService",
    "partialApprovalIndicator": "notSupported",
    "terminalLocationIndicator": "local",
    "cardholderPresenceIndicator": "present",
    "cardPresenceIndicator": "cardPresent",
    "cardCaptureCapabilityIndicator": "cardCaptureCapability",
    "transactionStatusIndicator": "normal",
    "transactionSecurityIndicator": "noSuspectedFraud",
    "terminalPOSType": "atm",
    "terminalInputCapability": "chip",
    "eloProductCode": "070"
  }
}
```

Exemplo de compra doméstica:

```json
{
	"purchase_id": "aut_a9e786ae-3e91-45e3-a473-45cb6a1e84dc",
	"account_id": "cta-be31f86a-dad1-43d2-b7b6-264123591160",
	"psProductCode": "010101",
	"psProductName": "CARTÃO PAYSMART GIFT CARD PF",
	"countryCode": "076",
	"source": "brand",
	"callingSystemName": "Autorizador ISO8583",
	"preAuthorization": false,
	"incrementalAuthorization": false,
	"authorization": {
		"code": "00",
		"description": "Sucesso/Transacao Aprovada"
	},
	"brand": "elo",
	"card": {
		"paysmart_id": "crt-82535ae1-0070-4c7f-8999-ce4431a0146d",
		"issuer_id": 3,
		"pan": "************3368",
		"panseq": "00"
	},
	"total_amount": {
		"amount": 11800,
		"currency_code": 986
	},
	"dollar_amount": null,
	"original_amount": {
		"amount": 11800,
		"currency_code": 986
	},
	"dollar_real_rate": null,
	"spread": null,
	"entry_mode": "chip",
	"processing_code": {
		"tipo_transacao": "purchase",
		"source_account_type": "credit_card_account",
		"destination_account_type": "not_applicable"
	},
	"holder_validation_mode": "online_pin",
	"fees": [],
	"establishment": {
		"mcc": 5712,
		"name": "ELO",
		"city": "BARUERI",
		"address": "ALAMEDAXINGU",
		"zipcode": "235432300",
		"country": "076",
		"cnpj": null
	},
	"internacional": false,
	"original_iso8583": {
		"mti": "0100",
		"de002": "************3368",
		"de003": "003000",
		"de004": "000000011800",
		"de005": null,
		"de006": null,
		"de007": "0219141112",
		"de008": null,
		"de009": null,
		"de010": null,
		"de011": "101841",
		"de012": "111112",
		"de013": "0219",
		"de014": "2412",
		"de015": null,
		"de016": null,
		"de018": "5712",
		"de019": "076",
		"de022": "051",
		"de023": "000",
		"de024": "100",
		"de025": null,
		"de026": null,
		"de028": null,
		"de029": null,
		"de032": "0025",
		"de033": null,
		"de035": "************3368D2412606***1010000000",
		"de036": null,
		"de037": "202289101841",
		"de038": null,
		"de039": null,
		"de041": "20172289",
		"de042": "020001605270002",
		"de043": "ELO                    BARUERI       076",
		"de045": null,
		"de046": null,
		"de047": null,
		"de048": "*CDT002T0*PRD003070",
		"de049": "986",
		"de050": null,
		"de051": null,
		"de052": "EFBDAF1CFDB9575A",
		"de053": null,
		"de054": null,
		"de055": "5F2A02098682020000950500000400009A032002199C01009F02060000000118009F03060000000000009F10200FA501000000000000000000000000000F0000000000000000000000049410109F1A0200769F2701009F3303E0A0C09F34030203009F3501229F360200589F370462EB99679F26088ED974CC3CA589E5",
		"de056": null,
		"de058": "ALAMEDAXINGU          23543230007612345ELO",
		"de059": null,
		"de060": "000001000PH00",
		"de062": null,
		"de063": null,
		"de090": null,
		"de095": null,
		"de105": null,
		"de106": null,
		"de107": null,
		"de121": null,
		"de122": null,
		"de123": null,
		"de124": null,
		"de125": null,
		"de126": null,
		"de127": 12192
	},
    "forceAccept": false,
    "fraudData": {
        "creditorFraudScore": 1234,
        "eloBrandFraudScore": 567,
        "fraudScorePrimaryReason": 1,
        "fraudScoreSecondaryReason": 2,
        "fraudScoreTertiaryReason": 0,
        "fraudDecisionRecommendation": "A",
        "scoreOriginIndicator": 1
    },
    "additionalTerminalData": {
        "terminalType": "terminalWithService",
        "partialApprovalIndicator": "notSupported",
        "terminalLocationIndicator": "local",
        "cardholderPresenceIndicator": "present",
        "cardPresenceIndicator": "cardPresent",
        "cardCaptureCapabilityIndicator": "cardCaptureCapability",
        "transactionStatusIndicator": "normal",
        "transactionSecurityIndicator": "noSuspectedFraud",
        "terminalPOSType": "atm",
        "terminalInputCapability": "chip"
    },
    "eloProductCode": "070"
}
```

##### **Compra Internacional**

Compras podem ser realizadas em moeda estrangeira para os emissores que possuem produtos internacionas. Para estes casos o valor total da transação sempre será convertido para a moeda do emissor, no caso Reais, e terá as taxas de *Markup* e *IOF* somadas ao valor total. O valor original da transação será o valor na moeda local onde ocorreu a transação.


Taxas que podem ser aplicadas a transação de compra:

Taxa         | Desrição
-------------| ----------------
iof          | IOF aplicado ao valor total de transação, para compras internacionais.
markup       | Markup aplicado ao valor total da transação, para compras internacionais.
boarding_fee | Taxa de embarque aplicada ao valor total da transação.   
others       | Outras taxas que podem ser aplicadas ao valor total da transação, como tarifa de pagamento de insumos, por exemplo.

Exemplo de aviso de autorização de compra internacional:

```json
{
    "purchase_id": "aut_d6189199-81d7-403f-8afa-1c43c1efff43",
    "account_id": "cta-597f552d-019a-4f97-a5f9-67ac59e2b883",
    "psProductCode": "003003",
    "psProductName": "ISSUER PESSOA FISICA INTERNACIONAL",
    "countryCode": "840",
    "source": "BRAND",
    "productType": "pre",
    "callingSystemName": "Autorizador ISO8583",
    "preAuthorization": false,
    "incrementalAuthorization": false,
    "authorization": {
        "code": "00",
        "description": "Sucesso/Transacao Aprovada"
    },
    "brand": "ELO",
    "card": {
        "paysmart_id": "crt-e01f67ea-b514-4955-8803-5e07cf81d92c",
        "pan": "************1591",
        "panseq": "00"
    },
    "total_amount": {
        "amount": 773859,
        "currency_code": 986
    },
    "dollar_amount": {
        "amount": 25200,
        "currency_code": 840
    },
    "original_amount": {
        "amount": 25200,
        "currency_code": 840
    },
    "dollar_real_rate": "4.0795",
    "spread": "0.02",
    "entry_mode": "CHIP",
    "processing_code": {
        "tipo_transacao": "PURCHASE",
        "source_account_type": "CREDIT_CARD_ACCOUNT",
        "destination_account_type": "NOT_APPLICABLE"
    },
    "holder_validation_mode": "OTHER",
    "fees": [
        {
            "amount": {
                "amount": 669000,
                "currency_code": 986
            },
            "type": "IOF"
        }
    ],
    "establishment": {
        "mcc": "5712",
        "name": "CHRISTMAS TREE12345688",
        "city": "UNION",
        "address": "FLATLANDSAVE",
        "zipcode": "123450000",
        "country": "840"
    },
    "internacional": true,
    "original_iso8583": {
        "mti": "0120",
        "de002": "************1591",
        "de003": "003000",
        "de004": "000000025200",
        "de007": "0323205502",
        "de011": "018797",
        "de012": "175502",
        "de013": "0323",
        "de014": "2812",
        "de018": "5712",
        "de019": "840",
        "de022": "051",
        "de023": "000",
        "de024": "100",
        "de025": "74",
        "de032": "1234",
        "de037": "621768491713",
        "de039": "59",
        "de041": "20172289",
        "de042": "020001605270002",
        "de043": "CHRISTMAS TREE12345688 UNION         NJ ",
        "de046": "  20000",
        "de048": "*BAN029          ASV34AK8918797X    ",
        "de049": "840",
        "de055": "5F2A020840820279008409A00000015230101010950580000400009A032203239C01009F02060000000252009F03060000000000009F10200FA501800000000000000000000000000F0000000000000000000000000010009F1A0208409F2701809F3303E0A0C09F34030203009F3501229F360200089F370499116A0B9F26084458A3C55977D2D7",
        "de058": "FLATLANDSAVE          12345000084012345ELO                  ",
        "de060": "000001000PH00",
        "de127": "12221"
    },
    "forceAccept": true,
    "nrid": "ASV34AK8918797X",
    "additionalTerminalData": {
      "terminalType": "terminalWithService",
      "partialApprovalIndicator": "notSupported",
      "terminalLocationIndicator": "local",
      "cardholderPresenceIndicator": "present",
      "cardPresenceIndicator": "cardPresent",
      "cardCaptureCapabilityIndicator": "cardCaptureCapability",
      "transactionStatusIndicator": "normal",
      "transactionSecurityIndicator": "noSuspectedFraud",
      "terminalPOSType": "atm",
      "terminalInputCapability": "chip"
    }
}
```

##### **Compra Parcelada**

Para o Pós-Pago, é possível parcelar uma compra, com isso apresentará o campo *``"InstallmentDetails"``*.

Exemplo de campo referente ao parcelamento:

```json
{
	"installmentDetails": {
		"finType": "interestFree",
		"fare_amount": {
		"amount": 123,
		"currency_code": 986
		},
		"insurance_amount": {
		"amount": 123,
		"currency_code": 986
		},
		"third_parties_paymnt_amount": {
		"amount": 123,
		"currency_code": 986
		},
		"records_payments_amount": {
		"amount": 123,
		"currency_code": 986
		},
		"issuer_total_calculated_amount": {
		"amount": 123,
		"currency_code": 986
		},
		"first_paymnt_date": "200107",
		"instalmnt_nbr": 2,
		"monthly_interest_rate": 123,
		"total_effective_cost_rate_cet": 2020,
		"instalmnt_amount": {
		"amount": 123,
		"currency_code": 986
		},
		"annual_interest_rate": 2341,
		"input_value": {
		"amount": 123,
		"currency_code": 986
		}
  	}
}
```

##### **Transação iniciada pelo Estabelecimento (MIT)**

Para compras parceladas ou recorrentes no Pós-Pago, a partir da Release 22.1 da Elo, o objeto da compra poderá apresentar o campo *``"MITAdditionalData"``*, com informações referentes à compra parcelada ou recorrente internacional.

Exemplo do campo referente aos dados adicionais de transação inciada pelo estabelecimento (MIT) para compra parcelada ou recorrente internacional:

```json
{
    "mitAdditionalData": {
      installmentTotalNbr: "5",
      paymentType: "fixed",
      transactionType: "installment",
      transactionAmountMIT: 50000,
      uniqueTransactionID: "2eec2bba-660e-429a-9881-0403b5f7dc02",
      transactionFrequency: "monthly",
      validationIndicator: "validated",
      validationReference: "d66bc449-5e4d-4f18-b43f-cb81f0b557d8",
      sequenceIndicator: 1
    }
}
```

##### ***QR Code***

>Para transações por *QR Code* ou com cartão tokenizado, o objeto da transação poderá apresentar o campo *``"tokenPaymentData"``*, com informações referentes ao pagamento via token.

```json
{
  "tokenPaymentData": {
    "fPan": "1234",
    "requesterIdToken": "12345678901",
    "pan": "************1234",
    "tokenPSN": "001",
    "tokenExpirationDate": "2908",
    "tokenStatus": "0100",
    "tokenCryptogramVerificationResults": "0200",
    "EMVTokenCryptogramVerificationResults": "0200",
    "tokenConstraintsVerificationStatus": "0300",
    "transactionDateTimeConstraint": "OK",
    "transactionAmountConstraint": "OK",
    "usageConstraint": "OK",
    "tokenATCVerificationResults": "E001",
    "CVE2TokenCryptogramVerificationStatus": "E001",
    "merchantVerification": "OK",
    "magstripeTokenCryptogramVerificationResults": "0200",
    "CVE2OutputTokenCryptogramVerificationResults": "0201"
  }
}
```

##### **Autorizações de compra aprovadas pelo sistema de *Stand-In***

Para emissores **pós-pago** que contratarem o serviço de *Stand-In* poderão receber avisos de autorização já aprovados pela bandeira ou processadora. Um aviso de autorização de compra (**MTI 0120**) é gerado pela bandeira e enviado para o emissor quando ocorre timeout na comunicação com a processadora/emissor para uma requisição de compra (**MTI 0100**).

O sistema de *Stand-In* autoriza a transação mediante parâmetros informados pelo emissor. 
Os avisos de autorização terão o MTI da mensagem ISO-8583 igual à **0120**, indicando um aviso de autorização e também possuirão o 
campo *``"authorizationAdvice"``* no objeto **_Purchase_** com o valor *``"True"``* e o campo *``"forceAccept"``* também com valor *``"True"``*.

Os avisos de autorização aprovados pelo *Stand-In* deverão ser aceitos pelo emissor mesmo que o cartão não possua saldo ou limite para a transação, 
pois a transação já foi aprovada pelo sistema de Stand-In e a transação já foi concluída pelo credenciador/lojista junto ao portador do cartão.

#### **Retorno**

Para este *endpoint* o emissor deve retornar:

HTTP Code | Objeto                    | Descrição
----------|---------------------------| ---------------------
200       | **Success**               | Transação aprovada com sucesso ou transação com *``"forceAccept"``* = *``"true"``*
400       | **InsufficientFunds**     | Saldo insuficiente
404       | **NotFoundError**         | Conta não encontrada ou cartão inexistente
412       | **OperationNotPermitted** | Operação não permitida para a transação
459       | **FraudSuspectError**     | Suspeita de fraude, transação negada
483       | **InvalidMcc**            | MCC inválido
499       | **Acknowledge**           | Mensagem recebida pelo emissor para casos de transação negada
500       | **GeneralError**          | Erro interno do sistema do emissor
503       | **SystemDownError**       | Sistema Indisponível

Exemplo de resposta para a compra:

```json
{
	"message": "Operação realizada com sucesso.",
	"code": 0,
	"authorization_id": 322169,
	"balance": {
		"amount": 200000,
		"currency_code": 986
	}
}
```

>**Obs.:** Para o Pós-Pago o campo *``"balance"``* não está presente na resposta.

&nbsp;

##### **Autorizações de compra com troco**

Esta Mensagem é utilizada em vendas de cartão para a compra de bens ou serviços com troco recebido pelo Portador do cartão.
Para requisições de compra com troco o tipo de transação do código de processamento da compra será:

```json
{
  "processing_code": {
    "tipo_transacao": "purchaseWithWithdrawal"
  }
}
```

Para as requisições de autorização de compra com troco a mensagem de autorização da compra(Purchase) possuirá o seguinte campo com as informações da compra com troco além dos demais campos normalmente enviados em uma requisição de compra.

O campo ``"purchaseOnlyApprovalSupport"`` é usado somente pela bandeira Mastercard e deverá ser processado somente por emissores Mastercard. 
Este campo é enviado com valor `true` somente para estabelecimentos comerciais que possuem terminais que suportam aprovação somente do valor da compra sem a devolução em dinheiro.  

```json
{
  "purchaseWithCashbackData": {
    "purchaseOnlyApprovalSupport": false,
    "purchaseOnlyAmount": {
      "amount": 5000,
      "currency_code": 986
    },
    "cashbackAmount": {
      "amount": 1000,
      "currency_code": 986
    }
  }
}
```

Neste caso o valor total da requisição de compra será a soma do valor somente da compra com o valor do desembolso.

```json
{
  "total_amount": {
    "amount": 6000,
    "currency_code": 986
  }
}
```

Para estes casos o emissor deverá aprovar a compra integralmente ou negar de acordo com suas regras de autorização e responder de acordo com a tabela de retorno abaixo.

#### **Retorno**

Para este *endpoint* o emissor deve retornar:

HTTP Code | Objeto                    | Descrição
----------|---------------------------| ---------------------
200       | **Success**               | Transação aprovada com sucesso ou transação com *``"forceAccept"``* = *``"true"``*
400       | **InsufficientFunds**     | Saldo insuficiente
404       | **NotFoundError**         | Conta não encontrada ou cartão inexistente
412       | **OperationNotPermitted** | Operação não permitida para a transação
459       | **FraudSuspectError**     | Suspeita de fraude, transação negada
483       | **InvalidMcc**            | MCC inválido
499       | **Acknowledge**           | Mensagem recebida pelo emissor para casos de transação negada
500       | **GeneralError**          | Erro interno do sistema do emissor
503       | **SystemDownError**       | Sistema Indisponível

Exemplo de resposta para a compra:

```json
{
	"message": "Operação realizada com sucesso.",
	"code": 0,
	"authorization_id": 322169,
	"balance": {
		"amount": 200000,
		"currency_code": 986
	}
}
```

&nbsp;


##### **Autorização parcial de compra**

Dependendo do terminal do estabelecimento comercial é possível efetuar a aprovação parcial de uma compra ou compra com troco.

Obs.: Somente a bandeira ELO suporta aprovação parcial de compra com troco. Para a bandeira Mastercard a compra com troco deve ser 
aprovada integralmente ou aprovado somente o valor da compra sem o valor do troco, dependendo do suporte de aprovação somente do valor da compra.

Ambas bandeiras ELO e Mastercard suportam aprovação parcial de uma compra `normal` (código de processamento igual à `purchase`).

A resposta do emissor para aprovação parcial deverá respeitar a indicação de suporte à aprovação parcial do estabelecimento comercial.

***Indicador de Aprovação Parcial***.

Valores válidos:

Nome                        |  Descrição
----------------------------|--------------------------------------------------------------------
notSupported                | Aprovação Parcial não suportado (este deve ser o valor enviado pelo Credenciador).
supported                   | Aprovação Parcial suportado: Compras podem ser aprovadas parcialmente. Saque pode ser aprovado parcialmente.
onlyPurchasesSupported      | Aprovação Parcial suportado: Compras podem ser aprovadas parcialmente. Saque deve ser aprovado por completo ou declinado.
onlyWithdrawalSupported     | Aprovação Parcial suportado: Compras podem ser aprovadas por completo ou declinadas. Saque pode ser aprovado parcialmente.
onlyTotalPurchasesSupported | Aprovação Parcial suportado: Compras podem ser aprovadas por completo ou declinadas. Saque deve ser aprovado por completo ou declinado

Por exemplo, nos dados adicionais de terminal da compra, o valor abaixo indica que a aprovação parcial do valor da compra e do saque são suportados:
```json
{
  "additionalTerminalData": {
    "partialApprovalIndicator": "supported"
  }
}
```

Para indicar os valores da aprovação parcial do emissor devem ser retornados os seguintes campos na resposta de autorização bem sucedida do emissor.

Para uma compra com troco com as seguintes características:

```json
{
  "purchaseWithCashbackData": {
    "purchaseOnlyApprovalSupport": false,
    "purchaseOnlyAmount": {
      "amount": 5000,
      "currency_code": 986
    },
    "cashbackAmount": {
      "amount": 2000,
      "currency_code": 986
    }
  }
}
```

Segue exemplo de resposta para a compra com troco aprovada parcialmente:

```json
{
  "message": "Operação realizada com sucesso.",
  "code": 0,
  "authorization_id": 123456,
  "balance": {
    "amount": 20000,
    "currency_code": 986
  },
  "purchaseOnlyApproval": false,
  "purchaseOnlyPartialAmountApproved": {
    "amount": 3000,
    "currency_code": 986
  },
  "cashbackOnlyPartialAmountApproved": {
    "amount": 1000,
    "currency_code": 986
  }
}
```

Caso o emissor queira aprovar parcialmento somente o valor da compra e negar o valor do troco, deverá responder conforme exemplo abaixo.

```json
{
  "message": "Operação realizada com sucesso.",
  "code": 0,
  "authorization_id": 123456,
  "balance": {
    "amount": 20000,
    "currency_code": 986
  },
  "purchaseOnlyApproval": false,
  "purchaseOnlyPartialAmountApproved": {
    "amount": 3000,
    "currency_code": 986
  },
  "cashbackOnlyPartialAmountApproved": {
    "amount": 0,
    "currency_code": 986
  }
}
```

Note que no último exemplo o valor `amount` do campo `cashbackOnlyPartialAmountApproved` está zerado, justamente para indicar que o desembolso de caixa(cashback) não foi autorizado, já o valor somente da compra(`purchaseOnlyPartialAmountApproved`) foi autorizado parcialmente.  

O campo `purchaseOnlyApproval` deverá ser retornado com valor `true` somente por emissores da bandeira Mastercard para as transações de terminal que suportam aprovação somente do valor da compra sem a devolução de dinheiro, caso seja do interesse do emissor.

Caso o emissor não queira aprovar o valor da compra nem o valor do desembolso de caixa, a transação deverá ser negada com um código HTTP diferente de 200, conforme retornos esperados abaixo.  

#### **Retorno**

Para este *endpoint* o emissor deve retornar:

HTTP Code | Objeto                    | Descrição
----------|---------------------------| ---------------------
200       | **Success**               | Transação aprovada com sucesso ou transação com *``"forceAccept"``* = *``"true"``*
400       | **InsufficientFunds**     | Saldo insuficiente
404       | **NotFoundError**         | Conta não encontrada ou cartão inexistente
412       | **OperationNotPermitted** | Operação não permitida para a transação
459       | **FraudSuspectError**     | Suspeita de fraude, transação negada
483       | **InvalidMcc**            | MCC inválido
499       | **Acknowledge**           | Mensagem recebida pelo emissor para casos de transação negada
500       | **GeneralError**          | Erro interno do sistema do emissor
503       | **SystemDownError**       | Sistema Indisponível

Exemplo de resposta para a compra:

```json
{
  "message": "Operação realizada com sucesso.",
  "code": 0,
  "authorization_id": 123456,
  "balance": {
    "amount": 20000,
    "currency_code": 986
  },
  "purchaseOnlyApproval": false,
  "purchaseOnlyPartialAmountApproved": {
    "amount": 3000,
    "currency_code": 986
  },
  "cashbackOnlyPartialAmountApproved": {
    "amount": 1000,
    "currency_code": 986
  }
}
```

&nbsp;

---

&nbsp;

### POST [/purchases/cancel](../assets/api/openapi.json#/purchase/createPurchaseCancellation)

#### Reversão de compra.

&nbsp;

![Solicitação de Cancelamento de compra](../assets/img/pt-BR/SolicitaCancelaCompra.png)

&nbsp;

A operação de reversão desfaz uma compra, devolvendo o saldo consumido se aquela compra havia sido aprovada 
anteriormente. Normalmente a reversão é disparada automaticamente a partir de um terminal do adquirente/credenciador 
ou a partir do backend da bandeira caso uma transação não tenha sido completada com sucesso. Normalmente disparado a
partir de mensagens com MTI = 420.

Da mesma forma que ocorre com uma requisição de compra, um aviso de cancelamento de compra também pode ser 
negada pela processadora, raramente, e enviada para o emissor somente para reconhecimento e registro do aviso em seu sistema, 
sem que o mesmo precise realizar nenhuma validação de saldo, entre outras validações, para este caso. Neste caso o MTI
da mensagem de cancelamento será igual à '`0420`' e o código de autorização será diferente de '`00`'. Para este caso o
emissor deve retornar **Acknowledge** indicando o reconhecimento e aceite da transação negada.

Avisos de cancelamento de compra aprovadas pela bandeira e processadora devem ser validadas pelo emissor e possuir
o saldo da conta do cartão retornado de acordo com o valor total do cancelamento da compra, caso o cancelamento seja 
aprovado pelo emissor. Geralmente cancelamentos de compra devem ser aceitos pelo emissor, pois são mensagens geradas 
automaticamente em caso de erro no processamento da transação, tanto na mensagem de requisição quanto na resposta da 
requisição. 

Avisos de cancelamento de compra também podem ser enviados para cancelar avisos de autorização de compra aprovados pelo 
sistema de Stand-In (MTI igual à '`0120`'). Para os cancelamentos de compras aprovadas pelo Stand-In o emissor também deve 
acatar o cancelamento e devolver o saldo comprometido pelo aviso de autorização de compra que está sendo cancelado.

Para este endpoint o emissor deve retornar **Success** caso o aviso de cancelamento de compra seja aprovada pelo
emissor após todas validações terem sido executadas com sucesso. Caso a operação de cancelamento de compra não seja 
permitida para a conta/cartão por algum motivo que não esteja nos padrões de retorno deve ser retornado 
**OperationNotPermitted**. Se a conta/cartão da transação ou compra original não existir na base do emissor deve 
ser retornado **NotFoundError**. Caso o sistema do emissor esteja em baixa e não possa responder a requisição no 
momento deve retornar **SystemDownError**. Ocorrendo algum erro interno no sistema do emissor deve ser retornado 
**GeneralError**.

&nbsp;

#### **Requisição**

A requisição de cancelamento de compra é realizada utilizando o objeto abaixo:

Objeto                   | Descrição 
-------------------------| ---------------------
**PurchaseCancellation** | Objeto de cancelamento de compra contendo as informações da transação de cancelamento.

Exemplo de requisição de cancelamento de compra:

```json
{
    "cancellation_id": "aut_3594713f-89c3-4b7b-aa5c-c0c179318be7",
    "original_purchase_id": "aut_142bff9b-4d20-46f7-b741-b2f6c9935975",
    "account_id": "cta-be31f86a-dad1-43d2-b7b6-264123591160",
    "psProductCode": "010101",
    "psProductName": "CARTÃO PAYSMART GIFT CARD PF",
    "countryCode": "076",
    "source": "brand",
    "callingSystemName": "Autorizador ISO8583",
    "original_authorization_id": 393981,
    "authorization": {
        "code": "00",
        "description": "Sucesso/Transacao Aprovada"
    },
    "brand": "elo",
    "card": { 
        "paysmart_id": "crt-82535ae1-0070-4c7f-8999-ce4431a0146d",
        "issuer_id": 3,
        "pan": "************3368",
        "panseq": "00"
    },
    "original_amount": { 
        "amount": 7700,
        "currency_code": 986
    },
    "entry_mode": "chip",
    "cancellation_reason": { 
        "code": 21,
        "description": "Tempo ultrapassado na espera da resposta"
    },
    "iso8583_message": {
        "mti": "0420",
        "de002": "************3368",
        "de003": "003000",
        "de004": "000000007700",
        "de005": null,
        "de006": null,
        "de007": "0213125802",
        "de008": null,
        "de009": null,
        "de010": null,
        "de011": "101336",
        "de012": "095802",
        "de013": "0213",
        "de014": "2412",
        "de015": null,
        "de016": null,
        "de018": "5712",
        "de019": "076",
        "de022": "051",
        "de023": "000",
        "de024": "400",
        "de025": "21",
        "de026": null,
        "de028": null,
        "de029": null,
        "de032": "0025",
        "de033": null,
        "de035": "************3368D2412606***1010000000",
        "de036": null,
        "de037": "202289101336",
        "de038": "393981",
        "de039": null,
        "de041": "20172289",
        "de042": "020001605270002",
        "de043": "ELO                    BARUERI       076",
        "de045": null,
        "de046": null,
        "de047": null,
        "de048": "*PRD003070",
        "de049": "986",
        "de050": null,
        "de051": null,
        "de052": null,
        "de053": null,
        "de054": null,
        "de055": null,
        "de056": null,
        "de058": "ALAMEDAXINGU          23543230007612345ELO",
        "de059": null,
        "de060": "000001000P500",
        "de062": null,
        "de063": null,
        "de090": "010010133609580102133939810000000000000000",
        "de095": null,
        "de105": null,
        "de106": null,
        "de107": null,
        "de121": null,
        "de122": null,
        "de123": null,
        "de124": null,
        "de125": null,
        "de126": null,
        "de127": 12192
    },
    "additionalTerminalData": {
      "terminalType": "terminalWithService",
      "partialApprovalIndicator": "notSupported",
      "terminalLocationIndicator": "local",
      "cardholderPresenceIndicator": "present",
      "cardPresenceIndicator": "cardPresent",
      "cardCaptureCapabilityIndicator": "cardCaptureCapability",
      "transactionStatusIndicator": "normal",
      "transactionSecurityIndicator": "noSuspectedFraud",
      "terminalPOSType": "atm",
      "terminalInputCapability": "chip"
    },
    "eloProductCode": "070"
}
```
---

#### **Retorno**

Para este *endpoint* o emissor deve retornar:

HTTP Code | Objeto                         | Descrição
----------| -------------------------------| ---------------------
200       | **Success**                    | Compra desfeita com sucesso
404       | **NotFoundError**              | Compra original não encontrada
409       | **TransactionAlreadyCanceled** | Transação já foi cancelada
412       | **OperationNotPermitted**      | Operação não permitida para a transação
499       | **Acknowledge**                | Mensagem recebida pelo emissor para casos de transação negada
500       | **GeneralError**               | Erro interno do sistema do emissor
503       | **SystemDownError**            | Sistema Indisponível

Exemplo de resposta do cancelamento de compra:

```json
{
    "message": "Operação realizada com sucesso.",
    "code": 0,
    "authorization_id": 797292,
    "balance": { 
        "amount": 200000,
        "currency_code": 986
    }
}
```

>**Obs.:** Para o Pós-Pago o campo *``"balance"``* não está presente na resposta.

---

## **Objetos**

Os Esquemas do objetos de **COMPRA** são estruturados da seguinte forma:

### [**Purchase**]({{ site.url }}{{ site.baseurl }}/pt-br/processor-api)

Objeto que representa uma requisição de compra. Enviado nas requisições de operação de compra.

Atributo             | Tipo                   | Obrigatório | Exemplo                              | Descrição
---------------------|------------------------|-------------|--------------------------------------|--------------------------------
***``"purchase_id"``***      | String                 | **Sim**     | 06deb28a-d5ab-45bd-8094-054694394b2a | Identificador interno paySmart da transação.
***``"account_id"``***       | String                 | **Sim**     | 08deb38b-a4ac-42be-a0c5-1674a44b94c1 | Identificador interno paySmart da conta.
***``"psProductCode"``***    | String                 | **Sim**     | 020101                               | Código do produto interno paySmart.
*``"psProductName"``*    | String                 | Não         | CARTÃO PAYSMART GIFT CARD PF         | Nome do produto interno paySmart.
***``"countryCode"``***      | String                 | **Sim**     | 076                                  | Código do país conforme ISO 3166-1.
***``"source"``***           | Enum String            | **Sim**     | brand                                | Sistema originador da transação. Possíveis valores são: paySmart, issuer, brand.
***``"callingSystemName"``*** | String                 | **Sim**     | Autorizador-8583                     | Nome/identificador do serviço que fez essa chamada. Geralmente Autorizador-8583.
***``"preAuthorization"``*** | Boolean                | **Sim**     | false                                | Indica se é uma pré-autorização(**true**) ou autorização normal(**false**).
***``"incrementalAuthorization"``*** | Boolean                | **Sim**     | false                                | Indica se é uma autorização incremental(**true**) ou não(**false**).
***``"authorization"``***    | Authorization          | **Sim**     | Ver definição do tipo                | Informações de Autorização provenientes da bandeira ou processadora.
***``"brand"``***            | Enum String            | **Sim**     | elo                                  | A bandeira ou esquema de pagamentos de onde a transação se originou. Possíveis valores são: elo, goodcard, mastercard, visa.
***``"card"``***             | Card                   | **Sim**     | Ver definição do tipo                | Informações do cartão originador da transação.
***``"total_amount"``***     | Amount                 | **Sim**     | Ver definição do tipo                | Valor total da transação com todas as taxas da transação somandas, caso existam.
*``"dollar_amount"``*        | Amount                 | Não         | Ver definição do tipo                | Valor da transação em dolares caso enviado na mensagem ISO-8583 de autorização ou valor na moeda local da transação. Somente se transação internacional.
***``"original_amount"``***  | Amount                 | **Sim**     | Ver definição do tipo                | Valor original da transação na moeda local da transação.
*``"dollar_real_rate"``*     | String                 | Não         | 4.40                                 | Cotação do Dolar para conversão de câmbio Dólar -> Real na data da transação. Somente se transação internacional.
*``"spread"``*               | String                 | Não         | 0.04                                 | Spread (ou markup) aplicado ao valor total da transação. Somente se transação internacional.
***``"entry_mode"``***       | EntryMode              | **Sim**     | chip                                 | Modo usado para a entrada do PAN na transação.
***``"processing_code"``***  | ProcessingCode         | **Sim**     | Ver definição do tipo                | Código de processamento da transação.
*``"holder_validation_mode"``* | Enum String            | Não         | online_pin                           | Modo usado para validar o portador.
***``"fees"``***             | Array Fee              | **Sim**     | Ver definição do tipo                | Lista de taxas da transação.
***``"establishment"``***    | Establishment          | **Sim**     | Ver definição do tipo                | Informações do estabelecimento onde ocorreu a transação.
***``"internacional"``***    | Boolean                | **Sim**     | false                                | Indica se a transação é internacional(true) ou nacional(false).
***``"original_iso8583"``*** | ISO8583Message         | **Sim**     | Ver definição do tipo                | Mensagem original ISO-8583 enviada pela bandeira e processada pela paySmart.
*``"forceAccept"``*          | Boolean                | Não         | false                                | Força que a transação seja aceita, mesmo que não tenha saldo suficiente ou algo do tipo. O emissor ainda pode recusar a compra por cartão/conta inexistente ou transação duplicada.  
*``"installmentDetails"``*   | InstallmentDetails     | Não         | Ver definição do tipo                | **Apenas para o Pós-Pago**, faz referência ao parcelamento do lojista com todas informações necessárias.
*``"authorizationAdvice"``*  | Boolean                | Não         | true                                 | Indica se esta compra é um aviso de autorização de compra (true) ou é uma requisição de compra que deve ser validada pelo emissor (false ou não informado). Avisos de autorização devem ser aceitos pelo emissor, pois já foram aprovados pelo sistema de Stand-In da bandeira ou processadora mediante parâmetros informados pelo emissor no sistema de Stand-In.
*``"mitAdditionalData"``*    | MITAdditionalData      | Não         | Ver definição do tipo                | Este conjunto de dados é utilizado para passar dados de identificação da transação original e do valor da transação original do Estabelecimento Comercial/Credenciador para o Emissor. Utilizado para Pagamentos Parcelados no âmbito internacional.
*``"additionalTerminalData"``* | AdditionalTerminalData | Não         | Ver difinição do tipo                | Contempla os dados adicionais pertencentes ao ponto de venda/saque (por exemplo, um POS) no momento em que a transação foi realizada.
*``"tokenPaymentData"``*     | TokenPaymentData       | Não      | Ver definições do tipo               |   Contém os Informações dos Dados Transacionais - Conjunto de dados de Pagamento via Token para compras por QrCode ou cartão tokenizado.
*``"hceTransaction"``*      | Boolean                | Não      | false                                |   Indica se é uma transação feita com cartão HCE (**true**) ou outro tipo de cartão (**false**).
*``"purchaseWithCashbackData"``* | PurchaseWithCashbackData | Não  | Ver definição do tipo      | Informações dos Dados de compra com devolução de dinheiro(Cashback). 
*``"eloProductCode"``* | 	EloProductCode | Não  | Ver definição do tipo      | Código do produto da transação. Usado pela bandeira ELO. 

&nbsp;
---

### [**PurchaseCancellation**]({{ site.url }}{{ site.baseurl }}/pt-br/processor-api)

Objeto que representa uma requisição de cancelamento de compra. Enviado nas requisições de operação de cancelamento de compra.

Atributo             | Tipo    | Obrigatório | Exemplo                         | Descrição 
---------------------|---------|-------------|---------------------------------|--------------------
***``"cancellation_id"``***  | String  | **Sim**     | 06deb28a-d5ab-45bd-8094-054694394b2a | Identificador interno paySmart da transação.
***``"original_purchase_id"``*** | String  | **Sim**     | 06deb28a-d5ab-45bd-8094-054694394b2a | Identificador interno paySmart da transação original a ser cancelada.
***``"account_id"``***       | String  | **Sim**     | 08deb38b-a4ac-42be-a0c5-1674a44b94c1 | Identificador interno paySmart da conta.
***``"psProductCode"``***    | String  | **Sim**     | 020101                          | Código do produto interno paySmart.
*``"psProductName"``*    | String  | Não         | CARTÃO PAYSMART GIFT CARD PF         | Nome do produto interno paySmart.
***``"countryCode"``***      | String  | **Sim**     | 076                             | Código do país conforme ISO 3166-1.
***``"source"``***           | Enum String | **Sim** | brand                           | Sistema originador da transação. Possíveis valores são: paySmart, issuer, brand.
***``"callingSystemName"``*** | String | **Sim**     | Autorizador-8583                | Nome/identificador do serviço que fez essa chamada. Geralmente Autorizador-8583.
***``"original_authorization_id"``*** | Integer | **Sim** | 123456                     | Código de autorização retornado na transação original. Em algumas situações, esse valor pode não corresponder ao da transação original.
***``"authorization"``***    | Authorization | **Sim** | Ver definição do tipo         | Informações de Autorização provenientes da bandeira ou processadora.
***``"brand"``***            | Enum String  | **Sim** | elo                            | A bandeira ou esquema de pagamentos de onde a transação se originou. Possíveis valores são: elo, goodcard, mastercard, visa.
***``"card"``***             | Card   | **Sim**      | Ver definição do tipo           | Informações do cartão originador da transação.
***``"original_amount"``***  | Amount  | **Sim**     | Ver definição do tipo           | Valor original da transação na moeda local da transação.
***``"entry_mode"``***       | EntryMode | **Sim**   | chip                            | Modo usado para a entrada do PAN na transação.
***``"cancellation_reason"``*** | CancellationReason | **Sim** | Ver definição do tipo | Motivo do cancelamento da transação original.
***``"iso8583_message"``***  | ISO8583Message | **Sim** | Ver definição do tipo        | Mensagem original ISO-8583 enviada pela bandeira e processada pela paySmart.
*``"additionalTerminalData"``*   | AdditionalTerminalData | Não | Ver difinição do tipo | Contempla os dados adicionais pertencentes ao ponto de venda/saque (por exemplo, um POS) no momento em que a transação foi realizada.
*``"tokenPaymentData"``*        | TokenPaymentData  | Não      | Ver definições do tipo  |   Contém os Informações dos Dados Transacionais - Conjunto de dados de Pagamento via Token para compras por QrCode ou cartão tokenizado.
*``"hceTransaction"``*          | Boolean                | Não      | false                                |   Indica se é uma transação feita com cartão HCE (**true**) ou outro tipo de cartão (**false**).
``"eloProductCode"`` | 	EloProductCode | Não  | Ver definição do tipo      | Código do produto da transação. Usado pela bandeira ELO. 


&nbsp;
---

### [PurchaseWithCashbackData]({{ site.url }}{{ site.baseurl }}/pt-br/processor-api)

Informações dos Dados de compra com devolução de dinheiro(Cashback).

Atributo                                          | Tipo    | Obrigatório | Exemplo               | Descrição
------------------------------------------------- |---------|-------------|-----------------------|--------------------
**purchaseOnlyApprovalSupport**                   | Boolean | Sim         | true                  | Indicador do terminal de aceitação de aprovação somente do valor da compra sem o valor da devolução em dinheiro(cashback) - SOMENTE para MASTERCARD.
**purchaseOnlyAmount**                            | Amount  | Sim         | Ver definição do tipo | Valor somente da compra sem a devolução de dinheiro. 
**cashbackAmount**                                | Amount  | Sim         | Ver definição do tipo | Valor somente da devolução de dinheiro(cashback). 
