---
title: API de Autorização do Emissor
permalink: /pt-br/issuer-authorization-api/

layout: swagger_layout
spec_path: "issuer-authorization-api.json"
language: pt-br
---

# Documentação API do emissor pré-pago
