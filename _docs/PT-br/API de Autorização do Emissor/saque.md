---
title: Operações de Saque
tags: 
 - saque
description: Operações de saque e cancelamentos de saque. 
permalink: /pt-br/docs/saque
language: pt-br
---

# Operações de Saque

[POST /withdrawals](saque#post-withdrawals) | 
[POST /withdrawals/cancel](saque#post-withdrawalscancel) | 
[Objetos de Saque](saque#objetos-de-saque) | 
[WithdrawalQueryResults](saque#withdrawalqueryresults]) |  
[WithdrawalQuery](saque#withdrawalquery) | 
[Withdrawal](saque#withdrawal) | 
[WithdrawalCancellation](saque#WithdrawalCancellation)

### POST [/withdrawals](../assets/api/openapi.json#/withdrawal/createWithdrawal)

#### Consome o saldo de uma conta de acordo com o valor total informado.

&nbsp;

![Solicitação de Autorização de saque]({{ site.url }}{{ site.baseurl }}/assets/img/pt-BR/SolicitaAutorizaSaque.png)

&nbsp;

Cria um novo registro de saque, que pode ter sido reprovado (registro apenas informativo) ou aprovado, nesse caso consumindo o saldo de acordo com o valor total informado, o qual considera o valor original e as taxas associadas à transação. Geradas a partir de ISO8583 com MTI = 0200.

Uma requisição de saque que foi aprovada pela bandeira e pela processadora deve ser validada pelo emissor quanto ao saldo e demais validações específicas do mesmo. Uma transação que foi negada pela bandeira ou pela processadora deve ser aceita pelo emissor sem comprometimento de saldo e demais validações do mesmo. 

Uma requisição de saque aprovada pela bandeira e pela processadora e que deve ser validada pelo emissor possui o campo MTI da mensagem original ISO-8583 igual à '`0100`' ou '`0200`' e código de autorização, campo `authorization.code`, da saque igual à '`00`'

Uma requisição de saque negada pela bandeira e pela processadora e que não deve ser validada pelo emissor possui o campo MTI da mensagem original ISO-8583 igual à '`0100`', '`0120`' ou '`0200`' e código de autorização diferente de '`00`'

Para os casos onde a requisição é negada pela bandeira ou processadora o emissor deve retornar o objeto **Acknowledge** aceitando a negação da transação.

Para autorizar um saque o emissor deve validar o saldo da conta/cartão com o valor total informado na transação. Deve validar se a conta/cartão existe e está habilitado para realizar o saque. Deve validar se a transação é permitida para a conta/cartão. Pode também validar o MCC do estabelecimento comercial se é um MCC válido para saque da conta/cartão.

Todas as taxas aplicadas a transação de saque já estão incluídas no valor total da transação, não sendo necessário adicionar nenhuma taxa extra ao debitar o valor total da conta/cartão do portador.

Para este endpoint o emissor deve retornar **Success** caso a requisição de saque seja aprovada pelo emissor após todas validações terem sido executadas com sucesso. Caso a conta/cartão não possua saldo suficiente para realizar o saque, o emissor deve retornar **InsufficientFunds**. Caso a operação de saque não seja permitida para a conta/cartão por algum motivo que não esteja nos padrões de retorno deve ser retornado **OperationNotPermitted**. Se a conta/cartão da transação não existir na base do emissor deve ser retornado **NotFoundError**. Caso o MCC da transação não seja permitido para a operação de saque para a conta/cartão da transação deve ser retornado **InvalidMcc**. Caso o sistema do emissor esteja em baixa e não possa responder a requisição no momento deve retornar **SystemDownError**. Ocorrendo algum erro interno no sistema do emissor deve ser retornado **GeneralError**.


Os saques que passarem pelo sistema anti-fraude da bandeira poderão conter informações de análise de fraude da bandeira e/ou credenciador.
Estas informações estão contidas no objeto do schema FraudData.


&nbsp;

* Taxas que podem ser aplicadas a transação de saque:

Taxa           | Descrição
---------------| ----------------
withdrawal_fee | Taxa de saque aplicada a operação.

&nbsp;

* Parâmetros enviados no corpo da requisição:

Objeto              | Descrição 
--------------------| ---------------------
**Withdrawal**      | Objeto saque contendo as informações da transação.

&nbsp;

* Possíveis retornos para o endpoint:

HTTP Code | Objeto                    | Descrição
----------| --------------------------| ---------------------
200       | **Success**               | Transação aprovada com sucesso.
400       | **InsufficientFunds**     | Saldo insuficiente.
404       | **NotFoundError**         | Conta não encontrada ou cartão inexistente.
412       | **OperationNotPermitted** | Operação não permitida para a transação.
459       | **FraudSuspectError**     | Suspeita de fraude, transação negada.
483       | **InvalidMcc**            | MCC inválido.
499       | **Acknowledge**           | Mensagem recebida pelo emissor para casos de transação negada.
503       | **SystemDownError**       | Sistema Indisponível.
500       | **GeneralError**          | Erro interno do sistema do emissor.

&nbsp;

#### Exemplo de requisição de saque:

```json
{    
    "withdrawal_id": "aut_ac6d2d98-a75d-4c8c-bcfc-061ccd5502f0",
    "account_id": "cta-689da0b9-eefa-4450-baf6-257c5113f2a6",
    "psProductCode": "010101",
    "psProductName": "CARTÃO PAYSMART GIFT CARD PF",
    "countryCode": "076",
    "source": "brand",
    "callingSystemName": "Autorizador ISO8583",
    "authorization": {        
        "code": "00",
        "description": "Sucesso/Transacao Aprovada"
    },
    "brand": "elo",
    "card": {        
        "paysmart_id": "174",
        "issuer_id": "000000000000011",
        "pan": "650406******4726",
        "panseq": "00"    
    },
    "total_amount": {        
        "amount": 100123,
        "currency_code": "986"    
    },
    "dollar_amount": null,
    "original_amount": {
        "amount": 100000,
        "currency_code": "986"    
    },
    "dollar_real_rate": null,
    "spread": null,
    "entry_mode": "magnetic_stripe",
    "processing_code": {
        "tipo_transacao": "withdrawal",
        "source_account_type": "credit_card_account",
        "destination_account_type": "not_applicable"    
    },
    "holder_validation_mode": "online_pin",
    "fees": [{        
        "amount": { 
            "amount": 123,
            "currency_code": "986"        
        },
        "type": "withdrawal_fee"    
    }],
    "establishment": { 
        "mcc": "5712",
        "name": "Saque Rede 24 Horas;",
        "city": "BARUERI",
        "address": "Rua Bonnard 980;",
        "zipcode": "006465134",
        "country": "076",
        "cnpj": ""   
    },
    "internacional": false,
    "original_iso8583": {        
        "mti": "0200",
        "de002": "************4726",
        "de003": "013000",
        "de004": "000000100000",
        "de005": null,
        "de006": null,
        "de007": "0110203315",
        "de008": null,
        "de009": null,
        "de010": null,
        "de011": "011497",
        "de012": "183315",
        "de013": "0110",
        "de014": "2409",
        "de015": null,
        "de016": null,
        "de018": "5712",
        "de019": "076",
        "de022": "021",
        "de023": null,
        "de024": null,
        "de025": null,
        "de026": null,
        "de028": null,
        "de029": null,
        "de032": "0025",
        "de033": null,
        "de035": "************4726=24092065240000000000",
        "de036": null,
        "de037": "202289011497",
        "de038": null,
        "de039": null,
        "de041": "20172289",
        "de042": "020001605270002",
        "de043": "Saque Rede 24 Horas;   BARUERI       076",
        "de045": null,
        "de046": null,
        "de047": null,
        "de048": "*VPS003101*CDT002T0*PRD003070",
        "de049": 986,
        "de050": null,
        "de051": null,
        "de052": "ED6203D01824A8E7",
        "de053": null,
        "de054": null,
        "de055": null,
        "de056": null,
        "de058": "Rua Bonnard 980;      00646513407612345Rede 24 Horas;",
        "de059": null,
        "de060": "100001000AH00",
        "de062": null,
        "de063": null,
        "de090": null,
        "de095": null,
        "de105": null,
        "de106": null,
        "de107": null,
        "de121": null,
        "de122": null,
        "de123": null,
        "de124": null,
        "de125": null,
        "de126": null,
        "de127": "12182"    
    },
    "fraudData": {
        "creditorFraudScore": 1234,
        "eloBrandFraudScore": 567,
        "fraudScorePrimaryReason": 1,
        "fraudScoreSecondaryReason": 2,
        "fraudScoreTertiaryReason": 0,
        "fraudDecisionRecommendation": "A",
        "scoreOriginIndicator": 1
    },
    "additionalTerminalData": {
      "terminalType": "terminalWithService",
      "partialApprovalIndicator": "notSupported",
      "terminalLocationIndicator": "local",
      "cardholderPresenceIndicator": "present",
      "cardPresenceIndicator": "cardPresent",
      "cardCaptureCapabilityIndicator": "cardCaptureCapability",
      "transactionStatusIndicator": "normal",
      "transactionSecurityIndicator": "noSuspectedFraud",
      "terminalPOSType": "atm",
      "terminalInputCapability": "chip"
    },
    "eloProductCode": "070"
}
```

&nbsp;

* Para transações por QrCode ou com cartão tokenizado, o objeto da transação poderá apresentar o campo "tokenPaymentData", com informações referentes ao pagamento via token.

### Exemplo do campo referente aos dados de pagamento via token para transações por QrCode ou com cartão tokenizado:

```json
{
  "tokenPaymentData": {
    "fPan": "1234",
    "requesterIdToken": "12345678901",
    "pan": "************1234",
    "tokenPSN": "001",
    "tokenExpirationDate": "2908",
    "tokenStatus": "0100",
    "tokenCryptogramVerificationResults": "0200",
    "EMVTokenCryptogramVerificationResults": "0200",
    "tokenConstraintsVerificationStatus": "0300",
    "transactionDateTimeConstraint": "OK",
    "transactionAmountConstraint": "OK",
    "usageConstraint": "OK",
    "tokenATCVerificationResults": "E001",
    "CVE2TokenCryptogramVerificationStatus": "E001",
    "merchantVerification": "OK",
    "magstripeTokenCryptogramVerificationResults": "0200",
    "CVE2OutputTokenCryptogramVerificationResults": "0201"
  }
}
```
---

#### Exemplo de resposta do saque acima:

```json
{    
    "message": "Operação realizada com sucesso.",
    "code": 0,
    "authorization_id": 123456,
    "balance": { 
        "amount": 123,
        "currency_code": 986
    }
}
```

* Para o Pós-Pago o campo "Balance" não está presente na resposta.

&nbsp;

---

&nbsp;

### POST [/withdrawals/cancel](../assets/api/openapi.json#/cancel/createWithdrawalCancellation)

#### Reversão de saque.

&nbsp;

![Solicitação de reversão de saque](../assets/img/pt-BR/SolicitaCancelaSaque.png)

&nbsp;

A operação de reversão desfaz um saque, devolvendo o saldo consumido caso ele tivesse sido aprovado. Normalmente a reversão é disparada automaticamente a partir de um terminal do adquirente ou a partir do backend da bandeira caso uma transação não tenha sido completada com sucesso. Pode ser disparada a partir de mensagens com MTI = 420.

Da mesma forma que ocorre com uma requisição de saque, uma requisição de cancelamento de saque também pode ser negada pela processadora e enviada para o emissor somente para reconhecimento e registro da requisição em seu sistema, sem que o mesmo precise realizar nenhuma validação de saldo, entre outras validações. Neste caso o MTI da mensagem de cancelamento será igual à '`0420`' e o código de autorização será diferente de '`00`'. Para este caso o emissor deve retornar **Acknowledge** indicando o reconhecimento e aceita da transação negada.

Requisições de cancelamento de saque aprovadas pela bandeira e processadora devem ser validadas pelo emissor e possuir o saldo da conta do cartão retornado de acordo com o valor total do cancelamento do saque, caso o cancelamento seja aprovado pelo emissor. Geralmente cancelamentos de saque devem ser aceitos pelo emissor, pois são mensagens geradas automaticamente em caso de erro no processamento da transação, tanto na mensagem de requisição quanto na resposta da requisição. 

Para este endpoint o emissor deve retornar **Success** caso a requisição de cancelamento de saque seja aprovada pelo emissor após todas validações terem sido executadas com sucesso. Caso a operação de cancelamento de saque não seja permitida para a conta/cartão por algum motivo que não esteja nos padrões de retorno deve ser retornado **OperationNotPermitted**. Se a conta/cartão da transação ou saque original não existir na base do emissor deve ser retornado **NotFoundError**. Caso o sistema do emissor esteja em baixa e não possa responder a requisição no momento deve retornar **SystemDownError**. Ocorrendo algum erro interno no sistema do emissor deve ser retornado **GeneralError**.

&nbsp;

* Parâmetros enviados no corpo da requisição:

Objeto                   | Descrição 
-------------------------| ---------------------
**WithdrawalCancellation** | Objeto de cancelamento de saque contendo as informações da transação de cancelamento.

&nbsp;

* Possíveis retornos para o endpoint:

HTTP Code | Objeto                         | Descrição
----------| -------------------------------| ---------------------
200       | **Success**                    | Saque desfeito com sucesso.
404       | **NotFoundError**              | Saque original não encontrada.
409       | **TransactionAlreadyCanceled** | Transação já foi cancelada.
412       | **OperationNotPermitted**      | Operação não permitida para a transação.
499       | **Acknowledge**                | Mensagem recebida pelo emissor para casos de transação negada.
503       | **SystemDownError**            | Sistema Indisponível.
500       | **GeneralError**               | Erro interno do sistema do emissor.

&nbsp;

#### Exemplo de requisição de cancelamento de saque:

```json
{    
    "cancellation_id": "aut_e1624a2a-32bd-4ac6-b936-0c3b7d32f824",
    "original_withdrawal_id": "aut_d5b6098c-de3b-4f3a-8af6-74fd81f89b15",
    "account_id": "cta-689da0b9-eefa-4450-baf6-257c5113f2a6",
    "psProductCode": "010101",
    "psProductName": "CARTÃO PAYSMART GIFT CARD PF",
    "countryCode": "076",
    "source": "brand",
    "callingSystemName": "Autorizador ISO8583",
    "original_authorization_id": "7998",
    "authorization": { 
        "code": "00",
        "description": "Sucesso/Transacao Aprovada"    
    },
    "brand": "elo",
    "card": {        
        "paysmart_id": "174",
        "issuer_id": "000000000000011",
        "pan": "************4726",
        "panseq": "00"    
    },
    "original_amount": {        
        "amount": 100000,
        "currency_code": 986    
    },
    "entry_mode": "fallback",
    "cancellation_reason": {        
        "code": "17",
        "description": "Suspeita de mal funcionamento / dinheiro não entregue"    
    },
    "iso8583_message": {        
        "mti": "0420",
        "de002": "************4726",
        "de003": "013000",
        "de004": "000000100000",
        "de005": null,
        "de006": null,
        "de007": "0121135747",
        "de008": null,
        "de009": null,
        "de010": null,
        "de011": "011660",
        "de012": "115747",
        "de013": "0121",
        "de014": "2409",
        "de015": null,
        "de016": null,
        "de018": "5712",
        "de019": "076",
        "de022": "851",
        "de023": null,
        "de024": "400",
        "de025": "17",
        "de026": null,
        "de028": null,
        "de029": null,
        "de032": "0025",
        "de033": null,
        "de035": null,
        "de036": null,
        "de037": "695217429605",
        "de038": "007998",
        "de039": null,
        "de041": "20172289",
        "de042": "020001605270002",
        "de043": "Saque Rede 24 Horas;   BARUERI       076",
        "de045": null,
        "de046": null,
        "de047": null,
        "de048": "*PRD003070",
        "de049": "986",
        "de050": null,
        "de051": null,
        "de052": null,
        "de053": null,
        "de054": null,
        "de055": null,
        "de056": null,
        "de058": "Rua Bonnard 980;      00646513407612345Rede 24 Horas;",
        "de059": null,
        "de060": "100001000AH00",
        "de062": null,
        "de063": null,
        "de090": "020001165911574601210079980000000000000000",
        "de095": null,
        "de105": null,
        "de106": null,
        "de107": null,
        "de121": null,
        "de122": null,
        "de123": null,
        "de124": null,
        "de125": null,
        "de126": null,
        "de127": "12182"    
    },
    "additionalTerminalData": {
      "terminalType": "terminalWithService",
      "partialApprovalIndicator": "notSupported",
      "terminalLocationIndicator": "local",
      "cardholderPresenceIndicator": "present",
      "cardPresenceIndicator": "cardPresent",
      "cardCaptureCapabilityIndicator": "cardCaptureCapability",
      "transactionStatusIndicator": "normal",
      "transactionSecurityIndicator": "noSuspectedFraud",
      "terminalPOSType": "atm",
      "terminalInputCapability": "chip"
    },
    "eloProductCode": "070"
}
```

#### Exemplo de resposta do cancelamento acima:

```json
{    
    "message": "Operação realizada com sucesso.",
    "code": 0,
    "authorization_id": 123456,
    "balance": {        
        "amount": 123,
        "currency_code": 986    
    }
}
```

* Para o Pós-Pago o campo "Balance" não está presente na resposta.

&nbsp;
---

### Objetos de Saque

Os Esquemas do objetos de **SAQUE** são estruturados da seguinte forma:

### [WithdrawalQueryResults](../assets/api/openapi.json#/WithdrawalQueryResults)

Objeto que deve ser retornado na operação de consulta de saque para retornar a taxa de saque e o saldo disponível do cartão pré-pago.

Atributo             | Tipo    | Obrigatório | Exemplo                         | Descrição 
---------------------|---------|-------------|---------------------------------|--------------------
**message**          | String  | **Sim**     | Operação realizada com sucesso. | Mensagem descritiva do resultado do processamento.
**code**             | Integer | **Sim**     | 0                               | Código do resultado do processamento.
**authorization_id** | Integer | **Sim**     | 123456                          | Código de autorização de 6 dígitos gerado pelo emissor. Deve ser um código único para cada transação durante o mesmo dia.
**fee**              | Amount  | **Sim**     | Ver definição do tipo.          | Taxa de saque que deve ser aplicada para transação de saque.
**balance**          | Amount  | **Sim**     | Ver definição do tipo.          | Saldo disponível da conta/cartão após o processamento da transação.


&nbsp;
---


### [WithdrawalQuery](../assets/api/openapi.json#/WithdrawalQuery)

Objeto que representa uma requisição de consulta de saque. Enviado nas requisições de operação de consulta de saque.

Atributo             | Tipo    | Obrigatório | Exemplo                         | Descrição 
---------------------|---------|-------------|---------------------------------|--------------------
**withdrawal_query_id** | String  | **Sim**     | 06deb28a-d5ab-45bd-8094-054694394b2a | Identificador interno paySmart da transação.
**account_id**       | String  | **Sim**     | 08deb38b-a4ac-42be-a0c5-1674a44b94c1 | Identificador interno paySmart da conta.
**psProductCode**    | String  | **Sim**     | 020101                          | Código do produto interno paySmart.
**psProductName**    | String  | Não         | CARTÃO PAYSMART GIFT CARD PF         | Nome do produto interno paySmart.
**countryCode**      | String  | **Sim**     | 076                             | Código do país conforme ISO 3166-1.
**source**           | Enum String | **Sim** | brand                           | Sistema originador da transação. Possíveis valores são: paySmart, issuer, brand.
**callingSystemName** | String | **Sim**     | Autorizador-8583                | Nome/identificador do serviço que fez essa chamada. Geralmente Autorizador-8583.
**authorization**    | Authorization | **Sim** | Ver definição do tipo         | Informações de Autorização provenientes da bandeira ou processadora.
**brand**            | Enum String  | **Sim** | elo                            | A bandeira ou esquema de pagamentos de onde a transação se originou. Possíveis valores são: elo, goodcard, mastercard, visa.
**card**             | Card   | **Sim**      | Ver definição do tipo           | Informações do cartão originador da transação.
**entry_mode**       | EntryMode | **Sim**   | chip                            | Modo usado para a entrada do PAN na transação.
**processing_code**  | ProcessingCode | **Sim** | Ver definição do tipo           | Código de processamento da transação.
**original_iso8583** | ISO8583Message | **Sim** | Ver definição do tipo        | Mensagem original ISO-8583 enviada pela bandeira e processada pela paySmart.
additionalTerminalData   | AdditionalTerminalData | Não | Ver difinição do tipo | Contempla os dados adicionais pertencentes ao ponto de venda/saque (por exemplo, um POS) no momento em que a transação foi realizada.
tokenPaymentData     | TokenPaymentData  | Não      | Ver definições do tipo  |   Contém os Informações dos Dados Transacionais - Conjunto de dados de Pagamento via Token para saques por QrCode ou cartão tokenizado.
hceTransaction      | Boolean                | Não      | false                                |   Indica se é uma transação feita com cartão HCE (true) ou outro tipo de cartão (false).
eloProductCode | 	EloProductCode | Não  | Ver definição do tipo      | Código do produto da transação. Usado pela bandeira ELO. 



&nbsp;
---

### [Withdrawal](../assets/api/openapi.json#/Withdrawal)

Objeto que representa uma requisição de saque. Enviado nas requisições de operação de saque.

Atributo             | Tipo    | Obrigatório | Exemplo                         | Descrição 
---------------------|---------|-------------|---------------------------------|--------------------
**withdrawal_id**    | String  | **Sim**     | 06deb28a-d5ab-45bd-8094-054694394b2a | Identificador interno paySmart da transação.
**account_id**       | String  | **Sim**     | 08deb38b-a4ac-42be-a0c5-1674a44b94c1 | Identificador interno paySmart da conta.
**psProductCode**    | String  | **Sim**     | 020101                          | Código do produto interno paySmart.
**psProductName**    | String  | Não         | CARTÃO PAYSMART GIFT CARD PF         | Nome do produto interno paySmart.
**countryCode**      | String  | **Sim**     | 076                             | Código do país conforme ISO 3166-1.
**source**           | Enum String | **Sim** | brand                           | Sistema originador da transação. Possíveis valores são: paySmart, issuer, brand.
**callingSystemName** | String | **Sim**     | Autorizador-8583                | Nome/identificador do serviço que fez essa chamada. Geralmente Autorizador-8583.
**authorization**    | Authorization | **Sim** | Ver definição do tipo         | Informações de Autorização provenientes da bandeira ou processadora.
**brand**            | Enum String  | **Sim** | elo                            | A bandeira ou esquema de pagamentos de onde a transação se originou. Possíveis valores são: elo, goodcard, mastercard, visa.
**card**             | Card   | **Sim**      | Ver definição do tipo           | Informações do cartão originador da transação.
**total_amount**     | Amount  | **Sim**     | Ver definição do tipo           | Valor total da transação com todas as taxas da transação somandas, caso existam.
dollar_amount        | Amount  | Não         | Ver definição do tipo           | Valor da transação em dolares caso enviado na mensagem ISO-8583 de autorização ou valor na moeda local da transação. Somente se transação internacional.
**original_amount**  | Amount  | **Sim**     | Ver definição do tipo           | Valor original da transação na moeda local da transação.
dollar_real_rate     | String  | Não         | 4.40                            | Cotação do Dolar para conversão de câmbio Dólar -> Real na data da transação. Somente se transação internacional.
spread               | String  | Não         | 0.04                            | Spread (ou markup) aplicado ao valor total da transação. Somente se transação internacional.
**entry_mode**       | EntryMode | **Sim**   | chip                            | Modo usado para a entrada do PAN na transação.
**processing_code**  | ProcessingCode | **Sim** | Ver definição do tipo           | Código de processamento da transação.
holder_validation_mode | Enum String | Não   | online_pin                      | Modo usado para validar o portador.
**fees**             | Array Fee | **Sim**   | Ver definição do tipo           | Lista de taxas da transação.
**establishment**    | Establishment | **Sim** | Ver definição do tipo         | Informações do estabelecimento onde ocorreu a transação.
**internacional**    | Boolean | **Sim**     | false                           | Indica se a transação é internacional(true) ou nacional(false).
**original_iso8583** | ISO8583Message | **Sim** | Ver definição do tipo        | Mensagem original ISO-8583 enviada pela bandeira e processada pela paySmart.
forceAccept       | Boolean          | Não    | false                       | Força que a transação seja aceita, mesmo que não tenha saldo suficiente ou algo do tipo. O emissor ainda pode recusar o saque por cartão/conta inexistente ou transação duplicada.
authorizationAdvice  | Boolean        | Não   | true                         | Indica se este saque é um aviso de autorização de saque (true) ou é uma requisição de saque que deve ser validada pelo emissor (false ou não informado). Avisos de autorização devem ser aceitos pelo emissor, pois já foram aprovados pelo sistema de Stand-In da bandeira ou processadora mediante parâmetros informados pelo emissor no sistema de Stand-In.
additionalTerminalData   | AdditionalTerminalData | Não | Ver difinição do tipo | Contempla os dados adicionais pertencentes ao ponto de venda/saque (por exemplo, um POS) no momento em que a transação foi realizada.
tokenPaymentData     | TokenPaymentData  | Não      | Ver definições do tipo  |   Contém os Informações dos Dados Transacionais - Conjunto de dados de Pagamento via Token para saques por QrCode ou cartão tokenizado.
hceTransaction      | Boolean                | Não      | false                                |   Indica se é uma transação feita com cartão HCE (true) ou outro tipo de cartão (false).
eloProductCode | 	EloProductCode | Não  | Ver definição do tipo      | Código do produto da transação. Usado pela bandeira ELO. 


&nbsp;
---

### [WithdrawalCancellation](../assets/api/openapi.json#/WithdrawalCancellation)

Objeto que representa uma requisição de cancelamento de saque. Enviado nas requisições de operação de cancelamento de saque.

Atributo             | Tipo    | Obrigatório | Exemplo                         | Descrição 
---------------------|---------|-------------|---------------------------------|--------------------
**cancellation_id**  | String  | **Sim**     | 06deb28a-d5ab-45bd-8094-054694394b2a | Identificador interno paySmart da transação.
**original_withdrawal_id** | String  | **Sim**     | 06deb28a-d5ab-45bd-8094-054694394b2a | Identificador interno paySmart da transação original a ser cancelada.
**account_id**       | String  | **Sim**     | 08deb38b-a4ac-42be-a0c5-1674a44b94c1 | Identificador interno paySmart da conta.
**psProductCode**    | String  | **Sim**     | 020101                          | Código do produto interno paySmart.
**psProductName**    | String  | Não         | CARTÃO PAYSMART GIFT CARD PF         | Nome do produto interno paySmart.
**countryCode**      | String  | **Sim**     | 076                             | Código do país conforme ISO 3166-1.
**source**           | Enum String | **Sim** | brand                           | Sistema originador da transação. Possíveis valores são: paySmart, issuer, brand.
**callingSystemName** | String | **Sim**     | Autorizador-8583                | Nome/identificador do serviço que fez essa chamada. Geralmente Autorizador-8583.
**original_authorization_id** | Integer | **Sim** | 123456                     | Código de autorização retornado na transação original. Em algumas situações, esse valor pode não corresponder ao da transação original.
**authorization**    | Authorization | **Sim** | Ver definição do tipo         | Informações de Autorização provenientes da bandeira ou processadora.
**brand**            | Enum String  | **Sim** | elo                            | A bandeira ou esquema de pagamentos de onde a transação se originou. Possíveis valores são: elo, goodcard, mastercard, visa.
**card**             | Card   | **Sim**      | Ver definição do tipo           | Informações do cartão originador da transação.
**original_amount**  | Amount  | **Sim**     | Ver definição do tipo           | Valor original da transação na moeda local da transação.
**entry_mode**       | EntryMode | **Sim**   | chip                            | Modo usado para a entrada do PAN na transação.
**cancellation_reason** | CancellationReason | **Sim** | Ver definição do tipo | Motivo do cancelamento da transação original.
**iso8583_message**  | ISO8583Message | **Sim** | Ver definição do tipo        | Mensagem original ISO-8583 enviada pela bandeira e processada pela paySmart.
additionalTerminalData   | AdditionalTerminalData | Não | Ver difinição do tipo | Contempla os dados adicionais pertencentes ao ponto de venda/saque (por exemplo, um POS) no momento em que a transação foi realizada.
tokenPaymentData        | TokenPaymentData  | Não      | Ver definições do tipo  |   Contém os Informações dos Dados Transacionais - Conjunto de dados de Pagamento via Token para saques por QrCode ou cartão tokenizado.
hceTransaction          | Boolean                | Não      | false                                |   Indica se é uma transação feita com cartão HCE (true) ou outro tipo de cartão (false).
eloProductCode | 	EloProductCode | Não  | Ver definição do tipo      | Código do produto da transação. Usado pela bandeira ELO. 
