---
title: Operações de Consulta
tags: 
 - consulta
 - verificação 
 - conta
description: Operações de consulta e verificação de conta de cartão 
permalink: /pt-br/docs/consulta
language: pt-br
---

# **Operações de Consulta**

1. [Endpoints](#endpoints)
    1. [GET /status](consulta#get-status)
        1. [Retorno](#retorno)
    1. [POST /queries](#post-queries)
        1. [Validação](#validação)
        1. [Requisição](#requisição)
        1. [Retorno](#retorno-1)
    1. [POST /withdrawalQueries](#post-withdrawalqueries)
        1. [Validação](#validac3a7c3a3o-1)
        1. [Requisição](#requisic3a7c3a3o-1)
        1. [Retorno](#retorno-2)
1. [Objetos](#objetos)
    1. [Query](#query)
    1. [WithdrawalQuery](#withdrawalquery)

## **Endpoints**

### **[GET /status]({{ site.url }}{{ site.baseurl }}/pt-br/processor-api)**

>Usado para recuperar a saúde do sistema do emissor.

&nbsp;

![Solicitação de Saúde do Sistema]({{ site.url }}{{ site.baseurl }}/assets/img/pt-BR/SolicitaSaude.png)

&nbsp;

Indica se o sistema está disponível para receber operações ou se está enfrentando problemas de estabilidade e saúde. Este *endpoint* é invocado pelo sistema de autorização da processadora periodicamente para monitorar a saúde do sistema do emissor.

#### **Retorno**

Para este *endpoint* o emissor deve retornar:

HTTP Code | Objeto              | Descrição
--------- | --------------------| ---------------------
200       | **SuccessStatus**   | Sucesso e status de saúde ok
500       | **GeneralError**    | Erro interno do sistema do emissor 
503       | **SystemDownError** | Sistema indisponível


Exemplo de resposta para consulta de status:

```json
{
  "message": "Operação realizada com sucesso.",
  "code": 0
}
```

---

### **[POST /queries]({{ site.url }}{{ site.baseurl }}/pt-br/processor-api)**

>Verifica que o cartão está apto a realizar aquela transação, e responde com o saldo do cartão.

&nbsp;

![Solicitação de verificação de conta]({{ site.url }}{{ site.baseurl }}/assets/img/pt-BR/SolicitaVerificaConta.png)

&nbsp;

Verifica se o cartão está apto a realizar uma compra com as caracteristícas passadas. No retorno também retorna o saldo do cartão. Geradas a partir de ISO8583 com MTI = 0100.

#### **Validação**

Para uma verificação de conta que foi aprovada pela bandeira e pela processadora, o emissor deverá validar:

Validação | Obrigatoriedade
-|-
Se a conta/cartão existe e está habilitada para realizar a compra | **Obrigatório**
Se a transação é permitida para a conta/cartão | **Obrigatório**
Se o MCC do estabelecimento comercial é um MCC válido para a conta/cartão transacionar| Opcional

>**Atenção**
>
>Para operação de verificação de conta **não** deve ser validado saldo da conta/cartão, somente se a conta/cartão está apta para realizar transações.

#### **Requisição**

A requisição de verificação de conta é realizada utilizando o objeto abaixo:

Objeto              | Descrição 
--------------------| ---------------------
**[Query](#query)**           | Objeto consulta contendo as informações da verificação de conta.

Exemplo de solicitação de verificação de conta:

```json
{    
    "query_id": "aut_f3be88fe-2010-40b7-83d3-22b9818f6d9d",
    "account_id": "cta-be31f86a-dad1-43d2-b7b6-264123591160",
    "psProductCode": "010101",
    "psProductName": "CARTÃO PAYSMART GIFT CARD PF",
    "countryCode": "076",
    "source": "brand",
    "callingSystemName": "Autorizador ISO8583",
    "authorization": { 
        "code": "00",
        "description": "Sucesso/Transacao Aprovada"    
    },
    "brand": "elo",
    "card": { 
        "paysmart_id": "220",
        "issuer_id": "3",
        "pan": "************3368",
        "panseq": "00"
    },
    "entry_mode": "ecommerce",
    "establishment": {        
        "mcc": "5712",
        "name": "ELO",
        "city": "BARUERI",
        "address": "ALAMEDAXINGU",
        "zipcode": "235432300",
        "country": "076",
        "cnpj": null
    },
    "internacional": false,
    "original_iso8583": {        
        "mti": "0100",
        "de002": "************3368",
        "de003": "183000",
        "de004": "000000000000",
        "de005": null,
        "de006": null,
        "de007": "1203113923",
        "de008": null,
        "de009": null,
        "de010": null,
        "de011": "097071",
        "de012": "083923",
        "de013": "1203",
        "de014": "2412",
        "de015": null,
        "de016": null,
        "de018": "5712",
        "de019": "076",
        "de022": "070",
        "de023": null,
        "de024": "100",
        "de025": null,
        "de026": null,
        "de028": null,
        "de029": null,
        "de032": "0025",
        "de033": null,
        "de035": null,
        "de036": null,
        "de037": "202289097071",
        "de038": null,
        "de039": null,
        "de041": "20172289",
        "de042": "020001605270002",
        "de043": "ELO                    BARUERI       076",
        "de045": null,
        "de046": null,
        "de047": null,
        "de048": "*CDT002T0*PRD003070",
        "de049": "986",
        "de050": null,
        "de051": null,
        "de052": null,
        "de053": null,
        "de054": null,
        "de055": null,
        "de056": null,
        "de058": "ALAMEDAXINGU          23543230007612345ELO",
        "de059": null,
        "de060": "2025100000V00",
        "de062": "*TID02012345678901234567890*ECI00207",
        "de063": null,
        "de090": null,
        "de095": null,
        "de105": null,
        "de106": null,
        "de107": null,
        "de121": null,
        "de122": null,
        "de123": null,
        "de124": null,
        "de125": null,
        "de126": "1349",
        "de127": "12192"    
    },
    "additionalTerminalData": {
      "terminalType": "terminalWithService",
      "partialApprovalIndicator": "notSupported",
      "terminalLocationIndicator": "local",
      "cardholderPresenceIndicator": "present",
      "cardPresenceIndicator": "cardPresent",
      "cardCaptureCapabilityIndicator": "cardCaptureCapability",
      "transactionStatusIndicator": "normal",
      "transactionSecurityIndicator": "noSuspectedFraud",
      "terminalPOSType": "atm",
      "terminalInputCapability": "chip"
    },
    "eloProductCode": "070"
}
```

>**Obs.:** Para transações por *QR Code* ou com cartão tokenizado, o objeto da transação poderá apresentar o campo *``"tokenPaymentData"``*, com informações referentes ao pagamento via token.

```json
{
  "tokenPaymentData": {
    "fPan": "1234",
    "requesterIdToken": "12345678901",
    "pan": "************1234",
    "tokenPSN": "001",
    "tokenExpirationDate": "2908",
    "tokenStatus": "0100",
    "tokenCryptogramVerificationResults": "0200",
    "EMVTokenCryptogramVerificationResults": "0200",
    "tokenConstraintsVerificationStatus": "0300",
    "transactionDateTimeConstraint": "OK",
    "transactionAmountConstraint": "OK",
    "usageConstraint": "OK",
    "tokenATCVerificationResults": "E001",
    "CVE2TokenCryptogramVerificationStatus": "E001",
    "merchantVerification": "OK",
    "magstripeTokenCryptogramVerificationResults": "0200",
    "CVE2OutputTokenCryptogramVerificationResults": "0201"
  }
}
```
#### **Retorno**

Para este *endpoint* o emissor deve retornar: 

HTTP Code | Objeto                    | Descrição
----------| --------------------------| ---------------------
200       | **Success**               | Conta verificada com sucesso, apta a transacionar
404       | **NotFoundError**         | Conta não encontrada ou cartão inexistente
483       | **InvalidMcc**            | MCC inválido
499       | **Acknowledge**           | Mensagem recebida pelo emissor para casos de transação negada
500       | **GeneralError**          | Erro interno do sistema do emissor
503       | **SystemDownError**       | Sistema Indisponível

Exemplo de resposta da verificação de conta:

```json
{    
    "message": "Operação realizada com sucesso.",
    "code": 0,
    "authorization_id": 165876,
    "balance": { 
        "amount": 200000,
        "currency_code": 986    
    }
}
```

>**Obs.:** Para o Pós-Pago o campo *``"balance"``* não está presente na resposta.

---

### [POST /withdrawalQueries]({{ site.url }}{{ site.baseurl }}/pt-br/processor-api)

>Retorna informações relacionadas às taxas de saque para um determinado cartão.

&nbsp;

![Solicitação de consulta de saque]({{ site.url }}{{ site.baseurl }}/assets/img/pt-BR/SolicitaConsultaSaque.png)

&nbsp;

Retorna informações relacionadas às taxas de saque para um determinado cartão. Pode ser chamada na operação de consulta de saque (Bit 03 = 0430) e na operação de saque (Bit 03 = 0130).

#### **Validação**

Para uma requisição de consulta de saque que foi aprovada pela bandeira e pela processadora, o emissor deverá validar:

Validação | Obrigatoriedade
-|-
Se a conta/cartão existe e está habilitada para realizar o saque | **Obrigatório**
Se a transação de saque é permitida para a conta/cartão | **Obrigatório**
Se o MCC do estabelecimento comercial é um MCC válido para a conta/cartão realizar um saque | Opcional

>**Atenção**
>
>Para a operação de consulta de saque **não** deve ser validado saldo da conta/cartão, somente se a conta/cartão está apta para realizar saques. 
>
>Para esta operação é necessário retornar também a taxa de saque que deve ser aplicada à operação de saque.

#### **Requisição**

A requisição de consulta de saque é realizada utilizando o objeto abaixo:

Objeto              | Descrição 
--------------------| ---------------------
**[WithdrawalQuery](#withdrawalquery)** | Objeto consulta de saque contendo as informações da operação de consulta.

Exemplo de requisição de consulta de saque:

```json
{    
    "withdrawal_query_id": "aut_f5c4dece-cf7b-40e6-a098-dcfc0c9c666f",
    "account_id": "cta-be31f86a-dad1-43d2-b7b6-264123591160",
    "psProductCode": "010101",
    "psProductName": "CARTÃO PAYSMART GIFT CARD PF",
    "countryCode": "076",
    "source": "brand",
    "callingSystemName": "Autorizador ISO8583", 
    "authorization": { 
        "code": "00",
        "description": "Sucesso/Transacao Aprovada"    
    },
    "brand": "elo",
    "card": { 
        "paysmart_id": "crt-82535ae1-0070-4c7f-8999-ce4431a0146d",
        "issuer_id": "3",
        "pan": "************3368",
        "panseq": "00"    
    },
    "entry_mode": "chip",
    "processing_code": { 
        "tipo_transacao": "simulationQuery",
        "source_account_type": "credit_card_account",
        "destination_account_type": "not_applicable"    
    },
    "internacional": false,
    "original_iso8583": {        
        "mti": "0200",
        "de002": "************3368",
        "de003": "043000",
        "de004": "000000008400",
        "de005": null,
        "de006": null,
        "de007": "0213141125",
        "de008": null,
        "de009": null,
        "de010": null,
        "de011": "101412",
        "de012": "111125",
        "de013": "0213",
        "de014": "2412",
        "de015": null,
        "de016": null,
        "de018": "5712",
        "de019": "076",
        "de022": "051",
        "de023": "000",
        "de024": "000",
        "de025": null,
        "de026": null,
        "de028": null,
        "de029": null,
        "de032": "0025",
        "de033": null,
        "de035": "50925700****3368D2412606***1010000000",
        "de036": null,
        "de037": "202289101412",
        "de038": null,
        "de039": null,
        "de041": "20172289",
        "de042": "020001605270002",
        "de043": "Saque Rede 24 Horas;   BARUERI       076",
        "de045": null,
        "de046": null,
        "de047": null,
        "de048": "*VPS003101*CDT002T0*PRD003070",
        "de049": "986",
        "de050": null,
        "de051": null,
        "de052": "EFBDAF1CFDB9575A",
        "de053": null,
        "de054": null,
        "de055": "5F2A02098682020000950500000000009A032002139C01009F02060000000084009F03060000000000009F10200FA501000000000000000000000000000F0000000000000000000000049410109F1A0200769F2701009F3303E0A0C09F34030203009F3501229F360200539F3704C9C5724C9F26083BE79059DCB8ADC1",
        "de056": null,
        "de058": "Rua Bonnard 980;      00646513407612345Rede 24 Horas;",
        "de059": null,
        "de060": "100001000AH00",
        "de062": null,
        "de063": null,
        "de090": null,
        "de095": null,
        "de105": null,
        "de106": null,
        "de107": null,
        "de121": null,
        "de122": null,
        "de123": null,
        "de124": null,
        "de125": null,
        "de126": null,
        "de127": "12192"    
    },
    "additionalTerminalData": {
      "terminalType": "terminalWithService",
      "partialApprovalIndicator": "notSupported",
      "terminalLocationIndicator": "local",
      "cardholderPresenceIndicator": "present",
      "cardPresenceIndicator": "cardPresent",
      "cardCaptureCapabilityIndicator": "cardCaptureCapability",
      "transactionStatusIndicator": "normal",
      "transactionSecurityIndicator": "noSuspectedFraud",
      "terminalPOSType": "atm",
      "terminalInputCapability": "chip"
    },
    "eloProductCode": "070"
}
```

#### **Retorno**

Para este endpoint o emissor deve retornar:

HTTP Code | Objeto                     | Descrição
----------| ---------------------------| ---------------------
200       | **WithdrawalQueryResults** | Objeto contendo os resultados da operação de consulta de saque com sucesso <br>(Contendo a taxa de saque que deve ser aplicada nas operações posteriores de saque)
404       | **NotFoundError**          | Conta não encontrada ou cartão inexistente.
483       | **InvalidMcc**             | MCC inválido
499       | **Acknowledge**            | Mensagem recebida pelo emissor para casos de transação negada
500       | **GeneralError**           | Erro interno do sistema do emissor
503       | **SystemDownError**        | Sistema Indisponível

Exemplo de resposta da consulta de saque:

```json
{    
    "message": "Operação realizada com sucesso.",
    "code": 0,
    "authorization_id": 892002,
    "fee": { 
        "amount": 500,
        "currency_code": 986    
    },
    "balance": { 
        "amount": 200000,
        "currency_code": 986    
    }
}
```

>**Obs.:** Para o Pós-Pago o campo *``"balance"``* não está presente na resposta.

---

### **Objetos**

Os Esquema dos objetos de **CONSULTA** são estruturados da seguinte forma:


#### **[Query]({{ site.url }}{{ site.baseurl }}/pt-br/processor-api/)**

Objeto que representa uma requisição de verificação de conta do cartão. 

Atributo             | Tipo    | Obrigatório | Exemplo                         | Descrição 
---------------------|---------|-------------|---------------------------------|--------------------
***``"query_id"``***         | String  | **Sim**     | 06deb28a-d5ab-45bd-8094-054694394b2a | Identificador interno paySmart da transação.
***``"account_id"``***       | String  | **Sim**     | 08deb38b-a4ac-42be-a0c5-1674a44b94c1 | Identificador interno paySmart da conta.
***``"psProductCode"``***    | String  | **Sim**     | 020101                          | Código do produto interno paySmart.
*``"psProductName"``*    | String  | Não         | CARTÃO PAYSMART GIFT CARD PF         | Nome do produto interno paySmart.
***``"countryCode"``***      | String  | **Sim**     | 076                             | Código do país conforme ISO 3166-1.
***``"source"``***           | Enum String | **Sim** | brand                           | Sistema originador da transação. Possíveis valores são: paySmart, issuer, brand.
***``"callingSystemName"``*** | String | **Sim**     | Autorizador-8583                | Nome/identificador do serviço que fez essa chamada. Geralmente Autorizador-8583.
***``"authorization"``***    | Authorization | **Sim** | Ver definição do tipo         | Informações de Autorização provenientes da bandeira ou processadora.
***``"brand"``***            | Enum String  | **Sim** | elo                            | A bandeira ou esquema de pagamentos de onde a transação se originou. Possíveis valores são: elo, goodcard, mastercard, visa.
***``"card"``***             | Card   | **Sim**      | Ver definição do tipo           | Informações do cartão originador da transação.
***``"entry_mode"``***       | EntryMode | **Sim**   | chip                            | Modo usado para a entrada do PAN na transação.
***``"establishment"``***    | Establishment | **Sim** | Ver definição do tipo         | Informações do estabelecimento onde ocorreu a transação.
***``"internacional"``***    | Boolean | **Sim**     | false                           | Indica se a transação é internacional(true) ou nacional(false).
***``"original_iso8583"``*** | ISO8583Message | **Sim** | Ver definição do tipo        | Mensagem original ISO-8583 enviada pela bandeira e processada pela paySmart.
``"additionalTerminalData"`` | AdditionalTerminalData | Não | Ver difinição do tipo | Contempla os dados adicionais pertencentes ao ponto de venda/saque (por exemplo, um POS) no momento em que a transação foi realizada.
``"tokenPaymentData"``     | TokenPaymentData  | Não      | Ver definições do tipo  |   Contém os Informações dos Dados Transacionais - Conjunto de dados de Pagamento via Token para consultas por QrCode ou cartão tokenizado.
``"hceTransaction"``      | Boolean                | Não      | false                                |   Indica se é uma transação feita com cartão HCE (true) ou outro tipo de cartão (false).
*``"eloProductCode"``* | 	EloProductCode | Não  | Ver definição do tipo      | Código do produto da transação. Usado pela bandeira ELO. 


#### **[withdrawalQuery]({{ site.url }}{{ site.baseurl }}/pt-br/processor-api/)**

Objeto que representa uma requisição de consulta de saque.

Atributo             | Tipo    | Obrigatório | Exemplo                         | Descrição 
---------------------|---------|-------------|---------------------------------|--------------------
***``"withdrawal_query_id"``***         | String  | **Sim**     | 06deb28a-d5ab-45bd-8094-054694394b2a | Identificador interno paySmart da transação.
***``"account_id"``***       | String  | **Sim**     | 08deb38b-a4ac-42be-a0c5-1674a44b94c1 | Identificador interno paySmart da conta.
***``"psProductCode"``***    | String  | **Sim**     | 020101                          | Código do produto interno paySmart.
*``"psProductName"``*   | String  | Não         | CARTÃO PAYSMART GIFT CARD PF         | Nome do produto interno paySmart.
***``"countryCode"``***      | String  | **Sim**     | 076                             | Código do país conforme ISO 3166-1.
***``"source"``***           | Enum String | **Sim** | brand                           | Sistema originador da transação. Possíveis valores são: paySmart, issuer, brand.
***``"callingSystemName"``*** | String | **Sim**     | Autorizador-8583                | Nome/identificador do serviço que fez essa chamada. Geralmente Autorizador-8583.
***``"authorization"``***    | Authorization | **Sim** | Ver definição do tipo         | Informações de Autorização provenientes da bandeira ou processadora.
***``"brand"``***            | Enum String  | **Sim** | elo                            | A bandeira ou esquema de pagamentos de onde a transação se originou. Possíveis valores são: elo, goodcard, mastercard, visa.
***``"card"``***             | Card   | **Sim**      | Ver definição do tipo           | Informações do cartão originador da transação.
***``"entry_mode"``***       | EntryMode | **Sim**   | chip                            | Modo usado para a entrada do PAN na transação.
***``"establishment"``***    | Establishment | **Sim** | Ver definição do tipo         | Informações do estabelecimento onde ocorreu a transação.
***``"internacional"``***    | Boolean | **Sim**     | false                           | Indica se a transação é internacional(true) ou nacional(false).
***``"original_iso8583"``*** | ISO8583Message | **Sim** | Ver definição do tipo        | Mensagem original ISO-8583 enviada pela bandeira e processada pela paySmart.
``"additionalTerminalData"`` | AdditionalTerminalData | Não | Ver difinição do tipo | Contempla os dados adicionais pertencentes ao ponto de venda/saque (por exemplo, um POS) no momento em que a transação foi realizada.
``"tokenPaymentData"``     | TokenPaymentData  | Não      | Ver definições do tipo  |   Contém os Informações dos Dados Transacionais - Conjunto de dados de Pagamento via Token para consultas por QrCode ou cartão tokenizado.
``"hceTransaction"``      | Boolean                | Não      | false                                |   Indica se é uma transação feita com cartão HCE (true) ou outro tipo de cartão (false).
*``"eloProductCode"``* | 	EloProductCode | Não  | Ver definição do tipo      | Código do produto da transação. Usado pela bandeira ELO. 
