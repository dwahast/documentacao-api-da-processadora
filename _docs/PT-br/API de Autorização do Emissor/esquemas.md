---
title: Esquemas dos objetos usados na API
tags: 
 - esquema
 - spec
 - objeto
description: Esquema com todos os objetos usados na API do emissor pré e pós pago. 
permalink: /pt-br/docs/esquemas
language: pt-br
---


# Esquemas dos objetos utilizados na API.


[Success](esquemas#success) | 
[SuccessStatus](esquemas#successstatus) | 
[GeneralError](esquemas#generalerror) | 
[NotFoundError](esquemas#notfounderror) | 
[TransactionAlreadyCanceled](esquemas#transactionalreadycanceled) | 
[SystemDownError](esquemas#systemdownerror) | 
[InsufficientFunds](esquemas#insufficientfunds) | 
[OperationNotPermitted](esquemas#operationnotpermitted) | 
[InvalidMcc](esquemas#invalidmcc) |
[FraudSuspectError](esquemas#fraudsuspecterror) |
[Acknowledge](esquemas#acknowledge) | 
[ISO8583Message](esquemas#iso8583message) | 
[Amount](esquemas#amount) | 
[Fee](esquemas#fee) | 
[EntryMode](esquemas#entrymode) | 
[CancellationReason](esquemas#cancellationreason) | 
[Card](esquemas#card) | 
[Establishment](esquemas#establishment) | 
[ProcessingCode](esquemas#processingcode) | 
[HolderValidationMode](esquemas#holdervalidationmode) | 
[Authorization](esquemas#authorization) | 
[Brand](esquemas#brand) | 
[Source](esquemas#source) | 
[PaymentType](esquemas#paymenttype) | 
[FraudData](esquemas#frauddata) |
[InstallmentDetails](esquemas#installmentdetails) | 
[MITAdditionalData](esquemas#mitadditionaldata) |
[AdditionalTerminalData](esquemas#additionalterminaldata) |
[TokenPaymentData](esquemas#tokenpaymentdata)


### [Success]({{ site.url }}{{ site.baseurl }}/assets/img/pt-BR/assets/api/API de Autorização do Emissor/issuer-authorization-api.json#/Success)

Objeto que deve ser retornado em caso de operação realizada e aprovada com sucesso.

Atributo                          | Tipo   | Obrigatório | Exemplo                         | Descrição 
----------------------------------|---------|-------------|---------------------------------|--------------------
**message**                       | String  | **Sim**     | Operação realizada com sucesso. | Mensagem descritiva do resultado do processamento.
**code**                          | Integer | **Sim**     | 0                               | Código do resultado do processamento.
**authorization_id**              | Integer | **Sim**     | 123456                          | Código de autorização de 6 dígitos gerado pelo emissor. Deve ser um código único para cada transação durante o mesmo dia.
**balance**                       | Amount  | **Sim**     | Ver definição do tipo.          | Saldo disponível da conta/cartão após o processamento da transação.
purchaseOnlyApproval              | Boolean | Não         | false                           | Indicador de aprovação somente da compra sem devolução de dinheiro(Cashback) - SOMENTE MASTERCARD. Somente deve ser usado por emissores processando cartões da bandeira Mastercard. Só deve ser usado para compras com devolução de dinheiro(PWCB) e quando o terminal do adquirente suportar aprovação de compra sem devolução de dinheiro. O emissor deve incluir este campo com o valor 'true' na resposta de autorização quando aprovando somente a compra sem devolução de dinheiro, de acordo com as condições acima. Neste caso o saldo do portador deverá ser impactado somente com o valor da compra(purchaseOnlyAmount) que não inclui o valor do cashback(cashbackAmount). Em caso de aprovação total (compra com devolução de dinheiro) este campo pode ser setado como 'false' ou não precisa ser incluído na resposta. Neste caso o saldo do portador deverá ser impactado com o valor total da compra(totalAmount) que inclui o valor de devolução em dinheiro. 
purchaseOnlyPartialAmountApproved | Amount  | Não         | Ver definição do tipo.          | Deve ser enviado pelo emissor somente se a compra tiver aprovação parcial, caso contrário este campo não deverá ser enviado. 
cashbackOnlyPartialAmountApproved | Amount  | Não         | Ver definição do tipo.          | Deve ser enviado pelo emissor somente se a compra com troco tiver aprovação parcial do valor da devolução de dinheiro(cashback), caso contrário este campo não deverá ser enviado. 

* Para o Pós-Pago o campo "Balance" não está presente na resposta.

&nbsp;
---

### [SuccessStatus](../assets/api/openapi.json#/SuccessStatus)

Objeto que deve ser retornado na operação de consulta de status da saúde do sistema do emissor.

Atributo             | Tipo    | Obrigatório | Exemplo                         | Descrição 
---------------------|---------|-------------|---------------------------------|--------------------
**message**          | String  | **Sim**     | Operação realizada com sucesso. | Mensagem descritiva do resultado do processamento.
**code**             | Integer | **Sim**     | 0                               | Código do resultado do processamento.


&nbsp;
---

### [GeneralError](../assets/api/openapi.json#/GeneralError)

Objeto que deve ser retornado, em todas operações, quando ocorre um erro geral, não especificado, não no sistema do emissor.

Atributo             | Tipo    | Obrigatório | Exemplo                         | Descrição 
---------------------|---------|-------------|---------------------------------|--------------------
**message**          | String  | **Sim**     | Não foi possível executar comando. Erro desconhecido. | Mensagem descritiva do erro no processamento.
**code**             | Integer | **Sim**     | 999                             | Código do erro no processamento. 


&nbsp;
---

### [NotFoundError](../assets/api/openapi.json#/NotFoundError)

Objeto que deve ser retornado, em todas operações, para os casos em que a conta ou cartão não são encontrados no sistema do emissor.

Atributo             | Tipo    | Obrigatório | Exemplo                         | Descrição 
---------------------|---------|-------------|---------------------------------|--------------------
**message**          | String  | **Sim**     | Número único de conta não encontrado. | Mensagem descritiva do erro no processamento.
**code**             | Integer | **Sim**     | 111                             | Código do erro no processamento. 


&nbsp;
---

### [TransactionAlreadyCanceled](../assets/api/openapi.json#/TransactionAlreadyCanceled)

Objeto que deve ser retornado, em as operações de cancelamento, para os casos em que a transação já foi cancelada.

Atributo             | Tipo    | Obrigatório | Exemplo                         | Descrição 
---------------------|---------|-------------|---------------------------------|--------------------
**message**          | String  | **Sim**     | Transação já cancelada.         | Mensagem descritiva do erro no processamento.
**code**             | Integer | **Sim**     | 144                             | Código do erro no processamento. 


&nbsp;
---

### [SystemDownError](../assets/api/openapi.json#/SystemDownError)

Objeto que deve ser retornado, em todas operações, para os casos em que o sistema do emissor está indisponível.

Atributo             | Tipo    | Obrigatório | Exemplo                         | Descrição 
---------------------|---------|-------------|---------------------------------|--------------------
**message**          | String  | **Sim**     | Sistema indisponível. Erro ao acessar base de dados. | Mensagem descritiva do erro no processamento.
**code**             | Integer | **Sim**     | 900                             | Código do erro no processamento. 


&nbsp;
---

### [InsufficientFunds](../assets/api/openapi.json#/InsufficientFunds)

Objeto que deve ser retornado, nas operações de autorização de compra e saque, para os casos em que o saldo disponível do cartão não é suficiente para efetivar a transação.

Atributo             | Tipo    | Obrigatório | Exemplo                         | Descrição 
---------------------|---------|-------------|---------------------------------|--------------------
**message**          | String  | **Sim**     | Saldo insuficiente.             | Mensagem descritiva do erro no processamento.
**balance**          | Amount  | **Sim**     | Ver definição do tipo.          | Saldo disponível da conta/cartão após o processamento da transação.
**code**             | Integer | **Sim**     | 530                             | Código do erro no processamento. 
useVoucher           | Boolean | Não         | false                           | Deverá ser utilizado pelo Emissor para sinalizar o estabelecimento que ele solicitou a autorização na função DÉBITO ou CRÉDITO, mas o cartão não possui essa função ativa ou o portador não possui saldo/crédito nessa função, mas possui saldo no voucher.

&nbsp;
---

### [OperationNotPermitted](../assets/api/openapi.json#/OperationNotPermitted)

Objeto que deve ser retornado, nas operações de autorização de compra, saque, cancelamentos e estornos, para os casos em que o emissor não permite a operação devido a alguma particularidade específica da operação.

Atributo             | Tipo    | Obrigatório | Exemplo                         | Descrição 
---------------------|---------|-------------|---------------------------------|--------------------
**message**          | String  | **Sim**     | Saldo insuficiente.             | Mensagem descritiva do erro no processamento.
**balance**          | Amount  | **Sim**     | Ver definição do tipo.          | Saldo disponível da conta/cartão após o processamento da transação.
**code**             | Integer | **Sim**     | 530                             | Código do erro no processamento. 
useVoucher           | Boolean | Não         | false                           | Deverá ser utilizado pelo Emissor para sinalizar o estabelecimento que ele solicitou a autorização na função DÉBITO ou CRÉDITO, mas o cartão não possui essa função ativa ou o portador não possui saldo/crédito nessa função, mas possui saldo no voucher.
useCredit            | Boolean | Não         | false                           | Esse código será utilizado pelo Emissor para sinalizar o estabelecimento que ele solicitou a autorização na função DÉBITO, mas o cartão não possui essa função ativa.
useDebit             | Boolean | Não         | false                           | Esse código será utilizado pelo Emissor para sinalizar o estabelecimento que ele solicitou a autorização na função CRÉDITO, mas o cartão não possui essa função ativa.


&nbsp;
---

### [InvalidMcc](../assets/api/openapi.json#/InvalidMcc)

Objeto que deve ser retornado, nas operações de autorização de compra e saque, para os casos em que o código de categoria do estabelecimento(MCC) não é permitido para o cartão ou conta especificado na transação.

Atributo             | Tipo    | Obrigatório | Exemplo                         | Descrição 
---------------------|---------|-------------|---------------------------------|--------------------
**message**          | String  | **Sim**     | MCC Inválido para este cartão.  | Mensagem descritiva do erro no processamento.
**code**             | Integer | **Sim**     | 434                             | Código do erro no processamento. 


&nbsp;
---

### [FraudSuspectError](../assets/api/openapi.json#/FraudSuspectError)

Objeto que deve ser retornado nas operações de compra, saque e transferência para os casos em que o emissor decide negar a transação por suspeita de fraude.

Atributo             | Tipo    | Obrigatório | Exemplo                               | Descrição
---------------------|---------|-------------|---------------------------------------|--------------------
**message**          | String  | **Sim**     | Suspeita de fraude, transação negada. | Mensagem descritiva do erro no processamento.
**code**             | Integer | **Sim**     | 459                                   | Código do erro no processamento.


&nbsp;
---

### [Acknowledge](../assets/api/openapi.json#/Acknowledge)

Objeto que deve ser retornado, nas operações de consulta, compra, saque, cancelamentos e estornos, para os casos em que a operação foi negada pelo próprio sistema da bandeira ou pelo sistema de autorização da processadora e está sendo enviada para o emissor para que o mesmo registre esta transação. Para este caso não é necessário realizar nenhuma validação de saldo.

Atributo             | Tipo    | Obrigatório | Exemplo                         | Descrição 
---------------------|---------|-------------|---------------------------------|--------------------
**message**          | String  | **Sim**     | Mensagem de negação recebida.   | Mensagem descritiva do erro no processamento.
**code**             | Integer | **Sim**     | 499                             | Código do erro no processamento. 


&nbsp;
---

### [ISO8583Message](../assets/api/openapi.json#/ISO8583Message)

Objeto que representa uma mensagem ISO-8583 contendo todos os campos da mensageria. Enviado como parte do corpo da mensagem nas operações de consulta, compra, saque, cancelamentos e estornos representando a mensagem ISO-8583 da requisição enviada pela bandeira.

Atributo             | Tipo    | Obrigatório | Exemplo                         | Descrição 
---------------------|---------|-------------|---------------------------------|--------------------
description          | String  | Não         | Mensagem original enviada pela bandeira e processada pela paySmart. | Texto descritivo da mensagem ISO-8583 enviada na requisição.
**mti**              | String  | **Sim**     | 0100                            | MTI(Indicador do tipo da mensagem). Valores mais comuns: 0100, 0110, 0120, 0130, 0200, 0210, 0400, 0410, 0420, 0430. Pode possuir outros valores de acordo com definição da bandeira.



&nbsp;
---

### [Amount](../assets/api/openapi.json#/Amount)

Objeto que representa um valor monetário, representado por um valor numérico e o código da moeda correspondente ao valor. Usado em praticamente todas operações e retornos que envolvem valores monetários.

Atributo             | Tipo    | Obrigatório | Exemplo                         | Descrição 
---------------------|---------|-------------|---------------------------------|--------------------
**amount**           | Integer | **Sim**     | 123                             | Valor monetário contendo as casas decimais, porém sem a pontuação decimal. Exemplo: R$ 1,23 seria representado por 123.
**currency_code**    | Integer | **Sim**     | 986                             | Código numérico de identificação da moeda de acordo com padrão ISO-4217.

&nbsp;
---

### [Fee](../assets/api/openapi.json#/Fee)

Objeto que representa uma taxa aplicada a uma transação. Enviado nas operações de compra e saque que possuem taxas aplicadas.

Atributo             | Tipo        | Obrigatório | Exemplo                         | Descrição 
---------------------|-------------|-------------|---------------------------------|--------------------
**amount**           | Amount      | **Sim**     | Ver definição do tipo.          | Valor monetário da taxa.
**type**             | Enum String | **Sim**     | iof                             | Tipo da taxa. 

#### Tipos de Taxa:

Nome           | Descrição
---------------| ---------
iof            | Taxa de IOF aplicado a transação internacional.
markup         | Taxa de Markup aplicado a transação internacional.
boarding_fee   | Taxa de embarque aplicada a transação.
withdrawal_fee | Taxa de saque aplicada a transação de saque.
interest       | Valor dos juros acumulados e aplicados na transação. 
others         | Outras taxas que podem ser aplicadas a transação, como tarifa de pagamento de insumos, por exemplo.

&nbsp;
---

### [EntryMode](../assets/api/openapi.json#/EntryMode)

Objeto que representa o modo de entrada do PAN. Enviado em todas operações de consulta, compra, saque, cancelamentos e estornos para identificar o modo de entrada do PAN. É definido pela enumeração abaixo.

Nome                 | Descrição
---------------------| ---------
unknown              | Quando o modo de entrada é desconhecido ou nenhum outro campo é aplicável. 
magnetic_stripe      | Cartão passado via tarja (tecnologia MST também usa).
fallback             | Tarja magnética quando usada como fallback do chip.
chip                 | Transação com chip via contato.
ecommerce            | Transações pela internet usando browser em um computador.
mcommerce            | Transações pela internet usando dispositivo móvel (por aplicativos ou browser mobile).
typed                | Transação digitada manualmente.
contactless          | Transações contactless, tanto com cartão quanto com dispositivos móveis.
stored_account       | transação de cartão não presente usando informações de cartão armazenadas.
bar_or_qr_code       | Número de cartão lido por código e barras ou QR code.
URA                  | Entrada por URA.

&nbsp;
---

### [CancellationReason](../assets/api/openapi.json#/CancellationReason)

Objeto que representa o motivo de um cancelamento. Enviado nas operações de cancelamentos, avisos de autorização/negação de transações e estornos.

Atributo             | Tipo    | Obrigatório | Exemplo                         | Descrição 
---------------------|---------|-------------|---------------------------------|--------------------
**description**      | String  | **Sim**     | Cancelamento do Consumidor      | Razão ou motivo das Mensagens de Aviso de Autorização 0120, de Aviso de Reversão 0420 e Mensagem de Pedido de Reversão 0400.
**code**             | Integer | **Sim**     | 0                               | Código do motivo do cancelamento da mensagem.


&nbsp;
---

### [Card](../assets/api/openapi.json#/Card)

Objeto que representa um cartão. Enviado nas operações de consulta, compra, saque, cancelamentos e estornos para representar o cartão correspondente a operação em processamento.

Atributo             | Tipo    | Obrigatório | Exemplo                         | Descrição 
---------------------|---------|-------------|---------------------------------|--------------------
**paysmart_id**      | String  | **Sim**     | 2347923847                      | Identificador interno paySmart do cartão.
issuer_id            | String  | Não         | 9012368543                      | Identificador interno do emissor.
**pan**              | String  | **Sim**     | ************7812                | Número do cartão PAN (mascarado).
panseq               | String  | Não         | 00                              | Número de sequência do PAN.


&nbsp;
---

### [Establishment](../assets/api/openapi.json#/Establishment)

Objeto que representa em estabelecimento comercial. Enviado nas operações de consulta, compra e saque.

Atributo             | Tipo    | Obrigatório | Exemplo                         | Descrição 
---------------------|---------|-------------|---------------------------------|--------------------
**mcc**              | String  | **Sim**     | 1234                            | MCC do estabelecimento.
name                 | String  | Não         | Submarino                       | Nome do estabelecimento.
city                 | String  | Não         | SAO PAULO                       | Cidade do estabelecimento.
address              | String  | Não         | RUA MANOELITO DE ORNE           | Endereço de onde se encontra o POS.
zipcode              | String  | Não         | 90110230                        | CEP de onde se encontra o POS.
country              | String  | Não         | 076                             | País do estabelecimento.
cnpj                 | String  | Não         | 30099070000100                  | Número do documento CNPJ do estabelecimento.


&nbsp;
---

### [ProcessingCode](../assets/api/openapi.json#/ProcessingCode)

Objeto que representa o código de processamento da operação. Enviado nas operações de compra e saque.

Atributo             | Tipo         | Obrigatório | Exemplo                         | Descrição 
---------------------|--------------|-------------|---------------------------------|--------------------
tipo_transacao       | Enum String  | Não         | purchase                        | Identifica qual é o tipo da transação (dado pelo bit 3 do ISO8583).



#### Tipos de Transação:

Nome                        | Descrição
----------------------------| ---------
purchase                    | Uso do cartão para compra de bens e serviços.
installmentPurchase         | Uso do cartão para compra parcelada de bens e serviços
withdrawal                  | Saque em caixas da rede ATM.
establishmentWithdrawal     | Saque no estabelecimento (cash advance).
simulationQuery             | Consulta e simulação de vendas e saques, com CET (Custo Efetivo Total).
purchaseWithWithdrawal      | Compra com cartão com retenção de dinheiro pelo portador.
fundsWithdrawal             | Obtenção de fundos de uma conta cartão com propósito de realizar uma transferência.
addressVerification         | Fornece o endereço para um faturamento recorrente.
recurringBilling            | Cobrança recorrente (mensal, bimestral, etc).
subscription                | Pagamento por subscrição em uma base regular (anualmente, por exemplo).
cardAccountVerification     | Verificação do número da conta do cartão e opcionalmente validar a identidade do portador do cartão.
merchandiseReturn           | Devolução de mercadorias.
loadTransaction             | Utilizada para carregar cartões pré-pagos com valores monetários.
queryAvailableFunds         | Retorna o valor que pode ser obtido da fonte.
queryBalance                | Consulta do saldo disponível na conta ou no cartão.
queryDiscoverPayRewardsAccount | Consulta de saldo para o produto "Discover Pay with rewards".
transfer                    | Transferência (de uma conta para outra, por exemplo).
transferFunds               | Transferência de fundos que são adicionados a uma conta de cartão ou à uma conta de depósito.
pinChange                   | Mudança de PIN
pinUnblock                  | Transação de desbloqueio de PIN.
withdrawalConfiguration     | Configuração do saque.


&nbsp;
---

### [HolderValidationMode](../assets/api/openapi.json#/HolderValidationMode)

Representa o modo de validação do portador. Enviado nas operações de compra e saque. Definido pela enumeração String abaixo.

Nome            | Descrição 
----------------|-----------------
online_pin      | Validação por PIN Online.
offline_pin     | Validação por PIN Offline.
other           | Outro modo de validação do portador.


&nbsp;
---

### [Authorization](../assets/api/openapi.json#/Authorization)

Objeto que representa o código de autorização da operação. Para requisições de autorização (MTI igual à 0100) onde o código de autorização for '00' o emissor deve validar a transação e o saldo. Para mensagens de aviso de autorização/negação(MTI igual à 0120) ou quando o código de autorização for diferente de '00' o emissor não deve validar a transação e saldo, pois a transação já foi negada pela bandeira ou processadora ou é uma mensagem de aviso de autorização/negação de transação.

Atributo             | Tipo    | Obrigatório | Exemplo                         | Descrição 
---------------------|---------|-------------|---------------------------------|--------------------
**code**             | String  | **Sim**     | 00                              | Código de autorização definido até esta etapa da autorização. Considerar como pedido de autorização apenas se código for igual a 00. As transações de compra e saque com código de autorização diferente de zero não podem consumir saldo!
**description**      | String  | **Sim**     | Transação Autorizada            | Descrição do código de autorização.


&nbsp;
---

### [Brand](../assets/api/openapi.json#/Brand)

Objeto que identifica a bandeira ou esquema de pagamentos de onde a transação se originou. É definido pela enumeração String abaixo.

Nome            | Descrição 
----------------|-----------------
elo             | Bandeira ELO.
goodcard        | Bandeira GoodCard.
mastercard      | Bandeira MasterCard.
visa            | Bandeira Visa.

&nbsp;
---

### [Source](../assets/api/openapi.json#/Source)

Objeto que identifica o sistema que originou a transação. É definido pela enumeração String abaixo.

Nome            | Descrição 
----------------|-----------------
paySmart        | Sistema originador da transação foi a paySmart.
issuer          | Sistema originador da transação foi o emissor.
brand           | Sistema originador da transação foi a bandeira.

&nbsp;
---

### [PaymentType](../assets/api/openapi.json#/PaymentType)

Objeto que identifica o tipo de pagamento da transferência. Este campo deve conter um dos seguintes itens.

Nome    | Descrição 
--------|-----------------
A2A     | conta para conta
B2A     | banco para conta
P2P     | pessoa para pessoa
CSB     | Cashback
DSB     | Desembolso
B2B     | Compra Simultânea 
M2M     | Aporte de Recursos


&nbsp;
---

### [FraudData](../assets/api/openapi.json#/FraudData)

A presença deste campo indica que um Score de Fraude é entregue ao Emissor juntamente com dados adicionais de fraude. O Credenciador pode enviar seu Score de Fraude para a Bandeira e Emissor utilizando este campo.

&nbsp;

Atributo             | Tipo    | Obrigatório | Exemplo          | Descrição 
---------------------|---------|-------------|------------------|--------------------
creditorFraudScore   | Integer  | Não      | 1234            | Score de Fraude Credenciador.
eloBrandFraudScore  | Integer | Não        | 567             | Score de Fraude Bandeira Elo.
fraudScorePrimaryReason | Integer   | Não       | 1          | Motivo primário do score de fraude.
fraudScoreSecondaryReason | Integer   | Não       | 2          | Motivo secundário do score de fraude.
fraudScoreTertiaryReason | Integer   | Não       | 0          | Motivo terciário do score de fraude.
fraudDecisionRecommendation | String | Não    | "A"          | Recomendação de decisão por fraude.
scoreOriginIndicator	| Integer   | Não   | 1              | Indicador de Origem do Score. '1' - O 'Score de Fraude Bandeira Elo' corresponde ao Score da transação corrente; '2' - O 'Score de Fraude Bandeira Elo' corresponde ao Score da transação anterior por motivo de indisponibilidade do sistema.

&nbsp;
---

### [InstallmentDetails](../assets/api/openapi.json#/installmentDetails)

Objeto que representa o parcelamento do lojista para compras parceladas, exclusivamente para operações de pós-pago. 

Atributo             | Tipo    | Obrigatório | Exemplo                         | Descrição 
---------------------|---------|-------------|---------------------------------|--------------------
**finType**          | String  | **Sim**     | interestFree                    | Identifica o tipo do financiamento, podendo ser: interestFree, withInterest ou cdc (crédito direto ao cliente).
**fare_amount**      | Amount  | **Não**     | Ver definição do tipo           | Valor da tarifa.
**insurance_amount** | Amount  | **Não**     | Ver definição do tipo           | Valor do seguro.
**third_parties_paymnt_amount** | Amount  | **Não**     | Ver definição do tipo           | Valor do pagamento de terceiros.
**records_payments_amount** | Amount  | **Não**     | Ver definição do tipo    | Valor do pagamento do registro.
**issuer_total_calculated_amount** | Amount  | **Não**     | Ver definição do tipo           | Valor total calculado do emissor.
**first_paymnt_date** | String | **Não**     | 200107                          | Data do pagamento da primeira parcela no formato AAMMDD.
**instalmnt_nbr**   | Integer  | **Sim**     | 2                               | Quantidade de parcelas.
**monthly_interest_rate** | Integer  | **Não**     | 123                       | Identifica a taxa de juros mensal. Deve ser informado valor considerando duas casas decimais, por exemplo: taxa de 1,23% deve ser enviada como 123.
**total_effective_cost_rate_cet** | Integer  | **Não**     | 2020              | Identifica o custo efetivo total (CET). Deve ser informado considerando duas casas decimais, por exemplo: O CET de 20,20% deve ser enviado como 2020.
**instalmnt_amount** | Amount  | **Sim**     | Ver definição do tipo           | Valor das parcelas.
**annual_interest_rate** | Integer  | **Não**| 2341                            | Identifica a taxa anual de juros. Deve ser informado valor considerando duas casas decimais, por exemplo: A taxa anual de juros de 23,41% deve ser enviada como 2341.
**input_value**    | Amount  | **Não**       | Ver definição do tipo           | Valor de entrada.

&nbsp;
---

### [MITAdditionalData](../assets/api/openapi.json#/mitadditionaldata)

Este conjunto de dados é utilizado para passar dados de identificação da transação original e do valor da transação original do Estabelecimento Comercial/Credenciador para o Emissor. Utilizado para Pagamentos Parcelados no âmbito internacional (cartões emitidos no Brasil e aceitos internacionalmente). Este modelo é conhecido como “Buy Now Pay Later” e serão oferecidos três tipos de parcelamentos e também o modelo de recorrência.

* **Parcelado Emissor**: Identificado pelo domínio “V” no campo “Status de Transação no POS”, nesse formato o estabelecimento recebe o valor total no ato da compra e o Emissor é o responsável pela gestão das regras e cobranças ao portador. O Emissor deve receber, processar e responder a transação em Autorização e Liquidação. Neste modelo o pagamento ao estabelecimento é à vista.

* **Parcelado Loja**: Identificado pelo domínio “S” no campo “Status de Transação no POS”, nesse formato o estabelecimento assume o risco e a responsabilidade pelo recebimento dos valores, onde deve enviar uma solicitação de autorização e uma transação de liquidação para cada parcela no intervalo de tempo acordado com o portador. O Emissor deve estar preparado para receber, processar e responder a todas as transações em autorização e liquidação.

* **Parcelado Participante Terceiro**: Identificado pelo domínio “T” no campo “Status de Transação no POS”, esse formato tem as mesmas regras e responsabilidades do Parcelado Loja, porém a transação é feita por um Estabelecimento provedor de parcelamento para um terceiro Estabelecimento. O Emissor deve receber, processar e responder a todas as transações em Autorização e Liquidação.

* **Transação Recorrente**: Identificado pelo domínio “R” no campo “Status de Transação no POS”, esse formato permite ao Estabelecimento oferecer um produto ou serviço com cobranças recorrentes, sendo de responsabilidade do Estabelecimento enviar no período e frequência acordados com o portador as solicitações de autorização e as transações de liquidação. O Emissor deve receber, processar e responder a todas as transações em Autorização e Liquidação.

Atributo                  | Tipo     | Obrigatório | Exemplo         | Descrição
--------------------------|----------|-------------|-----------------|--------------------
**installmentTotalNbr**   | String   | Não         | "02"            | Quantidade total de parcelamento. Indica o número de Pagamentos ou Prestações Recorrentes de acordo com o Contrato do Portador do Cartão com o Estabelecimento.
**paymentType**           | String   | Não         | "fixed"         | Tipo de Transação iniciada pelo Estabelecimento(MIT). Indica o Tipo de Transação do MIT indicado no BIT 60 Subcampo 7. Os valores são usados para processar a primeira transação de pagamento e as subsequentes, conforme listado abaixo.
**transactionAmountMIT**  | Integer  | Não         | 10000           | Valor da transação iniciada pelo estabelecimento(MIT). Indica o valor do Parcelamento ou Pagamento Recorrente, e está na mesma moeda do BIT 49 - Código da Moeda, desta Transação. Os valores são usados para processar a primeira e as transações de pagamento subsequentes para esses tipos de pagamento específicos. Este valor pode ser diferente do BIT 04 - Valor da Transação. Esta tag tem regra de alinhamento com zeros à esquerda e duas casas decimais (R$ 50 como 000000005000).
**uniqueTransactionID**   | String   | Não         | "2eec2bba-660e" | ID Único. Usado para identificar de forma única cada Pagamento Recorrente ou Parcelado (fornecido automaticamente). Este ID é usado para fazer referência a autorizações e avisos de autorizações. Os valores são usados para processar a primeira e as subsequentes Transações com Cartão de uma transação de pagamento.
**transactionFrequency**  | String   | Não         | "monthly"       | Frequência da transação iniciada pelo estabelecimento(MIT). Indica a frequência de um pagamento ou parcelamento recorrente.
**validationIndicator**   | Integer  | Não         | "validated"     | Indicador de validação. O adquirente verifica a transação do cartão com a fonte de validação e preenche o sinalizador.
**validationReference**   | String   | Não         | "cb81f0b557d8"  | Referência de validação. Um criptograma gerado pela fonte de validação para cada conta do titular do cartão, para um emissor validar uma transação com cartão. O valor é usado apenas para processar pagamentos recorrentes ou parcelamentos subsequentes.
**sequenceIndicator**     | Integer  | Não         | 1               | Indicador de sequência. Usado para identificar a sequência das transações quando vários pagamentos parcelados serão enviados. O indicador de sequência é preenchido em ordem crescente.

&nbsp;
---

### [AdditionalTerminalData](../assets/api/openapi.json#/additionalterminaldata)

Contempla os dados adicionais pertencentes ao ponto de venda/saque (por exemplo, um POS) no momento em que a transação foi realizada.

Atributo                            | Tipo     | Obrigatório | Exemplo                  | Descrição
------------------------------------|----------|-------------|--------------------------|--------------------
**terminalType**                    | String   | Não         | "terminalWithService"    | Indicador de atendimento no Terminal.
**partialApprovalIndicator**        | String   | Não         | "notSupported"           | Indicador de Aprovação Parcial.
**terminalLocationIndicator**       | String   | Não         | "local"                  | Indicador de Localização do Terminal.
**cardholderPresenceIndicator**     | String   | Não         | "present"                | Indicador de Presença do Portador.
**cardPresenceIndicator**           | String   | Não         | "cardPresent"            | Indicador de Presença do Cartão.
**cardCaptureCapabilityIndicator**  | String   | Não         | "cardCaptureCapability"  | Indicador de Capacidade de Captura de Cartão.
**transactionStatusIndicator**      | String   | Não         | "normal"                 | Indicador de Status da Transação, esse atributo corresponde ao propósito da solicitação de autorização.
**transactionSecurityIndicator**    | String   | Não         | "noSuspectedFraud"       | Indicador de Segurança da Transação.
**terminalPOSType**                 | String   | Não         | "atm"                    | Tipo do POS do Terminal (Dispositivo).
**terminalInputCapability**         | String   | Não         | "chip"                   | Capacidade de Entrada do Terminal.

&nbsp;
---

### TeminalType

Tipo de Terminal. Indicador de atendimento no Terminal.

Valores válidos:

Nome                        |  Descrição
----------------------------|--------------------------------------------------------------------
terminalWithService         | Terminal com atendimento (EC possui operador para o terminal).
selfServiceTerminal         | Terminal com autoatendimento (Ex.: Vending Machines).
noTerminal                  | Sem Terminal (Ex.: URA/Voz/e-Commerce).
mobile                      | Celular (Mobile).
unknown                     | Desconhecido.

&nbsp;
---

### PartialApprovalIndicator

Indicador de Aprovação Parcial.

Valores válidos:

Nome                        |  Descrição
----------------------------|--------------------------------------------------------------------
notSupported                | Aprovação Parcial não suportado (este deve ser o valor enviado pelo Credenciador).
supported                   | Aprovação Parcial suportado: Compras podem ser aprovadas parcialmente. Saque pode ser aprovado parcialmente.
onlyPurchasesSupported      | Aprovação Parcial suportado: Compras podem ser aprovadas parcialmente. Saque deve ser aprovado por completo ou declinado.
onlyWithdrawalSupported     | Aprovação Parcial suportado: Compras podem ser aprovadas por completo ou declinadas. Saque pode ser aprovado parcialmente.
onlyTotalPurchasesSupported | Aprovação Parcial suportado: Compras podem ser aprovadas por completo ou declinadas. Saque deve ser aprovado por completo ou declinado

&nbsp;
---

### TerminalLocationIndicator

Indicador de Localização do Terminal.

Valores válidos:

Nome                        |  Descrição
----------------------------|--------------------------------------------------------------------
local                       | No estabelecimento comercial
remote                      | Fora estabelecimento comercial (terminal remoto)
withCardHolder              | Junto ao portador (Ex.: PC nos casos de e-Commerce)
noTerminal                  | Sem Terminal (Ex.: Voz/URA)
unknown                     | Desconhecido

&nbsp;
---

### CardholderPresenceIndicator

Indicador de Presença do Portador.

Valores válidos:

Nome                        |  Descrição
----------------------------|--------------------------------------------------------------------
present                     | Portador está presente
notPresentNotSpecified      | Portador não presente, não especificado
notPresentMailFax           | Portador não presente, Mail/FAX Order
notPresentTelephone         | Portador não presente, Telephone Order
notPresentStandingRecurrent | Portador não presente, Standing Order/Pagamento recorrente
electronicOrder             | Eletronic Order (Ex.: e-Commerce)
payButton                   | Pay Button
unknown                     | Desconhecido

&nbsp;
---

### CardPresenceIndicator

Indicador de Presença do Cartão.

Valores válidos:

Nome                        |  Descrição
----------------------------|--------------------------------------------------------------------
cardPresent                 | Cartão presente
cardNotPresent              | Cartão não presente
unknown                     | Desconhecido

&nbsp;
---

### CardCaptureCapabilityIndicator

Indicador de Capacidade de Captura de Cartão.

Valores válidos:

Nome                        |  Descrição
----------------------------|--------------------------------------------------------------------
noCardCaptureCapability     | Terminal (POS)/Operador não tem capacidade de captura de cartão
cardCaptureCapability       | Terminal (POS)/Operador tem capacidade de captura de cartão
unknown                     | Desconhecido

&nbsp;
---

### TransactionStatusIndicator

Indicador de Status da Transação, esse atributo corresponde ao propósito da solicitação de autorização.

Valores válidos:

Nome                                |  Descrição
------------------------------------|--------------------------------------------------------------------
normal                              | Solicitação normal (apresentação original)
preAuthorization                    | Solicitação de Pré-Autorização
reAuthorization                     | Re-autorização do valor total (Ex.: após expiração do prazo de Liquidação), Recuperação de Dívida
delayedSale                         | Venda atrasada (Ex.: danos em hotel ou em aluguel de carro)
authorizationReAttempt              | Re-tentativa de autorização (Ex.: saldo insuficiente ou limite excedido)
aggregatedTransaction               | Transação agregada (modelo concentrado) utilizada no sistema de transportes
incrementalAuthorization            | Autorização Incremental
noShowCharge                        | Cobrança de não comparecimento (No-Show)
splitPartialShipping                | Envio Dividido/Parcial (Ex.: Envio mediante disponibilidade)
recurringPayment                    | Pagamento Recorrente
installmentPaymentEstablishment     | Pagamento Parcelado Loja/Estabelecimento
installmentPaymentThirdParty        | Pagamento Parcelado por Participante Terceiro
unscheduledPayment                  | Pagamento não programado (Ex.: transação que não ocorre em uma data programada ou regularmente, por exemplo, a recarga automática de um celular pré-pago)
installmentPaymentIssuer            | Pagamento Parcelado Emissor/Crediários
cryptocurrencyPurchase              | Compra de Criptomoeda

&nbsp;
---

### TransactionSecurityIndicator

Este subcampo será preenchido pelo Estabelecimento para indicar se existe suspeita de fraude ou não.

Valores válidos:

Nome                                |  Descrição
------------------------------------|--------------------------------------------------------------------
noSuspectedFraud                    | Não há suspeita.
ceSuspectedFraud                    | Existe suspeita de fraude por parte do EC.
cardholderDocumentsVerified         | Foram verificados os documentos do portador.
unknown                             | Desconhecido

&nbsp;
---

### TerminalPOSType

Tipo do POS do Terminal (Dispositivo).

Valores válidos:

Nome                                |  Descrição
------------------------------------|--------------------------------------------------------------------
atm                                 | ATM
phone                               | Tap On Phone / Tap On Mobile sem Add-On Hardware
mPos                                | POS Mobile Add-On Hardware
pos                                 | POS Padrão
tef                                 | TEF
notSpecified                        | Não especificado.
unknown                             | Desconhecido.

&nbsp;
---

### TerminalInputCapability

Capacidade de Entrada do Terminal.

Valores válidos:

Nome                                |  Descrição
------------------------------------|--------------------------------------------------------------------
undefined                           | Indefinido
noTerminal                          | Sem Terminal (URA/Voz)
magneticStripe                      | Leitor de trilha magnética
barCode                             | Leitor de código de barras
ocr                                 | Leitor OCR
chip                                | Leitor de CHIP
manual                              | Digitado
magneticStripeAndManual             | Leitor de trilha e digitado
rfidChipCtless                      | Radio Frequency Identification (RFID) – CHIP (Contactless)
hybrid                              | Híbrido – CHIP e Contactless
rfidMagneticStripe                  | Radio Frequency Identification (RFID) – Trilha Magnética
setWithCertificate                  | Secure Eletronic Transaction (SET) com certificado
setWithoutCertificate               | Secure Eletronic Transaction (SET) sem certificado
sslTransaction                      | Channel-encrypted Eletronic Commerce Transaction (SSL)
insecureEcommerceTransaction        | Non-secure Eletronic Commerce Transaction

&nbsp;
---

### [TokenPaymentData](../assets/api/openapi.json#/tokenpaymentdata)

Contempla os Dados Transacionais - Conjunto de dados de Pagamento via Token que poderá estar presente em todas as transações por QrCode ou com cartão tokenizado.

Atributo                                          | Tipo     | Obrigatório | Exemplo            | Descrição
--------------------------------------------------|----------|-------------|--------------------|--------------------
**fPan**                                          | String   | Não         | "1234"             | 4 últimos dígitos do número primário da conta (FPAN).
**requesterIdToken**                              | String   | Não         | "12345678901"      | Este tag contém o ID do requisitante do Token.
**pan**                                           | String   | Não         | "************1234" | Contém os dados do PAN (DPAN) de contas com token.
**tokenPSN**                                      | String   | Não         | "001"              | Token PAN Sequence Number.
**tokenExpirationDate**                           | String   | Não         | "2401"             | Data de expiração do token.
**tokenStatus**                                   | String   | Não         | "0100"             | Token Status.
**tokenCryptogramVerificationResults**            | String   | Não         | "0200"             | Token Cryptogram Verification Results.
**EMVTokenCryptogramVerificationResults**         | String   | Não         | "0200"             | EMV Token Cryptogram Verification Results.
**tokenConstraintsVerificationStatus**            | String   | Não         | "0300"             | Token Constraints Verification Status.
**transactionDateTimeConstraint**                 | String   | Não         | "OK"               | Transaction Date Time Constraint.
**transactionAmountConstraint**                   | String   | Não         | "OK"               | Transaction Amount Constraint.
**usageConstraint**                               | String   | Não         | "OK"               | Usage Constraint.
**tokenATCVerificationResults**                   | String   | Não         | "E001"             | Token ATC Verification Results.
**CVE2TokenCryptogramVerificationStatus**         | String   | Não         | "E001"             | CVE2 Token Cryptogram Verification Status.
**merchantVerification**                          | String   | Não         | "OK"               | Merchant Verification.
**magstripeTokenCryptogramVerificationResults**   | String   | Não         | "0200"             | Magstripe Token Cryptogram Verification Results.
**CVE2OutputTokenCryptogramVerificationResults**  | String   | Não         | "0201"             | CVE2 Output Token Cryptogram Verification Results.


&nbsp;
---

### Token Status

Token Status.

Valores válidos:

Nome    |  Descrição
--------|--------------------------------------------------------------------
0100    | Token encontrado
0101    | Token não encontrado
0102    | Token suspenso
0103    | Token encerrado
0104    | Token inativo
E001    | Processamento ignorado
E002    | Processamento desabilitado
F???    | Erro interno de processamento, sendo ??? o código do erro

&nbsp;
---

### Token Cryptogram Verification Results

Token Cryptogram Verification Results.

Valores válidos:

Nome    |  Descrição
--------|--------------------------------------------------------------------
0200    | Criptografia OK (todas as verificações de criptograma estão OK)
0201    | Criptografia falhou (pelo menos uma verificação de criptograma falhou)
E001    | Processamento ignorado
E002    | Processamento desabilitado
F???    | Erro interno de processamento, sendo ??? o código do erro

&nbsp;
---

### EMV Token Cryptogram Verification Results

EMV Token Cryptogram Verification Results.

Valores válidos:

Nome    |  Descrição
--------|--------------------------------------------------------------------
0200    | Criptograma OK
0201    | Criptograma inválido
E001    | Processamento ignorado
E002    | Processamento desabilitado
F???    | Erro interno de processamento, sendo ??? o código do erro

&nbsp;
---

### Token Constraints Verification Status

Token Constraints Verification Status.

Valores válidos:

Nome    |  Descrição
--------|--------------------------------------------------------------------
0300    | Restrições OK
0301    | Restrições falharam (pelo menos uma restrição falhou)
E001    | Processamento ignorado
E002    | Processamento desabilitado
F???    | Erro interno de processamento, sendo ??? o código do erro

&nbsp;
---

### Transaction Date Time Constraint

Transaction Date Time Constraint.

Valores válidos:

Nome    |  Descrição
--------|--------------------------------------------------------------------
OK      | Verificação OK
NC      | not constrained (Não verificado)
EFF     | Token ainda não é efetivo
EXP     | Token expirado
NA      | not allowed (verificação foi realizada e falhou)

&nbsp;
---

### Transaction Amount Constraint

Transaction Amount Constraint.

Valores válidos:

Nome    |  Descrição
--------|--------------------------------------------------------------------
OK      | verificação OK
NC      | not constrained (não verificado)
AMT     | Valor acima do permitido
CUR     | Moeda não permitida

&nbsp;
---

### Usage Constraint

Usage Constraint.

Valores válidos:

Nome    |  Descrição
--------|--------------------------------------------------------------------
OK      | verificação OK
NC      | not constrained (não verificado)
DP      | Token depleted (número de usos maior que o permitido)
UN      | Token is not usable. (Tentativas para usar o token foram marcadas como não utilizáveis. Foi declarado que o número máximo de tentativas é zero)

&nbsp;
---

### Token ATC Verification Results

Token ATC Verification Results.

Valores válidos:

 Nome |  Descrição
------|--------------------------------------------------------------------
 0500 | ATC OK
 05XX | Sendo que XX é definido como bitmap: b8-b1 (com o b8 sendo o bit mais significativo):
      | * b8 – RFU
      | * b7 – ATC validado anteriormente (transação repetida)
      | * b6 – RFU
      | * b5 – RFU
      | * b4 – RFU
      | * b3 – Salto "para trás"/negativo do ATC é maior que o permitido
      | * b2 – Salto "para frente"/positivo do ATC é maior que o permitido
      | * b1 – ATC é maior que o máximo permitido
 E001 | Processamento ignorado
 E002 | Processamento desabilitado
 F??? | Erro interno de processamento, sendo ?? o código do erro


&nbsp;
---

### CVE2 Token Cryptogram Verification Status

CVE2 Token Cryptogram Verification Status.

Valores válidos:

Nome    |  Descrição
--------|--------------------------------------------------------------------
0200    | Criptograma OK
0201    | Criptograma inválido
E001    | Processamento ignorado
E002    | Processamento desabilitado
F???    | Erro interno de processamento, sendo ??? o código do erro

&nbsp;
---

### Merchant Verification

Merchant Verification.

Valores válidos:

Nome    |  Descrição
--------|--------------------------------------------------------------------
OK      | Merchant OK
NC      | not constrained (não verificado)
NA      | Merchant não permitido

&nbsp;
---

### Magstripe Token Cryptogram Verification Results

Magstripe Token Cryptogram Verification Results.

Valores válidos:

Nome    |  Descrição
--------|--------------------------------------------------------------------
0200    | Criptograma OK
0201    | Criptograma inválido
E001    | Processamento ignorado
E002    | Processamento desabilitado
F???    | Erro interno de processamento, sendo ??? o código do erro

&nbsp;
---

### CVV2 Output Token Cryptogram Verification Results

CVV2 Output Token Cryptogram Verification Results.

Valores válidos:

Nome    |  Descrição
--------|--------------------------------------------------------------------
0200    | Criptograma OK
0201    | Criptograma inválido
E001    | Processamento ignorado
E002    | Processamento desabilitado
F???    | Erro interno de processamento, sendo ??? o código do erro
