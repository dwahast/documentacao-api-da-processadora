---
title: FAQ
tags: 
 - faq
 - perguntas e respostas
description: Perguntas e respostas mais comuns na integração dos emissores a API paySmart
permalink: pt-br/docs/faq
language: pt-br
---

# FAQ

Q1. **Há alguma limitação quanto ao tipo de pessoa (física ou jurídica) que pode ser titular de uma conta? E quanto ao portador de um cartão?**

   Não há nenhuma limitação ou vínculo nesse sentido. Tanto uma pessoa física (PF) quanto uma pessoa jurídica (PJ) pode deter uma conta, assim como ser portador de um cartão. No relacionamento entre esses dois elementos, nehuma restrição é imposta - por exemplo, um cenário hipotético onde o titular da conta seja uma pessoa física e o portador de um cartão vinculado seja uma pessoa jurídica seria equivalente a qualquer outra combinação possível entre a natureza legal das pessoas envolvidas e os elementos conta e cartão.

Q2. **Quando o titular da conta for uma pessoa jurídica, devo utilizar o campo "*requestingCompanyInfo*" para informar seus dados?**

   Não. Assim como na conta PF, os dados do titular de uma conta de PJ devem ser informados no campo "*accountOwner*".

Q3. **Quando, então, devo utilizar o campo "*requestingCompanyInfo*"?**

   O campo "*requestingCompanyInfo*" é voltado para modelos de negócios *business-to-business* (B2B), como o mercado de benefícios, por exemplo. No caso típico, o titular da conta é uma pessoa física com um saldo associado a si. No entanto, quando o saldo dessa conta é gerenciado por uma empresa (com a qual normalmente a PF titular possui um vínculo empregatício), os dados da mesma são informados no campo "*requestingCompanyInfo*".

Q4. **Preciso manter o saldo de uma conta atualizado junto a paySmart?**

   Não. A rigor, a paySmart não necessita saber o saldo de uma conta. O emissor / sub-emissor será consultado a cada transação realizada, sendo a mesma só efetivada se houver confirmação positiva da existência de saldo na conta que está sendo movimentada.

Q5. **É possível criar várias contas com o mesmo titular?**

   Sim. Para caracterizar uma conta, a paySmart utiliza uma tupla formada pelo documento do titular ("*accountOwner.identityDocumentNumber*"), o código do produto ("*psProductCode*") e pelo documento da empresa solicitante ("*requestingCompanyInfo.identityDocumentNumber*"), se houver. Mesmo esses três dados podem ser repetidos, se o campo identificando a conta do emissor / sub-emissor ("*issuerAccountId*") for diferente a cada nova requisição. Se os quatro campos forem idênticos a uma requisição anterior, será retornado um erro informando que aquela conta já existe.

Q6. **E quanto aos cartões, quais são as restrições quanto ao portador?**

   Não há restrições quanto ao número de cartões que um portador, identificado por um documento, pode ter. Isso se aplica para contas distintas ou para o escopo de uma mesma conta.

Q7. **É possível gerar e imprimir cartões sem um titular associado?**

   Sim. Na paySmart, cartões gerados com esse modelo são chamado "cartões anônimos". Há duas maneiras de solicitar sua confecção. A primeira delas é utilizando a chamada "*requestNewAnonymousCard*" para disparar o processo de geração e impressão, em conjunto com a chamada "*bindAnonymousCardRequest*" para associar o plástico a um portador. O segundo método consiste em criar o cartão vinculado a uma conta, sem especificar os dados do campo "cardholder", especificando-os futuramente através de uma chamada do tipo PUT. Mais detalhes são descritos na seção "Criação de cartões".

Q8. **Cartões anônimos podem realizar transações sem estar vinculados a um portador?**

   Não. Quando houver uma tentativa de desbloquear um cartão sem portador, ela será rejeitado com um erro específico. Um cartão bloqueado não irá conseguir realizar transações que movimentem uma conta.


Q9. **O que acontece se a chave de idempotência não for trocada a cada requisição?**

   No caso da chave de idempotência se repetir, a requisição que está sendo enviada não terá nenhum efeito prático no sistema - apenas será replicada a resposta da primeira requisição na qual aquela chave de idempotência foi observada. Esse mecanismo é útil para saber o que ocorreu com uma requisição anterior, cuja resposta não tenha chegado ao solicitante por qualquer motivo.

Q10. **Não estou conseguindo cadastrar cartões em e-commerces, o que pode ser?**

   Verificar se foi desenvolvido o endpoint /queries, consultar a documentação no [link]({{ site.url }}{{ site.baseurl }}/docs/consulta#post-queries).

Q11. **Na emissão de um cartão pré-pago, quando tempo demora para o status do cartão mudar de request para issuing e ter o last4Digits preenchido?**

   Em nossas janelas de execução de ETLs: quando é realizada uma requisição de cartão, há um processo interno nosso que roda e gera os arquivos de cartões. Essa geração é em uma janela de 1:30h. Ou seja, roda 9h, 10:30h, 12h.... Então o normal é que em produção em no máximo 1:30h o status mude.
Em Homologação esse processo ocorre a cada 30min.

Q12. **Existe algum webhook para notificar sobre as mudanças de status do cartão?**

   Deve ser verificado pelo método GET /cards.

Q13. **Ao efetuar o desbloqueio do cartão deve ser sempre informar 0 no campo unblockCode?**

   Sim. Deverá ser informado 0.

