---
title: Back Office
tags: 
 - backoffice
description: Informações sobre como funciona o Back Office paySmart
permalink: pt-br/docs/backoffice
language: pt-br
---
# ***Back Office***

1. [Introdução](#introdução)
1. [Transações de Cancelamento](#transações-de-cancelamento)
    1. [Requisição](#requisição)
1. [Transações de Ajuste Financeiro](#transações-de-ajuste-financeiro)
    1. [Requisição](#requisic3a7c3a3o-1)

## **Introdução**

O *Back Office* é uma aplicação interna utilizada pela paySmart com o intuito de realizar procedimentos que estão fora do escopo normal das transações. As operações do *Back Office* são convertidas em transações de cancelamento ou transações de ajuste financeiro.

Ambos os tipos de transações do *Back Office* apresentam id de transação no formato ``"Back-Office_XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX"``. 

## **Transações de Cancelamento**

Ocorrem nos casos de decurso de prazo: transações que não foram confirmadas na liquidação após o período determinado pelo Manual ELO.

Para as transações de cancelamento, são realizadas chamadas através dos *endpoints* [POST /purchases/cancel]({{ site.url }}{{ site.baseurl }}/pt-br/processor-api) e [POST /withdrawals/cancel]({{ site.url }}{{ site.baseurl }}/pt-br/processor-api).

Plataforma | Tempo de Liquidação
-|-
Credito | As transações podem ser encaminhadas na liquidação em até 30 dias após a autorização
Débito | As transações podem ser encaminhadas na liquidação em até 7 dias após a autorização
Saques | As transações podem ser encaminhadas na liquidação em até 10 dias úteis após a autorização

### **Requisição**
    
```json
{
    "cancellation_id": "Back-Office_XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX",
    "original_purchase_id": "aut_XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX",
    "account_id": "cta-XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX",
    "psProductCode": "0XX002",
    "countryCode": "076",
    "source": "PAYSMART",
    "callingSystemName": "Autorizador ISO8583",
    "original_authorization_id": 999999,
    "authorization": {
        "code": "00",
        "description": "Transacao Autorizada"
    },
    "brand": "ELO",
    "card": {
        "paysmart_id": "crt-XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX",
        "pan": "************3681"
    },
    "original_amount": {
        "amount": 39000,
        "currency_code": 986
    },
    "entry_mode": "CHIP",
    "cancellation_reason": {
        "code": 5,
        "description": "Valor original incorreto."
    },
    "iso8583_message": {
        "mti": "0420",
        "de007": "0809222757",
        "de011": "060885",
        "de037": "751462060885"
    }
}
```
    
## **Transações de Ajuste Financeiro**

São decorrentes de situações atípicas (falha sistêmica, desacordo, valor final diferente de valor pré-autorizado).

Para as transações de ajuste financeiro, são realizadas chamadas através do *endpoint* [POST /transfers]({{ site.url }}{{ site.baseurl }}/pt-br/processor-api)

>**Obs.:** O ajuste pode ser feito tanto na modalidade de débito quanto de crédito

### **Requisição**
    
```json
{
    "transfer_id": "Back-Office_XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX",
    "account_id": "cta-XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX",
    "psProductCode": "0XX001",
    "countryCode": "076",
    "source": "PAYSMART",
    "productType": "pre",
    "callingSystemName": "Back-Office Ajuste",
    "preAuthorization": false,
    "incrementalAuthorization": false,
    "authorization": {
        "code": "00",
        "description": "Transação Autorizada"
    },
    "brand": "ELO",
    "card": {
        "paysmart_id": "crt-XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX",
        "pan": "************7576"
    },
    "total_amount": {
        "amount": 38822,
        "currency_code": 986
    },
    "original_amount": {
        "amount": 38822,
        "currency_code": 986
    },
    "entry_mode": "TYPED",
    "processing_code": {
        "tipo_transacao": "TRANSFERFUNDS",
        "source_account_type": "CREDIT_CARD_ACCOUNT",
        "destination_account_type": "NOT_APPLICABLE"
    },
    "fees": [
        {
            "amount": {
                "amount": 0,
                "currency_code": 986
            },
            "type": "IOF"
        }
    ],
    "establishment": {
        "mcc": "4816",
        "name": "paySmart Ajustes      "
    },
    "internacional": false,
    "original_iso8583": {
        "mti": "0100",
        "de003": "533000",
        "de007": "0107195616",
        "de011": "188854",
        "de012": "195616",
        "de013": "0107",
        "de032": "0025",
        "de037": "708554685504",
        "de041": "00000000",
        "de042": "0000000000000000"
    },
    "forceAccept": false,
    "transferData": {
        "paymentType": "P2P",
        "uniqueReferenceNumber": "XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX",
        "countryOrStateCodeIfUS": "076"
    }
}
```