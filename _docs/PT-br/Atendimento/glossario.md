---
title: Glossário
tags: 
 - glossario
description: Conceitos mais comuns na integração dos emissores a API paySmart
permalink: pt-br/docs/glossario
language: pt-br
---

# Glossário

**Arquivo de Liquidação:** Arquivo que recebemos e enviamos para a Bandeira, com registros(transações) das compras realizadas no dia anterior. Este arquivo é recepcionado e processado pela paySmart e são realizadas as validações de cada transação, em caso de inconsistências, a transação (TE) e devolvida/rejeitada para a Bandeira.

**Arranjo de Pagamento:** Conjunto de procedimentos e regras que disciplina a prestação de serviços dos serviços de pagamento aceito por mais de um recebedor, mediante acesso direto pelos usuários, pagadores e recebedores, conforme as normas legais e regulamentares vigentes.

**Automatic Teller Machine (ATM):** caixa eletrônico, como os terminais da Rede Banco 24 horas, utilizados para Saque em espécie e serviços como pagamentos de contas e troca de senha. 

**Autorização:** É o processo de aprovação da transação de compra ou saque, na qual o Emissor valida os limites de créditos e itens de segurança do portador; Troca de mensagerias (não financeiro) entre os Participantes do Fluxo Transacional, sendo no momento do Pagamento, com a utilização de um Cartão de Crédito Pós-pago, Cartão de Crédito Pré-pago ou Cartão de Débito o início do processo, para que esta Transação (Pagamento) seja ou não autorizada.

**Banco Liquidante:** Instituição Financeira que presta serviços de transferência de valores entre os participantes do Arranjo de Pagamento por meio da Câmara de Compensação e Liquidação. No Caso da ISSUER, é o Banco que fará as liquidações em nome da ISSUER.

**Bandeira:** Empresa detentora da marca principal dos cartões e responsável pelas regras e o funcionamento do Arrjano de Pagamento. Ex: Elo, Mastercard eVisa.

**Chargeback:** Cancelamento de uma venda feita com cartão crédito, que pode acontecer quando não há reconhecimento da compra por parte do titular do cartão ou quando a transação não obedecer às regulamentações previstas nos contratos, junto a Bandeira. No contexto da ISSUER, é iniciado com o Portador entrando em contato com o Sub emissor que lhe deu o cartão pré-pago, não aceitando o valor ou a transação que apareceu em seu cartão.

**Cliente:** Pessoa física ou jurídica que contrata o serviço do Emissor para utilização do recurso/limite de crédito à partir das regras do Arranjo de Pagamento. No contexto ISSUER, normalmente é um cliente do Sub Emissor.

**Card Personalization Validation (CPV):** validação de cartões produzidos por uma Personalizadora de Cartões, para garantir que eles estejam em conformidade com os requisitos da Bandeira Mastercard  

**Credenciador:** Instituição de Pagamento (IP) que, sem gerenciar Conta de Pagamento, é responsável por prestar serviços integrados de credenciamento e manutenção de estabelecimentos Comerciais (ECs) e participa do processamento e liquidação de transações realizadas com os Instrumentos de Pagamento. É a entidade responsável pela administração do contrato com o estabelecimento comercial para aceitação do cartão de pagamento e pela comunicação da transação entre o estabelecimento e a bandeira. Habilita recebedores para a aceitação do cartão de pagamento e participa do processo de liquidação das transações de pagamento como credor perante o emissor (ISSUER). É quem aluga e mantém os equipamentos usados pelos estabelecimentos, por exemplo, o POS. Ex: Cielo

**Elo:** Elo Serviços S.A., instituidora dos Arranjos de Pagamento Elo.

**Emissor:** Instituição Financeira que emite e administra cartões próprios ou de terceiros aos portadores. O emissor é responsável pelo relacionamento com o portador para qualquer questão decorrente de posse e uso dos meios eletrônicos de pagamentos além de outros serviços próprios ou terceiros, que envolvam o negócio de meios de pagamento. (Ver também Sub-Emissor)

**Estabelecimento Comercial (EC):** Pessoa Física ou Jurídica, no Brasil ou no exterior, que se propõe a vender bens e/ou serviços ao usuário aceitando o Instrumento de Pagamento, mediante adesão a contrato específico de credenciamento a um Arranjo de Pagamento fornecido pelo Credenciador.

**Instituição de Pagamento (IP):** Pessoa Física ou Jurídica, autorizada pelo Banco Central do Brasil que, aderindo a um ou mais arranjos de pagamento, tenha como atividade principal: (I) Gerenciamento de Conta de Pagamento Pré-Paga, e disponibilização de transação de pagamento com base em moeda eletrônica aportada nessa conta, podendo credenciar a sua aceitação e converter tais recursos em moeda física ou escritural, ou vice-versa; (II) Gerenciamento de Conta de Pagamento Pós-Paga, e disponibilização de transação de pagamento com base nessa conta.; e (III) Habilitação de recebedores, pessoas naturais ou jurídicas, para a aceitação de instrumento de pagamento emitido por Instituição de Pagamento ou por Instituição Financeira participante de um mesmo Arranjo de Pagamento.

**Instrumento de Pagamento:** Dispositivo (por exemplo, cartão, celular ou pulseira), instrumento ou conjunto de procedimentos vinculados a Contas de Pagamento utilizado para realizar transação de pagamento de compra.

**Liquidação:** É o processo pelo qual os participantes do Arranjo de Pagamento liquidam suas obrigações junto aos demais participantes do arranjo, tendo como resultado final o crédito aos estabelecimentos comerciais. (Ver também Arquivo de Liquidação)

**Participante:** Instituição de Pagamento (IP) qualificada e aprovada para participar de um Arranjo de Pagamento, de acordo com os critérios estabelecidos em regulamento e em contrato assinado com o Instituidor dos Arranjos de Pagamento, sendo eles os emissores de moeda eletrônica, emissores de instrumento de pagamento pós-pago, emissores de instrumento de pagamento de depósito à vista, prestadores de serviço de rede, credenciador, Instituição Domicílio (ID), ID-Facilitadora e Facilitador.

**Personalizadora de Cartões:** Também conhecida como Gráfica ou Bureau de Personalização de Cartões, é uma empresa que produz ou compra plásticos de terceiros e faz a gravação eletrônica do chip e da tarja magnética dos cartões e impressão dos dados variáveis do portador nos cartões, de acordo com as regras da Bandeira

**Personal Identification Number (PIN):** Senha numérica, normalmente de 4 ou 6 dígitos utilizada para identificar o portador do cartão. 

**PIN Pad:** Teclado para digitação de um PIN. Normalmente, exibe apenas um asterisco para cada tecla digitada e cuida da proteção do PIN, seja com criptografia para envio ao Emissor ou envio direto para o cartão, ou ambos (envio cifrado para o cartão).

**Ponto de Venda (PDV):** No Brasil, se refere a um terminal de pagamento composto por um teclado seguro (PIN Pad) sem impressora e sem conexão de dados, utilizado no checkout (caixa) de estabelecimentos em conjunto com um computador desktop (PC).

**Point of Sale (POS):** No Brasil, se refere a terminais de pagamento autônomos, com conexão de dados, teclado seguro e impressora, comercializados por Credenciadores. 

**Tecban:** Empresa Responsável pela operação dos terminais eletrônicos de saque (ATMs) da Rede 24h (Banco 24 horas).

**Relatórios R1, R2, e R3 (e agenda emissor):** Refere-se a base gerada a partir do Arquivo de Liquidação, consolidados por data de movimento, vencimento, emissor, processadora e tipo de transação. Com essas informações os emissores podem programar recursos financeiros, contabilizações, controles, batimentos financeiros de inclusão, liquidação e saldo futuro (até 27 dias).

**Saque:** Operação onde, normalmente em um caixa eletrônico (ATM), o portador efetua saque em espécie.

Usuário: Pessoa natural ou jurídica que utiliza os serviços de pagamento, ou pessoa física ou jurídica, portadora de instrumento de pagamento (pré-pago, pós-pago e de depósito à vista), que pode adquirir bens e serviços comercializados pelos ECs, conforme aplicável.

Validação de Personalização de Cartões Elo (VCPE): processo de validação de um cartão produzido por uma Personalizadora de Cartões, para garantir que ele esteja em conformidade com os requisitos da Bandeira Elo.



