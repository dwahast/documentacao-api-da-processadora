---
title: Visão Geral
tags: 
 - fluxo geral
 - figuras
description: Funcionamento geral do ciclo de emissão
permalink: "/"
language: pt-br
---

# Visão Geral

Nesta seção, é apresentada uma visão geral do fluxo de chamadas que um emissor ou sub-emissor deve realizar para emitir cartões e processar transações com a Processadora paySmart. Para que o cartão chegue corretamente ao seu usuário final, detentor de uma conta em sua instituição, é necessário seguir os passos apresentados nas figuras abaixo.

&nbsp;

![Modelo 1 do fluxo geral de emissão - Parte 1]({{ site.url }}{{ site.baseurl }}/assets/img/pt-BR/API da Processadora/Gráfico processadora_1 PT.png)
&nbsp;
&nbsp;


![Modelo 1 do fluxo geral de emissão - Parte 2]({{ site.url }}{{ site.baseurl }}/assets/img/pt-BR/API da Processadora/Gráfico processadora_2 PT.png)
&nbsp;
&nbsp;


![Modelo 1 do fluxo geral de emissão - Parte 3]({{ site.url }}{{ site.baseurl }}/assets/img/pt-BR/API da Processadora/Gráfico processadora_3 PT.png)

&nbsp;
As figuras descrevem os principais pontos de interação entre um Emissor, a paySmart e uma personalizadora :

1. O Emissor cadastra os dados de um Portador (pessoa física ou jurídica), criando uma conta no sistema da paySmart. <a href="{{ site.url }}{{ site.baseurl }}/pt-br/accounts">Saiba mais</a>
2. O Emissor solicita cartões que serão vinculados à conta previamente criada. <a href="{{ site.url }}{{ site.baseurl }}/pt-br/cartoes/fisicos/criacao">Saiba mais</a>
3. A paySmart, em um processo assíncrono, combina as requisições do Emissor em requisições completas para a personalização de cartões que serão tratadas pela Personalizadora. Os cartões serão personalizados e enviados os Usuários Finais.
4. O Usuário, por meio de aplicativo ou outro canal disponibilizado pelo Emissor, realiza operações como desbloqueio, troca de senha ou consulta de saldo. Essas operações são realizadas com apoio da paySmart. <a href="{{ site.url }}{{ site.baseurl }}/pt-br/cartoes/gerenciamento">Saiba mais</a>
5. O Usuário realiza compras ou saques em terminais (POS, PDV ou ATMs) disponibilizados por Credenciadores. Essas transações são enviadas à bandeira, que por sua vez as direciona para a paySmart e para o Emissor. No caso de Emissores que mantém seu próprio saldo, estes devem estar prontos para responder a paySmart em consultas da API de Integração com Emissores. <a href="{{ site.url }}{{ site.baseurl }}/pt-br/processor-api/">Saiba mais</a>
