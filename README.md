# Como fazer mudanças

1. Existem 3 pastas/arquivos nesse repositório que podem receber novos arquivos ou alterações em arquivos existentes.
    - _docs
    - assets/api
    - _data/toc.yml
2. Na pasta _docs, é onde ficam armazenados os documentos, que podem ser editados pela equipe de CX, esses documentos, possuem um padrão de escrita que possibilita serem referenciados na próxima etapa. O padrão inicia e termina com "**---**". Agora abaixo uma descrição de cada atributo a ser definido:
    - **title:** é o titulo da página
    - **description:** é a descrição da página, e aparece em outras ocasiões (na seção Documento no canto direito superior da tela)
    - **permalink**: Quando forem criados novos documentos, é necessário definir o atributo **"permalink"**, que é o atributo que defini o caminho para essa página, de forma fixa, e que possibilita a referência no menu lateral (que é alterado no arquivo toc.yml).
        - Por exemplo **"permalink: /docs/minhapaginateste"**. Com isso quando fizer a configuração correta no arquivo toc.yml, conseguimos através do URL da página + permalink, acessar essa página/documento criado.
    - **layout**: Esse atributo só deve ser preenchido quando a página sendo descrita irá sustentar uma Especificação de API (aquela página com os endpoints). E quando for, deve ser preenchido com "swagger_layout".
        - Exemplo: **"layout: swagger_layout"**
    - **spec_path**: Esse atributo só deve ser preenchido quando a página sendo descrita irá sustentar uma Especificação de API (aquela página com os endpoints). E quando for, deve ser preenchido com o nome do arquivo _(yml, ou json no formato OpenAPI, que deve estar armazenado na pasta assets/api)_

3. Na pasta _data, temos o arquivo toc.yml, que define o menu lateral esquerdo. Logo se fizermos alterações nele, estamos alterando o que é mostrado nesse menu lateral.
    - Para criar um novo item basta seguir o padrão do arquivo, descrevendo de uma das formas:
    ```yml 
        #
        - title: Exemplo de Menu (clicável com documento) com maior hierarquia com filhos (definidos pela lista "links", que pode conter os exemplos abaixo)
          url: ""
          links:

        - title: Exemplo de Menu (clicável com documento) com hierarquia abaixo da do exemplo superior, podendo ser filho do exemplo acima, e também podendo receber filhos definidos pela lista "children"
          url: ""
          children: 
        
        - title: Exemplo de link (clicável com documento) com hierarquia abaixo da do exemplo superior, podendo ser filho dos dois exemplos acima exemplo acima. Sem filhos
          url: ""
         
    ```
    Dentre esses exemplo, todos estão apontando para o **"permalink: /"** ou **"permalink: """**, que é o permalink definido na página visão geral (accounts-and-cards/fluxo-geral-emissao.md)
    - Logo para criar um novo item nesse menu é necessário termos um "permalink" existente que deve ser definido no campo **url**


# Informações Técnicas
- [Hospedando no Gitlab](https://docs.gitlab.com/ee/user/project/pages/getting_started/pages_from_scratch.html)
- [Melhorias Possíveis](https://about.gitlab.com/blog/2021/02/05/ci-deployment-and-environments/)
- Esse site estático, foi criado com o framework Jekyll (e com um tema/[implementação](https://github.com/vsoch/docsy-jekyll.git) aplicado que é o [Docsy](https://vsoch.github.com/docsy-jekyll/)), que é um framework da linguagem Ruby.
- Para rodar esse site, localmente, com acesso ao seu navegador para poder testar mudanças sem fazer a implantação a púplico é possível através da instalação dos seguintes respectivos componentes no seu computador
    - Linux/Ubuntu:
        - Para instalação:
            ```bash
                sudo apt-get install ruby-full build-essential
                echo '# Install Ruby Gems to ~/gems' >> ~/.bashrc
                echo 'export GEM_HOME="$HOME/gems"' >> ~/.bashrc
                echo 'export PATH="$HOME/gems/bin:$PATH"' >> ~/.bashrc
                source ~/.bashrc
                gem install jekyll bundler
                jekyll --version
                bundle install
            ```
        - Para rodar localmente (ATENÇÃO, SE RODAR LOCALMENTE COM --livereload dará problema de CORS caso acesse as páginas de definção de APIs):
            ```bash
                bundle exec jekyll serve --livereload
            ```
