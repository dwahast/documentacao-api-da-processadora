---
layout: default-es
title: Documentación
permalink: es/docs/
---

# Documentación

Bienvenido a la página de documentación de {{ site.title }}!

### Enlaces rápidos:

<div class="section-index">
    <hr class="panel-line">
    {% for post in site.docs  %}
        {% if post.language == "es" %}
            <div class="entry">
                <h5><a href="{{ post.url | prepend: site.baseurl }}">{{ post.title }}</a></h5>
                <p>{{ post.description }}</p>
            </div>
        {% endif %}
    {% endfor %}
</div>
