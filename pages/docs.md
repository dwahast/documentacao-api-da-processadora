---
layout: page
title: Documentação
permalink: docs/
---

# Documentação

Bem-vindo a página de documentação da {{ site.title }}!

### Links rápidos:

<div class="section-index">
    <hr class="panel-line">
    {% for post in site.docs  %}
        {% if post.language == "pt-br" %}
            <div class="entry">
                <h5><a href="{{ post.url | prepend: site.baseurl }}">{{ post.title }}</a></h5>
                <p>{{ post.description }}</p>
            </div>
        {% endif %}
    {% endfor %}
</div>
