openapi: 3.0.0
info:
  version: 'v1'
  title: 'Processadora paySmart - API de Saldo e Limites'
  description: 'API usada para controlar saldo e limites de uma conta.'
  contact:
    url: https://paysmart.com.br/
    email: atendimento@paysmart.com.br
  license:
    name: Documento confidencial conforme NDA assinado entre emissor e paySmart
    url: https://paysmart.com.br/

paths:

  /accounts/{accountId}/balance:
    parameters:
      - $ref: '#/components/parameters/AccountId'
      - $ref: '#/components/parameters/IssuerRequestId'
    get:
      tags:
        - accounts
      summary: Obtém o saldo e limite restante associado a uma determinada conta.
      operationId: getAccountBalance

      responses:
        '200':
          description: Balanço obtido com sucess!
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Balance'
        '400':
          description: Erro na requisição da operação.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ResultData'
        '401':
          description: Erro na autorização.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ResultData'
        '404':
          description: Erro de conta não encontrada
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ResultData'
        '500':
          description: Erro no servidor
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ResultData'

  /accounts/{accountId}/maxCreditLimits:
    parameters:
      - $ref: '#/components/parameters/AccountId'
      - $ref: '#/components/parameters/IssuerRequestId'
    put:
      tags:
        - accounts
      summary: Muda os limites da conta.
      description: Permite que os limites de um conta sejam alterados.<br>Pelo menos um dos parâmetros de limite deve ser informado e todos os valores devem ser maiores ou igual a zero.<br>Caso apenas o parâmetro totalLimit seja informado, será feita uma validação do withdrawalLimit existente da conta baseado no novo limite.
      requestBody:
        description: Dados dos novos limites.
        required: true
        content:
          application/json:
           schema:
              $ref: '#/components/schemas/NewCreditLimits'
      responses:
        '200':
          description: Limites alterados com sucesso
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ResultData'
        '400':
          description: Erro na execução da operação
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ResultData'
        '401':
          description: Erro na autorização.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ResultData'
        '404':
          description: Erro de conta não encontrada
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ResultData'
        '500':
          description: Erro no servidor
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ResultData'

components:

  parameters:

    AccountId:
      name: accountId
      in: path
      description: Identificador único da conta.
      required: true
      schema:
        type: string
        example: 639f4187-a0eb-47f7-89eb-0b42ca3f51dc

    IssuerRequestId:
      name:  issuerRequestId
      in: header
      description: Identificador único da requisição gerado pelo emissor. Ecoado conforme enviado na requisição.
      schema:
        type: string
        example: 41cab09a-c20a-46d0-883a-8b2a359fc4ad

  schemas:

    Balance:
      type: object
      properties:
        accountId:
          type: string
          description: Identificador único da conta.
          example: 694cd1e3-19b9-4a25-86b6-9b39e3388520
        balance:
          $ref: '#/components/schemas/Amount'
        creditLimit:
          $ref: '#/components/schemas/Amount'
        withdrawalCreditLimit:
          $ref: '#/components/schemas/Amount'
        currentCreditLimit:
          $ref: '#/components/schemas/Amount'
        currentWithdrawalCreditLimit:
          $ref: '#/components/schemas/Amount'
        payamentDue:
          type: string
          description: Dia de vencimento da fatura mensal.
          example: 10
        query_date:
          type: string
          description: Data em formato ISO em que a pesquisa foi efetuada.
          example: 2024-01-01T08:30:22Z

    ResultData:
      type: object
      properties:
        resultCode:
          type: integer
          description: Código de resultado do processamento.
        resultDescription:
          type: string
          description: Descrição textual do resultado do processamento.
        issuerRequestId:
          type: string
          description: Identificador único da requisição gerado pelo emissor. Ecoado conforme enviado na requisição.
        psResponseId:
          type: string
          description: Identificador único da resposta. Gerado pela paySmart.

    Amount:
      type: object
      required:
        - amount
        - currencyCode
      properties:
        amount:
          type: integer
          format: int64
          example: 123
          description: Valor omitindo a vírgula.
            Por exemplo, R$ 1,23 ficaria "amount":123
        currencyCode:
          type: integer
          default: 986
          example: 986
          description: Código da moeda, conforme ISO-4217

    NewCreditLimits:
      type: object
      properties:
        totalLimit:
          $ref: '#/components/schemas/Amount'
        withdrawalLimit:
          $ref: '#/components/schemas/Amount'